package com.belongi.citycenter;

/**
 * Created by webwerks on 14/6/15.
 */
public class Constant {


  /*  public static final String PREF_NAME = "moe";
    public static final String PREF_KEY_NOTIFICATION = "notification";*/
   /* public static final String PREF_HOME_DATA="homescreen_data";
    public static final String PREF_HOME_SPOTLIGHT="spotlight_banner";
    public static final String PREF_HOME_MOVIE="movie";
    public static final String PREF_HOME_ENTERTAINMENT="entertainment";
    public static final String PREF_HOME_OFFER_EVENT="offer_event";
    public static final String PREF_HOME_OFFER="offer";
    public static final String PREF_HOME_EVENT="event";
    public static final String PREF_SHOP_CAT="shop_category";
    public static final String PREF_DINING_CAT="dine_category";
    public static final String PREF_MALL_PAGES="mall_pages";*/


    public static final String PREF_HOME_SCREEN = "homescreen";
    public static int LAST_SELECTED_MAP_INDEX;
    public static String CISCO_MALL_TAG = "deira";
    public static String CISCO_LOCATION_TAG = "";
    public static final String PREF_SELECTED_MALL = "selected_mall";
    public static final String PREF_MALL_ID = "mall_id";

    public static final String DETAIL_SHOP_TYPE = "DETAIL_SHOP_TYPE";
    public static final String TYPE_CLICKED = "type_clicked";
    public static final int CONTEST = 2;
    public static final int HOW_TO_PLAY = 3;
    public static final int TERMS_CONDITIONS = 4;

    public static final int PAST_WINNERS = 5;
    public static final int DATE_OF_DRAW = 6;
    public static final String SELECTED_CONTEST = "selected_contest";
    public static final String ATTEMPT_LEFT = "attempt_left";
    public static final String PDF_FILE = "PDF_FILE";
    /*public static final String PREF_KEY_IS_FIRST_LAUNCH = "is_first_launch";
    public static final String PREF_MALL_NOTIFICATION="show_notification";*/

    public static final String GCM_SENDER_ID = "873819204383";
    //public static final String GCM_SENDER_ID = "401231492994";

    public static final String DONT_SEND = "send";

    public static String BIG_BUS = "", METRO_TIMING = "", SOCIAL_WALL = "", STORE_LOCATOR = "", BOOKATAXI = "";
    public static boolean GIFT_CARD_AVAILABLE = false;

    public static String URL_JMAP =/*"https://maf.southeastasia.cloudapp.azure.com:8443/"*/ "http://maf.southeastasia.cloudapp.azure.com:8080";

    public static String KNOW_MORE_URL = "http://giftcard.mallgiftcard.ae/";
    public static String KNOW_MORE_URL_BERUIT = "http://www.citycentremallbeirut.com/events/c-card-world";
    public static String GIFT_CARD_URL = "http://mallgiftcard.ae/home";
    public static final String URL_PREFIX_COMMON = "http://www.citycentre";
    public static final String MALL_PROMOTION_URL_SUFFIX = "/e-coupon";

    public static class WebConstants {
        public static final String URL_YAHOO_API = "http://query.yahooapis.com/v1/public/yql";
        //public static final String URL_CURRENCY_CONVERTER="http://rate-exchange.appspot.com/currency";
        public static final String URL_CURRENCY_CONVERTER = "http://query.yahooapis.com/v1/public/yql";
        public static final int RESCODE_CURRENCY_CONVERTER = 1;
    }


    public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE", DEST_ID = "DEST_ID";

    //Category

    public static abstract class NotificationKey {
        public static final String ALL = "all";
        public static final String FASHION_LADIES = "fashionladies";
        public static final String FASHION_MEN = "fashionmen";
        public static final String DINING = "dining";
        public static final String OFFERS = "offers";
        public static final String EVENTS = "events";

        public static final String HOMESCREEN = "homescreen";
        public static final String GIFT_CARD = "giftcard";
        public static final String HANDS_FREE = "handsfree";
        public static final String SPIN_WIN = "spinwin";
        public static final String MALL_PROMOTIONS = "mallpromotions";
    }

}
