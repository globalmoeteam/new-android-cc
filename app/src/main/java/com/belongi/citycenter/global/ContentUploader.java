package com.belongi.citycenter.global;

import android.util.Log;

import com.belongi.citycenter.model.Notification;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;

/**
 * Created by yashesh on 16/6/15.
 */
public class ContentUploader {

    public static final int UPLOAD_NOTOFICATION=22;

    public static void updateUserPushNotifications(Notification notification,BackgroundJobClient client){


        SOAPRequest notificationRequest=new SOAPRequest();
        notificationRequest.setBaseUrl(WebApi.BASE_URL);
        notificationRequest.setNameSpace(WebApi.NAMESPACE);
        notificationRequest.setType(NetworkRequest.MethodType.POST);
        notificationRequest.setRequestCode(UPLOAD_NOTOFICATION);
        notificationRequest.setActionName("UpdateUserPushNotifications");
        notificationRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("MallID",
                App.get().getMallId(), SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("DeviceToken",
                notification.getGcmId(), SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Fashion_Ladies",
                (notification.isFashionLadies() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.INT));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Fashion_men",
                (notification.isFashionMens() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.INT));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Dinning",
                (notification.isDining() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Offer",
                (notification.isOffers() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Events",
                (notification.isEvents() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Weekly_1_6",
                (notification.isWeekly1() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Weekly_6_12",
                (notification.isWeekly2() ? 1 : 0) +"", SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Name",
                notification.getName(), SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("Email",
                notification.getEmailAddress(), SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("MobileNumber",
                notification.getMobNumber(), SOAPRequest.Parameter.DataType.STRING));
        notificationRequest.addSoapParameter(new SOAPRequest.Parameter("isMr",
                notification.getGender()+"",SOAPRequest.Parameter.DataType.INT));

        new SOAPNetworkJob(notificationRequest,client).execute();
    }
}
