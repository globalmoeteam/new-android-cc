package com.belongi.citycenter.global;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 2/9/2016.
 */
public class PermissionUtils {

	public static final int REQUEST_CAMERA                           = 102;
	static final        int CAMERA_PERMISSION_REQUEST_CODE           = 3;

    public static boolean hasPermission(Context context, String permission) {

        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED ? true : false;
    }

    public static boolean needsExplaination(Activity context, String permission) {

        return ActivityCompat.shouldShowRequestPermissionRationale(context, permission);
    }

    public static void requestPermission(Activity activity,int requestCode,String ... permissions){
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }

    public static void requestIfNotGranted(Activity context, String permission,int requestCode,PermissionCallback callback){

        if(hasPermission(context,permission)){
            callback.onPermissionResult(requestCode,true);
        }else{
            requestPermission(context,requestCode,permission);
        }
    }

    public static void requestIfNotGranted(Activity context, String[] permission,int requestCode,PermissionCallback callback){
        List<String> permissions=new ArrayList<>();
        boolean hasAll = true;
        for(String per:permission){
            if(!hasPermission(context,per)){
                permissions.add(per);
                hasAll = false;
            }
        }
        if(hasAll){
            callback.onPermissionResult(requestCode,true);
            return;
        }

        requestPermission(context,requestCode,permissions.toArray(new String[permissions.size()]));
    }

	public static boolean checkPermissionForCamera( Activity activity )
	{
		int result = ContextCompat.checkSelfPermission( activity, Manifest.permission.CAMERA );
		if ( result == PackageManager.PERMISSION_GRANTED )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static void requestPermissionForCamera( Activity activity )
	{
		if ( ActivityCompat.shouldShowRequestPermissionRationale( activity, Manifest.permission.CAMERA ) )
		{
			Toast.makeText( activity, "Camera permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG ).show();
		}
		else
		{
			ActivityCompat.requestPermissions( activity, new String[]{ Manifest.permission.CAMERA }, CAMERA_PERMISSION_REQUEST_CODE );
		}
	}

    public interface PermissionCallback{

      void onPermissionResult(int requestCode, boolean granted);

    }

    public interface PermissionProcessor{

        void setPermissionCallback(PermissionCallback callback);
        void notifyPermissionResult(int requestCode, boolean granted);
    }
}
