package com.belongi.citycenter.global;

import android.util.Log;

import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.onesignal.OneSignal;

/**
 * Created by yashesh on 6/8/2015.
 */
public class WebApi {
    public static String BASE_HOAST_URL;
    public static String BASE_URL;
    public static String NAMESPACE = "http://www.maf.ae/";
    public static final String DESTINATION_HOST = "http://map.maf.ae/rest/web/x/8039/en";
    //public static String BASE_URL_SPIN_WIN = "http://demospin.o2-cafe.net/spin_and_win_new.asmx";
    public static final String BASE_URL_SPIN_WIN = "http://spin.belongi.com/spin_and_win_new.asmx";
    public static final String NAMESPACE_SPIN_WIN = "http://spin.o2-cafe.net";
    public static final int REQ_GET_SERVICE = 40;
    public static String IMAGE_BASE_URL;
    public static String TAG = WebApi.class.getSimpleName();

    public static final class RequestCode {
        public static final int HOME_EVENTS = 10;
    }

    public static final void getHomeEvents(BackgroundJobClient client) {
        Log.e("HOMEEVENT", "CALL EVENT");

        SOAPRequest request = new SOAPRequest();
        request.setActionName("GetEventsListingByNoOfEvents");
        request.setBaseUrl(BASE_URL);
        request.setType(NetworkRequest.MethodType.POST);
        request.setNameSpace(NAMESPACE);
        request.setContentType(NetworkRequest.ContentType.TEXT_XML);
        request.setRequestCode(RequestCode.HOME_EVENTS);
        request.addSoapParameter(new SOAPRequest.Parameter("SiteID", App.get().getMallId()));
        request.addSoapParameter(new SOAPRequest.Parameter("noOfEvents", "6"));
        new SOAPNetworkJob(request, client).execute();
    }

    public static final void getHomeOffers(final BackgroundJobClient client) {
        Log.e("HOMEEVENT", "CALL OFFER " + App.get().getDeviceId());


        SOAPRequest request = new SOAPRequest();
        request.setActionName("GetOffersListingByUserBehavior");
        request.setBaseUrl(BASE_URL);
        request.setType(NetworkRequest.MethodType.POST);
        request.setNameSpace(NAMESPACE);
        request.setContentType(NetworkRequest.ContentType.TEXT_XML);
        request.setRequestCode(RequestCode.HOME_EVENTS);
        request.addSoapParameter(new SOAPRequest.Parameter("SiteID", App.get().getMallId()));
        request.addSoapParameter(new SOAPRequest.Parameter("NoOfOffers", (12 - ContentDownloader.NUM_EVENTS) + ""));
        request.addSoapParameter(new SOAPRequest.Parameter("DeviceToken", App.get().getDeviceId()));
        new SOAPNetworkJob(request, client).execute();

       /* OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {

                if (registrationId != null) {
                    App.get().setDeviceId(registrationId);


                }
            }
        });*/


        /*GcmUtility.getGcmId(App.get(), new GcmUtility.GcmListner() {
            @Override
            public void onGcmRegistered(String gcmKey, int code) {
                Log.e("GetOffersByUserBehavior","eneter response");
                Log.e("GetOffersByUserBehavior",gcmKey);
                SOAPRequest request = new SOAPRequest();
                request.setActionName("GetOffersListingByUserBehavior");
                request.setBaseUrl(BASE_URL);
                request.setType(NetworkRequest.MethodType.POST);
                request.setNameSpace(NAMESPACE);
                request.setContentType(NetworkRequest.ContentType.TEXT_XML);
                request.setRequestCode(RequestCode.HOME_EVENTS);
                request.addSoapParameter(new SOAPRequest.Parameter("SiteID", App.get().getMallId()));
                request.addSoapParameter(new SOAPRequest.Parameter("NoOfOffers", (12 - ContentDownloader.NUM_EVENTS) + ""));
                request.addSoapParameter(new SOAPRequest.Parameter("DeviceToken", gcmKey));
                new SOAPNetworkJob(request, client).execute();
            }
        }, Constant.GCM_SENDER_ID);*/
    }

    public static void updateUserHit(final String categoryId) {


        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                String text = "OneSignal UserID:\n" + userId + "\n\n";

                Log.e(TAG, "OneSignal " + text);
                if (registrationId != null) {
                    text += "Google Registration Id:\n" + registrationId;
                    Log.e(TAG, "OneSignal " + text);
                    App.get().setDeviceId(registrationId);

                    final SOAPRequest request = new SOAPRequest();
                    request.setBaseUrl(BASE_URL);
                    request.setNameSpace(NAMESPACE);
                    request.setActionName("UpdateUserCategoryHits");
                    request.setType(NetworkRequest.MethodType.POST);
                    request.setContentType(NetworkRequest.ContentType.TEXT_XML);
                    request.addSoapParameter(new SOAPRequest.Parameter("mallID", App.get().getMallId()));
                    request.addSoapParameter(new SOAPRequest.Parameter("deviceToken", "" + registrationId));
                    request.addSoapParameter(new SOAPRequest.Parameter("categoryID", categoryId));
                    new SOAPNetworkJob(request, new BackgroundJobClient() {
                        @Override
                        public void onBackgroundJobComplete(int requestCode, Object result) {
                            Log.w("UPDATE", result + "");
                        }

                        @Override
                        public void onBackgroundJobAbort(int requestCode, Object reason) {
                        }

                        @Override
                        public void onBackgroundJobError(int requestCode, Object error) {
                        }

                        @Override
                        public boolean needAsyncResponse() {
                            return true;
                        }

                        @Override
                        public boolean needResponse() {
                            return true;
                        }
                    }
                    ).execute();
                } else {
                    text += "Google Registration Id:\nCould not subscribe for push";
                    Log.e(TAG, "OneSignal " + text);
                }


            }
        });


       /* GcmUtility.getGcmId(App.get(), new GcmUtility.GcmListner() {
            @Override
            public void onGcmRegistered(String gcmKey, int code) {
                final SOAPRequest request = new SOAPRequest();
                request.setBaseUrl(BASE_URL);
                request.setNameSpace(NAMESPACE);
                request.setActionName("UpdateUserCategoryHits");
                request.setType(NetworkRequest.MethodType.POST);
                request.setContentType(NetworkRequest.ContentType.TEXT_XML);
                request.addSoapParameter(new SOAPRequest.Parameter("mallID", App.get().getMallId()));
                request.addSoapParameter(new SOAPRequest.Parameter("deviceToken", "" + gcmKey));
                request.addSoapParameter(new SOAPRequest.Parameter("categoryID", categoryId));
                new SOAPNetworkJob(request, new BackgroundJobClient() {
                    @Override
                    public void onBackgroundJobComplete(int requestCode, Object result) {
                        Log.w("UPDATE", result + "");
                    }

                    @Override
                    public void onBackgroundJobAbort(int requestCode, Object reason) {
                    }

                    @Override
                    public void onBackgroundJobError(int requestCode, Object error) {
                    }

                    @Override
                    public boolean needAsyncResponse() {
                        return true;
                    }

                    @Override
                    public boolean needResponse() {
                        return true;
                    }
                }
                ).execute();
            }
        }, Constant.GCM_SENDER_ID);*/
    }

    public static final String ACTION_GETCOMPAIGNS = "getCampaigns";
    public static final String ACTION_USER_LOGIN = "getUseronLogin";
    public static final String ACTION_USER_REGISTRATION = "RegisterUser";
    public static final String ACTION_FORGOT_PASSWORD = "ForgotPassword";
    public static final String ACTION_RESET_PASSWORD = "ResetPassword";
    public static final String ACTION_DRAW_DATES = "getDrawDates";
    public static final String ACTION_PAST_WINNERS = "getPastWinners";
    public static final String ACTION_GET_QUESTION = "getQuestionsByCampaignId";
    public static final String ACTION_REGISTER_USER_RIGHT_ANSWER = "RegisterUserRightAnswer";

    public static final String ACTION_GET_CONTENT_PAGE = "GetContentPages";
    public static final String ACTION_NOTIFICATION_IMAGE = "GetNotificationImage";

    public static final int REQ_NOTIFICATION_IMAGE = 47;
}
