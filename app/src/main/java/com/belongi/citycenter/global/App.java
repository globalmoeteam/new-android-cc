package com.belongi.citycenter.global;

import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.android.utilities.FontsOverride;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.data.entities.Malls;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.UserLogin;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.web.entities.home.HomeScreenData;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.network.Validator;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.activity.SplashActivity;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.GsonBuilder;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by yashesh on 6/7/2015.
 */
public class App extends MultiDexApplication {

    public GoogleAnalytics analytics;
    public Tracker mTracker;
    private String deviceId;
    private static App instance;
    private boolean insideTheMall = false;
    private MallContentPage[] arrContentPage;
    private HomeScreenData homeScreenData;
    private String mallId;
    private List<Campaigns.Compaign> compaignList;
    private UserLogin.LoginResponse loginResponse;
    private Campaigns.Compaign selectedContest;
    private boolean hasCampaigns;
    private Malls selectedMall;

    private String bearerToken = "jMz3IyozeZ8arjUC9oP0hRaaDYca";
    public static boolean EMSP_INITIALIZED;
    public static boolean EMSP_LOCATION_REGISTERED = false;

    private boolean isMapEnabled = false;

    public String notificationKey = "";


    public String getNotificationKey() {
        return notificationKey;
    }

    public void setNotificationKey(String notificationKey) {
        this.notificationKey = notificationKey;
    }

    public boolean isMapEnabled() {
        return isMapEnabled;
    }

    public void setMapEnabled(boolean mapEnabled) {
        isMapEnabled = mapEnabled;
    }

    public int pathSegment;
    public int currentSegment = 0;

    public int getCurrentSegment() {
        return currentSegment;
    }

    public void setCurrentSegment(int currentSegment) {
        this.currentSegment = currentSegment;
    }

    public int getPathSegment() {
        return pathSegment;
    }

    public void setPathSegment(int pathSegment) {
        this.pathSegment = pathSegment;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        // trust all SSL -> HTTPS connection
        //SSLCertificateHandler.nuke();

      /*  OneSignal.startInit(this)
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .autoPromptLocation(true)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();*/


        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                 /*.autoPromptLocation(true)*/
                .init();

        MultiDex.install(this);
        //initialize activeandroid.
        ActiveAndroid.initialize(this);
        homeScreenData = new HomeScreenData();

        if (EntityUtils.getMallCount() <= 0) {
            EntityUtils.setMall();
        }
        FontsOverride.setDefaultFont(this, "MONOSPACE", "GothamMedium.ttf");
        analytics = GoogleAnalytics.getInstance(this);
        //analytics.setLocalDispatchPeriod(1800);
        mTracker = analytics.newTracker("UA-65265354-2");
        mTracker.enableExceptionReporting(true);
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.enableAutoActivityTracking(true);

        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

    }

    synchronized public Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker("UA-65265354-2");
            mTracker.enableExceptionReporting(true);
            mTracker.enableAdvertisingIdCollection(true);
            mTracker.enableAutoActivityTracking(true);
            /*// To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);*/
        }
        return mTracker;
    }



   /* public Tracker getTracker() {
        return tracker;
    }*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public boolean isHasCampaigns() {
        return hasCampaigns;
    }

    public void setHasCampaigns(boolean hasCampaigns) {
        this.hasCampaigns = hasCampaigns;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public MallContentPage[] getArrContentPage() {
        return arrContentPage;
    }

    public void setArrContentPage(MallContentPage[] arrContentPage) {
        this.arrContentPage = arrContentPage;
    }

    public Campaigns.Compaign getSelectedContest() {
        return selectedContest;
    }

    public void setSelectedContest(Campaigns.Compaign selectedContest) {
        this.selectedContest = selectedContest;
    }

    public UserLogin.LoginResponse getLoginResponse() {
        return loginResponse;
    }

    public void setLoginResponse(UserLogin.LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
    }

    public List<Campaigns.Compaign> getCompaignList() {
        return compaignList;
    }

    public void setCompaignList(List<Campaigns.Compaign> compaignList) {
        this.compaignList = compaignList;
    }

    public boolean isInsideTheMall() {
        return insideTheMall;
    }

    public void setInsideTheMall(boolean insideTheMall) {
        this.insideTheMall = insideTheMall;
    }

    public static final App get() {
        return instance;
    }

    public HomeScreenData getHomeScreenData() {
        return homeScreenData;
    }

    public void setHomeScreenData(HomeScreenData homeScreenData) {
        this.homeScreenData = homeScreenData;
    }

    public String getMallId() {
        if (mallId == null) {
            mallId = getSharedPreferences(Constant.PREF_HOME_SCREEN, Context.MODE_PRIVATE)
                    .getString(Constant.PREF_MALL_ID, "");
        }
        return mallId;
    }

    public void setMallId(String mallId) {
        getSharedPreferences(Constant.PREF_HOME_SCREEN, Context.MODE_PRIVATE)
                .edit().putString(Constant.PREF_MALL_ID, mallId).commit();
        this.mallId = mallId;
    }

    public Malls getSelectedMall() {
        if (selectedMall == null) {

            selectedMall = (Malls) ContentDownloader.getObjectFromJson(getSharedPreferences(Constant.PREF_HOME_SCREEN, Context.MODE_PRIVATE)
                    .getString(Constant.PREF_SELECTED_MALL, ""), Malls.class);
            //Log.e("abc","selected mall "+selectedMall.getMallName()+" "+selectedMall.getImageUrl());

        }
        return selectedMall;
    }

    public void setSelectedMall(Malls selectedMall) {
        this.selectedMall = selectedMall;
        getSharedPreferences(Constant.PREF_HOME_SCREEN, Context.MODE_PRIVATE)
                .edit().putString(Constant.PREF_SELECTED_MALL, new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
                .toJson(selectedMall)).commit();
    }

    public boolean checkMall() {

        switch (App.get().getSelectedMall().getIdentifier()) {
            case SHINDAGHA:
            case MEAISEM:
                return false;

            default:
                return true;
        }

//        if (!App.get().getSelectedMall().getMallName().equalsIgnoreCase("City Centre Shindagha")) {
//            if (!App.get().getSelectedMall().getMallName().equalsIgnoreCase("City Centre Me’aisem")) {
//                return true;
//            }
//        }
//        return false;
    }


    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;

            if (data != null) {
                Log.e("notificationOpened=", "data=" + data.toString());

                try {
                    String category = data.getString("Category").toLowerCase();
                    String mall_ID = data.getString("Mall_ID");
                    List <Malls> mallsList=EntityUtils.getMalls(mall_ID);
                    if(mallsList.size()>0){
                        setNotificationKey(category);
                        navigateNext(mallsList.get(0));
                    }else {
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }


                   /* Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

           /* if (actionType == OSNotificationAction.ActionType.ActionTaken)
                Log.e("OneSignalExample", "Button pressed with id: " + result.action.actionID);*/

            // The following can be used to open an Activity of your choice.

            // Intent intent = new Intent(getApplication(), YourActivity.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            // startActivity(intent);

            // Add the following to your AndroidManifest.xml to prevent the launching of your main Activity
            //  if you are calling startActivity above.
         /*
            <application ...>
              <meta-data android:name="com.onesignal.NotificationOpened.DEFAULT" android:value="DISABLE" />
            </application>
         */
        }
    }

    public void navigateNext(Malls mall) {
        if (mall != null) {
            MSingleton.NullifyMInstance();
            //MSingleton.getI().onDestroy();
            App.get().setSelectedMall(mall);
            Constant.LAST_SELECTED_MAP_INDEX = 0;

            WebApi.BASE_URL = App.get().getSelectedMall().getMallUrl();
            WebApi.IMAGE_BASE_URL = App.get().getSelectedMall().getImageUrl();
            WebApi.BASE_HOAST_URL = App.get().getSelectedMall().getImageUrl();
            //if (!TextUtils.isEmpty(App.get().getSelectedMall().getSiteId())) {
            App.get().setMallId(App.get().getSelectedMall().getSiteId());
            //}

            //Log.e("SelectedMall", App.get().getSelectedMall().getMallName() + " : " + App.get().getSelectedMall().getJibeProjectId());
            if (Validator.isConnected()) {
                Intent intent = new Intent(this, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } else {
                List<Service> serviceList = EntityUtils.getAllGuestService();
                if (serviceList == null || serviceList.size() == 0) {
                    Toast.makeText(this, "No Data Available", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(this, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    //startActivity(new Intent(this, SplashActivity.class));

                }
            }
        }
    }
}
