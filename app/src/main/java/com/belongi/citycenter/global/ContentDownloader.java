package com.belongi.citycenter.global;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.data.entities.Malls;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.SpotlightBanner;
import com.belongi.citycenter.data.entities.UsefulLinksModel;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.Movie;
import com.belongi.citycenter.data.entities.web.entities.EntertainmentResponse;
import com.belongi.citycenter.data.entities.web.entities.SiteIdResponse;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.services.DataLoadService;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.google.gson.GsonBuilder;

/**
 * Created by yashesh on 6/10/2015.
 */
public class ContentDownloader {
    public static final String DOWNLOAD_PROGRESS = "DOWNLOAD_PROGRESS";
    public static int NUM_EVENTS = 0;
    static Context mContext;

    public static void getSiteID(final Malls mall, Context context) {
        mContext = context;
        SOAPRequest request = new SOAPRequest();
        request.setBaseUrl(WebApi.BASE_URL);
        request.setActionName("GetSiteID");
        request.setNameSpace(WebApi.NAMESPACE);
        request.setContentType(NetworkRequest.ContentType.TEXT_XML);
        request.setType(NetworkRequest.MethodType.POST);
        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(request, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    // EntityUtils.clearDataCache();
                    // set the current mall id at app level for usage.
                    final SiteIdResponse[] siteIdResponses = (SiteIdResponse[]) getObjectFromJson(result.toString(), SiteIdResponse[].class);
                    App.get().setMallId(siteIdResponses[0].getMallID());
                    // requestInitialContent(App.get().getMallId());
                    Log.e("SiteId", result + "");
                    mall.setWhatsapp(siteIdResponses[0].getWhatsApp());
                    mall.setFeedbackUrl(siteIdResponses[0].getFeedbackURL());

                    EntityUtils.updateMall(mall);
                    Log.e("Counter", App.get().getMallId());
                    SOAPRequest usefulLinksRequest = new SOAPRequest();
                    usefulLinksRequest.setBaseUrl(WebApi.BASE_URL);
                    usefulLinksRequest.setActionName("GetUsefulLinks");
                    usefulLinksRequest.setNameSpace(WebApi.NAMESPACE);
                    usefulLinksRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
                    usefulLinksRequest.setType(NetworkRequest.MethodType.POST);
                    usefulLinksRequest.addSoapParameter(new SOAPRequest.Parameter("mallID", App.get().getMallId(), SOAPRequest.Parameter.DataType.INT));

                    BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(usefulLinksRequest, new BackgroundJobClient() {
                        @Override
                        public void onBackgroundJobComplete(int requestCode, Object result) {
                            if (result != null) {
                                Log.e("UsefulLink", result + "");
                                UsefulLinksModel[] usefulLinksModels = (UsefulLinksModel[]) getObjectFromJson(result.toString(), UsefulLinksModel[].class);
                                if (usefulLinksModels != null && usefulLinksModels.length > 0) {
                                    Constant.BIG_BUS = usefulLinksModels[0].getByBigBus();
                                    Constant.METRO_TIMING = usefulLinksModels[0].getMetroTiming();
                                    Constant.SOCIAL_WALL = usefulLinksModels[0].getSocialWall();
                                    Constant.STORE_LOCATOR = usefulLinksModels[0].getStoreLocator();
                                    Constant.BOOKATAXI = usefulLinksModels[0].getBookATaxi();
                                    ActiveAndroid.beginTransaction();
                                    for (UsefulLinksModel link : usefulLinksModels) {
                                        link.setMallId(App.get().getMallId());
                                        long id = link.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                    ActiveAndroid.endTransaction();
                                }
                            }
                            //request the caching content.
                            requestInitialContent(siteIdResponses[0].getMallID());
                        }

                        @Override
                        public void onBackgroundJobAbort(int requestCode, Object reason) {
                        }

                        @Override
                        public void onBackgroundJobError(int requestCode, Object error) {
                            requestInitialContent(siteIdResponses[0].getMallID());
                        }

                        @Override
                        public boolean needAsyncResponse() {
                            return false;
                        }

                        @Override
                        public boolean needResponse() {
                            return true;
                        }
                    }));
                }
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
                Log.e("Enter", "Error");
                SOAPRequest usefulLinksRequest = new SOAPRequest();
                usefulLinksRequest.setBaseUrl(WebApi.BASE_URL);
                usefulLinksRequest.setActionName("GetUsefulLinks");
                usefulLinksRequest.setNameSpace(WebApi.NAMESPACE);
                usefulLinksRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
                usefulLinksRequest.setType(NetworkRequest.MethodType.POST);
                usefulLinksRequest.addSoapParameter(new SOAPRequest.Parameter("mallID", App.get().getMallId(), SOAPRequest.Parameter.DataType.INT));

                BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(usefulLinksRequest, new BackgroundJobClient() {
                    @Override
                    public void onBackgroundJobComplete(int requestCode, Object result) {
                        if (result != null) {
                            UsefulLinksModel[] usefulLinksModels = (UsefulLinksModel[]) getObjectFromJson(result.toString(), UsefulLinksModel[].class);
                            if (usefulLinksModels != null && usefulLinksModels.length > 0) {
                                Constant.BIG_BUS = usefulLinksModels[0].getByBigBus();
                                Constant.METRO_TIMING = usefulLinksModels[0].getMetroTiming();
                                Constant.SOCIAL_WALL = usefulLinksModels[0].getSocialWall();
                                Constant.STORE_LOCATOR = usefulLinksModels[0].getStoreLocator();
                                Constant.BOOKATAXI = usefulLinksModels[0].getBookATaxi();

                                ActiveAndroid.beginTransaction();
                                for (UsefulLinksModel link : usefulLinksModels) {
                                    link.setMallId(App.get().getMallId());
                                    long id = link.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                                ActiveAndroid.endTransaction();
                            }
                        }
                        //request the caching content.
                        requestInitialContent(App.get().getMallId());
                    }

                    @Override
                    public void onBackgroundJobAbort(int requestCode, Object reason) {
                    }

                    @Override
                    public void onBackgroundJobError(int requestCode, Object error) {
                        requestInitialContent(App.get().getMallId());
                    }

                    @Override
                    public boolean needAsyncResponse() {
                        return false;
                    }

                    @Override
                    public boolean needResponse() {
                        return true;
                    }
                }));
            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));
    }


    public static void publishProgress() {
        App.get().sendBroadcast(new Intent(DOWNLOAD_PROGRESS));
    }


    public static void requestInitialContent(final String siteId) {

        Intent intent = new Intent(mContext, DataLoadService.class);
       mContext.startService(intent);

        //////////////////////////////////////////
        //get home entertainment items

        SOAPRequest homeEntertainmentRequest = new SOAPRequest();
        homeEntertainmentRequest.setBaseUrl(WebApi.BASE_URL);
        homeEntertainmentRequest.setActionName("GetHomeEntertainmentListing");
        homeEntertainmentRequest.setNameSpace(WebApi.NAMESPACE);
        homeEntertainmentRequest.setType(NetworkRequest.MethodType.POST);
        homeEntertainmentRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        homeEntertainmentRequest.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId));
        new SOAPNetworkJob(homeEntertainmentRequest, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("HOME ENTR", result + "");
                if (result != null) {
                    EntertainmentResponse[] entertainment = (EntertainmentResponse[]) getObjectFromJson(result.toString(), EntertainmentResponse[].class);

                    if (entertainment != null) {
                        new Delete().from(Entertainment.class).where("mallid=?", App.get().getMallId())
                                .where("forHome=?", 0).execute();
                        ActiveAndroid.beginTransaction();
                        for (EntertainmentResponse resp : entertainment) {
                            if (resp != null) {
                                Log.e("ENTR ENTER", resp.getShops_list().size() + "");
                                for (Entertainment shop : resp.getShops_list()) {
                                    Log.e("ENTR ENTER", "INNER LOOP " + resp.getID() + " : " + resp.getCategoryName() + " : " + App.get().getMallId() + " : " + shop);
                                    shop.setCategoryId(resp.getID());
                                    shop.setCategoryName(resp.getCategoryName());
                                    shop.setMallId(App.get().getMallId());
                                    shop.setForHome(0);
                                    long id = shop.save();
                                    // long searchId = EntityUtils.getSearchObject(shop).save();
                                    Log.e("ENTR ENTER", id + " SEARCH");
                                }
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }

                   /* SharedPreferences.Editor enterEditor=homePref.edit();
                    Gson g=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    enterEditor.putString(Constant.PREF_HOME_ENTERTAINMENT, g.toJson(entertainment[0].getShops_list()));
                    enterEditor.commit();
                    App.get().getHomeScreenData().setEntertainments(entertainment[0].getShops_list());*/
                }
                // 1
                Log.e("Counter", "GetHomeEntertainmentListing" + App.get().getMallId());
                publishProgress();

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
                //1
                publishProgress();
            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }).execute();


        // get spotlight images
        SOAPRequest spotlightRequest = new SOAPRequest();
        spotlightRequest.setBaseUrl(WebApi.BASE_URL);
        spotlightRequest.setNameSpace(WebApi.NAMESPACE);
        spotlightRequest.setActionName("GetSpotlightBannerListing");
        spotlightRequest.setType(NetworkRequest.MethodType.POST);
        spotlightRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        spotlightRequest.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId));
        new SOAPNetworkJob(spotlightRequest, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {

                Log.e("SpotlightBanner", result.toString());

                if (result != null) {
                    SpotlightBanner[] spotlights = (SpotlightBanner[]) getObjectFromJson(result.toString(), SpotlightBanner[].class);
                    /*SharedPreferences.Editor spotEditor=homePref.edit();
                    spotEditor.putString(Constant.PREF_HOME_SPOTLIGHT,gson.toJson(spotlights));
                    spotEditor.commit();
                    App.get().getHomeScreenData().setSpotlightBanners(spotlights);*/

                    if (spotlights != null) {
                        new Delete().from(SpotlightBanner.class).where("mallid=?", App.get().getMallId()).execute();
                        ActiveAndroid.beginTransaction();
                        for (SpotlightBanner banner : spotlights) {
                            banner.setMallId(App.get().getMallId());
                            long id = banner.save();
                            Log.e("HOME SPOT", id + "");
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }

                }
                // 2
                Log.e("Counter", "GetSpotlightBannerListing" + App.get().getMallId());
                publishProgress();

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
                //2
                publishProgress();
            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }).execute();


        ////////////////////////////////////////////////////////////////////////////////
        ///// get movie

        SOAPRequest randomMovieRequest = new SOAPRequest();
        randomMovieRequest.setBaseUrl(WebApi.BASE_URL);
        randomMovieRequest.setActionName("GetRandomMovie");
        randomMovieRequest.setNameSpace(WebApi.NAMESPACE);
        randomMovieRequest.setType(NetworkRequest.MethodType.POST);
        randomMovieRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        randomMovieRequest.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId));
        new SOAPNetworkJob(randomMovieRequest, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    Movie[] movies = (Movie[]) getObjectFromJson(result.toString(), Movie[].class);

                    if (movies != null) {
                        new Delete().from(Movie.class).where("mallid=?", App.get().getMallId()).execute();
                        ActiveAndroid.beginTransaction();
                        for (Movie movie : movies) {
                            movie.setMallId(App.get().getMallId());
                            long id = movie.save();
                            Log.e("HOME SPOT MOVIE", id + "");
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }


                    /*SharedPreferences.Editor movEditor=homePref.edit();
                    movEditor.putString(Constant.PREF_HOME_MOVIE,gson.toJson(movies));
                    movEditor.commit();
                    AppDataLoadService.get().getHomeScreenData().setMovies(movies);*/
                }
                // 3
                Log.e("Counter", "GetRandomMovie" + App.get().getMallId());
                publishProgress();

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
                //3
                publishProgress();
            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }).execute();

        //   getHomeEventsAndOffers();
        ////////////////////////
        SOAPRequest requestContentPage = new SOAPRequest();
        requestContentPage.setBaseUrl(WebApi.BASE_URL);
        requestContentPage.setNameSpace(WebApi.NAMESPACE);
        requestContentPage.setType(NetworkRequest.MethodType.POST);
        requestContentPage.setActionName(WebApi.ACTION_GET_CONTENT_PAGE);
        requestContentPage.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestContentPage.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        new SOAPNetworkJob(requestContentPage, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("requestContentPage", result.toString());
                if (result != null) {
                    MallContentPage[] arrContentPage = (MallContentPage[]) ContentDownloader.getObjectFromJson(result.toString(), MallContentPage[].class);
                    if (arrContentPage != null) {
                        new Delete().from(MallContentPage.class).where("mallid=?", App.get().getMallId()).execute();
                        ActiveAndroid.beginTransaction();
                        for (MallContentPage content : arrContentPage) {
                            content.setMallId(App.get().getMallId());
                            long id = content.save();
                            Log.e("requestContentPage", id + "");
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                        // App.get().setArrContentPage(arrContentPage);
                        // *11

                    }
                }
                Log.e("Counter", "requestContentPage" + App.get().getMallId());
                //4
                publishProgress();

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
                //4
                publishProgress();
            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }

        }).execute();

        WebApi.getHomeEvents(new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    final Event[] events = (Event[]) getObjectFromJson(result.toString(), Event[].class);
                    Log.e("SplashEbvent", events.length + "");
                    if (events != null) {
                        new Delete().from(Event.class).where("mallid=?", App.get().getMallId())
                                .where("forHome=?", 0).execute();
                        ActiveAndroid.beginTransaction();
                        for (Event event : events) {
                            if (event != null) {
                                event.setMallId(App.get().getMallId());
                                event.setForHome(0);
                                long id = event.save();
                                Log.e("SplashEbvent", "Event : " + id + "");
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }

                    ContentDownloader.NUM_EVENTS = events.length;

                    WebApi.getHomeOffers(new BackgroundJobClient() {
                        @Override
                        public void onBackgroundJobComplete(int requestCode, Object result) {
                            if (result != null) {
                                Offer[] offers = (Offer[]) getObjectFromJson(result.toString(), Offer[].class);
                                if (offers != null && offers.length > 0) {
                                    Log.e("SplashEbvent", offers.length + "");
                                    if (offers != null) {
                                        new Delete().from(Offer.class).where("mallid=?", App.get().getMallId())
                                                .where("forHome=?", 0).execute();
                                        ActiveAndroid.beginTransaction();
                                        for (Offer offer : offers) {
                                            if (offer != null) {
                                                offer.setMallId(App.get().getMallId());
                                                offer.setForHome(0);
                                                long id = offer.save();
                                                Log.e("SplashEbvent", "Offer : " + id + "");
                                            }
                                        }
                                        ActiveAndroid.setTransactionSuccessful();
                                        ActiveAndroid.endTransaction();


                                    }
                                }
                            }
                            //6
                            publishProgress();
                        }

                        @Override
                        public void onBackgroundJobAbort(int requestCode, Object reason) {
                        }

                        @Override
                        public void onBackgroundJobError(int requestCode, Object error) {
                            //6
                            publishProgress();
                        }

                        @Override
                        public boolean needAsyncResponse() {
                            return false;
                        }

                        @Override
                        public boolean needResponse() {
                            return true;
                        }
                    });

                }

                Log.e("Counter", "HOME EVENT OFFER" + App.get().getMallId());
                //5
                //publishProgress();
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
//5
                publishProgress();
            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        });




    }

    public static void getNotificationImage(BackgroundJobClient client) {
        SOAPRequest requestNotificationImage = new SOAPRequest();
        requestNotificationImage.setBaseUrl(WebApi.BASE_URL);
        requestNotificationImage.setNameSpace(WebApi.NAMESPACE);
        requestNotificationImage.setType(NetworkRequest.MethodType.POST);
        requestNotificationImage.setRequestCode(WebApi.REQ_NOTIFICATION_IMAGE);
        requestNotificationImage.setActionName(WebApi.ACTION_NOTIFICATION_IMAGE);
        requestNotificationImage.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestNotificationImage.addSoapParameter(new SOAPRequest.Parameter("mallID", App.get().getMallId(), SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(requestNotificationImage, client).execute();
    }

    public static Object getObjectFromJson(String json, Class<?> type) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        return builder.create().fromJson(json, type);
        //return new Gson().fromJson(json, type);
    }
}
