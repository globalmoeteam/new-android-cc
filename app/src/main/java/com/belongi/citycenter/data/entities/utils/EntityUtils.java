package com.belongi.citycenter.data.entities.utils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.data.entities.ATMService;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.data.entities.Malls;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.data.entities.Searchable;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.UsefulLinksModel;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.MallSelectionActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by user on 6/17/2015.
 */

public class EntityUtils {

    private final  static String URL_PREFIX= Constant.URL_PREFIX_COMMON;

    public static Search getSearchObject(Searchable item) {
        Search search = new Search();
        search.setId(item.getIdentifier());
        search.setCategory(item.getSearchCategory().toString());
        search.setLabel(item.getLabel());
        search.setMallId(item.getMallId());
        return search;
    }

    public static void deleteAll(String tableName) {
        SQLiteUtils.execSql("DELETE FROM " + tableName);
    }

    public static void getSize() {
        int count = new Select().from(DiningShop.class).count();
    }

    public static List<Search> localSearchForKeyword(final String keyword) {
        List<Search> results = new Select().from(Search.class).where("label LIKE '%" + keyword + "%'")
                .where("mallid=?", App.get().getMallId()).execute();
        Collections.sort(results, new Comparator<Search>() {
            @Override
            public int compare(Search s1, Search s2) {

                String key = keyword.toLowerCase();
                if (s1.getLabel().toLowerCase().startsWith(key) && (!s2.getLabel().toLowerCase().startsWith(key))) {
                    return -1;
                } else if (s2.getLabel().toLowerCase().startsWith(key) && (!s1.getLabel().toLowerCase().startsWith(key))) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        return results;
    }


    public static void clearDataCache() {
        EntityUtils.deleteAll("Offer");
        EntityUtils.deleteAll("Service");
        EntityUtils.deleteAll("Hotel");
        EntityUtils.deleteAll("Entertainment");
        EntityUtils.deleteAll("Shopping");
        EntityUtils.deleteAll("Dining");
        EntityUtils.deleteAll("Event");
        EntityUtils.deleteAll("Search");
        EntityUtils.deleteAll("ShopImage");
        EntityUtils.deleteAll("FeaturedProduct");
        EntityUtils.deleteAll("ATM");
        EntityUtils.deleteAll("ShopCategory");
        EntityUtils.deleteAll("Movie");
        EntityUtils.deleteAll("SpotlightBanner");
    }

    public static List<Service> getAllGuestService() {
        List<Service> serviceList = new ArrayList<>();
        serviceList = new Select().from(Service.class).where("mallid=?", App.get().getMallId()).execute();
        if (serviceList != null && serviceList.size() > 0) {
            for (Service service : serviceList) {
                if (service.getServiceTitle().toLowerCase().equals("mallconnect")) {
                    service.setServiceTitle("Mallconnect Free Wi-Fi");
                }
            }
        }
        return serviceList;
    }

    public static int getAtmListCount() {
        return new Select().from(ATMService.class).where("mallid=?", App.get().getMallId()).count();
    }

    public static List<ATMService> getAllATMService() {
        return new Select().from(ATMService.class).where("mallid=?", App.get().getMallId()).execute();
    }

    public static List<Service> getBusService() {
        return new Select().from(Service.class).where("ServiceTitle = 'Shuttle Bus Services'")
                .where("mallid=?", App.get().getMallId()).execute();
    }

    public static UsefulLinksModel getUseFulLink(String mallId) {
        List<UsefulLinksModel> usefulLinksModelList = new Select().from(UsefulLinksModel.class).where("mallid=?", mallId).execute();
        return usefulLinksModelList.get(0);
    }

    public static List<MallContentPage> getTheMallContent(String mallId) {
        return new Select().from(MallContentPage.class).where("mallid=?", mallId).execute();
    }

    public static void setMall() {
        ActiveAndroid.beginTransaction();

        Malls deiraMall = new Malls();
        deiraMall.setMallName("City Centre Deira");
       /* deiraMall.setMallUrl("http://dcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        deiraMall.setImageUrl("http://dcc.mafpmalltest.com");*/
        deiraMall.setMallUrl(URL_PREFIX+"deira.com/ws/ws_mafMobileApplication.asmx");
        deiraMall.setImageUrl(URL_PREFIX+"deira.com");
        deiraMall.setMallPromotionUrl(deiraMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        deiraMall.setId(MallSelectionActivity.Mall.DEIRA);
        deiraMall.setSiteId("1050");
        deiraMall.setMallCOde("dcc");
        deiraMall.setFirstLaunch("true");
        deiraMall.setShoppingCoach("true");
        deiraMall.setLatitude("25.251513");
        deiraMall.setLongitude("55.33372");
        deiraMall.setMovieCount(1);
        deiraMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3608.5667653785863!2d55.33372!3d25.251513000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5cdf62ec2201%3A0x5a603bb4d5e3231e!2sCity+Centre+Deira!5e0!3m2!1sen!2sae!4v1416225321906'  width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        deiraMall.setJibeProjectId("2");
        deiraMall.save();

        Malls mirdifMall = new Malls();
        mirdifMall.setMallName("City Centre Mirdif");
       /* mirdifMall.setMallUrl("http://micc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        mirdifMall.setImageUrl("http://micc.mafpmalltest.com");*/
        mirdifMall.setMallUrl(URL_PREFIX+"mirdif.com/ws/ws_mafMobileApplication.asmx");
        mirdifMall.setImageUrl(URL_PREFIX+"mirdif.com");
        mirdifMall.setMallPromotionUrl(mirdifMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        mirdifMall.setId(MallSelectionActivity.Mall.MIRDIF);
        mirdifMall.setSiteId("5329");
        mirdifMall.setMallCOde("micc");
        mirdifMall.setFirstLaunch("true");
        mirdifMall.setShoppingCoach("true");
        mirdifMall.setLatitude("25.216319");
        mirdifMall.setLongitude("55.407795");
        mirdifMall.setMovieCount(1);
        mirdifMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3609.6115566763297!2d55.407795!3d25.216319!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f60b408cc6a1d%3A0x9124cf2bf00a52d7!2sMirdif+City+Centre!5e0!3m2!1sen!2sae!4v1416226559819' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        mirdifMall.setJibeProjectId("2217");
        mirdifMall.save();

        Malls sharjahMall = new Malls();
        sharjahMall.setMallName("City Centre Sharjah");
       /* sharjahMall.setMallUrl("http://scc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        sharjahMall.setImageUrl("http://scc.mafpmalltest.com");*/
        sharjahMall.setMallUrl(URL_PREFIX+"sharjah.com/ws/ws_mafMobileApplication.asmx");
        sharjahMall.setImageUrl(URL_PREFIX+"sharjah.com/");
        sharjahMall.setMallPromotionUrl(sharjahMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        sharjahMall.setId(MallSelectionActivity.Mall.SHARJAH);
        sharjahMall.setSiteId("4227");
        sharjahMall.setMallCOde("scc");
        sharjahMall.setFirstLaunch("true");
        sharjahMall.setShoppingCoach("true");
        sharjahMall.setLatitude("25.324985");
        sharjahMall.setLongitude("55.393528");
        sharjahMall.setMovieCount(0);
        sharjahMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3606.3812415889397!2d55.393528!3d25.324984999999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5be5bb3a2875%3A0xe4ebdaadcda73c51!2sSharjah+City+Centre!5e0!3m2!1sen!2sae!4v1418214339491' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        sharjahMall.setJibeProjectId("4105");
        sharjahMall.save();

        Malls ajmanMall = new Malls();
        ajmanMall.setMallName("City Centre Ajman");
       /* ajmanMall.setMallUrl("http://acc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        ajmanMall.setImageUrl("http://acc.mafpmalltest.com");*/
        ajmanMall.setMallUrl(URL_PREFIX+"ajman.com/ws/ws_mafMobileApplication.asmx");
        ajmanMall.setImageUrl(URL_PREFIX+"ajman.com");
        ajmanMall.setMallPromotionUrl(ajmanMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        ajmanMall.setId(MallSelectionActivity.Mall.AJMAN);
        ajmanMall.setSiteId("5231");
        ajmanMall.setMallCOde("acc");
        ajmanMall.setFirstLaunch("true");
        ajmanMall.setShoppingCoach("true");
        ajmanMall.setLatitude("25.3995");
        ajmanMall.setLongitude("55.4796");
        ajmanMall.setMovieCount(1);
        ajmanMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3604.1586352903078!2d55.4796!3d25.399499999999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5812404b8b6b%3A0x31c99f11ebd3d9ed!2sCity+Centre+Ajman!5e0!3m2!1sen!2sae!4v1418214871647' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        ajmanMall.setJibeProjectId("4564");
        ajmanMall.save();

        Malls fujairahMall = new Malls();
        fujairahMall.setMallName("City Centre Fujairah");
        /*fujairahMall.setMallUrl("http://fcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        fujairahMall.setImageUrl("http://fcc.mafpmalltest.com");*/
        fujairahMall.setMallUrl(URL_PREFIX+"fujairah.com/ws/ws_mafMobileApplication.asmx");
        fujairahMall.setImageUrl(URL_PREFIX+"fujairah.com/");
        fujairahMall.setMallPromotionUrl(fujairahMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        fujairahMall.setId(MallSelectionActivity.Mall.FUJAIRAH);
        fujairahMall.setSiteId("9164");
        fujairahMall.setMallCOde("fcc");
        fujairahMall.setFirstLaunch("true");
        fujairahMall.setShoppingCoach("true");
        fujairahMall.setLatitude("25.125694");
        fujairahMall.setLongitude("56.302206");
        fujairahMall.setMovieCount(1);
        fujairahMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3612.2956379475786!2d56.302206000000005!3d25.125694!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ef4f8af0e5505f3%3A0x55be81cdac67e968!2sFujairah+City+Centre!5e0!3m2!1sen!2sae!4v1418216024367' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        fujairahMall.setJibeProjectId("4663");
        fujairahMall.save();

        Malls muscatMall = new Malls();
        muscatMall.setMallName("City Centre Muscat");
        /*muscatMall.setMallUrl("http://mucc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        muscatMall.setImageUrl("http://mucc.mafpmalltest.com");*/
        muscatMall.setMallUrl(URL_PREFIX+"muscat.com/ws/ws_mafMobileApplication.asmx");
        muscatMall.setImageUrl(URL_PREFIX+"muscat.com");
        muscatMall.setMallPromotionUrl(muscatMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        muscatMall.setId(MallSelectionActivity.Mall.MUSCAT);
        muscatMall.setSiteId("7352");
        muscatMall.setMallCOde("mucc");
        muscatMall.setFirstLaunch("true");
        muscatMall.setShoppingCoach("true");
        muscatMall.setLatitude("23.59962");
        muscatMall.setLongitude("58.24804");
        muscatMall.setMovieCount(1);
        muscatMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3656.1314002334543!2d58.24804!3d23.599619999999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e8dfd973a81be23%3A0x7a023508d5a03c05!2sMuscat+City+Centre!5e0!3m2!1sen!2sae!4v1418215476148' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        muscatMall.setJibeProjectId("5401");
        muscatMall.save();

        Malls qurumMall = new Malls();
        qurumMall.setMallName("City Centre Qurum");
       /* qurumMall.setMallUrl("http://qcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        qurumMall.setImageUrl("http://qcc.mafpmalltest.com");*/
        qurumMall.setMallUrl(URL_PREFIX+"qurum.com/ws/ws_mafMobileApplication.asmx");
        qurumMall.setImageUrl(URL_PREFIX+"qurum.com");
        qurumMall.setMallPromotionUrl(qurumMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        qurumMall.setId(MallSelectionActivity.Mall.QURUM);
        qurumMall.setSiteId("8560");
        qurumMall.setMallCOde("qcc");
        qurumMall.setFirstLaunch("true");
        qurumMall.setShoppingCoach("true");
        qurumMall.setLatitude("23.608489");
        qurumMall.setLongitude("58.489741");
        qurumMall.setMovieCount(1);
        qurumMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3655.8841058442317!2d58.489740999999995!3d23.608489!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e91f8e93d42fdfd%3A0xa0f240df6e03bbc7!2sQurm+City+Center!5e0!3m2!1sen!2sae!4v1418215843284' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        qurumMall.setJibeProjectId("5644");
        qurumMall.save();

        Malls bahrainMall = new Malls();
        bahrainMall.setMallName("City Centre Bahrain");
        /*bahrainMall.setMallUrl("http://bhcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        bahrainMall.setImageUrl("http://bhcc.mafpmalltest.com");*/
        bahrainMall.setMallUrl(URL_PREFIX+"bahrain.com/ws/ws_mafMobileApplication.asmx");
        bahrainMall.setImageUrl(URL_PREFIX+"bahrain.com");
        bahrainMall.setMallPromotionUrl(bahrainMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        bahrainMall.setId(MallSelectionActivity.Mall.BAHRAIN);
        bahrainMall.setSiteId("6748");
        bahrainMall.setMallCOde("bhcc");
        bahrainMall.setFirstLaunch("true");
        bahrainMall.setShoppingCoach("true");
        bahrainMall.setLatitude("26.2336");
        bahrainMall.setLongitude("50.5543");
        bahrainMall.setMovieCount(0);
        bahrainMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3578.863273007928!2d50.554324599999994!3d26.233631800000012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e49a578c875ca27%3A0x3fb20205aef45778!2sBahrain+City+Center!5e0!3m2!1sen!2sae!4v1440673225539' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        bahrainMall.setJibeProjectId("2218");
        bahrainMall.save();

        Malls alexandriaMall = new Malls();
        alexandriaMall.setMallName("City Centre Alexandria");
       /* alexandriaMall.setMallUrl("http://alcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        alexandriaMall.setImageUrl("http://alcc.mafpmalltest.com");*/
        alexandriaMall.setMallUrl(URL_PREFIX+"alexandria.com/ws/ws_mafMobileApplication.asmx");
        //alexandriaMall.setImageUrl("http://alcc.mafmalls.com");
        alexandriaMall.setImageUrl(URL_PREFIX+"alexandria.com");
        alexandriaMall.setMallPromotionUrl(alexandriaMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        /*alexandriaMall.setMallUrl(URL_PREFIX+"alexandria.com/ws/ws_mafMobileApplication.asmx");
        alexandriaMall.setImageUrl(URL_PREFIX+"alexandria.com");*/
        alexandriaMall.setId(MallSelectionActivity.Mall.ALEXANDRIA);
        alexandriaMall.setSiteId("7956");
        alexandriaMall.setMallCOde("alcc");
        alexandriaMall.setFirstLaunch("true");
        alexandriaMall.setShoppingCoach("true");
        alexandriaMall.setLatitude("31.16865");
        alexandriaMall.setLongitude("29.932855");
        alexandriaMall.setMovieCount(0);
        alexandriaMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3413.8783750266243!2d29.932855!3d31.16865!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f5c30695c08535%3A0xfa139ddaade9ffd3!2sCarrefour+City+Center+-+Alexandria+2!5e0!3m2!1sen!2sae!4v1418216219671' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        alexandriaMall.setJibeProjectId("5862");
        alexandriaMall.save();

        Malls maadiMall = new Malls();
        maadiMall.setMallName("City Centre Maadi");
       /* maadiMall.setMallUrl("http://macc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        maadiMall.setImageUrl("http://macc.mafpmalltest.com");*/
        maadiMall.setMallUrl(URL_PREFIX+"maadi.com/ws/ws_mafMobileApplication.asmx?WSDL");
        //maadiMall.setImageUrl("http://macc.mafmalls.com");
        maadiMall.setImageUrl(URL_PREFIX+"maadi.com");
        /*maadiMall.setMallUrl(URL_PREFIX+"maadi.com/ws/ws_mafMobileApplication.asmx");
        maadiMall.setImageUrl(URL_PREFIX+"maadi.com");*/
        maadiMall.setMallPromotionUrl(maadiMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        maadiMall.setId(MallSelectionActivity.Mall.MAADI);
        maadiMall.setSiteId("9768");
        maadiMall.setMallCOde("macc");
        maadiMall.setFirstLaunch("true");
        maadiMall.setShoppingCoach("true");
        maadiMall.setLatitude("29.9667");
        maadiMall.setLongitude("31.2500");
        maadiMall.setMovieCount(0);
        maadiMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3455.8490655581754!2d31.316363499999994!3d29.9837674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458391ba956d28d%3A0xa0c1b5ead53243e0!2sMaadi+City+Centre+Garden!5e0!3m2!1sen!2sae!4v1441796664721' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        maadiMall.setJibeProjectId("5861");
        maadiMall.save();

        Malls beirutMall = new Malls();
        beirutMall.setMallName("City Centre Beirut");
      /*  beirutMall.setMallUrl("http://bcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        beirutMall.setImageUrl("http://bcc.mafpmalltest.com");*/
        beirutMall.setMallUrl(URL_PREFIX+"mallbeirut.com/ws/ws_mafMobileApplication.asmx");
        beirutMall.setImageUrl(URL_PREFIX+"mallbeirut.com");
        beirutMall.setMallPromotionUrl(beirutMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        beirutMall.setId(MallSelectionActivity.Mall.BEIRUT);
        beirutMall.setSiteId("2197");
        beirutMall.setMallCOde("bcc");
        beirutMall.setFirstLaunch("true");
        beirutMall.setShoppingCoach("true");
        beirutMall.setLatitude("33.8869");
        beirutMall.setLongitude("35.5131");
        beirutMall.setMovieCount(1);
        beirutMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2641.944239790688!2d35.52872464048409!3d33.861619621444206!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x0cb7cf8d31d35ce1!2sCarrefour!5e1!3m2!1sen!2sae!4v1441797395439' width='600' height='450' frameborder='0' style='border: 0;position:absolute;left:0; right:0;bottom:0; top:0' allowfullscreen=''></iframe>");
        beirutMall.setJibeProjectId("4104");
        beirutMall.save();

        Malls shindaghaMall = new Malls();
        shindaghaMall.setMallName("City Centre Al Shindagha");
       /* shindaghaMall.setMallUrl("http://sdcc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        shindaghaMall.setImageUrl("http://sdcc.mafpmalltest.com");*/
        shindaghaMall.setMallUrl(URL_PREFIX+"shindagha.com/ws/ws_mafMobileApplication.asmx");
        //shindaghaMall.setImageUrl("http://sdcc.mafmalls.com");
        shindaghaMall.setImageUrl(URL_PREFIX+"shindagha.com");
        shindaghaMall.setMallPromotionUrl(shindaghaMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        /*shindaghaMall.setMallUrl(URL_PREFIX+"shindagha.com/ws/ws_mafMobileApplication.asmx");
        shindaghaMall.setImageUrl(URL_PREFIX+"shindagha.com");*/
        shindaghaMall.setId(MallSelectionActivity.Mall.SHINDAGHA);
        shindaghaMall.setSiteId("44517");
        shindaghaMall.setMallCOde("sdcc");
        shindaghaMall.setFirstLaunch("true");
        shindaghaMall.setShoppingCoach("true");
        shindaghaMall.setLatitude("25.2583293");
        shindaghaMall.setLongitude("55.2912553");
        shindaghaMall.setMovieCount(1);
        shindaghaMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d115462.44529011291!2d55.21677915898753!3d25.26380907685818!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3e5f436b7d333701%3A0x17be1f6e7c2d1bc!2scity+centre+shindagha!3m2!1d25.2638256!2d55.2868195!5e0!3m2!1sen!2sae!4v1459920887348' width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>");
        shindaghaMall.setJibeProjectId("");
        shindaghaMall.save();

        Malls meaisemMall = new Malls();
        meaisemMall.setMallName("City Centre Me’aisem");
       /* meaisemMall.setMallUrl("http://mecc.mafpmalltest.com/ws/ws_mafMobileApplication.asmx");
        meaisemMall.setImageUrl("http://mecc.mafpmalltest.com");*/
        meaisemMall.setMallUrl(URL_PREFIX+"meaisem.com/ws/ws_mafMobileApplication.asmx");
        //meaisemMall.setImageUrl("http://mecc.mafmalls.com");
        meaisemMall.setImageUrl(URL_PREFIX+"meaisem.com");
        meaisemMall.setMallPromotionUrl(meaisemMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
       /* meaisemMall.setMallUrl(URL_PREFIX+"meaisem.com/ws/ws_mafMobileApplication.asmx");
        meaisemMall.setImageUrl(" http://www.citycentremeaisem.com/");*/
        meaisemMall.setId(MallSelectionActivity.Mall.MEAISEM);
        meaisemMall.setSiteId("49577");
        meaisemMall.setMallCOde("mecc");
        meaisemMall.setFirstLaunch("true");
        meaisemMall.setShoppingCoach("true");
        meaisemMall.setLatitude("25.0401949");
        meaisemMall.setLongitude("55.1953507");
        meaisemMall.setMovieCount(0);
        meaisemMall.setGettingHere("<iframe src='https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d115674.24779705283!2d55.127504454260034!3d25.040173683581262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3e5f6dba3d43bf93%3A0xbbf384dcbed97418!2scity+centre+me&#39;aisem!3m2!1d25.0401901!2d55.197544799999996!5e0!3m2!1sen!2sae!4v1459921064444' width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>");
        meaisemMall.setJibeProjectId("");
        meaisemMall.save();

        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
    }

    public static List<Malls> getMalls() {
        List<Malls> mallsList=new Select().from(Malls.class).execute();
     //   Log.e("Mall list",""+mallsList.toString());
        return mallsList;
    }

    public static List<Malls> getMalls(String mallID) {
        //List<Malls> mallsList=getMalls();
        //Log.e("Mall list",""+mallsList.toString());
        return new Select().from(Malls.class).where("siteid=?", mallID).execute();
    }

    public static int getMallCount() {
        return new Select().from(Malls.class).count();
    }

    public static void updateMall(Malls mall) {
        List<Malls> mallList = new Select().from(Malls.class).where("identifier=?", mall.getIdentifier()).execute();
        Malls selectedMall = mallList.get(0);
        selectedMall.setId(mall.getIdentifier());
        selectedMall.setMallName(mall.getMallName());
        selectedMall.setImageUrl(mall.getImageUrl());
        selectedMall.setMallUrl(mall.getMallUrl());
        selectedMall.setSiteId(App.get().getMallId());
        selectedMall.setFirstLaunch(mall.getFirstLaunch());
        selectedMall.setShoppingCoach(mall.getShoppingCoach());
        selectedMall.setMallPromotionUrl(selectedMall.getImageUrl()+Constant.MALL_PROMOTION_URL_SUFFIX);
        selectedMall.save();
    }

    public static List<Service> containsGiftVoucher() {
        return new Select().from(Service.class).where("ServiceTitle=?", "Gift Voucher")
                .where("mallid=?", App.get().getMallId()).execute();
    }
}
