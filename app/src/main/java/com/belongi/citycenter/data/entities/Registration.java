package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 18/6/15.
 */
public class Registration {

    @Expose
    private String title;

    @Expose
    private String name;

    @Expose
    private String email;

    @Expose
    private String mobile;

    @Expose
    private String countryRes;

    @Expose
    private String password;

    @Expose
    private String nationality;

    @Expose
    @SerializedName("Data")
    private String success;

    @Expose
    @SerializedName("Error")
    private String error;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryRes() {
        return countryRes;
    }

    public void setCountryRes(String countryRes) {
        this.countryRes = countryRes;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
