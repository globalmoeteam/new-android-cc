package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yashesh on 6/17/2015.
 */
@Table(name = "Shopping")
public class ShoppingShop extends Model implements Searchable, Serializable {

    @Expose
    @Column(name = "categoryId")
    private String categoryId;

    @Expose
    @Column(name = "categoryName")
    private String categoryName;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    private boolean selected = false;


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ShoppingShop() {
        super();
    }

    @Expose
    @SerializedName("ImagesList")
    List<ShopImage> ImagesList;

    @Expose
    List<FeaturedProduct> Featured_Products;

    public List<FeaturedProduct> getFeatured_Products() {
        return Featured_Products;
    }

    public List<FeaturedProduct> getFeatured() {
        return getMany(FeaturedProduct.class, "shopId");
    }


    public List<ShopImage> getShopImages() {

        if (ImagesList == null) {
            ImagesList = new Select().from(ShopImage.class).where("shopId = ?", new String[]{getIdentifier()}).execute();
        }
        return ImagesList;
    }


    public void setFeatured_Products(List<FeaturedProduct> featured_Products) {
        Featured_Products = featured_Products;
    }

    public List<ShopImage> getImagesList() {

        return ImagesList;
    }

    public void setImagesList(List<ShopImage> imagesList) {
        ImagesList = imagesList;
    }

    @Expose
    @Column(name = "Shop_Title")
    @SerializedName("Shop_Title")
    private String shopTitle;

    @Expose
    @Column(name = "Summary")
    @SerializedName("Summary")
    private String summary;

    @Expose
    @Column(name = "Description")
    @SerializedName("Description")
    private String description;

    @Expose
    @Column(name = "Image")
    @SerializedName("Image")
    private String image;
    @Expose
    @Column(name = "Floor")
    private String Floor;

    @Expose
    @Column(name = "Destination_ID")
    @SerializedName("Destination_ID")
    private String destinationID;

    @Expose
    @Column(name = "Shop_Nearest_parking")
    @SerializedName("Shop_Nearest_parking")
    private String shopNearParking;

    @Expose
    @Column(name = "Shop_Telephone")
    @SerializedName("Shop_Telephone")
    private String shopTelephone;

    @Expose
    @Column(name = "FB_URL")
    @SerializedName("FB_URL")
    private String fbUrl;

    @Expose
    @Column(name = "Twitter_URL")
    @SerializedName("Twitter_URL")
    private String twitterUrl;

    @Expose
    @Column(name = "identifier")
    private String ID;


    @Column(name = "favourited", notNull = false)
    private boolean favourited = false;


    public boolean isFavourited() {
        return favourited;
    }

    public void setFavourited(boolean favourited) {
        this.favourited = favourited;
    }


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description.replaceAll("&lt;", "<").
                replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                replaceAll("&amp;", "&");
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getDestinationID() {
        return destinationID;
    }

    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }

    public String getShopNearParking() {
        return shopNearParking;
    }

    public void setShopNearParking(String shopNearParking) {
        this.shopNearParking = shopNearParking;
    }

    public String getShopTelephone() {
        return shopTelephone;
    }

    public void setShopTelephone(String shopTelephone) {
        this.shopTelephone = shopTelephone;
    }

    public String getFbUrl() {
        return fbUrl;
    }

    public void setFbUrl(String fbUrl) {
        this.fbUrl = fbUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }


    @Override
    public String getIdentifier() {
        return ID;
    }

    @Override
    public String getLabel() {
        return getShopTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.SHOPPING;
    }


    public Long saveAll() {
        if (getFeatured_Products() != null) {
            for (FeaturedProduct product : getFeatured_Products()) {
                product.setShopId(getID());
                product.save();
            }
        }

        if (getImagesList() != null) {

            for (ShopImage image : getImagesList()) {
                image.setShopId(getID());
                image.save();
            }
        }


        return super.save();
    }

    @Override
    public String toString() {
        return getShopTitle();
    }
}
