package com.belongi.citycenter.data.entities;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 29/6/15.
 */

@Table(name="ATM")
public class ATMService extends BaseService implements Searchable, Serializable{

    @Expose
    @Column(name="identifier")
    @SerializedName("ID")
    private String ID;

    @Expose
    @Column(name="image")
    @SerializedName("Image")
    private String image;

    @Expose
    @Column(name="PageDetail")
    @SerializedName("PageDetail")
    private String pageDetail;

    @Expose
    @Column(name="Title")
    @SerializedName("Title")
    private String title;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public ATMService(){
        super();
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPageDetail() {
        return pageDetail;
    }

    public void setPageDetail(String pageDetail) {
        this.pageDetail = pageDetail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getServiceId() {
        return getID();
    }

    @Override
    public String getServicePageTitle() {
        return getTitle();
    }

    @Override
    public String getServicePageDetail() {
        return getPageDetail();
    }

    @Override
    public String getServiceImage() {
        return getImage();
    }

    @Override
    public int getServicePageIcon() {
        return 0;
    }

    @Override
    public String getIdentifier() {
        return getID();
    }

    @Override
    public String getLabel() {
        return getTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.ATM;
    }
}
