package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 18/6/15.
 */
public class ForgotPassword {

    @Expose
    @SerializedName("Data")
    private String success;

    @Expose
    @SerializedName("Error")
    private String error;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
