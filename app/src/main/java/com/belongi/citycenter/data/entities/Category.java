package com.belongi.citycenter.data.entities;

/**
 * Created by yashesh on 6/15/2015.
 */
public enum Category {
    DINING("DINING"),
    SHOPPING("SHOPPING"),
    OFFER("OFFER"),
    EVENT("EVENT"),
    HOTEL("HOTEL"),
    ENTERTAINMENT("ENTERTAINMENT"),
    SERVICE("SERVICE"),
    ATM("ATM")
    ;

    private final String text;

    /**
     * @param text
     */
    private Category(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
