package com.belongi.citycenter.data.entities.web.entities;

import com.google.gson.annotations.Expose;

/**
 * Created by yashesh on 6/25/2015.
 */
public class SiteIdResponse {

    @Expose
    private String MallID;

    @Expose
    private String ServerURL;

    @Expose
    private String DeviceID;

    @Expose
    private String LanguageCode;

    @Expose
    private String WhatsApp;

    @Expose
    private String FeedbackURL;

    public String getWhatsApp() {
        return WhatsApp;
    }

    public void setWhatsApp(String whatsApp) {
        WhatsApp = whatsApp;
    }

    public String getFeedbackURL() {
        return FeedbackURL;
    }

    public void setFeedbackURL(String feedbackURL) {
        FeedbackURL = feedbackURL;
    }

    public String getMallID() {
        return MallID;
    }

    public void setMallID(String mallID) {
        MallID = mallID;
    }

    public String getServerURL() {
        return ServerURL;
    }

    public void setServerURL(String serverURL) {
        ServerURL = serverURL;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getLanguageCode() {
        return LanguageCode;
    }

    public void setLanguageCode(String languageCode) {
        LanguageCode = languageCode;
    }
}
