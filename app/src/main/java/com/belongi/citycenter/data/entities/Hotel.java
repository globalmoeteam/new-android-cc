package com.belongi.citycenter.data.entities;

import android.hardware.camera2.params.StreamConfigurationMap;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by yashesh on 6/15/2015.
 */

@Table(name="Hotel")
public class Hotel extends Model implements  Searchable,Serializable{


    public Hotel(){
        super();
    }
    @Expose
    @Column(name = "identifier")
    private String ID;

    @Expose
    @Column(name = "Hotel_Name")
    private String Hotel_Name;

    @Expose
    @Column(name = "Description")
    private String Description;

    @Expose
    @Column(name = "URL")
    private String URL;

    @Expose
    @Column(name = "Image")
    private String Image;

    @Expose
    @Column(name = "Telephone")
    private String Telephone;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    private boolean isFavourited;

    public boolean isFavourited() {
        return isFavourited;
    }

    public void setIsFavourited(boolean isFavourited) {
        this.isFavourited = isFavourited;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getHotel_Name() {
        return Hotel_Name;
    }

    public void setHotel_Name(String hotel_Name) {
        Hotel_Name = hotel_Name;
    }

    public String getDescription() {
        return Description.replaceAll("&lt;", "<").
                replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                replaceAll("&amp;", "&");
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    @Override
    public String getIdentifier() {
        return ID;
    }

    @Override
    public String getLabel() {
        return getHotel_Name();
    }

    @Override
    public Category getSearchCategory() {
        return Category.HOTEL;
    }
}
