package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.belongi.citycenter.ui.activity.MallSelectionActivity;
import com.google.gson.annotations.Expose;

/**
 * Created by webwerks on 28/7/15.
 */

@Table(name="Mall")
public class Malls extends Model{

    @Expose
    @Column(name = "identifier")
    private MallSelectionActivity.Mall id;

    @Expose
    @Column(name="name")
    private String mallName;

    @Expose
    @Column(name="url")
    private String mallUrl;

    @Expose
    @Column(name="mallCode")
    private String mallCOde;

    @Expose
    @Column(name="image_url")
    private String imageUrl;

    @Expose
    @Column(name = "longitude")
    private String longitude;

    @Expose
    @Column(name = "latitude")
    private String latitude;

    @Expose
    @Column(name="siteid")
    private String siteId;

    @Expose
    @Column(name="notification_setting")
    private String notificationSetting;

    @Expose
    @Column(name="first_launch")
    private String firstLaunch;

    @Expose
    @Column(name="shopping_coach")
    private String shoppingCoach;

    @Expose
    @Column(name="getting_here")
    private String gettingHere;

    @Expose
    @Column(name="mall_movie_count")
    private int movieCount;

    @Expose
    @Column(name="whatsApp")
    private String whatsapp;

    @Expose
    @Column(name="feedback_url")
    private String feedbackUrl;

    @Expose
    @Column(name="jibe_project_id")
    private String jibeProjectId;

    @Expose
    @Column(name="mall_promotion_url")
    private String mallPromotionUrl;


    public String getMallPromotionUrl() {
        return mallPromotionUrl;
    }

    public void setMallPromotionUrl(String mallPromotionUrl) {
        this.mallPromotionUrl = mallPromotionUrl;
    }

    public String getJibeProjectId() {
        return jibeProjectId;
    }

    public void setJibeProjectId(String jibeProjectId) {
        this.jibeProjectId = jibeProjectId;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getFeedbackUrl() {
        return feedbackUrl;
    }

    public void setFeedbackUrl(String feedbackUrl) {
        this.feedbackUrl = feedbackUrl;
    }

    public int getMovieCount() {
        return movieCount;
    }

    public void setMovieCount(int movieCount) {
        this.movieCount = movieCount;
    }

    private double distance;

    public String getNotificationSetting() {
        return notificationSetting;
    }

    public void setNotificationSetting(String notificationSetting) {
        this.notificationSetting = notificationSetting;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getGettingHere() {
        return gettingHere;
    }

    public void setGettingHere(String gettingHere) {
        this.gettingHere = gettingHere;
    }

    public String getShoppingCoach() {
        return shoppingCoach;
    }

    public void setShoppingCoach(String shoppingCoach) {
        this.shoppingCoach = shoppingCoach;
    }

    public String getFirstLaunch() {
        return firstLaunch;
    }

    public void setFirstLaunch(String firstLaunch) {
        this.firstLaunch = firstLaunch;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public MallSelectionActivity.Mall getIdentifier() {
        return id;
    }

    public void setId(MallSelectionActivity.Mall id) {
        this.id = id;
    }

    public String getMallName() {
        return mallName;
    }

    public void setMallName(String mallName) {
        this.mallName = mallName;
    }

    public String getMallUrl() {
        return mallUrl;
    }

    public void setMallUrl(String mallUrl) {
        this.mallUrl = mallUrl;
    }

    public String getMallCOde() {
        return mallCOde;
    }

    public void setMallCOde(String mallCOde) {
        this.mallCOde = mallCOde;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Malls{" +
                "id=" + id +
                ", mallName='" + mallName + '\'' +
                ", mallUrl='" + mallUrl + '\'' +
                ", mallCOde='" + mallCOde + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", siteId='" + siteId + '\'' +
                ", notificationSetting='" + notificationSetting + '\'' +
                ", firstLaunch='" + firstLaunch + '\'' +
                ", shoppingCoach='" + shoppingCoach + '\'' +
                ", gettingHere='" + gettingHere + '\'' +
                ", movieCount=" + movieCount +
                ", whatsapp='" + whatsapp + '\'' +
                ", feedbackUrl='" + feedbackUrl + '\'' +
                ", jibeProjectId='" + jibeProjectId + '\'' +
                ", mallPromotionUrl='" + mallPromotionUrl + '\'' +
                ", distance=" + distance +
                '}';
    }
}