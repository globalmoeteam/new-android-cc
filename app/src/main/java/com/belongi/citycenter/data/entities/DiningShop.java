package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by user on 6/17/2015.
 */
@Table(name = "Dining")
public class DiningShop extends Model implements Searchable, Serializable {

    @Expose
    @Column(name = "favourited", notNull = false)
    private boolean favourited = false;

    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isFavourited() {
        return favourited;
    }

    public void setFavourited(boolean favourited) {
        this.favourited = favourited;
    }

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    @Expose
    @Column(name = "categoryId")
    private String categoryId;

    @Expose
    @Column(name = "categoryName")
    private String categoryName;

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "Floor")
    private String Floor;

    @Expose
    @Column(name = "Dining_Nearest_Parking")
    private String Dining_Nearest_Parking;

    @Expose
    @Column(name = "Dining_Telephone")
    private String Dining_Telephone;

    @Expose
    @Column(name = "Destination_ID")
    private String Destination_ID;

    @Expose
    @Column(name = "Summary")
    private String Summary;

    @Expose
    @Column(name = "Description")
    private String Description;

    @Expose
    @Column(name = "Shop_Code")
    private String Shop_Code;

    @Expose
    @Column(name = "Floor_ID")
    private String Floor_ID;

    @Expose
    @Column(name = "Store_Timings")
    private String Store_Timings;

    @Expose
    @Column(name = "Landmark")
    private String Landmark;

    @Expose
    @Column(name = "Telephone")
    private String Telephone;

    @Expose
    @Column(name = "Logo")
    private String Logo;

    @Expose
    @Column(name = "Thumbnail")
    private String Thumbnail;

    @Expose
    @Column(name = "FB_URL")
    private String FB_URL;

    @Expose
    @Column(name = "Twitter_URL")
    private String Twitter_URL;

    @Expose
    @Column(name = "identifier")
    private String ID;

    public DiningShop() {
        super();
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getDining_Nearest_Parking() {
        return Dining_Nearest_Parking;
    }

    public void setDining_Nearest_Parking(String dining_Nearest_Parking) {
        Dining_Nearest_Parking = dining_Nearest_Parking;
    }

    public String getDining_Telephone() {
        return Dining_Telephone;
    }

    public void setDining_Telephone(String dining_Telephone) {
        Dining_Telephone = dining_Telephone;
    }

    public String getDestination_ID() {
        return Destination_ID;
    }

    public void setDestination_ID(String destination_ID) {
        Destination_ID = destination_ID;
    }

    public String getSummary() {
        return Summary;
    }

    public void setSummary(String summary) {
        Summary = summary;
    }

    public String getDescription() {
        return Description.replaceAll("&lt;", "<").
                replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                replaceAll("&amp;", "&");
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getShop_Code() {
        return Shop_Code;
    }

    public void setShop_Code(String shop_Code) {
        Shop_Code = shop_Code;
    }

    public String getFloor_ID() {
        return Floor_ID;
    }

    public void setFloor_ID(String floor_ID) {
        Floor_ID = floor_ID;
    }

    public String getStore_Timings() {
        return Store_Timings;
    }

    public void setStore_Timings(String store_Timings) {
        Store_Timings = store_Timings;
    }

    public String getLandmark() {
        return Landmark;
    }

    public void setLandmark(String landmark) {
        Landmark = landmark;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getFB_URL() {
        return FB_URL;
    }

    public void setFB_URL(String FB_URL) {
        this.FB_URL = FB_URL;
    }

    public String getTwitter_URL() {
        return Twitter_URL;
    }

    public void setTwitter_URL(String twitter_URL) {
        Twitter_URL = twitter_URL;
    }

    @Override
    public String getIdentifier() {
        return ID;
    }

    @Override
    public String getLabel() {
        return getTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.DINING;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
