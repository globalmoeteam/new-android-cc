package com.belongi.citycenter.data.entities.web.entities;

import com.belongi.citycenter.data.entities.Sopping;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by yashesh on 6/16/2015.
 */
public class ShoppingStoreResponse extends StoreDetailResponse{

    @Expose
    List<Sopping> Shops_list;

    public List<Sopping> getShopsList() {
        return Shops_list;
    }

    public void setShopsList(List<Sopping> shopsList) {
        Shops_list = shopsList;
    }

}
