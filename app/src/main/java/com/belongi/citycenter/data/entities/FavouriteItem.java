package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

/**
 * Created by yashesh on 6/25/2015.
 */
public class FavouriteItem extends Model{


    public FavouriteItem(){
        super();
    }

    @Expose
    @Column(name = "identifier")
    private String identifier;

    @Expose
    @Column(name = "type")
    private String type;

    @Expose
    @Column(name="label")
    private String label;

    @Expose
    @Column(name="mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
