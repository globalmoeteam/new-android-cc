package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 23/6/15.
 */
public class RegisterAnswer implements Serializable {

    @Expose
    @SerializedName("Data")
    private String data;

    @Expose
    @SerializedName("AttemptsLeft")
    private String attemptsLeft;

    @Expose
    @SerializedName("Error")
    private String error;

    @Expose
    private String correctAns;

    public String getCorrectAns() {
        return correctAns;
    }

    public void setCorrectAns(String correctAns) {
        this.correctAns = correctAns;
    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAttemptsLeft() {
        return attemptsLeft;
    }

    public void setAttemptsLeft(String attemptsLeft) {
        this.attemptsLeft = attemptsLeft;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
