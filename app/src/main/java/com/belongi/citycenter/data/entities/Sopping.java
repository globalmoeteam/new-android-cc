package com.belongi.citycenter.data.entities;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yashesh on 6/15/2015.
 */
public class Sopping {

    @Expose
    private ShoppingShop Shop;

    public ShoppingShop getShop() {
        return Shop;
    }

    public void setShop(ShoppingShop shop) {
        Shop = shop;
    }

}
