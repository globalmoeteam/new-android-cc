package com.belongi.citycenter.data.entities.web.entities;


import com.belongi.citycenter.data.entities.Entertainment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 6/17/2015.
 */
public class EntertainmentResponse  extends StoreDetailResponse{

    @Expose
    private List<Entertainment> Shops_list;

    public List<Entertainment> getShops_list() {
        return Shops_list;
    }

    public void setShops_list(List<Entertainment> shops_list) {
        Shops_list = shops_list;
    }
}
