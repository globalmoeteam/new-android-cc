package com.belongi.citycenter.data.entities;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by webwerks on 24/6/15.
 */

@Table(name="Parking")
public class Parkings extends Model implements Serializable {

    @Expose
    @Column(name="image_path"  )
    private String imagePath;

    @Expose
    private Long identifier;

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    /* @Column(name="desc")
    private String desc;*/

    @Expose
    @Column(name="title")
    private String title;

    @Expose
    @Column(name="created_date")
    private String createdDate;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public Parkings(){
        super();
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

   /* public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }*/

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}

