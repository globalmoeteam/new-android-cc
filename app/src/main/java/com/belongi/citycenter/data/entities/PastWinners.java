package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by webwerks on 22/6/15.
 */
public class PastWinners {

    @Expose
    @SerializedName("Data")
    List<DrawDatesAndPastWinner> winnerLists;

    @Expose
    @SerializedName("Error")
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<DrawDatesAndPastWinner> getWinnerLists() {
        return winnerLists;
    }

    public void setWinnerLists(List<DrawDatesAndPastWinner> winnerLists) {
        this.winnerLists = winnerLists;
    }
}
