package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by yashesh on 6/22/2015.
 */
@Table(name="FeaturedProduct")
public class FeaturedProduct extends Model {

    public FeaturedProduct(){
        super();
    }

    @Expose
    @Column(name="shopId")
    private String shopId;

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "Thumbnail")
    private String Thumbnail;

    @Expose
    @Column(name = "Product_PDF")
    private String Product_PDF;

    @Expose
    @Column(name = "Brief_Description")
    private String Brief_Description;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getProduct_PDF() {
        return Product_PDF;
    }

    public void setProduct_PDF(String product_PDF) {
        Product_PDF = product_PDF;
    }

    public String getBrief_Description() {
        return Brief_Description;
    }

    public void setBrief_Description(String brief_Description) {
        Brief_Description = brief_Description;
    }
}
