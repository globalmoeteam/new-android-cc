package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by user on 7/15/2015.
 */


@Table(name = "UsefulLinksModel")
public class UsefulLinksModel extends Model{

    @Expose
    @Column(name = "StoreLocator")
    private String StoreLocator;

    @Expose
    @Column(name = "SocialWall")
    private String SocialWall;

    @Expose
    @Column(name = "BigBus")
    private String byBigBus;

    @Expose
    @Column(name = "MetroTiming")
    private String MetroTiming;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getBookATaxi() {
        return BookATaxi;
    }

    public void setBookATaxi(String bookATaxi) {
        BookATaxi = bookATaxi;
    }

    @Expose
    @Column(name = "bookATaxi")
    private String BookATaxi;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public String getStoreLocator() {
        return StoreLocator;
    }

    public void setStoreLocator(String storeLocator) {
        StoreLocator = storeLocator;
    }

    public String getSocialWall() {
        return SocialWall;
    }

    public void setSocialWall(String socialWall) {
        SocialWall = socialWall;
    }

    public String getByBigBus() {
        return byBigBus;
    }

    public void setByBigBus(String byBigBus) {
        this.byBigBus = byBigBus;
    }

    public String getMetroTiming() {
        return MetroTiming;
    }

    public void setMetroTiming(String metroTiming) {
        MetroTiming = metroTiming;
    }
}
