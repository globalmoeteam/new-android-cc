package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by user on 6/15/2015.
 */
@Table(name="Service")
public class Service extends BaseService implements Searchable,Serializable{


    public Service(){
        super();
    }

    @Expose
    @Column(name = "identifier")
    private String ID;

    @Expose
    @Column(name = "ServiceTitle")
    @SerializedName("Title")
    private String ServiceTitle;

    @Expose
    @Column(name = "PageDetail")
    @SerializedName("PageDetail")
    private String pageDetail;

    @Expose
    @Column(name = "Image")
    private String Image;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    private int serviceIcon;

    public int getServiceIcon() {
        return serviceIcon;
    }

    public void setServiceIcon(int serviceIcon) {
        this.serviceIcon = serviceIcon;
    }


    public String getPageDetail() {
        return pageDetail;
    }

    public void setPageDetail(String pageDetail) {
        this.pageDetail = pageDetail;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getServiceTitle() {
        return ServiceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        ServiceTitle = serviceTitle;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    @Override
    public String getIdentifier() {
        return getID();
    }

    @Override
    public String getLabel() {
        return getServiceTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.SERVICE;
    }


    @Override
    public String getServiceId() {
        return getID();
    }

    @Override
    public String getServicePageTitle() {
        return getServiceTitle();
    }

    @Override
    public String getServicePageDetail() {
        return getPageDetail();
    }

    @Override
    public String getServiceImage() {
        return getImage();
    }

    @Override
    public int getServicePageIcon() {
        return getServiceIcon();
    }

}
