package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by yashesh on 6/19/2015.
 */
@Table(name="SpotlightBanner")
public class SpotlightBanner extends Model implements Serializable {

    @Expose
    @Column(name = "Image_ID")
    @SerializedName("Image_ID")
    private String imageId;

    @Expose
    @Column(name = "Image")
    @SerializedName("Image")
    private String image;

    @Expose
    @Column(name = "Mode")
    @SerializedName("Mode")
    private String mode;

    @Expose
    @Column(name = "identifier")
    private String ID;

    @Expose
    @Column(name = "URL")
    @SerializedName("URL")
    private String url;

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
