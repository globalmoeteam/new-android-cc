package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.belongi.citycenter.ui.fragment.categories.BaseCategoryListFragment;
import com.google.gson.annotations.Expose;

/**
 * Created by user on 6/25/2015.
 */


@Table(name = "ShopCategory")
public class ShopCategory extends Model{

    @Expose
    @Column(name = "category_name")
    private String CategoryName;

    @Expose
    @Column(name = "identifier")
    private String ID;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    @Expose
    @Column(name = "type")
    private BaseCategoryListFragment.CategoryType type;

    public BaseCategoryListFragment.CategoryType getType() {
        return type;
    }

    public void setType(BaseCategoryListFragment.CategoryType type) {
        this.type = type;
    }

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public ShopCategory() {
    }

    public ShopCategory(String name) {
        setCategoryName(name);
        setID("0");
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return CategoryName;
    }
}
