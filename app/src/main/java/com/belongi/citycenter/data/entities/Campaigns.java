package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class Campaigns {

    @Expose
    @SerializedName("Data")
    private List<Compaign> listCompagins;

    @Expose
    @SerializedName("Error")
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Compaign> getListCompagins() {
        return listCompagins;
    }

    public void setListCompagins(List<Compaign> listCompagins) {
        this.listCompagins = listCompagins;
    }

    public class Compaign implements Serializable {

        @Expose
        @SerializedName("CampaignId")
        private String compaignId;

        @Expose
        @SerializedName("CampaignTitle")
        private String campaignTitle;

        @Expose
        @SerializedName("CampaignDesc")
        private String campaignDesc;

        @Expose
        @SerializedName("CampaignThmb")
        private String campaignThmb;

        @Expose
        @SerializedName("CampaignPrize")
        private String campaignPrize;

        @Expose
        @SerializedName("StartDate")
        private String startDate;

        @Expose
        @SerializedName("EndDate")
        private String endDate;

        @Expose
        @SerializedName("campaignDetailDesc")
        private String campaignDetailDesc;

        @Expose
        @SerializedName("terms_conditions")
        private String termsConditions;

        @Expose
        @SerializedName("summary")
        private String summary;

        @Expose
        @SerializedName("Status")
        private String status;

        public String getCompaignId() {
            return compaignId;
        }

        public void setCompaignId(String compaignId) {
            this.compaignId = compaignId;
        }

        public String getCampaignTitle() {
            return campaignTitle;
        }

        public void setCampaignTitle(String campaignTitle) {
            this.campaignTitle = campaignTitle;
        }

        public String getCampaignDesc() {
            return campaignDesc;
        }

        public void setCampaignDesc(String campaignDesc) {
            this.campaignDesc = campaignDesc;
        }

        public String getCampaignThmb() {
            return campaignThmb;
        }

        public void setCampaignThmb(String campaignThmb) {
            this.campaignThmb = campaignThmb;
        }

        public String getCampaignPrize() {
            return campaignPrize;
        }

        public void setCampaignPrize(String campaignPrize) {
            this.campaignPrize = campaignPrize;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getCampaignDetailDesc() {
            return campaignDetailDesc;
        }

        public void setCampaignDetailDesc(String campaignDetailDesc) {
            this.campaignDetailDesc = campaignDetailDesc;
        }

        public String getTermsConditions() {
            return termsConditions;
        }

        public void setTermsConditions(String termsConditions) {
            this.termsConditions = termsConditions;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}


