package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;

import java.io.Serializable;

/**
 * Created by webwerks on 29/6/15.
 */
public abstract class BaseService extends Model implements Serializable{

    //private String id,title,pageDetail,image,serviceIcon;

    public abstract String getServiceId();

    public abstract String getServicePageTitle();

    public abstract String getServicePageDetail();

    public abstract String getServiceImage();

    public abstract int getServicePageIcon();

}
