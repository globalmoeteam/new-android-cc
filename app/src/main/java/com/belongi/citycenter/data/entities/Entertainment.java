package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by yashesh on 6/15/2015.
 */
@Table(name = "Entertainment")
public class Entertainment extends Model implements Searchable, Serializable {


    public Entertainment() {
        super();
    }

    @Expose
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    @Expose
    @Column(name = "categoryId")
    private String categoryId;

    @Expose
    @Column(name = "categoryName")
    private String categoryName;

    @Expose
    @Column(name = "identifier")
    private String ID;

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "Floor")
    private String Floor;

    @Expose
    @Column(name = "Nearest_Parking")
    private String Nearest_Parking;

    @Expose
    @Column(name = "Entertainment_Telephone")
    private String Entertainment_Telephone;


    @Expose
    @Column(name = "Destination_ID")
    private String Destination_ID;


    @Expose
    @Column(name = "Summary")
    private String Summary;


    @Expose
    @Column(name = "Description")
    private String Description;


    @Expose
    @Column(name = "Shop_Code")
    private String Shop_Code;


    @Expose
    @Column(name = "Shop_Timings")
    private String Shop_Timings;


    @Expose
    @Column(name = "Logo")
    private String Logo;


    @Expose
    @Column(name = "Thumbnail")
    private String Thumbnail;


    @Expose
    @Column(name = "FB_URL")
    private String FB_URL;


    @Expose
    @Column(name = "Twitter_Page_URL")
    private String Twitter_Page_URL;

    @Expose
    @Column(name = "forHome")
    private int forHome;

    // 0 : true , 1 : false

    public int isForHome() {
        return forHome;
    }

    public void setForHome(int forHome) {
        this.forHome = forHome;
    }

    @Column(name = "favourited", notNull = false)
    private boolean favourited = false;

    public boolean isFavourited() {
        return favourited;
    }

    public void setFavourited(boolean favourited) {
        this.favourited = favourited;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getNearest_Parking() {
        return Nearest_Parking;
    }

    public void setNearest_Parking(String nearest_Parking) {
        Nearest_Parking = nearest_Parking;
    }

    public String getEntertainment_Telephone() {
        return Entertainment_Telephone;
    }

    public void setEntertainment_Telephone(String entertainment_Telephone) {
        Entertainment_Telephone = entertainment_Telephone;
    }

    public String getDestination_ID() {
        return Destination_ID;
    }

    public void setDestination_ID(String destination_ID) {
        Destination_ID = destination_ID;
    }

    public String getSummary() {
        return Summary;
    }

    public void setSummary(String summary) {
        Summary = summary;
    }

    public String getDescription() {
        return Description.replaceAll("&lt;", "<").
                replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                replaceAll("&amp;", "&");
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getShop_Code() {
        return Shop_Code;
    }

    public void setShop_Code(String shop_Code) {
        Shop_Code = shop_Code;
    }

    public String getShop_Timings() {
        return Shop_Timings;
    }

    public void setShop_Timings(String shop_Timings) {
        Shop_Timings = shop_Timings;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getFB_URL() {
        return FB_URL;
    }

    public void setFB_URL(String FB_URL) {
        this.FB_URL = FB_URL;
    }

    public String getTwitter_Page_URL() {
        return Twitter_Page_URL;
    }

    public void setTwitter_Page_URL(String twitter_Page_URL) {
        Twitter_Page_URL = twitter_Page_URL;
    }

    @Override
    public String getIdentifier() {
        return ID;
    }

    @Override
    public String getLabel() {
        return getTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.ENTERTAINMENT;
    }


    @Override
    public String toString() {
        return getTitle();
    }
}
