package com.belongi.citycenter.data.entities.web.entities;

import com.belongi.citycenter.data.entities.Dining;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by yashesh on 6/15/2015.
 */
public class DiningStoreResponse extends StoreDetailResponse {

    @Expose
    List<Dining> Shops_list;

    public List<Dining> getShopsList() {
        return Shops_list;
    }

    public void setShopsList(List<Dining> shopsList) {
        Shops_list = shopsList;
    }
}
