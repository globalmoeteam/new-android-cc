package com.belongi.citycenter.data.entities.utils;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.belongi.citycenter.data.entities.Parkings;
import com.belongi.citycenter.global.App;

import java.util.List;

/**
 * Created by webwerks on 24/6/15.
 */
public class ParkingDAO {

    public static List<Parkings> getAllParkings(){
        return new Select().all().from(Parkings.class).where("mallid=?", App.get().getMallId()).execute();
    }

    public static void insertParking(Parkings parking){
        parking.save();
    }

    public static void deleteParking(Parkings parking){
        if(parking.getIdentifier()!=null)
            new Delete().from(Parkings.class).where("Id=?",parking.getIdentifier()).where("mallid=?",parking.getMallId()).execute();
    }

    public static void insertOrUpdate(Parkings parking){
        Parkings parkingObj;
        if(parking.getIdentifier()!=null){
            List<Parkings> parkingsList=new Select().from(Parkings.class).where("Id=?",parking.getIdentifier()).where("mallid=?",parking.getMallId()).execute();

            if(parkingsList!=null &&parkingsList.size()>0){
                parkingObj=parkingsList.get(0);
                parkingObj.setCreatedDate(parking.getCreatedDate());
                parkingObj.setTitle(parking.getTitle());
                parkingObj.setImagePath(parking.getImagePath());
            }else{
                parkingObj=parking;
            }
        }else{
            parkingObj=parking;
        }
        parkingObj.save();

//        new Update(Parkings.class).set("title=?,image_path=?,created_date=?",parking.getTitle(),parking.getImagePath(),parking.getCreatedDate()).execute();
    }
}
