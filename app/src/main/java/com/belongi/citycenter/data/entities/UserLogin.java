package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class UserLogin {

    @Expose
    @SerializedName("Data")
    private LoginResponse loginRespo;

    @Expose
    @SerializedName("Error")
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LoginResponse getLoginRespo() {
        return loginRespo;
    }

    public void setLoginRespo(LoginResponse loginRespo) {
        this.loginRespo = loginRespo;
    }

    public class LoginResponse{

        @Expose
        @SerializedName("Email")
        private String email;

        @Expose
        @SerializedName("UserId")
        private String userId;

        @Expose
        @SerializedName("Mobile")
        private String mobile;

        @Expose
        @SerializedName("Country")
        private String country;

        @Expose
        @SerializedName("Name")
        private String name;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
