package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 25/6/15.
 */
@Table(name = "TheMall")
public class MallContentPage extends Model implements Serializable{

    public MallContentPage(){
        super();
    }

    @Expose
    @Column(name = "identifier")
    @SerializedName("ID")
    private String id;

    @Expose
    @Column(name = "Title")
    @SerializedName("Title")
    private String title;

    @Expose
    @Column(name = "PageDetail")
    @SerializedName("PageDetail")
    private String pageDetail;

    @Expose
    @Column(name = "Image")
    @SerializedName("Image")
    private String image;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    private String contentType;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getIdentifier() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPageDetail() {
        return pageDetail;
    }

    public void setPageDetail(String pageDetail) {
        this.pageDetail = pageDetail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
