package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by yashesh on 6/22/2015.
 */
@Table(name = "ShopImage")
public class ShopImage extends Model{


    public ShopImage(){
        super();
    }

    @Expose
    @Column(name="shopId")
    private String shopId;

    @Expose
    @Column(name="Image")
    private String Image;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
