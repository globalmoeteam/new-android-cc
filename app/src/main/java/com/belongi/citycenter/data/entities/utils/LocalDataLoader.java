package com.belongi.citycenter.data.entities.utils;

import com.belongi.citycenter.global.BackgroundJobManager;
import com.belongi.citycenter.threading.BackgroundJob;
import com.belongi.citycenter.threading.BackgroundJobClient;

/**
 * Created by yashesh on 6/18/2015.
 */
public class LocalDataLoader extends BackgroundJob{
    int mRequestCode=0;

    public LocalDataLoader(QueryClient client,int requestCode) {
        super(client);
        mRequestCode=requestCode;
    }

    @Override
    public void run() {
            notifyCompletion(mRequestCode,((QueryClient)mClient).executeQuery());
    }

    public static interface QueryClient extends BackgroundJobClient{
        public Object executeQuery();
    }

    public void execute(){
        BackgroundJobManager.getInstance().submitJob(this);
    }
}
