package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class DrawDatesAndPastWinner {

   /* @SerializedName("Data")
    List<WinnerList> winnerLists;

    @SerializedName("Error")
    private String error;*/

    @Expose
    private int sequence;

    @Expose
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    private String desc;

    @Expose
    @SerializedName("winnerId")
    private String winnerId;

    @Expose
    @SerializedName("userId")
    private String userId;

    @Expose
    @SerializedName("user_name")
    private String userName;

    @Expose
    @SerializedName("campaignTitle")
    private String compaignTitle;

    @Expose
    @SerializedName("drawdate")
    private String drawDate;

    @Expose
    @SerializedName("status")
    private String status;

    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompaignTitle() {
        return compaignTitle;
    }

    public void setCompaignTitle(String compaignTitle) {
        this.compaignTitle = compaignTitle;
    }

    public String getDrawDate() {
        return drawDate;
    }

    public void setDrawDate(String drawDate) {
        this.drawDate = drawDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
