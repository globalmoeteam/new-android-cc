package com.belongi.citycenter.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 22/6/15.
 */
public class QuestionAnswer{

    @Expose
    @SerializedName("QuestionId")
    private String questionId;

    @Expose
    @SerializedName("Question")
    private String question;

    @Expose
    @SerializedName("Answer")
    private Answer answer;

    @Expose
    @SerializedName("CorrectAnswerId")
    private String correctAnswerId;

    @Expose
    @SerializedName("AttemptId")
    private String attemptId;

    @Expose
    @SerializedName("AttemptsLeft")
    private String attemptLeft;

    @Expose
    @SerializedName("Error")
    private String error;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public String getCorrectAnswerId() {
        return correctAnswerId;
    }

    public void setCorrectAnswerId(String correctAnswerId) {
        this.correctAnswerId = correctAnswerId;
    }

    public String getAttemptId() {
        return attemptId;
    }

    public void setAttemptId(String attemptId) {
        this.attemptId = attemptId;
    }

    public String getAttemptLeft() {
        return attemptLeft;
    }

    public void setAttemptLeft(String attemptLeft) {
        this.attemptLeft = attemptLeft;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String correctAns;

    public String getCorrectAns() {
        return correctAns;
    }

    public void setCorrectAns(String correctAns) {
        this.correctAns = correctAns;
    }

    public class Answer implements Serializable{

        @Expose
        @SerializedName("AnswerId1")
        private String answerId1;

        @Expose
        @SerializedName("Answer1")
        private String answer1;

        @Expose
        @SerializedName("AnswerId2")
        private String answerId2;

        @Expose
        @SerializedName("Answer2")
        private String answer2;

        @Expose
        @SerializedName("AnswerId3")
        private String answerId3;

        @Expose
        @SerializedName("Answer3")
        private String answer3;

        @Expose
        @SerializedName("AnswerId4")
        private String answerId4;

        @Expose
        @SerializedName("Answer4")
        private String answer4;

        public String getAnswerId1() {
            return answerId1;
        }

        public void setAnswerId1(String answerId1) {
            this.answerId1 = answerId1;
        }

        public String getAnswer1() {
            return answer1;
        }

        public void setAnswer1(String answer1) {
            this.answer1 = answer1;
        }

        public String getAnswerId2() {
            return answerId2;
        }

        public void setAnswerId2(String answerId2) {
            this.answerId2 = answerId2;
        }

        public String getAnswer2() {
            return answer2;
        }

        public void setAnswer2(String answer2) {
            this.answer2 = answer2;
        }

        public String getAnswerId3() {
            return answerId3;
        }

        public void setAnswerId3(String answerId3) {
            this.answerId3 = answerId3;
        }

        public String getAnswer3() {
            return answer3;
        }

        public void setAnswer3(String answer3) {
            this.answer3 = answer3;
        }

        public String getAnswerId4() {
            return answerId4;
        }

        public void setAnswerId4(String answerId4) {
            this.answerId4 = answerId4;
        }

        public String getAnswer4() {
            return answer4;
        }

        public void setAnswer4(String answer4) {
            this.answer4 = answer4;
        }
    }

}
