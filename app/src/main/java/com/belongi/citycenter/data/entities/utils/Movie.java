package com.belongi.citycenter.data.entities.utils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by yashesh on 6/19/2015.
 */

@Table(name = "Movie")
public class Movie extends Model implements Serializable {

    @Expose
    @Column(name = "title")
    private String Title;

    @Expose
    @Column(name = "image")
    private String Image;

    @Expose
    @Column(name = "url")
    private String URL;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
