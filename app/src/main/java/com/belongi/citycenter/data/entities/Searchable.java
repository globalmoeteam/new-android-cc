package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by user on 6/15/2015.
 */

public  interface Searchable {

    public String getIdentifier();

    public String getLabel();

    public Category getSearchCategory();

    public String getMallId();
}
