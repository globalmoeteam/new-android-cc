package com.belongi.citycenter.data.entities.web.entities;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by yashesh on 6/16/2015.
 */
public class StoreCategoriesResponse {

    @Expose
    private String CategoryName;

    @Expose
    private String ID;

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


}
