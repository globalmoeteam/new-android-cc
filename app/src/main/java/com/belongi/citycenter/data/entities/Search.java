package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by yashesh on 6/17/2015.
 */
@Table(name = "Search")
public class Search extends Model {

    @Expose
    @Column(name = "mallid")
    private String mallId;

    @Expose
    @Column(name = "identifier")
    private String id;

    @Expose
    @Column(name = "label",index = true)
    private String label;

    @Expose
    @Column(name="category")
    private String category;

    @Expose
    private String level;

    @Expose
    private int floor;

    @Expose
    private String call;

    @Expose
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public Search(){
        super();
    }

    public String getIdentifier() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
