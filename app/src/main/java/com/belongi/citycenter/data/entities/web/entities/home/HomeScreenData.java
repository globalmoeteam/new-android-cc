package com.belongi.citycenter.data.entities.web.entities.home;

import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.SpotlightBanner;
import com.belongi.citycenter.data.entities.utils.Movie;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 6/19/2015.
 */

public class HomeScreenData {

    SpotlightBanner[] spotlightBanners;
    Movie[] movies;
    List<Entertainment> entertainments;
    private List offersAndEvents;

    public List<Entertainment> getEntertainments() {
        return entertainments;
    }

    public void setEntertainments(List<Entertainment> entertainments) {
        this.entertainments = entertainments;
    }

    public Movie[] getMovies() {
        return movies;
    }

    public void setMovies(Movie[] movies) {
        this.movies = movies;
    }

    public SpotlightBanner[] getSpotlightBanners() {
        return spotlightBanners;
    }

    public void setSpotlightBanners(SpotlightBanner[] spotlightBanners) {
        this.spotlightBanners = spotlightBanners;
    }

    public List getOffersAndEvents(){
        if (offersAndEvents == null) {
            offersAndEvents = new ArrayList();
        }
        return offersAndEvents;
    }

    public void setOffersAndEvents(List offersAndEvents) {
        this.offersAndEvents = new ArrayList();
        this.offersAndEvents.addAll(offersAndEvents);
    }
}
