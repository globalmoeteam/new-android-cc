package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by yashesh on 6/15/2015.
 */

@Table(name="Offer")
public class Offer extends Model implements  Searchable,Serializable {


    public Offer(){
        super();
    }

    @Expose
    @Column(name="identifier")
    private String ID;

    @Expose
    @Column(name="Title")
    private String Title;

    @Expose
    @Column(name="Description")
    private String Description;

    @Expose
    @Column(name="Start_Date")
    private String Start_Date;

    @Expose
    @Column(name="End_Date")
    private String End_Date;

    @Expose
    @Column(name="Image_URL")
    private String Image_URL;

    @Expose
    @Column(name="Category_ID")
    private String Category_ID;

    @Expose
    @Column(name= "Store_ID")
    @SerializedName("Store_ID")
    private String storeId;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    @Expose
    @Column(name = "forHome")
    private int forHome;

    public int getForHome() {
        return forHome;
    }

    public void setForHome(int forHome) {
        this.forHome = forHome;
    }

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }


    public String getCategory_ID() {
        return Category_ID;
    }

    public void setCategory_ID(String category_ID) {
        Category_ID = category_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description.replaceAll("&lt;", "<").
                replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                replaceAll("&amp;", "&");
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getStart_Date() {
        return Start_Date;
    }

    public void setStart_Date(String start_Date) {
        Start_Date = start_Date;
    }

    public String getEnd_Date() {
        return End_Date;
    }

    public void setEnd_Date(String end_Date) {
        End_Date = end_Date;
    }

    public String getImage_URL() {
        return Image_URL;
    }

    public void setImage_URL(String image_URL) {
        Image_URL = image_URL;
    }

    @Override
    public String getIdentifier() {
        return ID;
    }

    @Override
    public String getLabel() {
        return getTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.OFFER;
    }
}
