package com.belongi.citycenter.data.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by yashesh on 6/15/2015.
 */
public class Event extends Model implements Searchable, Serializable {


    public Event() {
        super();
    }

    @Expose
    @Column(name = "identifier")
    private String ID;

    @Expose
    @Column(name = "Title")
    private String Title;

    @Expose
    @Column(name = "Sort_Description")
    private String Short_Description;

    @Expose
    @Column(name = "Event_Details")
    private String Event_Details;

    @Expose
    @Column(name = "Event_Start_Date")
    private String Event_Start_Date;

    @Expose
    @Column(name = "Event_End_Date")
    private String Event_End_Date;

    @Expose
    @Column(name = "Event_Image")
    private String Event_Image;

    @Expose
    @Column(name = "mallid")
    private String mallId;

    @Expose
    @Column(name = "forHome")
    private int forHome;

    public int getForHome() {
        return forHome;
    }

    public void setForHome(int forHome) {
        this.forHome = forHome;
    }

    public String getMallId() {
        return mallId;
    }

    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public String getEvent_End_Date() {
        return Event_End_Date;
    }

    public void setEvent_End_Date(String event_End_Date) {
        Event_End_Date = event_End_Date;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getShort_Description() {
        return Short_Description;
    }

    public void setShort_Description(String short_Description) {
        Short_Description = short_Description;
    }

    public String getEvent_Details() {
        return Event_Details;
    }

    public void setEvent_Details(String event_Details) {
        Event_Details = event_Details;
    }

    public String getEvent_Start_Date() {
        return Event_Start_Date;
    }

    public void setEvent_Start_Date(String event_Start_Date) {
        Event_Start_Date = event_Start_Date;
    }


    public String getEvent_Image() {
        return Event_Image;
    }

    public void setEvent_Image(String event_Image) {
        Event_Image = event_Image;
    }

    @Override
    public String getIdentifier() {
        return getID();
    }

    @Override
    public String getLabel() {
        return getTitle();
    }

    @Override
    public Category getSearchCategory() {
        return Category.EVENT;
    }
}
