package com.belongi.citycenter.jibe.overlays;

import com.belongi.citycenter.jibe.DrawerMenuOject;

/**
 * Created by webwerks on 30/11/16.
 */
public interface OnItemClickListerner {
        void onItemClickListener(int position, DrawerMenuOject object);
}
