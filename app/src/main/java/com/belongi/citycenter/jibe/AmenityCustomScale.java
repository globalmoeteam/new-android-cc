package com.belongi.citycenter.jibe;

import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.jibestream.jibestreamandroidlibrary.main.Camera;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyleIcon;

/**
 * Created by emmanuel on 15-07-05.
 */
public class AmenityCustomScale extends com.jibestream.jibestreamandroidlibrary.elements.Amenity {

    public AmenityCustomScale() {
        super();
        setHeadsUp(true);
        // getTransform().setScale(0.5f);
        RenderStyleIcon renderStyleIcon = new RenderStyleIcon();
        renderStyleIcon.renderStyleBG = new RenderStyle(Paint.Style.FILL);
        renderStyleIcon.renderStyleMG = new RenderStyle(Paint.Style.FILL);
        renderStyleIcon.renderStyleFG = new RenderStyle(Paint.Style.FILL);
        renderStyleIcon.renderStyleBG.paintFill.setColor(Color.RED);
        renderStyleIcon.renderStyleMG.paintFill.setColor(Color.WHITE);
        renderStyleIcon.renderStyleFG.paintFill.setColor(Color.WHITE);
        setStyleIdle(renderStyleIcon);
    }


    private final float[] matrixArray = new float[9];

    @Override
    public void onPreRender(M m, long timeElapsed, long timeTotal, int fps, Camera camera) {
        super.onPreRender(m, timeElapsed, timeTotal, fps, camera);
        if (getDirtyTransform() || camera.isTransformationDirty()) {
            float cameraScale = camera.getScale();
            float cameraRec = 1f / cameraScale;
            float zoomThreshold = 2f;
            if (cameraRec > zoomThreshold) {
                final Matrix transformation = getTransformation();
                transformation.getValues(matrixArray);
                float x = matrixArray[Matrix.MTRANS_X];
                float y = matrixArray[Matrix.MTRANS_Y];
                transformation.postScale(cameraScale, cameraScale, x, y);
                transformation.postScale(zoomThreshold, zoomThreshold, x, y);
            }
        }
    }
}
