package com.belongi.citycenter.jibe.overlays;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;
import com.jibestream.jibestreamandroidlibrary.elements.Element;
import com.jibestream.jibestreamandroidlibrary.elements.Unit;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.main.VenueData;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Category;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Destination;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;

import java.util.ArrayList;

/**
 * Created by webwerks on 14/11/16.
 */
public class AttractionOverlay extends FrameLayout implements View.OnClickListener {
    Context mContext;
    LinearLayout llFoodCourt, llCinema, llEntertainment;
    public String selected;
    StoreLocatorNewFragment storeFragment = null;

    public AttractionOverlay(Context context,StoreLocatorNewFragment storeFragment) {
        super(context);
        mContext = context;
        this.storeFragment = storeFragment;

        View view = LayoutInflater.from(mContext).inflate(R.layout.attractions_overlay, null);
        addView(view);

        llFoodCourt = (LinearLayout) view.findViewById(R.id.ll_foodCourt);
        llCinema = (LinearLayout) view.findViewById(R.id.ll_cinema);
        llEntertainment = (LinearLayout) view.findViewById(R.id.ll_entertainment);

        llFoodCourt.setOnClickListener(this);
        llCinema.setOnClickListener(this);
        llEntertainment.setOnClickListener(this);

        View cinemaView=view.findViewById(R.id.view_cinema);
        LinearLayout llRoot= (LinearLayout) findViewById(R.id.ll_root);

        switch (App.get().getSelectedMall().getIdentifier()) {
            case SHARJAH:
            case MAADI:
                llCinema.setVisibility(GONE);
                cinemaView.setVisibility(GONE);
                llRoot.setWeightSum(2);
                break;

            default:
                llCinema.setVisibility(VISIBLE);
                cinemaView.setVisibility(VISIBLE);
                llRoot.setWeightSum(3);
                break;
        }
    }

    public void clearHighLights() {
        M m = MSingleton.getI();
        m.unHighlightUnits();
    }

    @Override
    public void onClick(View view) {
        clearHighLights();
        M m = MSingleton.getI();
        m.camera.zoomTo(m.defaultFramings[Constant.LAST_SELECTED_MAP_INDEX]);
        m.camera.setRoll(0);
        switch (view.getId()) {
            case R.id.ll_foodCourt:
                ColorDrawable cd = (ColorDrawable)llFoodCourt.getBackground();
                if(cd.getColor() ==  ContextCompat.getColor(mContext, R.color.attraction_selected)){
                    llFoodCourt.setBackgroundColor(ContextCompat.getColor(mContext, R.color.map_menu_selected));
                    selected = "";
                    clearHighLights();
                }else{
                    llFoodCourt.setBackgroundColor(ContextCompat.getColor(mContext, R.color.attraction_selected));
                    selected = "FOOD COURTS";
                    highLightBasedOnCategory("FOOD COURTS");
                }
                llCinema.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                llEntertainment.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));

                break;

            case R.id.ll_cinema:

                ColorDrawable cdCinema = (ColorDrawable)llCinema.getBackground();
                if(cdCinema.getColor() ==  ContextCompat.getColor(mContext, R.color.attraction_selected)){
                    llCinema.setBackgroundColor(ContextCompat.getColor(mContext, R.color.map_menu_selected));
                    selected = "";
                    clearHighLights();
                }else{
                    llCinema.setBackgroundColor(ContextCompat.getColor(mContext, R.color.attraction_selected));
                    selected = "CINEMA";
                    highLightBasedOnCategory("CINEMA");
                }
                llFoodCourt.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                llEntertainment.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                break;

            case R.id.ll_entertainment:

                ColorDrawable cdEntertainment = (ColorDrawable)llEntertainment.getBackground();
                if(cdEntertainment.getColor() ==  ContextCompat.getColor(mContext, R.color.attraction_selected)){
                    llEntertainment.setBackgroundColor(ContextCompat.getColor(mContext, R.color.map_menu_selected));
                    selected = "";
                    clearHighLights();
                }else{
                    llEntertainment.setBackgroundColor(ContextCompat.getColor(mContext, R.color.attraction_selected));
                    selected = "ENTERTAINMENT";
                    highLightBasedOnCategory("ENTERTAINMENT");
                }
                llCinema.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                llFoodCourt.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));

                break;
        }
    }

    public String getSelectedType() {
        return selected;
    }

    public void highLightBasedOnCategory(String selected) {

        int id = 0;
        if (!TextUtils.isEmpty(selected)) {
           /* if (selected.equalsIgnoreCase("FOOD COURTS")) {
                id = 7442;
            } else if (selected.equalsIgnoreCase("CINEMA")) {
                id = 7441;
            } else if (selected.equalsIgnoreCase("ENTERTAINMENT")) {
                id = 6233;
            }*/
            final VenueData venueData = MSingleton.getI().venueData;
            Category[] categories=venueData.categories;
            for(Category cat:categories){
                //Log.e("Category","Category Name"+cat.name);
                if(cat.name.contains("Food Court") && selected.equalsIgnoreCase("FOOD COURTS")){
                    id=cat.id;
                    break;
                }else if(cat.name.contains("Cinema") && selected.equalsIgnoreCase("CINEMA")){
                    id=cat.id;
                    break;
                }else if(cat.name.contains("Entertainment") && selected.equalsIgnoreCase("ENTERTAINMENT")){
                    id=cat.id;
                    break;
                }
            }


            M m = MSingleton.getI();
            //final VenueData venueData = MSingleton.getI().venueData;
            com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Category[] arrCategory = venueData.categories;
            com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Category cat = null;

            for (com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Category category : arrCategory) {
                if (category.id == id) {
                    cat = category;
                    break;
                }
            }

            Element[] elements = m.getElementsByType(Unit.class);
            final int size = elements.length;

            // collect all units with required category
            Category categoryToHighlight = cat;
            //Log.e("Category Highlight", categoryToHighlight.name);

            RectF regionToZoomTo = new RectF();
            ArrayList<Unit> unitsOfCategoryX = new ArrayList<>();
            ArrayList<Unit> unitsOfCategoryXOnSameLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Unit unit = (Unit) elements[i];
                if (unit.getDestinations() == null) continue;
                if (unit.getDestinations().length == 0) continue;
                Destination[] destinations = unit.getDestinations();
                for (int j = 0; j < destinations.length; j++) {
                    Destination destination = destinations[j];

                    if (destination == null) continue;
                    if (destination.categoryId == null) continue;
                    int[] categoryIds = destination.categoryId;
                    for (int k = 0; k < categoryIds.length; k++) {
                        int categoryId = categoryIds[k];
                        if (categoryToHighlight != null && categoryId == categoryToHighlight.id) {
                            unitsOfCategoryX.add(unit);
                            if(unit.getLevel() == Constant.LAST_SELECTED_MAP_INDEX) {
                                unitsOfCategoryXOnSameLevel.add(unit);
                                RenderStyle renderStyleHighlight = new RenderStyle(Paint.Style.FILL);
                                renderStyleHighlight.setFillColor(Color.parseColor("#D13D56"));
                                unit.setStyleHighlighted(renderStyleHighlight);
                                unit.setHighlightState(true);
                                regionToZoomTo.union(unit.getBBox());
                            }
                        }
                    }


                }
            }

            //int lastLevel=unitsOfCategoryX.get(0).getLevel();
            String strLevel="";
            String lastLevel="";

            for(Unit unit1:unitsOfCategoryX){
                if(unit1.getLevel() != Constant.LAST_SELECTED_MAP_INDEX
                        && (TextUtils.isEmpty(lastLevel) || !lastLevel.equals(unit1.getLevel()+""))){
                    lastLevel=unit1.getLevel()+"";
                    if(TextUtils.isEmpty(strLevel)){
                        strLevel=getFullLevel(unit1.getLevel());
                    }else if(strLevel.contains(","))
                    {
                        strLevel+="and "+getFullLevel(unit1.getLevel());
                    }
                    else {
                        strLevel += ", " + getFullLevel(unit1.getLevel());
                    }
                }
            }

            // Frame highlighted units
            // For each level get the bounding box of the units
            m.camera.zoomTo(regionToZoomTo, 50);

            if(unitsOfCategoryXOnSameLevel.size() == 0 && unitsOfCategoryX.size() > 0) {
                storeFragment.showAttractionMissingDialog(unitsOfCategoryX/*.get(0).getLevel()*/);
            }else{
                if(!TextUtils.isEmpty(strLevel)) {
                    showAttractionDialog("The attraction you have chosen is also available on " + strLevel);
                }
            }

        }
    }

    Dialog alertDialog=null;

    public void showAttractionDialog(String text) {

        alertDialog = new Dialog(mContext);
        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_attraction_availbility, null);
        alertDialog.setContentView(view);

        DisplayMetrics disMat = getResources().getDisplayMetrics();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = (int) (disMat.widthPixels * (0.70));
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView)view.findViewById(R.id.lblMessage)).setText(text);
        (view.findViewById(R.id.llLevel)).setVisibility(GONE);
        ((Button) view.findViewById(R.id.btnClose)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#222222")));
        alertDialog.show();
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public String getFullLevel(int level) {
        String label = "";
        if (level == 0) {
            label = "Ground Floor";
        } else if (level < 0) {
            label = "Level " + (MSingleton.getI().venueData.maps.length - 1);
        } else
            label = "Level " + (level);
        return label;
    }
}
