package com.belongi.citycenter.jibe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;
import com.jibestream.jibestreamandroidlibrary.elements.YouAreHere;
import com.jibestream.jibestreamandroidlibrary.intentFilters.IntentFilterWayfind;
import com.jibestream.jibestreamandroidlibrary.intents.IntentWayfind;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.astar.PathPerFloor;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.MapFull;
import com.jibestream.jibestreamandroidlibrary.shapes.Image;

import java.util.ArrayList;

/**
 * Created by webwerks on 8/6/16.
 */
public class WayFindManager {
    private static final String TAG = "WayFindManager";
    private M m;
    private Context context;
    public PathPerFloor[] pathPerFloors;
    public Handler handler;
    public ArrayList<Segment> segments;
    public static StoreLocatorNewFragment storeLocatorFragment;

    public YouAreHere youAreHere;


    public WayFindManager(M m, Context context, StoreLocatorNewFragment storeLocatorFragment) {
        this.m = m;
        this.context = context;
        this.storeLocatorFragment = storeLocatorFragment;

        m.setActiveWayfindKiosks(false);// for hide star on map

        m.youAreHere.setActive(false);
        youAreHere = new YouAreHere();
        //youAreHere.setHeadsUp(true);
        //youAreHere.setConstantScale(true);

        Image image=new Image();
        Bitmap startPin = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_start);
        image.setBitmap(startPin);

        youAreHere.setShape(image);
        m.addToMap(youAreHere);
        m.youAreHere = youAreHere;
        m.youAreHere.setActive(true);

        handler = new Handler(Looper.getMainLooper());
    }

    // Start the wayfinding
    public void start() {
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiverWayfind, new IntentFilterWayfind(m, IntentFilterWayfind.ACTION));
    }

    // Stop the wayfinding
    public void stop() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiverWayfind);
    }


    // Begin animation
    public void startAnimation() {
        if (segments != null) {
            handler.post(segments.get(0));
        }
    }

    // Stop animation
    public void stoptAnimation() {
        if (segments != null) {
            for (int i = 0; i < segments.size(); i++) {
                Segment segment = segments.get(i);
                segment.unregister();
                handler.removeCallbacks(segment);
            }
        }
    }

    final BroadcastReceiver broadcastReceiverWayfind = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "wayfind");
            stoptAnimation();
            IntentWayfind intentWayfind = (IntentWayfind) intent;
            pathPerFloors = intentWayfind.pathPerFloors;
            Log.i("pathPerFloors", "" + pathPerFloors);
            if (pathPerFloors == null) return;

            //getTextDir(pathPerFloors);
            int length = pathPerFloors.length;
            /*for (int i = 0; i < m.venueData.maps.length; i++) {
                if (pathPerFloors[0].mapId == m.venueData.maps[i].map.mapId) {
                    setFromLevel(i);
                }
                if (pathPerFloors[length - 1].mapId == m.venueData.maps[i].map.mapId) {
                    setToLevel(i);
                }
            }*/
            segments = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                PathPerFloor pathPerFloor = pathPerFloors[i];
                if (pathPerFloor.points.length < 2) continue;
                final int mapId = pathPerFloor.mapId;
                final MapFull mapFull = m.getMapWithID(mapId);
                segments.add(new Segment(m, mapFull, handler, context));
            }
            length = segments.size();

            App.get().setPathSegment(length);
            //App.get().setCurrentSegment(0);

            for (int i = 0; i < length; i++) {
                if (i + 1 < length) segments.get(i).setNext(segments.get(i + 1));
            }
            // should be triggered from elsewhere
            startAnimation();
        }
    };

    /*private void getTextDir(final PathPerFloor[] pointArray) {
        new Thread(new Runnable() {
            @Override
            public void run() {
               // directionList.clear();
                String instructions = "";

                TextDirectionOptions options=new TextDirectionOptions();
                options.filter=true;
                options.pointArray=pointArray;
                options.uTurnMeters=30;
                options.joinForwardInstructionWithinMeters=50;

                Log.e("MNAGER Text Direction",MSingleton.getI().getToWaypoint().id + ":::" + MSingleton.getI().getFromWaypoint().id);
                ArrayList<ArrayList<TDInstruction>> textDirectionInstruction = MSingleton.getI()
                        .getTextDirectionInstruction(options);

                Log.e("MNAGER Direction", textDirectionInstruction + "::::::");
                if (textDirectionInstruction == null) {
                    instructions = "No text direction available ";
                } else {
                    for (int i = 0; i < textDirectionInstruction.size(); i++) {
                        TDInstruction[] textDirectionInstructions = textDirectionInstruction.get(i).toArray(new TDInstruction[textDirectionInstruction.size()]);
                        for (TDInstruction tdInstruction : textDirectionInstructions) {
                            if (tdInstruction != null) {
                                if (!tdInstruction.output.isEmpty()) {
                                    int distance_calc = 0;
                                    String distance = "";
                                    distance_calc = (int) Math.floor(tdInstruction.distanceToNextMeters);
                                    distance = Integer.toString(distance_calc);
                                    if (!distance.equals("0")) {
                                        //directionList.add( tdInstruction.output + " for " + distance + " meters ");
                                        instructions += "<font color=\"red\"> <b>&#8226;</b> </font> " + tdInstruction.output + " for " + distance + " meters \n" + "<br /> <br /> ";
                                    } else {
                                        //directionList.add(tdInstruction.output);
                                        instructions += "<font color=\"red\"> <b>&#8226;</b> </font>" + tdInstruction.output + "\n" + "<br /> <br /> ";
                                    }
                                }
                            }
                        }
                    }
                }
                Log.e("TEXT INSTRUCTION",instructions);
            }
        }).start();
    }*/


    private static class Segment implements Runnable {
        private M m;
        private MapFull mapFull;
        private Segment next;
        private Handler handler;
        private Context context;
        private BroadcastReceiver br;

        public Segment(M m, MapFull mapFull, Handler handler, Context context) {
            this.m = m;
            this.mapFull = mapFull;
            this.handler = handler;
            this.context = context;
        }

        public void unregister() {
            if(br != null)
                LocalBroadcastManager.getInstance(context).unregisterReceiver(br);
        }

        @Override
        public synchronized void run() {
            try
            {
                Log.e(TAG, "set level " + mapFull.map.mapId);
                for (int l = 0; l < MSingleton.getI().venueData.maps.length; l++) {
                    if (m.venueData.maps[l].map.mapId == mapFull.map.mapId) {
                        m.setLevel(MSingleton.getI().venueData.maps[l]);
                        m.setCurrentMapIndex(l);
                        Constant.LAST_SELECTED_MAP_INDEX = l;
                        m.camera.zoomTo(m.route.getBBox(), 150);
                    }
                }
                //storeLocatorFragment.updateLevelState();
                //unregister();
                br = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        Log.i(TAG, "path_complete " + this.toString());
                        unregister();
                        if (next != null) {
                            handler.postDelayed(next, 300);
                        }else{
                            Log.e(TAG, "path_complete " + this.toString());
                        }
                    }
                };
                LocalBroadcastManager.getInstance(context).registerReceiver(br, new IntentFilter(PathCustomAnimatable.PATH_COMPLETE));
            }
            catch (Exception ex)
            {
                Log.e(TAG, "Excetoptions " + ex);
            }
        }
        public synchronized void setNext(Segment next) {
            this.next = next;
        }
    }
}
