package com.belongi.citycenter.jibe.overlays;

import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Destination;

/**
 * Created by webwerks on 16/11/16.
 */
public interface MapActionListener {

     void onCloseClick();

     //void plotRoute(Destination toDestination, Destination fromDestination);
     void plotRoute(String toDestinationId,String fromDestinationId);

     void setFromPoint(Destination fromPoint);

     void changeLevel(String level);

}
