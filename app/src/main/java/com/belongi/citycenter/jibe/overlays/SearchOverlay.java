package com.belongi.citycenter.jibe.overlays;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.jibe.MSingleton;
import com.google.android.gms.analytics.HitBuilders;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.main.VenueData;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Category;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Destination;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.MapFull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by webwerks on 9/11/16.
 */

public class SearchOverlay extends FrameLayout implements TextWatcher,AdapterView.OnItemClickListener{

    Context mContext;
    ListView lstStore;
    SearchResultAdapter searchResultsAdapter;
    ArrayList<Search> stores;
    String selectedCat;
    Spinner spnrCategories;
    EditText edtSearch;
    List<Category> lstCategory;
    CategoryAdapter adapter;
    MapActionListener mListener;

    public SearchOverlay(Context context,MapActionListener listener) {
        super(context);

        mContext=context;
        mListener=listener;

        View view= LayoutInflater.from(context).inflate(R.layout.search_overlay,null);
        addView(view);

        spnrCategories= (Spinner) view.findViewById(R.id.spnrCategories);
        lstStore= (ListView) view.findViewById(R.id.lstStores);
        edtSearch= (EditText) view.findViewById(R.id.edtSearch);
        edtSearch.addTextChangedListener(this);

        /*edtSearch.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                search("");
            }
        });*/

        searchResultsAdapter=new SearchResultAdapter(context,new ArrayList<Search>());
        lstStore.setAdapter(searchResultsAdapter);
        lstStore.setOnItemClickListener(this);
        final VenueData venueData = MSingleton.getI().venueData;

        stores = new ArrayList<Search>();

        Category cat=new Category();
        cat.text="Search by Category";

        lstCategory= new ArrayList<>();
        lstCategory.add(0, cat);

        spnrCategories.setAdapter(adapter = new CategoryAdapter(mContext, lstCategory));
        spnrCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCat = ((Category) adapterView.getSelectedItem()).text;
                sendCatSelectedEvent(selectedCat);
                //if (!TextUtils.isEmpty(edtSearch.getText()))
                search(edtSearch.getText().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        new Thread(){
            @Override
            public void run() {
                super.run();
                for (int i = 0; i < venueData.destinations.length; i++) {
                    Search search = new Search();
                    search.setLabel("" + Html.fromHtml(venueData.destinations[i].name));

                    search.setId(venueData.destinations[i].id +" ");
                    search.setKeyword(venueData.destinations[i].keywords);
                    M m = MSingleton.getI();
                    MapFull mapFull = m.getMapFullByDestinationId(venueData.destinations[i].id);

                    search.setLevel(getShortFormLevel(mapFull.map.floorSequence - 1));
                    search.setFloor(mapFull.map.floorSequence - 1);
                    search.setCategory(getCategories(venueData.destinations[i].category));

                    stores.add(search);
                }

                /*for(Category cat:venueData.categories){
                    Log.e("Category",cat.name + " ::" + cat.id);
                }*/

                if(venueData!=null){
                    Category[] arrCategory=venueData.categories;
                    lstCategory.addAll(Arrays.asList(arrCategory));
                    handler.sendEmptyMessage(0);
                }
            }
        }.start();

        view.findViewById(R.id.ll_root).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId()==R.id.ll_root){
                }
            }
        });
    }

    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    adapter.notifyDataSetChanged();
                    search("");
                    break;
            }
        }
    };

    private String getShortFormLevel(int level) {
        String label = "";
        if (level == 0) {
            label = "GF";
        } else if (level < 0) {
            label = "L" + (MSingleton.getI().venueData.maps.length - 1);
        } else
            label = "L" + (level);
        return label;
    }

    public String getCategories(String[] strings) {
        String s = " ";
        for (int i = 0; i < strings.length; i++) {
            if (i == strings.length - 1) {
                s = s + strings[i];
            } else {
                s = s + strings[i] + ", ";
            }
        }
        return s.toUpperCase();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    List<Search> searchResult=new ArrayList<>();

    @Override
    public void onTextChanged(final CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            //perform search
            search(s.toString());
        } else {
            //clear the adapter
            searchResultsAdapter.clear();
            searchResultsAdapter.notifyDataSetChanged();
        }
    }

    public void pushSearchEvent(String search){
        //dataLayer.pushEvent("SearchStore", DataLayer.mapOf("serach_keyword", search));
        //DataLayer dataLayer = TagManager.getInstance(mContext).getDataLayer();
        //dataLayer.push(DataLayer.mapOf("Event", "search", "JMAP(Search Store)", search));
        //dataLayer.pushEvent("search",DataLayer.mapOf("JMAP(Search Store)", search));
        App.get().getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Store Locator")
                .setAction("Search store")
                .setLabel(search)
                .build());
    }

    public void sendCatSelectedEvent(String cat){
        App.get().getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Store Locator")
                .setAction("Category")
                .setLabel(cat)
                .build());
    }

    public void showSoftKeyboard(){
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtSearch , InputMethodManager.SHOW_IMPLICIT);
    }

    public void search(String keyword){
        searchResult.clear();

        try{
       /* if(TextUtils.isEmpty(keyword)){
            lstStore.setVisibility(VISIBLE);
            searchResultsAdapter.clear();
            searchResultsAdapter.addAll(stores);
            searchResultsAdapter.notifyDataSetChanged();
        }else{*/
            for(Search search:stores){
            /*if(search.getLabel().toLowerCase().startsWith(keyword.toLowerCase()) && shopInCategory(selectedCat,search)){
                searchResult.add(search);
            }*/

                if((search.getLabel().toLowerCase().contains(keyword.toLowerCase())
                        || search.getKeyword().toLowerCase().contains(keyword.toLowerCase()))
                        && shopInCategory(selectedCat,search) ){
                    searchResult.add(search);
                }
            }

            if(searchResult.size()>0) {
                lstStore.setVisibility(VISIBLE);
                searchResultsAdapter.clear();
                searchResultsAdapter.addAll(searchResult);
                searchResultsAdapter.notifyDataSetChanged();
            }else{
                searchResultsAdapter.clear();
                searchResultsAdapter.notifyDataSetChanged();
                lstStore.setVisibility(GONE);
                Toast.makeText(mContext,"No Result Found",Toast.LENGTH_SHORT).show();
            }
            // }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean shopInCategory(String selectedCat,Search store){
        if(selectedCat.equalsIgnoreCase("Search by Category")){
            return true;
        }else {
            String[] arrCat = store.getCategory().split(",");
            for (String str : arrCat) {
                if (str.toLowerCase().trim().equals(selectedCat.toLowerCase().trim())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void afterTextChanged(Editable s) {

        if(TextUtils.isEmpty(s.toString().trim())){
            search("");
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        int id=Integer.parseInt(((Search) adapterView.getItemAtPosition(i)).getIdentifier().trim());
        Destination destination=MSingleton.getI().getDestinationByID(id);
        if (destination == null) return;
        mListener.setFromPoint(destination);

        removeAllViews();
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        List<Search> searchList=new ArrayList<>();
        searchList.add(((Search)adapterView.getItemAtPosition(i)));

        addView(new MapStoreDetailOverlay(mContext,searchList,mListener));
        pushSearchEvent(((Search)adapterView.getItemAtPosition(i)).getLabel());
    }

    public class CategoryAdapter extends ArrayAdapter<Category>{
        Context mContext;
        public CategoryAdapter(Context context, List<Category> objects) {
            super(context, 0, objects);
            mContext=context;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                convertView=LayoutInflater.from(mContext).inflate(android.R.layout.simple_spinner_dropdown_item,null);
            }
            TextView txt= (TextView) convertView.findViewById(android.R.id.text1);
            txt.setTextSize(14);
            txt.setTextColor(Color.parseColor("#FFFFFF"));
            txt.setPadding(10,20,10,20);
            txt.setText(Html.fromHtml(getItem(position).text.replace("&AMP;","&amp;")));
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            convertView=LayoutInflater.from(mContext).inflate(android.R.layout.simple_spinner_dropdown_item,null);
            TextView txt= (TextView) convertView.findViewById(android.R.id.text1);
            txt.setTextSize(14);
            txt.setTextColor(Color.parseColor("#60504D"));
            txt.setText(Html.fromHtml(getItem(position).text.replace("&AMP;","&amp;")));
            txt.setPadding(10,20,10,20);
            return convertView;
        }
    }

    public class SearchResultAdapter extends ArrayAdapter<Search>{
        Context mContext;
        public SearchResultAdapter(Context context, List<Search> objects) {
            super(context, 0, objects);
            mContext=context;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                convertView=LayoutInflater.from(mContext).inflate(R.layout.row_map_stores,null);
            }
            TextView txt= (TextView) convertView.findViewById(R.id.txtCategory);
            txt.setText(getItem(position).getLabel());

            TextView mtextView = (TextView) convertView.findViewById(R.id.lblLevel);
            mtextView.setText(getItem(position).getLevel());

            return convertView;
        }
    }
}
