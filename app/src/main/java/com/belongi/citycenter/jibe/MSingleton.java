package com.belongi.citycenter.jibe;

import android.content.Context;
import android.util.Log;

import com.jibestream.jibestreamandroidlibrary.main.EngineView;
import com.jibestream.jibestreamandroidlibrary.main.M;

/**
 * Created by webwerks on 26/5/16.
 */
    public class MSingleton {
    private static final String TAG = "MSingleton";
    private static Context context;
    private static M m;

    public static void initialize(Context ctx) {
        context = ctx.getApplicationContext();
        m = new M(context);
        //camera max zoom in amount
        m.camera.setZoomInMax(12f);
        // define custom classes
        EngineView.DPI = EngineView.DPI_560;
        m.classLib.routeClass = CustomRoute.class;
        m.classLib.unitClass = SelectedUnits.class;
        m.classLib.amenityClass = AmenityCustomScale.class;
        m.classLib.unitLabelClass = UnitLabelsSameSize.class;
        m.classLib.backgroundClass= MapBackgroundTouch.class;

    }

    public static M getI() {
        /*if (m == null) {

            Log.e("CREATE M","M");


        }*/
        return m;
    }

    public static void NullifyMInstance()
    {
        if(m != null) {
            m.onDestroy();
            m = null;
        }
    }
}
