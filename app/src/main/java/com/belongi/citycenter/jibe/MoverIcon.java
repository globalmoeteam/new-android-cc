package com.belongi.citycenter.jibe;

import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.jibestream.jibestreamandroidlibrary.elements.ElementIcon;
import com.jibestream.jibestreamandroidlibrary.main.Camera;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyleIcon;

/**
 * Created by jibestream on 2016-06-20.
 */
public class MoverIcon extends ElementIcon {
    public MoverIcon() {
        super();
        setHeadsUp(true);
        //  getTransform().setScale(0.5f);
        RenderStyleIcon renderStyleIcon = new RenderStyleIcon();
        renderStyleIcon.renderStyleBG = new RenderStyle(Paint.Style.FILL);
        renderStyleIcon.renderStyleMG = new RenderStyle(Paint.Style.FILL);
        renderStyleIcon.renderStyleFG = new RenderStyle(Paint.Style.FILL);
        renderStyleIcon.renderStyleBG.paintFill.setColor(Color.BLACK);
        renderStyleIcon.renderStyleMG.paintFill.setColor(Color.WHITE);
        renderStyleIcon.renderStyleFG.paintFill.setColor(Color.WHITE);
        //renderStyleIcon.renderStyleBG.paintFill.setAlpha(125);
        setStyleIdle(renderStyleIcon);
    }

    private final float[] matrixArray = new float[9];

    @Override
    public void onPreRender(M m, long timeElapsed, long timeTotal, int fps, Camera camera) {
        super.onPreRender(m, timeElapsed, timeTotal, fps, camera);
        if (getDirtyTransform() || camera.isTransformationDirty()) {
            float cameraScale = camera.getScale();
            float cameraRec = 1f / cameraScale;
            float zoomThreshold = 2f;
            if (cameraRec > zoomThreshold) {
                final Matrix transformation = getTransformation();
                transformation.getValues(matrixArray);
                float x = matrixArray[Matrix.MTRANS_X];
                float y = matrixArray[Matrix.MTRANS_Y];
                transformation.postScale(cameraScale, cameraScale, x, y);
                transformation.postScale(zoomThreshold, zoomThreshold, x, y);
            }
        }
    }
}
