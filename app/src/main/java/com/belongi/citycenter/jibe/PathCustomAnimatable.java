package com.belongi.citycenter.jibe;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.support.v4.content.LocalBroadcastManager;

import com.belongi.citycenter.global.App;
import com.jibestream.jibestreamandroidlibrary.shapes.JPath;

/**
 * Created by jibestream on 2016-06-19.
 */
// todo use a time stamp and control opacity of dashes to ease them in
public class PathCustomAnimatable extends JPath {
    public static String PATH_COMPLETE = "PATH-COMPLETE";
    //
    private volatile Path path = new Path();
    // dictates the width of the path
    private float pathLength;
    private PathMeasure pathMeasure = new PathMeasure();
    // for broadcasting
    private Context context;
    // run once flag
    private volatile boolean isComplete = false;
    // paint when path animation is complete in order to do a simple drawPath call
    public Paint paintFirst;
    public Paint paintDashes;
    float i;
    float ii;

    public PathCustomAnimatable(Context context) {
        this.context = context;
        paintFirst = new Paint();
        paintFirst.setStyle(Paint.Style.STROKE);
        paintFirst.setStrokeWidth(4f);
        paintFirst.setStrokeJoin(Paint.Join.ROUND);
        paintFirst.setColor(Color.RED);
        paintDashes = new Paint();
        paintDashes.setStyle(Paint.Style.STROKE);
        paintDashes.setStrokeWidth(3f);
        paintDashes.setPathEffect(new DashPathEffect(new float[]{5, 5}, 0));
        paintDashes.setColor(Color.parseColor("#800000")/*Color.WHITE*/);
    }

    private void calc() {
        pathMeasure.setPath(path, false);
        pathLength = pathMeasure.getLength();
        final RectF bbox = new RectF();
        path.computeBounds(bbox, true);
        setBBox(bbox);
        i = pathLength;
        ii = pathLength;
    }

    @Override
    public void setPath(Path path) {
        super.setPath(path);
        if (path == null) return;
        this.path = new Path(path);
        isComplete = false;
        calc();
        paintFirst.setColor(Color.RED);
    }

    @Override
    public void onDraw(Canvas canvas, Paint paint) {
        if (i >= 0) {
            i--;
        } else {
            i = pathLength;
        }
        if (ii <= 0) {
            ii = 0;
        } else {
            ii -= 5f;
        }
        ii += 2f;
        paintFirst.setPathEffect(new DashPathEffect(new float[]{pathLength, pathLength}, ii));
        canvas.drawPath(path, paintFirst);

        if (isComplete) {
            //Log.e("PATH COM",App.get().getCurrentSegment() + "::::" + App.get().getPathSegment());
            if(App.get().getCurrentSegment() == App.get().getPathSegment()){
                paintFirst.setColor(Color.parseColor("#FFB90F"));
            }else{
                paintFirst.setColor(Color.RED);
            }
            paintDashes.setPathEffect(new DashPathEffect(new float[]{5, 5}, i));
            canvas.drawPath(path, paintDashes);
        }
        // checks 1px i assume
        if (ii < 1f && !isComplete) {
            paintFirst.setColor(Color.RED);
            App.get().setCurrentSegment(App.get().getCurrentSegment()+1);
            // Broadcast a message to catch and set the next level
            final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(context.getApplicationContext());
            lbm.sendBroadcast(new Intent(PATH_COMPLETE));
            isComplete = true;
        }
    }
}
