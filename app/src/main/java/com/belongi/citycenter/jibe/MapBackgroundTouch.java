package com.belongi.citycenter.jibe;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.jibestream.jibestreamandroidlibrary.elements.Background;

/**
 * Created by webwerks on 5/12/16.
 */

public class MapBackgroundTouch extends Background {

    public MapBackgroundTouch(){
        setSelectable(true);
    }

    @Override
    public void onRender(Canvas canvas, Paint touchPaint) {
        super.onRender(canvas, touchPaint);

        if(touchPaint!=null){
            canvas.drawColor(touchPaint.getColor());
        }else{
            canvas.drawColor(getStyleIdle().paintFill.getColor());
        }
    }
}
