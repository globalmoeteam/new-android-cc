package com.belongi.citycenter.jibe;

/**
 * Created by webwerks on 8/6/16.
 */
public class DrawerMenuOject {
    String title;
    String url;
    String levelShort;
    int id;
    String iconSvgPath;
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconSvgPath() {
        return iconSvgPath;
    }

    public void setIconSvgPath(String iconSvgPath) {
        this.iconSvgPath = iconSvgPath;
    }

    int activeImgId;

    public int getActiveImgId() {
        return activeImgId;
    }

    public void setActiveImgId(int activeImgId) {
        this.activeImgId = activeImgId;
    }

    int inActiveImgId;

    public int getInActiveImgId() {
        return inActiveImgId;
    }

    public void setInActiveImgId(int inActiveImgId) {
        this.inActiveImgId = inActiveImgId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevelShort()
    {
        return levelShort;
    }

    public void setLevelShort( String levelShort )
    {
        this.levelShort = levelShort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;
    boolean isUnderLineVisible;
    boolean isSelected;
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public boolean isUnderLineVisible() {
        return isUnderLineVisible;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUnderLineVisible(boolean underLineVisible) {
        isUnderLineVisible = underLineVisible;
    }
}
