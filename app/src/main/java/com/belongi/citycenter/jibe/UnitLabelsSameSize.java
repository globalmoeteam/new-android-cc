package com.belongi.citycenter.jibe;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;

import com.belongi.citycenter.global.App;
import com.jibestream.jibestreamandroidlibrary.elements.UnitLabel;
import com.jibestream.jibestreamandroidlibrary.main.Camera;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.utils.ColorsMaterialDesign;

/**
 * Created by emmanuel on 2015-10-08.
 */
public class UnitLabelsSameSize extends UnitLabel {
    private Rect bounds = new Rect();
    private float textOffsetY;
    private float textWidth;
    private float textHeight;
    private final Paint paint;
    private volatile boolean show = false;

    public UnitLabelsSameSize() {
        setConstantScale(true);
        setHeadsUpFlip(false);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        Typeface typeface = Typeface.createFromAsset(App.get().getAssets(), "GothamMedium.ttf");
        paint.setTypeface(typeface);
        paint.setTextSize(25);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setAntiAlias(true);
        paint.setColor(ColorsMaterialDesign.GREY9);
        calc();
    }

    @Override
    public void setText(String s) {
        super.setText(s);
        calc();
    }

    private void calc() {
        final String text = getText();
        if (text == null) return;
        paint.getTextBounds(text, 0, text.length(), bounds);
        final RectF bBox = new RectF(bounds);
        bBox.offset(-bounds.centerX(), 0);
        textWidth = bounds.width();
        textHeight = bounds.height();
        textOffsetY = bounds.height() * 0.5f;
    }

    private boolean testDesiredVisibility(float cameraZoom) {
        float calculatedWidth = width * cameraZoom;
        float calculatedHeight = height * cameraZoom;
        if (calculatedWidth > textWidth && calculatedHeight > textHeight) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onUpdate(M m, long timeElapsed, long timeTotal, int fps, Camera camera) {
        super.onUpdate(m, timeElapsed, timeTotal, fps, camera);
        if (textString == null || textString.isEmpty()) {
            show = false;
            return;
        }
        if (testDesiredVisibility(camera.getZoom())) {
            show = false;
        } else {
            show = true;
        }
    }

    @Override
    public void onRender(Canvas canvas, Paint touchPaint) {
        if (!show) return;
        canvas.drawText(textString, 0, textOffsetY, paint);
    }

}
