package com.belongi.citycenter.jibe.overlays;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.jibe.DrawerMenuOject;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ViewTarget;
import com.google.android.gms.analytics.HitBuilders;
import com.jibestream.jibestreamandroidlibrary.elements.Amenity;
import com.jibestream.jibestreamandroidlibrary.elements.Element;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyleIcon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 15/11/16.
 */
public class AmenitiesOverlay extends FrameLayout implements View.OnClickListener, OnItemClickListerner {

    Context mContext;
    RecyclerView lstAmenities;
    ArrayList<DrawerMenuOject> amenitiesList;
    StoreLocatorNewFragment stroreFrag;
    AmenitiesAdapter mAdapter;
    MapActionListener mListener;
    String selectedAmenities;
    Button btnPrev, btnNext;

    public AmenitiesOverlay(Context context, ArrayList<DrawerMenuOject> amenitiesList, StoreLocatorNewFragment frag, MapActionListener listener) {
        super(context);
        mContext = context;
        this.amenitiesList = amenitiesList;
        stroreFrag = frag;
        mListener = listener;

        /*for (int i=0;i<amenitiesList.size();i++){
            String inActiveIconName="ic_"+amenitiesList.get(i).getTitle().replaceAll(" ", "_").replaceAll("&_","").replaceAll("'", "").toLowerCase();
            String activeIconName=inActiveIconName+"_active";
            int inActiveIconID=getResources().getIdentifier(inActiveIconName,"drawable",mContext.getPackageName());
            int activeIconID=getResources().getIdentifier(activeIconName,"drawable",mContext.getPackageName());

            Log.e("Amenities",amenitiesList.get(i).getUrl());

            amenitiesList.get(i).setActiveImgId(activeIconID);
            amenitiesList.get(i).setInActiveImgId(inActiveIconID);
        }
*/
        Element[] amenities = MSingleton.getI().getElementsByType(Amenity.class);
        for (int i = 0; i < amenities.length; i++) {
            Amenity amenity = (Amenity) amenities[i];
            for (int j = 0; j < amenitiesList.size(); j++) {
                if (amenitiesList.get(j).getTitle().equalsIgnoreCase(amenity.amenityComponent.bean.localizedText)) {
                    amenitiesList.get(j).setIconSvgPath(amenity.amenityComponent.bean.filePathSVG);
                    amenitiesList.get(j).setUrl(Constant.URL_JMAP +
                            amenity.amenityComponent.bean.iconImagePath);
                    break;
                }
            }
        }

        View view = LayoutInflater.from(context).inflate(R.layout.layout_amenities_overlay, null);
        addView(view);

        lstAmenities = (RecyclerView) view.findViewById(R.id.list_amenities);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        lstAmenities.setLayoutManager(horizontalLayoutManagaer);
        lstAmenities.setAdapter(mAdapter = new AmenitiesAdapter(amenitiesList, this));

        btnPrev = (Button) view.findViewById(R.id.btnPrev);
        btnPrev.setOnClickListener(this);
        btnNext = (Button) view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        lstAmenities.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int count = recyclerView.getAdapter().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) lstAmenities.getLayoutManager())
                        .findLastVisibleItemPosition();
                int firstVisibleItem = ((LinearLayoutManager) lstAmenities.getLayoutManager())
                        .findFirstVisibleItemPosition();

                if(lastVisibleItem == (count-1) && firstVisibleItem == 0) {
                    btnPrev.setVisibility(GONE);
                    btnNext.setVisibility(GONE);
                } else {

                    if (lastVisibleItem == (count - 1)) {
                        btnNext.setVisibility(GONE);
                        btnPrev.setVisibility(VISIBLE);
                        //Log.e("ScrollListener", "PREV VISIBLE " + lastVisibleItem + " FirstVisibleItem " + firstVisibleItem);
                    } else if (firstVisibleItem == 0) {
                        btnPrev.setVisibility(GONE);
                        btnNext.setVisibility(VISIBLE);
                        //Log.e("ScrollListener", "NEXT VISIBLE " + lastVisibleItem + " FirstVisibleItem " + firstVisibleItem);
                    } else {
                        btnPrev.setVisibility(VISIBLE);
                        btnNext.setVisibility(VISIBLE);
                        //Log.e("ScrollListener", "BOTH VISIBLE " + lastVisibleItem + " FirstVisibleItem " + firstVisibleItem);

                    }
                }
            }
        });

        Runnable fitsOnScreen = new Runnable() {
            @Override
            public void run() {
                int count = lstAmenities.getAdapter().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) lstAmenities.getLayoutManager())
                        .findLastVisibleItemPosition();
                if(lastVisibleItem == (count - 1) && lstAmenities.getChildAt(lastVisibleItem).getRight() <= lstAmenities.getWidth()) {
                    // It fits!
                    btnPrev.setVisibility(GONE);
                    btnNext.setVisibility(GONE);
                    /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.addRule(RelativeLayout.CENTER_IN_PARENT);
                    lstAmenities.setLayoutParams(params);
                    lstAmenities.requestLayout();*/
                } else {
                    // It doesn't fit...
                    btnPrev.setVisibility(VISIBLE);
                    btnNext.setVisibility(VISIBLE);
                }

            }
        };
        lstAmenities.post(fitsOnScreen);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPrev:
                lstAmenities.smoothScrollToPosition(0);
                break;

            case R.id.btnNext:
                lstAmenities.smoothScrollToPosition(amenitiesList.size() - 1);
                break;
        }
    }

    public void sendAmenityEvent(String amenity){
        App.get().getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Store Locator")
                .setAction("Amenity")
                .setLabel(amenity)
                .build());
    }

    @Override
    public void onItemClickListener(int position, DrawerMenuOject object) {

        try {
            final DrawerMenuOject drawerMenuOject = object;
            if (drawerMenuOject.getType() != null) {
                if (drawerMenuOject.getType().equalsIgnoreCase("amenities")) {

                    selectedAmenities = drawerMenuOject.getTitle();
                    M m = MSingleton.getI();
                    m.camera.zoomTo(m.defaultFramings[m.getCurrentMapIndex()]);
                    m.camera.setRoll(0);

                    final int realJ = position;
                    if (realJ >= 0 && realJ < MSingleton.getI().getAmenitiesVisibility().length)
                        if (MSingleton.getI().getAmenitiesVisibility()[realJ]) {
                            resetAmenities();
                            MSingleton.getI().setAmenityVisibility(realJ, false);
                            changeImg(position, false);
                        } else {
                            sendAmenityEvent(amenitiesList.get(realJ).getTitle());
                            resetAmenities();
                            MSingleton.getI().setAmenityVisibility(realJ, true);
                            changeImg(position, true);

                            new Handler().postDelayed(new Runnable() {
                                                          @Override
                                                          public void run() {
                                                              stroreFrag.amenitiesAvailabilityLevels(realJ, drawerMenuOject.getTitle());
                                                          }
                                                      },
                                    500);

                            RectF amenitiesRectF = new RectF();
                            Element[] amenities = m.getElementsByType(Amenity.class);
                            for (int i = 0; i < amenities.length; i++) {
                                Amenity amenity = (Amenity) amenities[i];
                                if (amenity.amenityComponent.bean.localizedText.equalsIgnoreCase(drawerMenuOject.getTitle())) {
                                    //amenity.setVisible(true);
                                    RenderStyle styleBG = new RenderStyle(Paint.Style.FILL);
                                    styleBG.setFillColor(ContextCompat.getColor(mContext, R.color.aminities_select));

                                    RenderStyle styleFG = new RenderStyle(Paint.Style.FILL);
                                    styleFG.setFillColor(ContextCompat.getColor(mContext, R.color.white));

                                    RenderStyleIcon renderStyleIcon = new RenderStyleIcon();
                                    renderStyleIcon.renderStyleBG = styleBG;
                                    renderStyleIcon.renderStyleFG = styleFG;
                                    amenity.getTransform().setScale(1.3f);
                                    amenity.setStyleIdle(renderStyleIcon);

                                    if (amenity.getLevel() == MSingleton.getI().getCurrentMapIndex()) {
                                        amenitiesRectF.union(amenity.getBBox());
                                    }
                                    m.camera.zoomTo(amenitiesRectF, 50);
                                }
                            }
                        }
                }
            }
        }catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        /*if(mListener!=null){
            mListener.onCloseClick();
        }*/
    }

    public String getSelectedAmenities() {
        return selectedAmenities;
    }

    public void changeImg(final int pos, boolean value) {
        for (int i = 0; i < amenitiesList.size(); i++) {
            amenitiesList.get(i).setSelected(false);
        }
        amenitiesList.get(pos).setSelected(value);
        mAdapter.notifyDataSetChanged();
    }

    public void resetAmenities() {
        for (int l = 0; l < MSingleton.getI().getAmenitiesVisibility().length; l++) {
            MSingleton.getI().setAmenityVisibility(l, false);
        }

        Element[] amenList = MSingleton.getI().getElementsByType(Amenity.class);
        for (int i = 0; i < amenList.length; i++) {
            Amenity amenity = (Amenity) amenList[i];

            if(amenity.amenityComponent.bean.description.contains("Parking")) {
                MSingleton.getI().setAmenityVisibility(i, true);
                MSingleton.getI().setAmenityVisibility(amenity,true);
                amenity.setVisible(true);
            }
        }
    }

    public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.AmenitiesViewHolder> {

        private List<DrawerMenuOject> amenitiesList;
        OnItemClickListerner mListener;
        private static final int CLICK_ON_WEBVIEW = 1;

        public Drawable covertBitmapToDrawable(Context context, Bitmap bitmap) {
            Drawable d = new BitmapDrawable(context.getResources(), bitmap);
            return d;
        }

        public Bitmap convertDrawableToBitmap(Drawable drawable) {
            if (drawable instanceof BitmapDrawable) {
                return ((BitmapDrawable) drawable).getBitmap();
            }

            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                    drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);

            return bitmap;
        }



        public AmenitiesAdapter(List<DrawerMenuOject> drawerMenuObjects, OnItemClickListerner listener) {
            amenitiesList = drawerMenuObjects;
            mListener = listener;
        }

        @Override
        public AmenitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_amenities, parent, false);
            return new AmenitiesViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final AmenitiesViewHolder holder, int position) {
            final DrawerMenuOject drawerMenuOject = amenitiesList.get(position);
            final int pos = position;

            //holder.wvAmenities.setTag(drawerMenuOject);
            holder.flRoot.setTag(drawerMenuOject);

            if (drawerMenuOject.getType().equalsIgnoreCase("amenities")) {

                /*holder.wvAmenities.getSettings().setJavaScriptEnabled(true);
                holder.wvAmenities.setBackgroundColor(0x00000000);*/

                String loadData = "";
                if (!TextUtils.isEmpty(amenitiesList.get(pos).getUrl())) {
                    if (amenitiesList.get(position).isSelected()) {
                        /*loadData = amenitiesList.get(pos).getIconSvgPath()
                                .replace(".Background{fill:#010101;}", ".Background{fill:#D13D56;}")
                                .replace(".st0{fill:#3882C5;}", ".st0{fill:#D13D56;}")
                                .replace(".st0{fill:#3CAF6A;}", ".st0{fill:#D13D56;}")
                                .replace(".st0{fill:#E76733;}", ".st0{fill:#D13D56;}")
                                .replace(".st0{fill:#E03445;}", ".st0{fill:#D13D56;}");*/
                        //holder.imgAmenities.setImageResource(amenitiesList.get(pos).getActiveImgId());
                        Glide.with(mContext).load(amenitiesList.get(pos).getUrl())
                                .into(new ViewTarget<ImageView, GlideDrawable>(holder.imgAmenities) {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    @Override
                                    public void onResourceReady(GlideDrawable resource,
                                                                GlideAnimation<? super GlideDrawable> glideAnimation) {
                                        GlideDrawable d=resource;
                                        d.setColorFilter(ContextCompat.getColor(mContext,R.color.aminities_select), PorterDuff.Mode.ADD);

                                        Bitmap b=getCroppedBitmap(convertDrawableToBitmap(resource));
                                        (holder.imgAmenities).setImageDrawable(covertBitmapToDrawable(mContext,b));
                                    }
                                });

                    } else {
                        /*loadData = amenitiesList.get(pos).getIconSvgPath()
                                .replace(".Background{fill:#010101;}", ".Background{fill:#598CAB;}")
                                .replace(".st0{fill:#3882C5;}", ".st0{fill:#598CAB;}")
                                .replace(".st0{fill:#3CAF6A;}", ".st0{fill:#598CAB;}")
                                .replace(".st0{fill:#E76733;}", ".st0{fill:#598CAB;}")
                                .replace(".st0{fill:#E03445;}", ".st0{fill:#598CAB;}");*/
                        //holder.imgAmenities.setImageResource(amenitiesList.get(pos).getInActiveImgId());
                        Glide.with(mContext).load(amenitiesList.get(pos).getUrl())
                                .into(new ViewTarget<ImageView, GlideDrawable>(holder.imgAmenities) {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    @Override
                                    public void onResourceReady(GlideDrawable resource,
                                                                GlideAnimation<? super GlideDrawable> glideAnimation) {
                                        GlideDrawable d=resource;
                                        d.setColorFilter(ContextCompat.getColor(mContext,R.color.aminities_non_select), PorterDuff.Mode.ADD);

                                        Bitmap b=getCroppedBitmap(convertDrawableToBitmap(resource));
                                        (holder.imgAmenities).setImageDrawable(covertBitmapToDrawable(mContext,b));

                                    }
                                });
                    }
                }

                Log.e("AMENITY",amenitiesList.get(pos).getDescription()+ "::::" +amenitiesList.get(pos).getTitle());

                switch (App.get().getSelectedMall().getIdentifier()){
                    case DEIRA:
                    case MIRDIF:
                        if((amenitiesList.get(pos).getDescription().contains("Parking") &&
                                !amenitiesList.get(pos).getDescription().contains("Validation")) ||
                                (amenitiesList.get(pos).getDescription().contains("Elevator") ||
                                amenitiesList.get(pos).getDescription().contains("Escalator"))
                                ){
                            holder.flRoot.setLayoutParams(new LinearLayout.LayoutParams(1,1));
                            holder.flRoot.setVisibility(GONE);
                        }else{
                            holder.flRoot.setLayoutParams(new LinearLayout.LayoutParams(dpToPx(50), dpToPx(50)));
                            holder.flRoot.setVisibility(VISIBLE);
                        }
                        break;

                    default:
                        if(amenitiesList.get(pos).getTitle().contains("Parking Validation") ||
                                amenitiesList.get(pos).getDescription().contains("Parking Validation") ||
                                amenitiesList.get(pos).getDescription().contains("Parking") ||
                                amenitiesList.get(pos).getTitle().contains("Parking")){
                            holder.flRoot.setLayoutParams(new LinearLayout.LayoutParams(1,1));
                            holder.flRoot.setVisibility(GONE);
                        }else{
                            holder.flRoot.setLayoutParams(new LinearLayout.LayoutParams(dpToPx(50), dpToPx(50)));
                            holder.flRoot.setVisibility(VISIBLE);
                        }
                        break;
                }

                //holder.wvAmenities.loadDataWithBaseURL(null, loadData, "text/html", "utf-8", null);
                /*if(*//*amenitiesList.get(pos).getDescription().contains("Entrance") ||
                        amenitiesList.get(pos).getDescription().contains("Information") ||
                        amenitiesList.get(pos).getDescription().contains("Toilets") ||
                        amenitiesList.get(pos).getDescription().contains("Prayer") ||
                        amenitiesList.get(pos).getDescription().contains("Metro") ||
                        amenitiesList.get(pos).getDescription().contains("Parking") ||
                        amenitiesList.get(pos).getDescription().contains("Telephone")*//*
                        (App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.DEIRA) ||
                        App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.MIRDIF)) &&
                                amenitiesList.get(pos).getDescription().contains("Parking Validation")) {
                    // holder.imgAmenities.setVisibility(GONE);
                    holder.flRoot.setLayoutParams(new LinearLayout.LayoutParams(1,1));
                    *//*holder.wvAmenities.setVisibility(GONE);
                    holder.llRoot.setVisibility(GONE);*//*
                    holder.flRoot.setVisibility(GONE);
                }else{
                    //holder.imgAmenities.setVisibility(VISIBLE);
                    holder.flRoot.setLayoutParams(new LinearLayout.LayoutParams(dpToPx(50), dpToPx(50)));
                    *//*holder.wvAmenities.setVisibility(VISIBLE);
                    holder.llRoot.setVisibility(VISIBLE);*//*
                    holder.flRoot.setVisibility(VISIBLE);
                }*/
            }

            holder.flRoot.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickListener(pos, drawerMenuOject);
                }
            });
        }

        public Bitmap getCroppedBitmap(Bitmap bitmap) {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            /*paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);*/
            canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                    bitmap.getWidth() / 2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
            //return _bmp;
            return output;
        }

        public int dpToPx(int dp){
            Resources r = getResources();
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
            return (int)px;
        }

        @Override
        public int getItemCount() {
            return amenitiesList.size();
        }

        public class AmenitiesViewHolder extends RecyclerView.ViewHolder {
            //LinearLayout llRoot;
            FrameLayout flRoot;
            ImageView imgAmenities;
            //WebView wvAmenities;

            public AmenitiesViewHolder(View view) {
                super(view);
                //llRoot = (LinearLayout) view.findViewById(R.id.ll_root);
                flRoot= (FrameLayout) view.findViewById(R.id.fl_root);
                imgAmenities= (ImageView) view.findViewById(R.id.imgAmenities);
                //wvAmenities = (WebView) view.findViewById(R.id.wvAmenities);
            }
        }
    }

/*    Context mContext;
    RecyclerView lstAmenities;
    ArrayList<DrawerMenuOject> amenitiesList;
    StoreLocatorNewFragment stroreFrag;
    AmenitiesAdapter mAdapter;
    MapActionListener mListener;
    String selectedAmenities;
    Button btnPrev, btnNext;

    public AmenitiesOverlay(Context context, ArrayList<DrawerMenuOject> amenitiesList, StoreLocatorNewFragment frag, MapActionListener listener) {
        super(context);false
        mContext = context;
        this.amenitiesList = amenitiesList;
        stroreFrag = frag;
        mListener = listener;

        for (int i = 0; i < amenitiesList.size(); i++) {
            String inActiveIconName = "ic_" + amenitiesList.get(i).getTitle().replaceAll(" ", "_").replaceAll("&_", "").replaceAll("'", "").toLowerCase();
            String activeIconName = inActiveIconName + "_active";
            int inActiveIconID = getResources().getIdentifier(inActiveIconName, "drawable", mContext.getPackageName());
            int activeIconID = getResources().getIdentifier(activeIconName, "drawable", mContext.getPackageName());

            amenitiesList.get(i).setActiveImgId(activeIconID);
            amenitiesList.get(i).setInActiveImgId(inActiveIconID);
        }

        View view = LayoutInflater.from(context).inflate(R.layout.layout_amenities_overlay, null);
        addView(view);

        lstAmenities = (RecyclerView) view.findViewById(R.id.list_amenities);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        lstAmenities.setLayoutManager(horizontalLayoutManagaer);
        lstAmenities.setAdapter(mAdapter = new AmenitiesAdapter(amenitiesList, this));

        btnPrev = (Button) view.findViewById(R.id.btnPrev);
        btnPrev.setOnClickListener(this);
        btnNext = (Button) view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        lstAmenities.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int count = recyclerView.getAdapter().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) lstAmenities.getLayoutManager())
                        .findLastVisibleItemPosition();
                int firstVisibleItem = ((LinearLayoutManager) lstAmenities.getLayoutManager())
                        .findFirstVisibleItemPosition();

                if (lastVisibleItem == (count - 1)) {
                    btnNext.setTextColor(Color.parseColor("#888888"));
                    btnPrev.setTextColor(Color.parseColor("#FFFFFF"));
                } else if (firstVisibleItem == 0) {
                    btnPrev.setTextColor(Color.parseColor("#888888"));
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                } else {
                    btnPrev.setTextColor(Color.parseColor("#FFFFFF"));
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }
        });

        Runnable fitsOnScreen = new Runnable() {
            @Override
            public void run() {
                int count = lstAmenities.getAdapter().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) lstAmenities.getLayoutManager())
                        .findLastVisibleItemPosition();

                if (lastVisibleItem == count - 1 && lstAmenities.getChildAt(lastVisibleItem).getRight() <= lstAmenities.getWidth()) {
                    // It fits!
                    btnPrev.setVisibility(GONE);
                    btnNext.setVisibility(GONE);
                } else {
                    // It doesn't fit...
                    btnPrev.setVisibility(VISIBLE);
                    btnNext.setVisibility(VISIBLE);
                }
            }
        };
        lstAmenities.post(fitsOnScreen);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPrev:
                lstAmenities.smoothScrollToPosition(0);
                break;

            case R.id.btnNext:
                lstAmenities.smoothScrollToPosition(amenitiesList.size() - 1);
                break;
        }
    }

    @Override
    public void onItemClickListener(int position, DrawerMenuOject object) {

        DrawerMenuOject drawerMenuOject = object;
        if (drawerMenuOject.getType() != null) {
            if (drawerMenuOject.getType().equalsIgnoreCase("amenities")) {
                selectedAmenities = drawerMenuOject.getTitle();
                M m = MSingleton.getI();
                m.camera.zoomTo(m.defaultFramings[m.getCurrentMapIndex()]);
                m.camera.setRoll(0);

                int realJ = position;
                if (realJ >= 0 && realJ < MSingleton.getI().getAmenitiesVisibility().length)
                    if (MSingleton.getI().getAmenitiesVisibility()[realJ]) {
                        resetAmenities();
                        MSingleton.getI().setAmenityVisibility(realJ, false);
                        changeImg(position, false);
                    } else {
                        resetAmenities();
                        MSingleton.getI().setAmenityVisibility(realJ, true);
                        changeImg(position, true);
                        stroreFrag.amenitiesAvailabilityLevels(realJ, drawerMenuOject.getTitle());

                        RectF amenitiesRectF = new RectF();
                        Element[] amenities = m.getElementsByType(Amenity.class);
                        for (int i = 0; i < amenities.length; i++) {
                            Amenity amenity = (Amenity) amenities[i];
                            if (amenity.amenityComponent.bean.localizedText.equalsIgnoreCase(drawerMenuOject.getTitle())) {

                                RenderStyle styleBG = new RenderStyle();
                                styleBG.setFillColor(ContextCompat.getColor(mContext, R.color.aminities_select));

                                RenderStyle styleFG = new RenderStyle();
                                styleFG.setFillColor(ContextCompat.getColor(mContext, R.color.white));

                                RenderStyleIcon renderStyleIcon = new RenderStyleIcon();
                                renderStyleIcon.renderStyleBG = styleBG;
                                renderStyleIcon.renderStyleFG = styleFG;
                                amenity.setStyleIdle(renderStyleIcon);

                                if (amenity.getLevel() == MSingleton.getI().getCurrentMapIndex()) {
                                    amenitiesRectF.union(amenity.getBBox());
                                }
                                m.camera.zoomTo(amenitiesRectF, 50);
                            }
                        }
                    }
            }
        }
    }

    public String getSelectedAmenities() {
        return selectedAmenities;
    }

    public void changeImg(final int pos, boolean value) {
        for (int i = 0; i < amenitiesList.size(); i++) {
            amenitiesList.get(i).setSelected(false);
        }
        amenitiesList.get(pos).setSelected(value);
        mAdapter.notifyDataSetChanged();
    }

    public void resetAmenities() {
        for (int l = 0; l < MSingleton.getI().getAmenitiesVisibility().length; l++) {
            MSingleton.getI().setAmenityVisibility(l, false);
        }
    }

    public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.AmenitiesViewHolder> {

        private List<DrawerMenuOject> amenitiesList;
        OnItemClickListerner mListener;

        public AmenitiesAdapter(List<DrawerMenuOject> drawerMenuObjects, OnItemClickListerner listener*//* StoreLocatorTestFragment storeLocatorFragment*//*) {
            amenitiesList = drawerMenuObjects;
            mListener = listener;
        }

        @Override
        public AmenitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_amenities, parent, false);
            return new AmenitiesViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(AmenitiesViewHolder holder, int position) {
            final DrawerMenuOject drawerMenuOject = amenitiesList.get(position);
            final int pos = position;

            holder.llRoot.setTag(drawerMenuOject);

            if (amenitiesList.get(position).isSelected()) {
                holder.imgAmenities.setImageResource(amenitiesList.get(pos).getActiveImgId());
            } else {
                holder.imgAmenities.setImageResource(amenitiesList.get(pos).getInActiveImgId());
            }

            if (amenitiesList.get(pos).getDescription().contains("Entrance") ||
                    amenitiesList.get(pos).getDescription().contains("Information") ||
                    amenitiesList.get(pos).getDescription().contains("Toilets") ||
                    amenitiesList.get(pos).getDescription().contains("Prayer") ||
                    amenitiesList.get(pos).getDescription().contains("Metro") ||
                    amenitiesList.get(pos).getDescription().contains("Parking") ||
                    amenitiesList.get(pos).getDescription().contains("Telephone")) {
                holder.imgAmenities.setVisibility(GONE);
                holder.llRoot.setVisibility(GONE);
            } else {
                holder.imgAmenities.setVisibility(VISIBLE);
                holder.llRoot.setVisibility(VISIBLE);
            }

            holder.llRoot.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickListener(pos, drawerMenuOject);
                }
            });
        }

        @Override
        public int getItemCount() {
            return amenitiesList.size();
        }

        public class AmenitiesViewHolder extends RecyclerView.ViewHolder {
            LinearLayout llRoot;
            ImageView imgAmenities;

            public AmenitiesViewHolder(View view) {
                super(view);
                llRoot = (LinearLayout) view.findViewById(R.id.ll_root);
                imgAmenities = (ImageView) view.findViewById(R.id.imgAmenities);
            }
        }
    }*/
}
