package com.belongi.citycenter.jibe.overlays;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.ui.view.viewpagerindicator.CirclePageIndicator;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.main.VenueData;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Destination;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.textDirections.TDInstruction;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.textDirections.TextDirectionOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 11/11/16.
 */
public class DirectionOverlay extends FrameLayout implements View.OnClickListener{

    MapActionListener mListener;
    ViewPager pagerTextDirection;
    TextDirectionAdapter mAdapter;
    List<String> directionList=new ArrayList<String>();
    //Destination mToDestination,mFromDestination;
    LinearLayout llAccessibility;
    ImageView imgAccessibilityMode,imgLeftArrow,imgLocation,imgAccessibility,imgMyLocation;
    LinearLayout llTextDirection;
    String strToId,strFromId;
    TextView lblOpen;

    View line;
    //TextView imgMyLocation;

    String levelFrom,levelTo;

    public DirectionOverlay(Context context,String storeToId,String storeFromId,MapActionListener listener) {
        super(context);

        mListener=listener;

        strFromId=storeFromId;
        strToId=storeToId;

        View view= LayoutInflater.from(context).inflate(R.layout.direction_overlay,null);
        addView(view);

        mListener.plotRoute(storeToId, storeFromId);
        llTextDirection= (LinearLayout) view.findViewById(R.id.ll_text_direction);
        pagerTextDirection= (ViewPager) view.findViewById(R.id.pagerTextDirection);
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.circlePageIndicator);
        pagerTextDirection.setAdapter(mAdapter=new TextDirectionAdapter(context,directionList));
        indicator.setViewPager(pagerTextDirection);

        view.findViewById(R.id.lblReplay).setOnClickListener(this);
        imgAccessibility= (ImageView) view.findViewById(R.id.imgAccessible);

        M m = MSingleton.getI();
        if(/*mFromDestination!=null*/!TextUtils.isEmpty(storeFromId)) {
            levelFrom=getShortFormLevel((m.getMapFullByDestinationId(Integer.parseInt(storeFromId.trim()))).map.floorSequence-1);
            //lblFrom.setText(mFromDestination.name);
            ((TextView) view.findViewById(R.id.lblLevelFrom)).setText(levelFrom);
        }

        if(/*mToDestination!=null*/ !TextUtils.isEmpty(storeToId)) {
            levelTo=getShortFormLevel((m.getMapFullByDestinationId(Integer.parseInt(storeToId.trim()))).map.floorSequence-1);
            //lblTo.setText(mToDestination.name );
            ((TextView) view.findViewById(R.id.lblLevelTo)).setText(levelTo);
        }

        view.findViewById(R.id.ll_prev).setOnClickListener(this);
        view.findViewById(R.id.ll_next).setOnClickListener(this);
        view.findViewById(R.id.btnNext).setOnClickListener(this);
        view.findViewById(R.id.btnPrev).setOnClickListener(this);
        llAccessibility= (LinearLayout) view.findViewById(R.id.ll_accessibility);
        llAccessibility.setOnClickListener(this);

        imgAccessibilityMode= (ImageView) view.findViewById(R.id.accessibility_mode);
        imgLeftArrow= (ImageView) view.findViewById(R.id.imgLeftArrow);
        //imgRightArrow= (ImageView) view.findViewById(R.id.imgRightArrow);

        if (!TextUtils.isEmpty(levelTo) && levelTo.equalsIgnoreCase(levelFrom)) {
            imgAccessibilityMode.setVisibility(View.GONE);
        } else {
            imgAccessibilityMode.setVisibility(View.VISIBLE);
            if (MSingleton.getI().getAccessLevel() == 50) {
                imgAccessibilityMode.setImageResource(R.drawable.ic_elevator);
            } else {
                imgAccessibilityMode.setImageResource(R.drawable.ic_escalator);
            }
        }

        imgLocation= (ImageView) view.findViewById(R.id.imgLocation);
        imgMyLocation= (ImageView) view.findViewById(R.id.imgMyLocation);

        imgLocation.setOnClickListener(this);
        imgMyLocation.setOnClickListener(this);
        lblOpen= (TextView) view.findViewById(R.id.lblOpen);
        lblOpen.setOnClickListener(this);

        line=view.findViewById(R.id.lineView);
        playRouteAnimation();

        view.findViewById(R.id.ll_root).setOnClickListener(this);
        getTextDir();
    }

    public void playRouteAnimation(){

        imgLocation.post(new Runnable() {
            @Override
            public void run() {
                TranslateAnimation moveLeftToRight = new TranslateAnimation(imgMyLocation.getX(), (line.getWidth()- getPixels(TypedValue.COMPLEX_UNIT_DIP,20)), 0, 0);
                moveLeftToRight.setDuration(3000);
                moveLeftToRight.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        //if (levelTo.equalsIgnoreCase(levelFrom)) {
                        imgLeftArrow.setVisibility(VISIBLE);
                        imgLeftArrow.startAnimation(animation);
                        //}
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                //if (levelTo.equalsIgnoreCase(levelFrom)) {
                imgLeftArrow.setVisibility(VISIBLE);
                imgLeftArrow.startAnimation(moveLeftToRight);
                //}
            }
        });
    }

    static int getPixels(int unit, float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(unit, size, metrics);
    }

    private String getShortFormLevel(int level) {
        String label = "";
        if (level == 0) {
            label = "GF";
        } else if (level < 0) {
            int lev = (MSingleton.getI().venueData.maps.length - 1);
            if(lev == 0) {
                label = "GF";
            } else
            {
                label = "L" + (MSingleton.getI().venueData.maps.length - 1);
            }
        } else
            label = "L" + (level);
        return label;
    }

    String finalInstructions;

    private void getTextDir() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                directionList.clear();
                String instructions = "";

                TextDirectionOptions options=new TextDirectionOptions();
                options.filter=true;



                Log.e("Text Direction",MSingleton.getI().getToWaypoint().id + ":::" + MSingleton.getI().getFromWaypoint().id);
                ArrayList<ArrayList<TDInstruction>> textDirectionInstruction = MSingleton.getI()
                        .getTextDirectionInstruction(true, 30, 50);

                Log.e("Direction", textDirectionInstruction + "::::::");
                if (textDirectionInstruction == null) {
                    instructions = "No text direction available ";
                } else {
                    for (int i = 0; i < textDirectionInstruction.size(); i++) {
                        TDInstruction[] textDirectionInstructions = textDirectionInstruction.get(i).toArray(new TDInstruction[textDirectionInstruction.size()]);
                        for (TDInstruction tdInstruction : textDirectionInstructions) {
                            if (tdInstruction != null) {
                                if (!tdInstruction.output.isEmpty()) {
                                    /*int distance_calc = 0;
                                    String distance = "";
                                    distance_calc = (int) Math.floor(tdInstruction.distanceToNextMeters);
                                    distance = Integer.toString(distance_calc);
                                    if (!distance.equals("0")) {
                                        directionList.add( tdInstruction.output + " for " + distance + " meters ");
                                        instructions += "<font color=\"red\"> <b>&#8226;</b> </font> " + tdInstruction.output + " for " + distance + " meters \n" + "<br /> <br /> ";
                                    } else {
                                        directionList.add(tdInstruction.output);
                                        instructions += "<font color=\"red\"> <b>&#8226;</b> </font>" + tdInstruction.output + "\n" + "<br /> <br /> ";
                                    }*/
                                    directionList.add(tdInstruction.output);
                                    instructions += "<font color=\"red\"> <b>&#8226;</b> </font>" + tdInstruction.output + "\n" + "<br /> <br /> ";
                                }
                            }
                        }
                    }
                }
                handler.sendEmptyMessage(0);
                finalInstructions = instructions;
                Log.e("TEXT INSTRUCTION",finalInstructions);
            }
        }).start();
    }

    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //llTextDirection.setVisibility(VISIBLE);
            Log.e("Enter", directionList.size() + ":::::");
            //lblOpen.setEnabled(true);
            mAdapter.notifyDataSetChanged();
            pagerTextDirection.setCurrentItem(0);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lblReplay:
                M m = MSingleton.getI();
                m.resetWayfind();
                App.get().setCurrentSegment(0);
                mListener.plotRoute(strToId, strFromId);
                pagerTextDirection.setCurrentItem(0);
                playRouteAnimation();
                break;

            case R.id.ll_next:
            case R.id.btnNext:
                if(pagerTextDirection.getCurrentItem()<pagerTextDirection.getAdapter().getCount()-1){
                    pagerTextDirection.setCurrentItem(pagerTextDirection.getCurrentItem()+1);
                }
                break;

            case R.id.ll_prev:
            case R.id.btnPrev:
                if(pagerTextDirection.getCurrentItem()>0){
                    pagerTextDirection.setCurrentItem(pagerTextDirection.getCurrentItem()-1);
                }
                break;

            case R.id.ll_accessibility:
                //Log.e("accessibility", "" + MSingleton.getI().getAccessLevel());
                MSingleton.getI().resetWayfind();
                MSingleton.getI().unHighlightUnits();
                llTextDirection.setVisibility(GONE);
                if (MSingleton.getI().getAccessLevel() == 50) {
                    MSingleton.getI().setAccessLevel(100);
                    imgAccessibility.setImageDrawable(getResources().getDrawable(R.drawable.ic_disable_normal));
                } else {
                    MSingleton.getI().setAccessLevel(50);
                    imgAccessibility.setImageDrawable(getResources().getDrawable(R.drawable.ic_disable_active));
                }
                mListener.plotRoute(strToId,strFromId);

                if (levelTo.equalsIgnoreCase(levelFrom)) {
                    imgAccessibilityMode.setVisibility(View.GONE);
                } else {
                    imgAccessibilityMode.setVisibility(View.VISIBLE);
                    if (MSingleton.getI().getAccessLevel() == 50) {
                        imgAccessibilityMode.setImageResource(R.drawable.ic_elevator);
                    } else {
                        imgAccessibilityMode.setImageResource(R.drawable.ic_escalator);
                    }
                }

                playRouteAnimation();
                getTextDir();

                break;

            case R.id.imgLocation:
                mListener.changeLevel(levelTo);
                break;

            case R.id.imgMyLocation:
                mListener.changeLevel(levelFrom);
                break;

            case R.id.lblOpen:
                if(llTextDirection.getVisibility()==VISIBLE){
                    //lblOpen.setText("Open");
                    lblOpen.setBackgroundResource(R.drawable.ic_down_arrow);
                    llTextDirection.setVisibility(GONE);
                }else{
                    //lblOpen.setText("Close");
                    lblOpen.setBackgroundResource(R.drawable.ic_up_arrow);
                    llTextDirection.setVisibility(VISIBLE);
                }
                break;

            case R.id.ll_root:
                break;
            /*case R.id.btnClose:
                if(mListener!=null){
                    mListener.onCloseClick();
                }
                break;*/
        }
    }

    public class TextDirectionAdapter extends PagerAdapter{

        Context mContext;
        List<String> lstDirs;

        public TextDirectionAdapter(Context context, List<String> items) {
            mContext=context;
            lstDirs = items;
        }

        @Override
        public int getCount() {
            return lstDirs.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (TextView) object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((TextView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, null);

            ((TextView)view.findViewById(android.R.id.text1)).setText(lstDirs.get(position));
            ((TextView)view.findViewById(android.R.id.text1)).setTextColor(ContextCompat.getColor(mContext,android.R.color.black));
            container.addView(view);
            return view;
        }
    }
}
