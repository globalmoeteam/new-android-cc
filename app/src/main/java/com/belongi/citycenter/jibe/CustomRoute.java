package com.belongi.citycenter.jibe;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;

import com.jibestream.jibestreamandroidlibrary.main.Camera;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;

/**
 * Created by jibestream on 2016-06-19.
 */
public class CustomRoute extends com.jibestream.jibestreamandroidlibrary.elements.Route {
    public PathCustomAnimatable pathCustomAnimatable;

  /*  @Override
    public void onCreate(Context context, M m, long timeElapsed, long timeTotal, Camera camera) {
        super.onCreate(context, m, timeElapsed, timeTotal, camera);
// use our custom shape
        pathCustomAnimatable = new PathCustomAnimatable(context);
        setShape(pathCustomAnimatable);
    }*/

    public CustomRoute() {
        super();
        // overriding assigned styling
        // this will passed down to the onDraw of the Shape
        RenderStyle pathRenderStyle = new RenderStyle(Paint.Style.FILL);
        pathRenderStyle.paintFill.setColor(Color.RED);
        pathRenderStyle.paintFill.setStrokeWidth(5);
        setStyleIdle(pathRenderStyle);
    }

    @Override
    public void onCreate(Context context, M m, long timeElapsed, long timeTotal, Camera camera) {
        super.onCreate(context, m, timeElapsed, timeTotal, camera);
        // use our custom shape
        pathCustomAnimatable = new PathCustomAnimatable(context);
        setShape(pathCustomAnimatable);
    }


}
