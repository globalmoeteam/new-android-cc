package com.belongi.citycenter.jibe.overlays;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.Model;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.logic.StoreListLogic;
import com.belongi.citycenter.ui.view.CustomTextView;

import java.util.List;



/**
 * Created by webwerks on 10/11/16.
 */
public class MapStoreDetailOverlay extends FrameLayout implements View.OnClickListener,StoreListLogic.StoreListCallback {

    Context mContext;
    //Search mStore;
    MapActionListener mListener;
    LinearLayout frameCallNow;
    TextView lblFloor;
    List<Search> mDescList;
    ListView lstStores;
    TextView lblMessage;

    public MapStoreDetailOverlay(Context context,List<Search> storeList,MapActionListener listener) {
        super(context);

        mContext=context;
        //mStore=store;
        mListener=listener;
        mDescList=storeList;

        for(Search s:storeList){
            StoreListLogic.getShop(this, s.getLabel());
        }


        View view= LayoutInflater.from(context).inflate(R.layout.store_detail_overlay,null);
        ObjectAnimator.ofFloat(view, "alpha", 0, 1).setDuration(500).start();
        addView(view);

        lstStores= (ListView) view.findViewById(R.id.lstStores);
        lblFloor= (TextView) view.findViewById(R.id.lblFloor);

        if(storeList.size()>1){
            lblFloor.setVisibility(VISIBLE);
            lblFloor.setText(getFullLevel(storeList.get(0).getFloor()));
            lstStores.setBackgroundColor(Color.parseColor("#F28AB6D1"));
        }else{
            lblFloor.setVisibility(GONE);
            lstStores.setBackgroundColor(Color.parseColor("#8AB6D1"));
        }

        /*((CustomTextView)view.findViewById(R.id.lblStoreName)).setText(store.getLabel().trim());
        ((TextView)view.findViewById(R.id.lblStoreCategory)).setText(store.getCategory().trim());
        ((TextView)view.findViewById(R.id.lblLevel)).setText(getFullLevel(store.getFloor()));

        frameCallNow= (LinearLayout) view.findViewById(R.id.ll_callNow);

        frameCallNow.setOnClickListener(this);
        view.findViewById(R.id.ll_direction).setOnClickListener(this);*/
        //view.findViewById(R.id.btnClose).setOnClickListener(this);

        view.findViewById(R.id.ll_root).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId()==R.id.ll_root){
                }
            }
        });

    }

    public void showAlert(final String number){

        final Dialog alertDialog = new Dialog(mContext);
        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_call_confirmation, null);
        lblMessage=(TextView)view.findViewById(R.id.lblMessage);
        lblMessage.setText(number);
        alertDialog.setContentView(view);

        DisplayMetrics disMat = getResources().getDisplayMetrics();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = (int) (disMat.widthPixels * (0.70));
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ( view.findViewById(R.id.btn_no)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });

        ((Button) view.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        ( view.findViewById(R.id.btn_yes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                mContext.startActivity(intent);
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#222222")));
        alertDialog.show();
        alertDialog.getWindow().setAttributes(layoutParams);

    }

    public String getFullLevel(int level){
        String label = "";
        if (level == 0) {
            label = "Ground Floor";
        } else if (level < 0) {
            label = "Level " + (MSingleton.getI().venueData.maps.length - 1);
        } else
            label = "Level " + (level);
        return label;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_direction:
                removeAllViews();
                //addView(new GetDirectionOverlay(mContext,mStore,mListener));
                break;

            case R.id.ll_callNow:
                if (frameCallNow.getTag() != null) {
                    showAlert((String) frameCallNow.getTag());
                } else {
                    Toast.makeText((Activity) mContext, "Phone number is not available", Toast.LENGTH_LONG).show();
                }
                break;

           /* case R.id.btnClose:
                if(mListener!=null){
                    mListener.onCloseClick();
                }
                break;*/
        }
    }

    int count=0;

    @Override
    public void onStoreListReceived(Object storeList) {

        count++;
        List<Model> mStoreList= (List<Model>) storeList;

        if(mStoreList.size()>0){
            Model model=mStoreList.get(0);
            for(Search s:mDescList){
                if(s.getLabel().equalsIgnoreCase(model.toString())){
                    if(model instanceof ShoppingShop){
                        s.setCall(((ShoppingShop)model).getShopTelephone());
                    }else if(model instanceof DiningShop){
                        s.setCall(((DiningShop)model).getDining_Telephone());
                    }else if(model instanceof Entertainment){
                        s.setCall(((Entertainment)model).getEntertainment_Telephone());
                    }
                }
            }


            /*if(model instanceof ShoppingShop){
                frameCallNow.setTag(((ShoppingShop)model).getShopTelephone());
            }else if(model instanceof DiningShop){
                frameCallNow.setTag(((DiningShop)model).getDining_Telephone());
            }else if(model instanceof Entertainment){
                frameCallNow.setTag(((Entertainment)model).getEntertainment_Telephone());
            }*/
        }else{
            count=0;
        }

        if(count==mStoreList.size()){
            lstStores.setAdapter(new MapStoreDetailAdapter(mContext,mDescList));
        }

    }

    @Override
    public void onEmptyStoreList() {
    }

    public class MapStoreDetailAdapter extends ArrayAdapter<Search> implements OnClickListener{
        Context mContext;
        List<Search> searchList;

        public MapStoreDetailAdapter(Context context, List<Search> objects) {
            super(context, 0 , objects);
            mContext=context;
            searchList=objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView==null){
                convertView=LayoutInflater.from(mContext).inflate(R.layout.row_map_store_detail,null);
                holder= new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder= (ViewHolder) convertView.getTag();
            }

            holder.lblStoreName.setText(Html.fromHtml(getItem(position).getLabel().replace("&AMP;","&amp;")));
            holder.lblStoreCategory.setText(Html.fromHtml(getItem(position).getCategory().trim().split(",")[0].replace("&AMP;","&amp;")));

            if(searchList.size()>1) {
                holder.lblLevel.setVisibility(GONE);
                holder.lblCallNow.setVisibility(GONE);
                holder.lblGetDirection.setVisibility(GONE);
            }else{
                holder.lblLevel.setVisibility(VISIBLE);
                holder.lblLevel.setText(getFullLevel(getItem(position).getFloor()) + "");
            }

            holder.llCallNow.setTag(getItem(position).getCall());
            holder.llDirection.setTag(getItem(position));

            holder.llCallNow.setOnClickListener(this);
            holder.llDirection.setOnClickListener(this);

            return convertView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ll_callNow:
                    if (v.getTag() != null) {
                        showAlert((String) v.getTag());
                    } else {
                        Toast.makeText((Activity) mContext, "Phone number is not available", Toast.LENGTH_LONG).show();
                    }
                    break;

                case R.id.ll_direction:
                    removeAllViews();
                    addView(new GetDirectionOverlay(mContext, (Search) v.getTag(),mListener));
                    break;
            }
        }

        class ViewHolder{
            CustomTextView lblStoreName;
            TextView lblStoreCategory,lblLevel,lblCallNow,lblGetDirection;
            LinearLayout llCallNow,llDirection;

            public ViewHolder(View convertView){
                lblStoreName= (CustomTextView) convertView.findViewById(R.id.lblStoreName);
                lblStoreCategory= (TextView) convertView.findViewById(R.id.lblStoreCategory);
                lblLevel= (TextView) convertView.findViewById(R.id.lblLevel);

                llCallNow= (LinearLayout) convertView.findViewById(R.id.ll_callNow);
                llDirection= (LinearLayout) convertView.findViewById(R.id.ll_direction);
                lblCallNow= (TextView) convertView.findViewById(R.id.lblCallNow);
                lblGetDirection= (TextView) convertView.findViewById(R.id.lblGetDirections);
            }
        }
    }
}
