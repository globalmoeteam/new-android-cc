package com.belongi.citycenter.jibe;

import android.graphics.Color;
import android.graphics.Paint;

import com.jibestream.jibestreamandroidlibrary.elements.Unit;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;
import com.jibestream.jibestreamandroidlibrary.utils.ColorsMaterialDesign;

/**
 * Created by webwerks on 13/6/16.
 */
public class SelectedUnits extends Unit {
    public SelectedUnits() {
        /*styleHighlighted = new RenderStyle(Paint.Style.FILL_AND_STROKE);
        styleHighlighted.setFillColor(Color.argb(200, 211, 211, 211));
        styleHighlighted.setStrokeColor(ColorsMaterialDesign.GREY8);
        styleHighlighted.setStrokeWidth(3);*/

        styleHighlighted = new RenderStyle(Paint.Style.FILL_AND_STROKE);
        styleHighlighted.setFillColor(Color.argb(200, 226, 3, 3));//rgb(226,3,3)
        //styleHighlighted.setFillColor(Color.parseColor("#E20303"));
        //styleHighlighted.setStrokeColor(ColorsMaterialDesign.GREY8);
        styleHighlighted.setStrokeColor(Color.parseColor("#BDBBB7"));
        styleHighlighted.setStrokeWidth(1);

       /* styleIdle=new RenderStyle(Paint.Style.FILL);
        styleIdle.setFillColor(Color.parseColor("#F7EEE0"));
*/
    }
}
