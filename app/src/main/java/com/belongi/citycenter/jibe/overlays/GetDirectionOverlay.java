package com.belongi.citycenter.jibe.overlays;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.jibe.MSingleton;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.main.VenueData;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.MapFull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 10/11/16.
 */
public class GetDirectionOverlay extends FrameLayout implements View.OnClickListener {

    Context mContext;

    //AutoCompleteTextView edtFrom;
    EditText edtFrom;
    ArrayList<Search> stores;
    TextView lblTo;
    MapActionListener mListener;
    ListView lstStores;
    StoreAdapter mAdapter;
    Search storeTo,storeFrom;
    TextView lblSearchResult;

    public GetDirectionOverlay(Context context, Search store,MapActionListener listener) {
        super(context);

        mContext=context;
        mListener=listener;
        storeTo=store;

        View view= LayoutInflater.from(context).inflate(R.layout.get_direction_overlay,null);
        addView(view);

        edtFrom= (EditText) view.findViewById(R.id.edtFrom);
        lblTo= (TextView) view.findViewById(R.id.lblTo);
        lstStores= (ListView) view.findViewById(R.id.lstStores);
        view.findViewById(R.id.lblSwap).setOnClickListener(this);

        final VenueData venueData = MSingleton.getI().venueData;
        stores=new ArrayList<Search>();
        for (int i = 0; i < venueData.destinations.length; i++) {
            Search search = new Search();
            search.setLabel("" + Html.fromHtml(venueData.destinations[i].name));
            search.setId(venueData.destinations[i].id +" ");

            M m = MSingleton.getI();
            MapFull mapFull = m.getMapFullByDestinationId(venueData.destinations[i].id);

            search.setLevel(getShortFormLevel(mapFull.map.floorSequence - 1));
            search.setCategory(getCategories(venueData.destinations[i].category));

            stores.add(search);
        }

        edtFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() > 0) {
                    //perform search
                    search(s.toString());
                } else {
                    //clear the adapter
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(TextUtils.isEmpty(editable.toString().trim())){
                    search("");
                }
            }
        });

        /*edtFrom.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                search("");
            }
        });*/

        mAdapter=new StoreAdapter(context,new ArrayList<Search>());
        lstStores.setAdapter(mAdapter);
        lstStores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                storeFrom=((Search)adapterView.getItemAtPosition(i));
                edtFrom.setText(storeFrom.getLabel().trim());
                hideSoftKeyboard();
                mAdapter.clear();
                mAdapter.notifyDataSetChanged();
                lstStores.setVisibility(GONE);
                lblSearchResult.setVisibility(GONE);
            }
        });

       /* if(stores.size()>0){
            edtFrom.setAdapter(new StoreAdapter(context,stores));
            edtFrom.setThreshold(1);
        }*/

        lblTo.setText(store.getLabel().trim());
        view.findViewById(R.id.lblGetDirections).setOnClickListener(this);
        lblSearchResult= (TextView) findViewById(R.id.lblSearchResult);
        //view.findViewById(R.id.btnClose).setOnClickListener(this);
        view.findViewById(R.id.ll_root).setOnClickListener(this);
        search("");
    }

    public void hideSoftKeyboard(){
        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtFrom.getWindowToken(), 0);
    }

    List<Search> searchResult=new ArrayList<>();

    public void search(String keyword){
        searchResult.clear();

        /*if(TextUtils.isEmpty(keyword)){
            lstStores.setVisibility(VISIBLE);
            mAdapter.clear();
            mAdapter.addAll(stores);
            mAdapter.notifyDataSetChanged();
        }else{*/
        for(Search search:stores){
            if(search.getLabel().toLowerCase().contains(keyword.toLowerCase())){
                searchResult.add(search);
            }
        }

        if(searchResult.size()>0) {
            lblSearchResult.setVisibility(VISIBLE);
            lstStores.setVisibility(VISIBLE);
            mAdapter.clear();
            mAdapter.addAll(searchResult);
            mAdapter.notifyDataSetChanged();
        }else{
            mAdapter.clear();
            mAdapter.notifyDataSetChanged();
            lblSearchResult.setVisibility(GONE);
            lstStores.setVisibility(GONE);
            Toast.makeText(mContext,"No Result Found",Toast.LENGTH_SHORT).show();
        }
        //}
    }

    private String getShortFormLevel(int level) {
        String label = "";
        if (level == 0) {
            label = "GF";
        } else if (level < 0) {
            label = "L" + (MSingleton.getI().venueData.maps.length - 1);
        } else
            label = "L" + (level);
        return label;
    }

    public String getCategories(String[] strings) {
        String s = " ";
        for (int i = 0; i < strings.length; i++) {
            if (i == strings.length - 1) {
                s = s + strings[i];
            } else {
                s = s + strings[i] + ", ";
            }
        }
        return s.toUpperCase();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lblSwap:
                if(!TextUtils.isEmpty(edtFrom.getText().toString())){
                    String from=edtFrom.getText().toString();

                    edtFrom.setFocusable(false);
                    edtFrom.setFocusableInTouchMode(false);
                    edtFrom.setText(lblTo.getText().toString());
                    edtFrom.setFocusable(true);
                    edtFrom.setFocusableInTouchMode(true);
                    lblTo.setText(from);
                }
                break;

            case R.id.lblGetDirections:
                //Destination toDestination=null,fromDestination=null;
                if((!TextUtils.isEmpty(edtFrom.getText())) && (!TextUtils.isEmpty(lblTo.getText()))){
                    Log.e("GETDOR",storeTo.getIdentifier()+ ":::::" +storeFrom.getIdentifier());
                    removeAllViews();
                    /*addView(new DirectionOverlay(mContext,
                            lblTo.getText().toString(),edtFrom.getText().toString(),mListener),
                            LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);*/
                    addView(new DirectionOverlay(mContext,
                                    storeTo.getIdentifier(),storeFrom.getIdentifier(),mListener),
                            LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
                }else{
                    Toast.makeText(mContext,"Please provide source point",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ll_root:
                break;
           /* case R.id.btnClose:
                if(mListener!=null){
                    mListener.onCloseClick();
                }
                break;*/
        }
    }



    public class StoreAdapter extends ArrayAdapter<Search>{
        Context mContext;

        public StoreAdapter(Context context, List<Search> objects) {
            super(context, 0, objects);
            mContext=context;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                convertView=LayoutInflater.from(mContext).inflate(R.layout.row_map_stores,null);
            }
            TextView txt= (TextView) convertView.findViewById(R.id.txtCategory);
            txt.setText(getItem(position).getLabel());

            TextView mtextView = (TextView) convertView.findViewById(R.id.lblLevel);
            mtextView.setText(getItem(position).getLevel());

            return convertView;
        }
    }
}
