package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.belongi.citycenter.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by yashesh on 6/29/2015.
 */
public class DetailImagesAdapter extends PagerAdapter{

    List<String> images;
    Context mContext;
    public DetailImagesAdapter(Context context,List<String> images){
        this.images=images;
        mContext=context;

    }


    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout)object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.layout_detail_image,null);

        final ImageView img= (ImageView)view.findViewById(R.id.imgDetail);
        Picasso.with(mContext).load(images.get(position)).placeholder(R.drawable.place_holder_event1_6p).fit().into(img, new Callback() {
            @Override
            public void onSuccess() {
                Animation scaleAnim = AnimationUtils.loadAnimation(mContext, R.anim.scale_animation);
                img.startAnimation(scaleAnim);
            }

            @Override
            public void onError() {

            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
