package com.belongi.citycenter.ui.activity.spinwin;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.QuestionAnswer;
import com.belongi.citycenter.data.entities.RegisterAnswer;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 22/6/15.
 */
public class WrongAnswerActivity extends BaseActivity implements View.OnClickListener{

    RegisterAnswer registerAnswerRespo;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_wrong_answer);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
        findViewById(R.id.btnTryAgain).setOnClickListener(this);
        TextView lblAttemLeft= (TextView) findViewById(R.id.lblAttemLeft);
        TextView lblCorrectAns= (TextView) findViewById(R.id.lblCorrectAns);
        ImageView imgStar1= (ImageView) findViewById(R.id.imgStar1);
        ImageView imgStar2= (ImageView) findViewById(R.id.imgStar2);
        ImageView imgStar3= (ImageView) findViewById(R.id.imgStar3);
        registerAnswerRespo= (RegisterAnswer) getIntent().getSerializableExtra(Constant.ATTEMPT_LEFT);

        lblCorrectAns.setText("Correct answer was " + registerAnswerRespo.getCorrectAns());

        if(registerAnswerRespo.getAttemptsLeft().equals("2")){
            imgStar1.setImageResource(R.drawable.icon_false);
            lblAttemLeft.setText("You have "+ 2 + " more attempts left");
        }else if(registerAnswerRespo.getAttemptsLeft().equals("1")){
            imgStar1.setImageResource(R.drawable.icon_false);
            imgStar2.setImageResource(R.drawable.icon_false);
            lblAttemLeft.setText("You have "+ 1 + " more attempts left");
        }else if(registerAnswerRespo.getAttemptsLeft().equals("0")){
            imgStar1.setImageResource(R.drawable.icon_false);
            imgStar2.setImageResource(R.drawable.icon_false);
            imgStar3.setImageResource(R.drawable.icon_false);
            lblAttemLeft.setText("You have "+ 0 + " attempts left");
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnTryAgain:
                if(registerAnswerRespo.getAttemptsLeft().equals("0")){
                    //startActivity(new Intent(this,SpinToWinDashboardActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(this,PlayContestActivity.class));
                    finish();
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN";
    }
}
