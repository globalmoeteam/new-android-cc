package com.belongi.citycenter.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Malls;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.network.Validator;
import com.belongi.citycenter.ui.adapters.MallSelectionAdapter;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by webwerks on 28/7/15.
 */
public class MallSelectionActivity extends BaseActivity implements LocalDataLoader.QueryClient, AdapterView.OnItemClickListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    ListView lstMalls;
    TextView lblNearestMall, lblChangemall;
    GoogleApiClient googleApiClient;
    Malls nearMall;
    MallSelectionAdapter adapter;
    LinearLayout llNearMall;

    @Override
    public String getScreenName() {
        return "Mall Selection";
    }

    @Override
    protected void releaseUi() {
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setNumUpdates(1);

      LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        //imgNearMall.setVisibility(View.VISIBLE);
        llNearMall.setVisibility(View.VISIBLE);
        lblChangemall.setText("CHANGE MALL");
        Log.e("MALL LOCATION API", location.getLatitude() + "  " + location.getLongitude());
        if (mallList != null && mallList.size() > 0) {
            double nearMallDist = distance(location.getLatitude(), location.getLongitude(), Double.parseDouble(mallList.get(0).getLatitude()), Double.parseDouble(mallList.get(0).getLongitude()));
            nearMall = mallList.get(0);
            for (Malls mall : mallList) {
                double dist = distance(location.getLatitude(), location.getLongitude(), Double.parseDouble(mall.getLatitude()), Double.parseDouble(mall.getLongitude()));
                Log.e("MALL SELECTION", mall.getMallName() + "  : " + mall.getLatitude() + "  : " + mall.getLongitude() + "  : " + dist);
                mall.setDistance(dist);
                if (dist < nearMallDist) {
                    nearMallDist = dist;
                    nearMall = mall;
                }
            }

            String iconName = "mn_" + nearMall.getMallName().replace("’", "").replaceAll(" ", "_").toLowerCase();
            if (iconName != null) {
                int iconId = getResources().getIdentifier(iconName, "drawable", getPackageName());
                llNearMall.setBackgroundResource(iconId);
            }
            lblNearestMall.setText(nearMall.getMallName());
            mallList.remove(nearMall);
            Collections.sort(mallList, new Comparator<Malls>() {
                @Override
                public int compare(Malls malls, Malls malls2) {
                    return malls.getDistance() > malls2.getDistance() ? 1 : -1;
                }
            });
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
       /* EMSP.getLocation().registerDeviceAtCurrentLocation(new EMSPLocationRegisterListener() {
            @Override
            public void onRegisterSuccess(WiFiLocation wiFiLocation) {
                Toast.makeText(MallSelectionActivity.this, " onRegisterSuccess ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRegisterFailure(StatusHandle.Status status) {
                Toast.makeText(MallSelectionActivity.this, " onRegisterFailure ", Toast.LENGTH_LONG).show();
            }
        });*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    private double distance(double currentLat, double currentLong, double compareLat, double compareLong) {
        double latDistance = Math.toRadians(currentLat - (compareLat));
        double lngDistance = Math.toRadians(currentLong - (compareLong));

        double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) +
                (Math.cos(Math.toRadians(currentLat))) *
                        (Math.cos(Math.toRadians(compareLat))) *
                        (Math.sin(lngDistance / 2)) *
                        (Math.sin(lngDistance / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = 6371 * c;
        return dist;
    }

    public enum Mall {
        DEIRA, BEIRUT, SHARJAH, MIRDIF, AJMAN, BAHRAIN, MUSCAT, ALEXANDRIA, QURUM, FUJAIRAH, MAADI, SHINDAGHA, MEAISEM;
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_mall_selection);
    }


    @Override
    protected void initializeUi() {
        lstMalls = (ListView) findViewById(R.id.lstMalls);
        lstMalls.setOnItemClickListener(this);
        View headerView = LayoutInflater.from(MallSelectionActivity.this).inflate(R.layout.header_mall_selection, null);

        lblChangemall = (TextView) headerView.findViewById(R.id.lblChangemall);
        llNearMall = (LinearLayout) headerView.findViewById(R.id.layout_near_mall);
        lblNearestMall = (TextView) headerView.findViewById(R.id.lblNearestMall);
        llNearMall.setOnClickListener(this);
        lstMalls.addHeaderView(headerView);

        new LocalDataLoader(this, 1).execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        navigateNext((Malls) parent.getItemAtPosition(position));
    }

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        if (Validator.isConnected()) {
            connectGoogleApiClient();
        }
        // testPermission();

        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;

    public void testPermission() {
        if (Build.VERSION.SDK_INT > 22) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
            }
        }
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    //final private int REQUEST_CODE_ALTERT=130;
    private void connectGoogleApiClient() {
        if (Build.VERSION.SDK_INT > 22) {
            int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
                return;
            } else {
                googleApiClient = getGoogleApiClient();
                googleApiClient.connect();
            }
        } else {
            googleApiClient = getGoogleApiClient();
            googleApiClient.connect();
        }
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults.length > 0)
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        googleApiClient = getGoogleApiClient();
                        googleApiClient.connect();
                    } else {
                        // Permission Denied
                        Toast.makeText(MallSelectionActivity.this, "Location Permission Denied", Toast.LENGTH_SHORT).show();
                    }
                break;


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void navigateNext(Malls mall) {
        if (mall != null) {
            MSingleton.NullifyMInstance();
            //MSingleton.getI().onDestroy();
            App.get().setSelectedMall(mall);
            Constant.LAST_SELECTED_MAP_INDEX=0;

            WebApi.BASE_URL = App.get().getSelectedMall().getMallUrl();
            WebApi.IMAGE_BASE_URL = App.get().getSelectedMall().getImageUrl();
            WebApi.BASE_HOAST_URL = App.get().getSelectedMall().getImageUrl();
            //if (!TextUtils.isEmpty(App.get().getSelectedMall().getSiteId())) {
            App.get().setMallId(App.get().getSelectedMall().getSiteId());
            //}

            //Log.e("SelectedMall", App.get().getSelectedMall().getMallName() + " : " + App.get().getSelectedMall().getJibeProjectId());
            if (Validator.isConnected()) {
                startActivity(new Intent(MallSelectionActivity.this, SplashActivity.class));
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            } else {
                List<Service> serviceList = EntityUtils.getAllGuestService();
                if (serviceList == null || serviceList.size() == 0) {
                    Toast.makeText(MallSelectionActivity.this, "No Data Available", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(MallSelectionActivity.this, SplashActivity.class));
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
            }
        }
    }

    List<Malls> mallList;

    @Override
    public Object executeQuery() {

        return EntityUtils.getMalls();
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        if (result != null) {
            mallList = (List<Malls>) result;
            adapter = new MallSelectionAdapter(MallSelectionActivity.this, mallList);
            lstMalls.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        if (nearMall != null) {
            navigateNext(nearMall);
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }
}