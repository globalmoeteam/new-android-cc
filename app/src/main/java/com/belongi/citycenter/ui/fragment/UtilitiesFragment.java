package com.belongi.citycenter.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.PermissionUtils;
import com.belongi.citycenter.logic.QRLogic;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.activity.utility.CurrencyConverterActivity;
import com.belongi.citycenter.ui.activity.utility.FashionConverterActivity;
import com.belongi.citycenter.ui.activity.utility.ShoeSizeConverterActivity;
import com.belongi.citycenter.ui.activity.utility.ZBarScannerActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Locale;

/**
 * Created by webwerks on 14/6/15.
 */

public class UtilitiesFragment extends BaseFragment implements View.OnClickListener {

	TextView btnCurrencyConverter, btnShoeSizeConverter, btnQrScanner, btnFashionConverter;

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		return inflater.inflate( R.layout.fragment_utilities, null );
	}

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		setActionbarTitle( "UTILITIES" );
		btnCurrencyConverter = ( TextView ) view.findViewById( R.id.btnCurrencyConverter );
		btnShoeSizeConverter = ( TextView ) view.findViewById( R.id.btnShoeSizeConverter );
		btnQrScanner = ( TextView ) view.findViewById( R.id.btnQrScanner );
		btnFashionConverter = ( TextView ) view.findViewById( R.id.btnFashionConverter );

		btnShoeSizeConverter.setOnClickListener( this );
		btnCurrencyConverter.setOnClickListener( this );
		btnQrScanner.setOnClickListener( this );
		btnFashionConverter.setOnClickListener( this );
	}

	@Override
	public void onResume() {
		super.onResume();
		Animation animRightLeft = AnimationUtils.loadAnimation( getActivity(), R.anim.slide_right_to_left );
		Animation animLeftRight = AnimationUtils.loadAnimation( getActivity(), R.anim.slide_left_to_right );
		btnShoeSizeConverter.startAnimation( animLeftRight );
		btnFashionConverter.startAnimation( animRightLeft );
		btnCurrencyConverter.startAnimation( animLeftRight );
		btnQrScanner.startAnimation( animRightLeft );
	}

	@Override
	public void onClick( View v ) {
		switch ( v.getId() ) {
			case R.id.btnCurrencyConverter:
				getActivity().startActivity( new Intent( getActivity(), CurrencyConverterActivity.class ) );
				getActivity().overridePendingTransition( R.anim.activity_in, R.anim.activity_out );
				break;

			case R.id.btnShoeSizeConverter:
				getActivity().startActivity( new Intent( getActivity(), ShoeSizeConverterActivity.class ) );
				getActivity().overridePendingTransition( R.anim.activity_in, R.anim.activity_out );
				break;

			case R.id.btnQrScanner:

				checkCameraPermission();

				/*IntentIntegrator integrator = IntentIntegrator.forSupportFragment( this );
				integrator.setDesiredBarcodeFormats( IntentIntegrator.QR_CODE_TYPES );
				integrator.setCameraId( 0 );  // Use a specific camera of the device
				integrator.setPrompt( "" );
				integrator.setBeepEnabled( false );
				integrator.setOrientationLocked( false );
				integrator.initiateScan();*/
			   /* getActivity().startActivity(new Intent(getActivity(), ZBarScannerActivity.class));
			    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);*/
				break;

			case R.id.btnFashionConverter:
				getActivity().startActivity( new Intent( getActivity(), FashionConverterActivity.class ) );
				getActivity().overridePendingTransition( R.anim.activity_in, R.anim.activity_out );
				break;
		}
	}

	private void checkCameraPermission() {
		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
			if ( ActivityCompat.checkSelfPermission( getActivity(), Manifest.permission.CAMERA ) != PackageManager.PERMISSION_GRANTED ) {
				requestPermissions( new String[]{ Manifest.permission.CAMERA }, PermissionUtils.REQUEST_CAMERA );
			}
			else {
				onCameraClicked();
			}
		}
		else {
			onCameraClicked();
		}
	}

	@Override
	public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults )
	{
		//super.onRequestPermissionsResult( requestCode, permissions, grantResults );

		if(requestCode == PermissionUtils.REQUEST_CAMERA){
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				onCameraClicked();
			} else {
				Toast.makeText(getActivity(), "Permission Denied !", Toast.LENGTH_SHORT).show();
			}
		}
	}




	private void onCameraClicked() {
		if ( getActivity() != null ) {
			if ( !PermissionUtils.checkPermissionForCamera( getActivity() ) ) {
				PermissionUtils.requestPermissionForCamera( getActivity() );
			}
			else {
				/*Log.e("permit... ",PermissionUtils.checkPermissionForExternalStorage( getActivity() )+"");
				if ( !PermissionUtils.checkPermissionForExternalStorage( getActivity() ) ) {
					PermissionUtils.requestPermissionForExternalStorage( getActivity() );
				}
				else {*/
				openQR();
				/*}*/
			}
		}
	}

	private void openQR() {
		IntentIntegrator integrator = IntentIntegrator.forSupportFragment( this );
		integrator.setDesiredBarcodeFormats( IntentIntegrator.QR_CODE_TYPES );
		integrator.setCameraId( 0 );  // Use a specific camera of the device
		integrator.setPrompt( "" );
		integrator.setBeepEnabled( false );
		integrator.setOrientationLocked( false );
		integrator.initiateScan();
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent intent ) {
		IntentResult result = IntentIntegrator.parseActivityResult( requestCode, resultCode, intent );
		if ( result != null ) {
			if ( result.getContents() == null ) {
				Toast.makeText( getActivity(), "Please try again ...", Toast.LENGTH_SHORT ).show();
			}
			else {
				Log.e( "QRSCANNINGNEW", "Scanned: " + result.getContents() );
				if ( result.getContents().contains( "BEGIN:VCARD" ) ) {
					QRLogic.parseVcard( getActivity(), result.getContents() );
				}
				else if ( result.getContents().contains( "SMSTO" ) ) {
					QRLogic.sendSms( getActivity(), result.getContents() );
				}
				else if ( result.getContents().contains( "MATMSG" ) ) {
					QRLogic.sendEmail( getActivity(), result.getContents() );
				}
				else if ( result.getContents().startsWith( "tel:" ) ) {
					startActivity( new Intent( Intent.ACTION_DIAL ).setData( Uri.parse( result.getContents() ) ) );
				}
				else {

					if ( result.getContents().startsWith( "www" ) || result.getContents().startsWith( "http" ) ) {
						if ( result.getContents().startsWith( "http" ) ) {
							startActivity( new Intent( getActivity(), WebContentPreviewActivity.class )
									               .putExtra( "url", result.getContents() )
									               .putExtra( "title", "UTILITIES" )
									               .putExtra( "category", Constant.DONT_SEND ) );
						}
						else {
							startActivity( new Intent( getActivity(), WebContentPreviewActivity.class )
									               .putExtra( "url", "https://" + result.getContents() )
									               .putExtra( "title", "UTILITIES" )
									               .putExtra( "category", Constant.DONT_SEND ) );
						}
					}
					else {
						Toast.makeText( getActivity(), result.getContents(), Toast.LENGTH_LONG ).show();
					}
				}
			}
		}
		else {
			Toast.makeText( getActivity(), "Please try again ...", Toast.LENGTH_SHORT ).show();
			super.onActivityResult( requestCode, resultCode, intent );
		}
	}

	@Override
	public String getScreenName() {
		return "Utilities Listing";
	}
}
/*public class UtilitiesFragment extends BaseFragment implements View.OnClickListener, PermissionUtils.PermissionCallback {

    TextView btnCurrencyConverter,btnShoeSizeConverter,btnQrScanner,btnFashionConverter;
    public static UtilitiesFragment newInstance(PermissionUtils.PermissionProcessor processor) {
        UtilitiesFragment fragment = new UtilitiesFragment();
        processor.setPermissionCallback(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_utilities,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("UTILITIES");
        ((HomeActivity)getActivity()).setPermissionCallback(this);
        btnCurrencyConverter= (TextView) view.findViewById(R.id.btnCurrencyConverter);
        btnShoeSizeConverter= (TextView) view.findViewById(R.id.btnShoeSizeConverter);
        btnQrScanner= (TextView) view.findViewById(R.id.btnQrScanner);
        btnFashionConverter= (TextView) view.findViewById(R.id.btnFashionConverter);

        btnShoeSizeConverter.setOnClickListener(this);
        btnCurrencyConverter.setOnClickListener(this);
        btnQrScanner.setOnClickListener(this);
        btnFashionConverter.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Animation animRightLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_to_left);
        Animation animLeftRight = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_to_right);
        btnShoeSizeConverter.startAnimation(animLeftRight);
        btnFashionConverter.startAnimation(animRightLeft);
        btnCurrencyConverter.startAnimation(animLeftRight);
        btnQrScanner.startAnimation(animRightLeft);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCurrencyConverter:
                getActivity().startActivity(new Intent(getActivity(), CurrencyConverterActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnShoeSizeConverter:
                getActivity().startActivity(new Intent(getActivity(), ShoeSizeConverterActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnQrScanner:
                PermissionUtils.requestIfNotGranted(getActivity(), Manifest.permission.CAMERA, 0, UtilitiesFragment.this);
               *//* getActivity().startActivity(new Intent(getActivity(), ZBarScannerActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);*//*
                break;

            case R.id.btnFashionConverter:
                getActivity().startActivity(new Intent(getActivity(), FashionConverterActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

       *//* Configuration config = new Configuration();
        Locale locale = new Locale("zh");
        Locale.setDefault(locale);
        config.locale = locale;
        getResources().updateConfiguration(config, null);
*//*
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(getActivity(),"Please try again ...", Toast.LENGTH_SHORT).show();
            } else {
                Log.e("QRSCANNINGNEW", "Scanned: " + result.getContents());
                if(result.getContents().contains("BEGIN:VCARD")){
                    QRLogic.parseVcard(getActivity(), result.getContents());
                }else if(result.getContents().contains("SMSTO")){
                    QRLogic.sendSms(getActivity(),result.getContents());
                }else if(result.getContents().contains("MATMSG")){
                    QRLogic.sendEmail(getActivity(), result.getContents());
                }else if(result.getContents().startsWith("tel:")){
                    startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse(result.getContents())));
                }else{

                    if(result.getContents().startsWith("www") || result.getContents().startsWith("http")){
                        if(result.getContents().startsWith("http")){
                            startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                                    .putExtra("url", result.getContents())
                                    .putExtra("title", "UTILITIES")
                                    .putExtra("category", Constant.DONT_SEND));
                        }else{
                            startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                                    .putExtra("url", "https://"+result.getContents())
                                    .putExtra("title", "UTILITIES")
                                    .putExtra("category", Constant.DONT_SEND));
                        }
                    }else{
                        Toast.makeText(getActivity(), result.getContents(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        } else {
            Toast.makeText(getActivity(),"Please try again ...",Toast.LENGTH_SHORT).show();
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public String getScreenName() {
        return "Utilities Listing";
    }

    @Override
    public void onPermissionResult(int requestCode, boolean granted) {
        if (granted) {
            openScanner();
        }
    }
    public void openScanner() {
        IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setPrompt("");
        integrator.setBeepEnabled(false);
        integrator.setOrientationLocked(false);
        integrator.initiateScan();
    }
}*/
