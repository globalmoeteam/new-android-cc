package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader.QueryClient;
import com.belongi.citycenter.emsp.EmspHelper;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.activity.services.ATMServiceActivity;
import com.belongi.citycenter.ui.activity.services.ServiceDetailActivity;
import com.belongi.citycenter.ui.adapters.ServiceAdapter;

import java.util.List;

/**
 * Created by webwerks on 26/6/15.
 */
public class ServiceFragment extends BaseFragment implements QueryClient, AdapterView.OnItemClickListener, BackgroundJobClient {

    List<BaseService> serviceList;
    GridView gridService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("SERVICES");

        if (App.EMSP_INITIALIZED) {
            if (Validation.Network.isConnected(getActivity())) {
                // show General Ads

                EmspHelper.showOverlayAd(getActivity(), "services");
                // EmspHelper.showFullScreenAd(getActivity(), "services");

                //show Location based Ads
                if (App.EMSP_LOCATION_REGISTERED) {

                    EmspHelper.showLocBasedOverlayAd(getActivity(), "services");
                    //  EmspHelper.showLocBasedFullscreenAd(getActivity(), "services");
                }
            }
        }

        gridService = (GridView) view.findViewById(R.id.gridService);
        gridService.setOnItemClickListener(this);
        new LocalDataLoader(this, WebApi.REQ_GET_SERVICE).execute();
    }

    @Override
    public Object executeQuery() {
        return EntityUtils.getAllGuestService();
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        if (result != null) {
            switch (requestCode) {
                case WebApi.REQ_GET_SERVICE:
                    serviceList = (List<BaseService>) result;

                    if (EntityUtils.getAtmListCount() > 0) {
                        Service serAtm = new Service();
                        serAtm.setServiceTitle("ATM'S");
                        serviceList.add(serAtm);
                    }

                    for (int i = 0; i < serviceList.size(); i++) {
                        //  Log.e("SERVICEFRAG",((Service) serviceList.get(i)).getPosition()  + "");
                        String iconName = ((Service) serviceList.get(i)).getServiceTitle().replaceAll(" ", "_").replaceAll("-", "_").replaceAll("&_", "").replaceAll("'", "").replace("/_", "").toLowerCase();
                        Log.e("SERVCIEFRAG", iconName);
                        int iconId;
                        if (iconName != null) {
                            if (iconName.equals("gift_vouchers")) {
                                iconId = getResources().getIdentifier("gift_card", "drawable", getActivity().getPackageName());
                            } else if (iconName.equals("metro_connection")) {
                                iconId = getResources().getIdentifier("metro", "drawable", getActivity().getPackageName());
                            } else if (iconName.equals("public_pay_phones")) {
                                iconId = getResources().getIdentifier("public_phone_booth", "drawable", getActivity().getPackageName());
                            } else if (iconName.equals("tax_free_services")) {
                                iconId = getResources().getIdentifier("taxi_bays", "drawable", getActivity().getPackageName());
                            } else if (iconName.equals("gift_card_services")) {
                                iconId = getResources().getIdentifier("gift_card", "drawable", getActivity().getPackageName());
                            } else if (iconName.equals("wheelchairs")) {
                                iconId = getResources().getIdentifier("wheel_chair", "drawable", getActivity().getPackageName());
                            } else {
                                iconId = getResources().getIdentifier(iconName, "drawable", getActivity().getPackageName());
                            }
                            ((Service) serviceList.get(i)).setServiceIcon(iconId);
                        }
                    }
                    gridService.setAdapter(new ServiceAdapter(getActivity(), serviceList));
                    gridService.setLayoutAnimation(new LayoutAnimationController
                            (AnimationUtils.loadAnimation(getActivity(), R.anim.animation_translate_in)));
                    break;
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return (isAdded() && isVisible());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        BaseService selectedService = (BaseService) parent.getItemAtPosition(position);

        if (selectedService.getServicePageTitle().equals("ATM'S")) {
            startActivity(new Intent(getActivity(), ATMServiceActivity.class));
            getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        } else {
            Log.e("onItemClick", "" + selectedService.getServicePageTitle());
            if (selectedService.getServicePageTitle().equalsIgnoreCase("Shuttle Bus Services")) {
                if (App.get().getSelectedMall().getMallName().equalsIgnoreCase("City Centre Mirdif")) {
                    startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                            .putExtra("url", "http://www.citycentremirdif.com/mobile/shuttle-bus-service")
                            .putExtra("title", selectedService.getServicePageTitle())
                            .putExtra("category", "Service - " + selectedService.getServicePageTitle()));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else if (App.get().getSelectedMall().getMallName().equalsIgnoreCase("City Centre Deira")) {

                    startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                            .putExtra("url", "http://www.citycentredeira.com/mobile/shuttle-bus-service")
                            .putExtra("title", selectedService.getServicePageTitle())
                            .putExtra("category", "Service - " + selectedService.getServicePageTitle()));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else {
                    startActivity(new Intent(getActivity(), ServiceDetailActivity.class).putExtra("Model", selectedService));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
            } else {
                startActivity(new Intent(getActivity(), ServiceDetailActivity.class).putExtra("Model", selectedService));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }
        }
    }

    @Override
    public String getScreenName() {
        return "Services";
    }
}