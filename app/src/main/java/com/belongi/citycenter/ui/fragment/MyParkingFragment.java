package com.belongi.citycenter.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.utilities.FileUtils;
import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Parkings;
import com.belongi.citycenter.data.entities.utils.ParkingDAO;
import com.belongi.citycenter.emsp.EmspHelper;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.PermissionUtils;
import com.belongi.citycenter.ui.activity.AddParkingActivity;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.adapters.ParkingsAdapter;

import java.util.List;

/**
 * Created by webwerks on 24/6/15.
 */
public class MyParkingFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener,PermissionUtils.PermissionCallback {

    ListView lstParkings;
    ParkingsAdapter parkingsAdapter;
    List<Parkings> parkingsList;
    String path = null;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("MY PARKING");

        if(App.EMSP_INITIALIZED){
            if(Validation.Network.isConnected(getActivity())){
                // show General Ads

                EmspHelper.showOverlayAd(getActivity(),"myparking");
               // EmspHelper.showFullScreenAd(getActivity(), "myparking");

                //show Location based Ads
                if(App.EMSP_LOCATION_REGISTERED){

                    EmspHelper.showLocBasedOverlayAd(getActivity(),"myparking");
                  //  EmspHelper.showLocBasedFullscreenAd(getActivity(), "myparking");
                }
            }
        }

        lstParkings = (ListView) view.findViewById(R.id.lstParkings);
        ((HomeActivity)getActivity()).setPermissionCallback(this);
        View footerLayout = LayoutInflater.from(getActivity()).inflate(R.layout.footer_my_parkings, null);
        footerLayout.findViewById(R.id.btnAddMore).setOnClickListener(this);
        lstParkings.addFooterView(footerLayout);
        lstParkings.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        parkingsList = ParkingDAO.getAllParkings();
        parkingsAdapter = new ParkingsAdapter(getActivity(), parkingsList, this);
        lstParkings.setAdapter(parkingsAdapter);
        lstParkings.setLayoutAnimation(new LayoutAnimationController
                (AnimationUtils.loadAnimation(getActivity(), R.anim.animation_translate_in)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_parking, null);
    }

    public void setImagePath(String path) {
        this.path = path;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == 150) {
            if (resultCode == Activity.RESULT_OK && path != null) {
                Parkings model = new Parkings();
                model.setImagePath(path);
                startActivity(new Intent(getActivity(), AddParkingActivity.class).putExtra("Model", model));
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), getResources().getString(R.string.msgCancel), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.msgFailed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static final String[] CAMERA_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddMore:
                PermissionUtils.requestIfNotGranted(getActivity(),CAMERA_PERMISSIONS,0,MyParkingFragment.this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        Parkings selectedModel = (Parkings) parent.getItemAtPosition(position);
        selectedModel.setIdentifier(selectedModel.getId());
        startActivity(new Intent(getActivity(), AddParkingActivity.class).putExtra("Model", selectedModel));
        getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    @Override
    public String getScreenName() {
        return "My Parking";
    }

    @Override
    public void onPermissionResult(int requestCode, boolean granted) {
        if(granted){
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri fileUri = FileUtils.getOutputMediaFileUri(1, "CityCentre");
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            path = fileUri.getPath();
            startActivityForResult(cameraIntent, 150);
        }
    }
}
