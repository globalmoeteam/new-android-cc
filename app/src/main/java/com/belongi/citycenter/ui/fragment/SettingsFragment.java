package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.activity.MallSelectionActivity;
import com.belongi.citycenter.ui.activity.MyFavActivity;

/**
 * Created by webwerks on 14/6/15.
 */
public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    TextView lblNotification,lblFavorites,lblChangeMall;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        setActionbarTitle("SETTINGS");
        lblNotification= (TextView) view.findViewById(R.id.lblNotificationMangement);
        lblFavorites= (TextView) view.findViewById(R.id.lblFavorites);
        lblChangeMall= (TextView) view.findViewById(R.id.lblChangeMall);
        lblNotification.setOnClickListener(this);
        lblFavorites.setOnClickListener(this);
        lblChangeMall.setOnClickListener(this);
        final Animation animNotification = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_to_left);
        final Animation animFavourite = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_to_left);
        final Animation animChangeMall = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_to_left);

        animNotification.setDuration(300);
        animFavourite.setDuration(300);
        animChangeMall.setDuration(300);

        Animation.AnimationListener animationListener=new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                lblFavorites.setVisibility(View.VISIBLE);
                animFavourite.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        lblChangeMall.setVisibility(View.VISIBLE);
                        lblChangeMall.startAnimation(animChangeMall);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                lblFavorites.startAnimation(animFavourite);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };

        animNotification.setAnimationListener(animationListener);
        lblNotification.startAnimation(animNotification);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lblNotificationMangement :
                ((HomeActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        NotificationFragment.newInstance(false),R.id.container,true);
                break;
            case R.id.lblFavorites:
                getActivity().startActivity(new Intent(getActivity(), MyFavActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.lblChangeMall:
                getActivity().startActivity(new Intent(getActivity(), MallSelectionActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Settings List";
    }
}
