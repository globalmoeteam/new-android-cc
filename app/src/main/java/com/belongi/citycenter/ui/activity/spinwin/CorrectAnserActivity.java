package com.belongi.citycenter.ui.activity.spinwin;

import android.widget.ImageView;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by webwerks on 22/6/15.
 */
public class CorrectAnserActivity extends BaseActivity {
    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_correct_answer);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
       // Campaigns.Compaign  contest= (Campaigns.Compaign) getIntent().getSerializableExtra(Constant.SELECTED_CONTEST);
        Campaigns.Compaign  contest= App.get().getSelectedContest();
        TextView lblWinTitle= (TextView) findViewById(R.id.lblWinTitle);
        Picasso.with(this).load(contest.getCampaignThmb()).placeholder(R.drawable.place_holder_event1_6p).into((ImageView) findViewById(R.id.imgContest));
        lblWinTitle.setText(lblWinTitle.getText().toString() + "" +contest.getCampaignTitle());
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN";
    }
}
