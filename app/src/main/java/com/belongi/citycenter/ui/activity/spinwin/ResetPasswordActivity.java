package com.belongi.citycenter.ui.activity.spinwin;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.ForgotPassword;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 17/6/15.
 */
public class ResetPasswordActivity extends BaseActivity implements View.OnClickListener,BackgroundJobClient{

    EditText txtEmail,txtSecurityCode,txtPassword,txtConfirmPwd;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_reset_password);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("RESET PASSWORD");
        txtEmail= (EditText) findViewById(R.id.txtEmail);
        txtSecurityCode= (EditText) findViewById(R.id.txtSecurityCode);
        txtPassword= (EditText) findViewById(R.id.txtPassword);
        txtConfirmPwd= (EditText) findViewById(R.id.txtConfirmPwd);
        findViewById(R.id.btnReset).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnReset:
                if(validate()){
                    SpinAndWinLogic.resetPassword(ResetPasswordActivity.this,txtEmail.getText().toString().trim(),
                            txtSecurityCode.getText().toString().trim(),
                            txtPassword.getText().toString().trim());
                    showProgressLoading();
                }
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN Reset Password";
    }

    public boolean validate(){
        if(!Validation.isValidEmail(txtEmail.getText().toString().trim())){
            txtEmail.setError("Please enter a valid email address.");
            txtEmail.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(txtSecurityCode.getText().toString().trim())){
            txtSecurityCode.setError("Please enter security code");
            txtSecurityCode.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(txtPassword.getText().toString().trim())){
            txtPassword.setError("Please enter password");
            txtPassword.requestFocus();
            txtPassword.requestFocus();
            return false;
        }

        if(!txtPassword.getText().toString().trim().equals(txtConfirmPwd.getText().toString().trim())){
            txtConfirmPwd.setError("The Confirm password does not match the Password.");
            txtConfirmPwd.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        stopLoading();
        if(result!=null) {
            ForgotPassword respo= (ForgotPassword) ContentDownloader.getObjectFromJson(result.toString(),ForgotPassword.class);
            if(TextUtils.isEmpty(respo.getError())){
                Validation.UI.showLongToast(this,"The password has been reset successfully.");
                finish();
            }else{
                Validation.UI.showLongToast(this,respo.getError());
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }
}
