package com.belongi.citycenter.ui.view.swipelistview;

public class SimpleSwipeListener implements SwipeLayout.SwipeListener {

   /* @Override
    public void onStartOpen(com.daimajia.swipe.SwipeLayout layout) {
    }

    @Override
    public void onOpen(com.daimajia.swipe.SwipeLayout layout) {
    }

    @Override
    public void onStartClose(com.daimajia.swipe.SwipeLayout layout) {
    }

    @Override
    public void onClose(com.daimajia.swipe.SwipeLayout layout) {
    }

    @Override
    public void onUpdate(com.daimajia.swipe.SwipeLayout layout, int leftOffset, int topOffset) {
    }

    @Override
    public void onHandRelease(com.daimajia.swipe.SwipeLayout layout, float xvel, float yvel) {
    }*/

    @Override
    public void onStartOpen(SwipeLayout layout) {

    }

    @Override
    public void onOpen(SwipeLayout layout) {

    }

    @Override
    public void onStartClose(SwipeLayout layout) {

    }

    @Override
    public void onClose(SwipeLayout layout) {

    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

    }
}
