package com.belongi.citycenter.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.utilities.FileUtils;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Parkings;
import com.belongi.citycenter.data.entities.utils.ParkingDAO;
import com.belongi.citycenter.ui.activity.AddParkingActivity;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.fragment.MyParkingFragment;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by webwerks on 24/6/15.
 */

public class ParkingsAdapter extends ArrayAdapter<Parkings> implements View.OnClickListener {

    Context mContext;
    Fragment frag;
    List<Parkings> parkingsList;

    public ParkingsAdapter(Context context, List<Parkings> objects, Fragment frag) {
        super(context, 0, objects);
        mContext = context;
        this.frag = frag;
        this.parkingsList = objects;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDelete:

                Log.e("Enter","Delete " + ((Parkings) view.getTag()).getId());

                Parkings delParking = (Parkings) view.getTag();
                delParking.setIdentifier(delParking.getId());
                ParkingDAO.deleteParking(delParking);
                parkingsList.remove(delParking);
                notifyDataSetChanged();
                break;
        }
    }

    class ViewHolder {
        TextView lblCreatedDate, lblTitle;
        ImageView imgPark;
        Button btnDelete;

        public ViewHolder(View convertView) {
            imgPark = (ImageView) convertView.findViewById(R.id.imgPark);
            lblCreatedDate = (TextView) convertView.findViewById(R.id.lblCreatedDate);
            lblTitle = (TextView) convertView.findViewById(R.id.lblTitle);
            btnDelete = (Button) convertView.findViewById(R.id.btnDelete);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_parking, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.lblTitle.setText(getItem(position).getTitle());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date();
        date.setTime(Long.parseLong(getItem(position).getCreatedDate()));
        String formatted = dateFormat.format(date);
        holder.lblCreatedDate.setText(formatted);

        if (getItem(position).getImagePath() != null) {
            Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
            Picasso.with(mContext).load(new File(getItem(position).getImagePath())).resize(display.getWidth(), 400).centerCrop().into(holder.imgPark);
        }

        holder.btnDelete.setTag(getItem(position));
        holder.btnDelete.setOnClickListener(this);
        return convertView;
    }
}
