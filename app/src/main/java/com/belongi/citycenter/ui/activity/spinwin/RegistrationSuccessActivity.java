package com.belongi.citycenter.ui.activity.spinwin;

import android.content.Intent;
import android.view.View;

import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.fragment.SpinToWinFragment;

/**
 * Created by webwerks on 17/6/15.
 */
public class RegistrationSuccessActivity extends BaseActivity implements View.OnClickListener {

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnSpinandWin:
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN Registration Success";
    }

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_registration_success);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
        findViewById(R.id.btnSpinandWin).setOnClickListener(this);
    }
}
