package com.belongi.citycenter.ui.activity;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.activeandroid.query.Select;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.SpotlightBanner;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.ui.activity.services.ServiceDetailActivity;
import com.belongi.citycenter.ui.view.viewpagerindicator.CirclePageIndicator;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yashesh on 6/7/15.
 */
public class SpotlightDetailsActivity extends  BaseActivity implements View.OnClickListener{

    ViewPager viewPager;
    public static final String SPOTLIGHT_INDEX="INDEX";

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_spotlight_details);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("WHAT'S HAPPENING");

        viewPager= (ViewPager) findViewById(R.id.pager);
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        PagerAdapter adapter=new PagerAdapter() {
            @Override
            public int getCount() {
                return App.get().getHomeScreenData().getSpotlightBanners().length;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == (FrameLayout)object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                View view=  LayoutInflater.from(SpotlightDetailsActivity.this).inflate(R.layout.layout_touchimageview, null);

                String url=App.get().getHomeScreenData().getSpotlightBanners()[position].getImage();

                if (!url.startsWith("http")) {
                    url= WebApi.BASE_HOAST_URL+url;
                }
                Picasso.with(SpotlightDetailsActivity.this).load(url)
                        .placeholder(R.drawable.place_holder_event1_6p)
                        .error(R.drawable.place_holder_event1_6p)
                        .into((ImageView) view.findViewById(R.id.imgSpotLight));
                container.addView(view);
                view.findViewById(R.id.imgSpotLight).setOnClickListener(SpotlightDetailsActivity.this);
                view.findViewById(R.id.imgSpotLight).setTag(App.get().getHomeScreenData().getSpotlightBanners()[position]);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((FrameLayout) object);
            }
        };

        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        // Toast.makeText(this,spotlightBanners.length+"...",Toast.LENGTH_SHORT).show();
        //  viewPager.setCurrentItem(getIntent().getIntExtra(SPOTLIGHT_INDEX,0));
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()){
            case R.id.imgSpotLight:
               final SpotlightBanner banner= (SpotlightBanner) view.getTag();
                String mode=banner.getMode();
                if(mode!=null){
                    if(mode.contains("url")){
                        Log.e("SpotlightBanner URL",banner.getUrl());
                        Intent intent=new Intent(SpotlightDetailsActivity.this,WebContentPreviewActivity.class);
                        intent.putExtra("title", "WHAT'S HAPPENING");
                        String link= banner.getUrl();

                        if(TextUtils.isEmpty(link)){
                            link=WebApi.BASE_HOAST_URL;
                        }else if(!link.startsWith("http")){
                            link= WebApi.BASE_HOAST_URL +link;
                        }
                        intent.putExtra("url",link);
                        intent.putExtra("category","WHAT'S HAPPENING");
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);

                    }else if(mode.contains("media")){
                        // do nothing read only image.
                    }else{
                        new LocalDataLoader(new LocalDataLoader.QueryClient() {
                            @Override
                            public Object executeQuery() {
                                List<Search> searchResult=new Select().from(Search.class).where("identifier = ?",new String[]{banner.getID()}).execute();
                                if(searchResult!=null && searchResult.size()>0){
                                    return searchResult.get(0);
                                }
                                return null;
                            }

                            @Override
                            public void onBackgroundJobComplete(int requestCode, Object result) {
                                    if(result!=null){
                                        Search item= (Search) result;
                                        ////////////////////////////////////////////////////////////////////////////////////
                                        Serializable object=null;
                                        Log.w("CATEGORY :", item.getIdentifier() + "..." + item.getCategory());

                                        if(item.getCategory().equals("HOTEL")) {

                                            List<Hotel> hotelShops = new Select().from(Hotel.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
                                            if (hotelShops != null && hotelShops.size() > 0) {
                                                object = hotelShops.get(0);
                                            }

                                        }else
                                        if(item.getCategory().equals("DINING")) {
                                            List<DiningShop> diningShops = new Select().from(DiningShop.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
                                            if (diningShops != null && diningShops.size() > 0) {
                                                object = diningShops.get(0);
                                            }
                                        }else
                                        if(item.getCategory().equals("SHOPPING")) {
                                            Log.e("GETTING DATA",""+item.getIdentifier());
                                            List<ShoppingShop> shoppingShops = new Select().from(ShoppingShop.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
                                            if (shoppingShops != null && shoppingShops.size() > 0) {
                                                object = shoppingShops.get(0);
                                            }
                                        }else
                                        if(item.getCategory().equals("EVENT")) {
                                            List<Event> events = new Select().from(Event.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
                                            if (events != null && events.size() > 0) {
                                                object = events.get(0);
                                            }
                                        }else
                                        if(item.getCategory().equals("OFFER")) {
                                            List<Offer> offers = new Select().from(Offer.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
                                            if (offers != null && offers.size() > 0) {
                                                object = offers.get(0);
                                            }
                                        }else
                                        if(item.getCategory().equals("ENTERTAINMENT")) {
                                            List<Entertainment> entertainments = new Select().from(Entertainment.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
                                            if (entertainments != null && entertainments.size() > 0) {
                                                object = entertainments.get(0);
                                            }

                                        }else
                                        if(item.getCategory().equals("SERVICE")) {
                                            List<BaseService> service=new Select().from(Service.class).where("identifier = ?",new String[]{item.getIdentifier()}).execute();
                                            if(service != null && service.size() > 0){
                                                object=service.get(0);
                                            }
                                        }

                                        Intent detailIntent;
                                        if(!item.getCategory().equals("SERVICE")){
                                            detailIntent=new Intent(SpotlightDetailsActivity.this, ShopDetailActivity.class);
                                            detailIntent.putExtra(Constant.DETAIL_SHOP_TYPE, object);
                                        }else{
                                            detailIntent=new Intent(SpotlightDetailsActivity.this, ServiceDetailActivity.class);
                                            detailIntent.putExtra("Model",object);
                                        }
                                        //Intent detailIntent=new Intent(this, ShopDetailActivity.class);

                                        startActivity(detailIntent);
                                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                                        /////////////////////////////////////////////////////////////////////////

                                    }else{
                                        Log.e("SpotlightBanner Content",banner.getUrl());
                                        Intent intent=new Intent(SpotlightDetailsActivity.this,WebContentPreviewActivity.class);
                                        intent.putExtra("title", "WHAT'S HAPPENING");
                                        String link= banner.getUrl();

                                        if(TextUtils.isEmpty(link)){
                                            link=WebApi.BASE_HOAST_URL;
                                        }else if(!link.startsWith("http")){
                                            link= WebApi.BASE_HOAST_URL +link;
                                        }
                                        intent.putExtra("url",link);
                                        intent.putExtra("category","WHAT'S HAPPENING");
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                                    }
                            }

                            @Override
                            public void onBackgroundJobAbort(int requestCode, Object reason) {

                            }

                            @Override
                            public void onBackgroundJobError(int requestCode, Object error) {

                            }

                            @Override
                            public boolean needAsyncResponse() {
                                return true;
                            }

                            @Override
                            public boolean needResponse() {
                                return true;
                            }
                        },0).execute();

                    }
                }
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "What's Happening";
    }
}
