package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.MetroMapActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.activity.services.ServiceDetailActivity;

import java.util.List;

/**
 * Created by webwerks on 26/6/15.
 */
public class BookTaxiFragment extends BaseFragment implements View.OnClickListener,LocalDataLoader.QueryClient{

    public static final int REQ_BUS_SERVICE=48;
    LinearLayout llShuttleBus;
    List<BaseService> busService;
    View viewShuttleBus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_book_taxi,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("BOOK A TAXI");
        view.findViewById(R.id.lblTransport).setOnClickListener(this);
      /*  view.findViewById(R.id.lblMetroTaxi).setOnClickListener(this);
        view.findViewById(R.id.lblNationalTaxi).setOnClickListener(this);*/
        view.findViewById(R.id.lblCityTaxi).setOnClickListener(this);
        view.findViewById(R.id.lblGhazalTaxi).setOnClickListener(this);
        view.findViewById(R.id.btnMetroTiming).setOnClickListener(this);
        view.findViewById(R.id.btnMetroMap).setOnClickListener(this);
        view.findViewById(R.id.btnBigBusMoreInfo).setOnClickListener(this);
        view.findViewById(R.id.btnShuttleBusMoreInfo).setOnClickListener(this);
        viewShuttleBus=view.findViewById(R.id.viewShuttleBus);
        llShuttleBus= (LinearLayout) view.findViewById(R.id.llShuttleBus);

        new LocalDataLoader(this,REQ_BUS_SERVICE).execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lblTransport:
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:600 522264")));
                break;

           /* case R.id.lblMetroTaxi:
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:600 566000")));
                break;

            case R.id.lblNationalTaxi:
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel: 600 543322")));
                break;*/

            case R.id.lblCityTaxi:
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:600 52 52 52")));
                break;

            case R.id.lblGhazalTaxi:
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:02-4447787")));
                break;

            case R.id.btnMetroTiming:
                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                        .putExtra("url", Constant.METRO_TIMING)
                        .putExtra("title","METRO TIMING")
                        .putExtra("category","Book A Taxi - Metro Timing"));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnMetroMap:
                startActivity(new Intent(getActivity(), MetroMapActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnBigBusMoreInfo:
                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                        .putExtra("url",Constant.BIG_BUS)
                        .putExtra("title","BY BIG BUS")
                        .putExtra("category","Book A Taxi - By Big Bus"));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnShuttleBusMoreInfo:
                startActivity(new Intent(getActivity(), ServiceDetailActivity.class)
                        .putExtra("Model",busService.get(0))
                        .putExtra("title","SHUTTLE BUS INFO"));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public Object executeQuery() {
        return EntityUtils.getBusService();
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        if(result!=null){
            busService= (List<BaseService>) result;
            if(busService!=null && busService.size()>0) {
                viewShuttleBus.setVisibility(View.VISIBLE);
                llShuttleBus.setVisibility(View.VISIBLE);
            }
            Log.e("Bus Service", result.toString());
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    @Override
    public String getScreenName() {
        return "Book A Taxi";
    }
}
