package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.SpotlightBanner;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.view.horizontalgrid.TwoWayAbsListView;
import com.belongi.citycenter.ui.view.horizontalgrid.TwoWayGridView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by yashesh on 6/22/2015.
 */
public class HomeTopGridAdapter extends ArrayAdapter{

    Context mContext;



    public HomeTopGridAdapter(Context context) {
        super(context, 0, App.get().getHomeScreenData().getOffersAndEvents());
        mContext=context;
        Toast.makeText(mContext,getCount()+"",Toast.LENGTH_SHORT).show();
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(getItem(position) instanceof Offer){
            // populate event view
            ImageView imageView=new ImageView(mContext);
            imageView.setLayoutParams(new TwoWayAbsListView.LayoutParams(450, 450));
            if(((Offer)getItem(position)).getImage_URL().length()>0) {
                Picasso.with(mContext).load("http://www.theinquirer.net/IMG/331/193331/google-android-logo-green-black.jpg").into(imageView);
            }

            return imageView;
        }else if(getItem(position) instanceof Event){
            //populate offer view
            ImageView imageView=new ImageView(mContext);
            imageView.setLayoutParams(new TwoWayAbsListView.LayoutParams(450, 450));
            if(((Event)getItem(position)).getEvent_Image().length()>0) {
                Picasso.with(mContext).load("http://www.theinquirer.net/IMG/331/193331/google-android-logo-green-black.jpg").into(imageView);
            }
            return imageView;

        }else{
            //populate soptlight view
            ImageView imageView=new ImageView(mContext);
            imageView.setLayoutParams(new TwoWayAbsListView.LayoutParams(450, 450));
            if(((SpotlightBanner[]) getItem(position))[0].getImage().length()>0) {
                Picasso.with(mContext).load("http://www.theinquirer.net/IMG/331/193331/google-android-logo-green-black.jpg").into(imageView);
            }

            return imageView;

        }











    }
}
