package com.belongi.citycenter.ui.activity;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.joanzapata.pdfview.PDFView;

import java.io.File;

/**
 * Created by webwerks on 13/7/15.
 */
public class PDFViewActivity extends  BaseActivity{

    PDFView pdfView;

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_pdf_view);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("FEATURED PRODUCT");
        pdfView= (PDFView) findViewById(R.id.pdfview);
        pdfView.fromFile(new File( getIntent().getStringExtra(Constant.PDF_FILE))).swipeVertical(true)
                .showMinimap(false)
                .enableSwipe(true)
                .load();
    }

    @Override
    public String getScreenName() {
        return "Featured Products";
    }
}
