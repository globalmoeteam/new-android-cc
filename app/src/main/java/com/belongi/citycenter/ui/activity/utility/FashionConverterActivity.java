package com.belongi.citycenter.ui.activity.utility;

import android.support.v4.app.FragmentManager;
import android.view.MenuItem;

import com.belongi.citycenter.R;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.fragment.fashionconverter.FashionOptionFragment;


public class FashionConverterActivity extends BaseActivity {

	@Override
	protected void releaseUi() {

	}

	@Override
	protected void setContent() {
		setContentView(R.layout.activity_shoe_converter);
	}

	@Override
	protected void initializeUi() {
		super.initializeUi();
		setActionbarTitle("FASHION CONVERTER");
		fragmentTransaction(REPLACE_FRAGMENT,new FashionOptionFragment(), R.id.container,false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				FragmentManager fm = getSupportFragmentManager();
				if (fm.getBackStackEntryCount() > 0) {
					fm.popBackStack();
				}else{
					super.onOptionsItemSelected(item);
				}
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public String getScreenName() {
		return "Fashion Converter";
	}
}
