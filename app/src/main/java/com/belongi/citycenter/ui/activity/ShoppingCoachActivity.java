package com.belongi.citycenter.ui.activity;

import android.view.View;

import com.android.utilities.Preferences;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;

/**
 * Created by webwerks on 17/8/15.
 */
public class ShoppingCoachActivity extends BaseActivity implements View.OnClickListener{
    @Override
    public String getScreenName() {
        return Constant.DONT_SEND;
    }

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_coach);
    }

    @Override
    protected void initializeUi() {
        findViewById(R.id.layout_coach).setBackgroundResource(R.drawable.shopping_coach);
        findViewById(R.id.btnSkip).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //Preferences.setBoolean(Constant.PREF_NAME, ShoppingCoachActivity.this, Constant.PREF_KEY_SHOPPING, false);
        setActionbarTitle("SHOPPING");
        finish();
    }
}
