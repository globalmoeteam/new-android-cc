package com.belongi.citycenter.ui.fragment.fashionconverter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.utility.FashionConverterActivity;
import com.belongi.citycenter.ui.fragment.BaseFragment;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by webwerks on 15/6/15.
 */
public class FashionTypeFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    ListView lstFashionType;
    FashionTypeAdapter adapter;
    String type = null;
    String[] tmp = null;


    public static FashionTypeFragment newInstance(String type) {
        FashionTypeFragment fragment = new FashionTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragement_fashion_types,null);
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lstFashionType = (ListView)view.findViewById(R.id.lstConverterType);

        type = getArguments().getString("type");
        //setActionbarTitle("CATEGORIES");
        if(type.equals("Men")) {
            setActionbarTitle("MEN");
            tmp = new String[]{"Suits", "Shirts", "Waist", "Collar"};
        }else if(type.equals("Women")) {
            setActionbarTitle("WOMEN");
            tmp = new String[]{"Bra", "Dresses"};
        }else if(type.equals("Children")) {
            setActionbarTitle("KIDS");
            tmp = new String[]{"Clothing"};
        }

        adapter = new FashionTypeAdapter(tmp);
        lstFashionType.setAdapter(adapter);
        lstFashionType.setLayoutAnimation(new LayoutAnimationController
                (AnimationUtils.loadAnimation(getActivity(), R.anim.animation_translate_in)));
        lstFashionType.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ((FashionConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                FashionConverterFragment.newInstance(type,tmp[position]),R.id.container, true);
    }

    @Override
    public String getScreenName() {
        if(getArguments().getString("type").equals("Men")) {
            return "Utilities - Men  Fashion Converter";
        }else if(getArguments().getString("type").equals("Women")) {
            return "Utilities - Women Fashion Converter";
        }else if(getArguments().getString("type").equals("Children")) {
            return "Utilities - Kids Fashion Converter";
        }else{
            return Constant.DONT_SEND;
        }
    }

    class FashionTypeAdapter extends ArrayAdapter<String> {

        public FashionTypeAdapter(String[] objects) {
            super(getActivity(), 0, objects);
            sort();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(android.R.layout.simple_list_item_1,null);
            }
            TextView lblTitle= (TextView) convertView.findViewById(android.R.id.text1);
            lblTitle.setPadding(10,10,10,10);
            lblTitle.setTextColor(getActivity().getResources().getColor(R.color.text_color));
            lblTitle.setBackgroundResource(R.drawable.row_background);
            lblTitle.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_6,0);
            lblTitle.setText(getItem(position).toUpperCase());
            return convertView;
        }

        public void sort(){
            Collections.sort(Arrays.asList(tmp), new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return lhs.compareTo(rhs); //acs
                }
            });
        }
    }

}
