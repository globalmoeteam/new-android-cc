package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.activity.themall.MallContentPageActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by webwerks on 16/6/15.
 */
public class TheMallFragment extends BaseFragment implements View.OnClickListener,LocalDataLoader.QueryClient/*,View.OnTouchListener*/{

    MallContentPage[] arrContentPage=null;
    MallContentPage openingHour,contactDetails,facilities,gettingHere;

    ImageView imgOpening,imgContact,imgFacilities,imgGetting;
    TextView txtOpening,txtContact,txtFacilities,txtGetting;

    ImageView imgTheMall;
    WebView wvMallDesc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_the_mall,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("THE MALL");

        view.findViewById(R.id.btnOeningHours).setOnClickListener(this);
        view.findViewById(R.id.btnContactUs).setOnClickListener(this);
        view.findViewById(R.id.btnFacilities).setOnClickListener(this);
        view.findViewById(R.id.btnGettingHere).setOnClickListener(this);

        imgOpening= (ImageView) view.findViewById(R.id.imgOpening);
        imgContact= (ImageView) view.findViewById(R.id.imgContact);
        imgFacilities= (ImageView) view.findViewById(R.id.imgFacilities);
        imgGetting= (ImageView) view.findViewById(R.id.imgGetting);

        txtOpening= (TextView) view.findViewById(R.id.txtOpening);
        txtContact= (TextView) view.findViewById(R.id.txtContact);
        txtFacilities= (TextView) view.findViewById(R.id.txtFacilities);
        txtGetting= (TextView) view.findViewById(R.id.txtGetting);

        imgTheMall= (ImageView) view.findViewById(R.id.imgTheMall);
        wvMallDesc= (WebView) view.findViewById(R.id.wvMallDesc);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e("Enter","ONResume");
        imgOpening.setColorFilter(Color.rgb (255, 255, 255));
        imgContact.setColorFilter(Color.rgb (255, 255, 255));
        imgFacilities.setColorFilter(Color.rgb (255, 255, 255));
        imgGetting.setColorFilter(Color.rgb(255, 255, 255));

        txtOpening.setTextColor(Color.rgb(255, 255, 255));
        txtContact.setTextColor(Color.rgb(255, 255, 255));
        txtFacilities.setTextColor(Color.rgb(255, 255, 255));
        txtGetting.setTextColor(Color.rgb(255, 255, 255));


        arrContentPage=App.get().getArrContentPage();
        if(arrContentPage!=null && arrContentPage.length>0){
            MallContentPage theMall=null;
            for(MallContentPage mallContent:arrContentPage){
                if(mallContent.getTitle().equals("The Mall")){
                    theMall=mallContent;
                }else if(mallContent.getTitle().equals("Contact Details")){
                    contactDetails=mallContent;
                }else if(mallContent.getTitle().equals("Opening Hours")){
                    openingHour=mallContent;
                }else if(mallContent.getTitle().equals("Facilities")){
                    facilities=mallContent;
                }else if(mallContent.getTitle().equals("Getting Here")){
                    gettingHere=mallContent;
                }
            }

            if(theMall!=null){
                String loadData=((BaseActivity) getActivity()).getStyleString(theMall.getPageDetail());
                String desc =  loadData.replaceAll("&lt;", "<").
                        replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                        replaceAll("&amp;", "&").replaceAll("width=\"600\"", "width=\"100%\"");

                Picasso.with(getActivity()).load(WebApi.IMAGE_BASE_URL+theMall.getImage()).placeholder(R.drawable.place_holder_event1_6p).into(imgTheMall);
                wvMallDesc.getSettings().setJavaScriptEnabled(true);
                wvMallDesc.loadDataWithBaseURL("file:///android_asset/", desc, "text/html", "UTF-8", null);
                wvMallDesc.setBackgroundColor(0x00000000);
                wvMallDesc.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                //wvMallDesc.setScrollContainer(false);
                /*wvMallDesc.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        return (motionEvent.getAction() == MotionEvent.ACTION_MOVE);
                    }
                });*/

                wvMallDesc.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url.contains("tel:")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                            startActivity(intent);
                        } else if (url.contains("mailto:")) {
                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                            emailIntent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                            emailIntent.setType("text/plain");
                            String strTo = url.replace("file:///android_asset/%22", "")
                                    .substring(url.replace("file:///android_asset/%22", "").indexOf(":") + 1,
                                            url.replace("file:///android_asset/%22", "").length());
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{strTo});
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        } else {
                            Log.e("Url", url + " \n" +
                                    url.replace("file:///android_asset/%22/", "").replace("file:///","").replace("%22", ""));
                            startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                                    .putExtra("url", url.replace("file:///android_asset/%22/", "").replace("file:///", "").replace("%22", ""))
                                    .putExtra("title", "THE MALL")
                                    .putExtra("category", Constant.DONT_SEND));
                            getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                        }
                        return true;
                    }
                });
            }
        }else{
            new LocalDataLoader(this,0);
        }
    }

    @Override
    public void onClick(View view) {
        if(arrContentPage!=null){
            switch (view.getId()){
                case R.id.btnOeningHours:
                   // imgOpening.setBa
                    imgOpening.setColorFilter(Color.rgb (129, 207, 255));
                    imgContact.setColorFilter(Color.rgb (255, 255, 255));
                    imgFacilities.setColorFilter(Color.rgb (255, 255, 255));
                    imgGetting.setColorFilter(Color.rgb(255, 255, 255));

                    txtOpening.setTextColor(Color.rgb(129, 207, 255));
                    txtContact.setTextColor(Color.rgb(255, 255, 255));
                    txtFacilities.setTextColor(Color.rgb(255, 255, 255));
                    txtGetting.setTextColor(Color.rgb(255, 255, 255));
                    getActivity().startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                            .putExtra("PageModel", openingHour));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    break;

                case R.id.btnFacilities:
                    imgOpening.setColorFilter(Color.rgb (255, 255, 255));
                    imgFacilities.setColorFilter(Color.rgb (129, 207, 255));
                    imgContact.setColorFilter(Color.rgb (255, 255, 255));
                    imgGetting.setColorFilter(Color.rgb (255, 255, 255));

                    txtFacilities.setTextColor(Color.rgb(129, 207, 255));
                    txtContact.setTextColor(Color.rgb(255, 255, 255));
                    txtOpening.setTextColor(Color.rgb(255, 255, 255));
                    txtGetting.setTextColor(Color.rgb(255, 255, 255));

                    getActivity().startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                            .putExtra("PageModel",facilities));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    break;

                case R.id.btnContactUs:
                    imgOpening.setColorFilter(Color.rgb (255, 255, 255));
                    imgContact.setColorFilter(Color.rgb (129, 207, 255));
                    imgFacilities.setColorFilter(Color.rgb (255, 255, 255));
                    imgGetting.setColorFilter(Color.rgb (255, 255, 255));

                    txtContact.setTextColor(Color.rgb(129, 207, 255));
                    txtOpening.setTextColor(Color.rgb(255, 255, 255));
                    txtFacilities.setTextColor(Color.rgb(255, 255, 255));
                    txtGetting.setTextColor(Color.rgb(255, 255, 255));

                    getActivity().startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                            .putExtra("PageModel", contactDetails));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    break;

                case R.id.btnGettingHere:
                    imgOpening.setColorFilter(Color.rgb (255, 255, 255));
                    imgContact.setColorFilter(Color.rgb (255, 255, 255));
                    imgGetting.setColorFilter(Color.rgb (129, 207, 255));
                    imgFacilities.setColorFilter(Color.rgb (255, 255, 255));

                    txtGetting.setTextColor(Color.rgb(129, 207, 255));
                    txtContact.setTextColor(Color.rgb(255, 255, 255));
                    txtFacilities.setTextColor(Color.rgb(255, 255, 255));
                    txtOpening.setTextColor(Color.rgb(255, 255, 255));

                    getActivity().startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                            .putExtra("PageModel", gettingHere));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    break;
            }
        }
    }

    @Override
    public String getScreenName() {
        return "TheMall";
    }

    @Override
    public Object executeQuery() {
            return new Select().from(MallContentPage.class).where("mallid=?",App.get().getMallId()).execute();
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        if(result!=null){
            arrContentPage= new MallContentPage[((List<MallContentPage>) result).size()];
            ((List<MallContentPage>) result).toArray(arrContentPage);
            App.get().setArrContentPage(arrContentPage);
            if(arrContentPage!=null && arrContentPage.length>0){
                MallContentPage theMall=null;
                for(MallContentPage mallContent:arrContentPage){
                    if(mallContent.getTitle().equals("The Mall")){
                        theMall=mallContent;
                    }else if(mallContent.getTitle().equals("Contact Details")){
                        contactDetails=mallContent;
                    }else if(mallContent.getTitle().equals("Opening Hours")){
                        openingHour=mallContent;
                    }else if(mallContent.getTitle().equals("Facilities")){
                        facilities=mallContent;
                    }else if(mallContent.getTitle().equals("Getting Here")){
                        gettingHere=mallContent;
                    }
                }

                if(theMall!=null){
                    Picasso.with(getActivity()).load(WebApi.IMAGE_BASE_URL+theMall.getImage()).placeholder(R.drawable.place_holder_event1_6p).into(imgTheMall);
                    wvMallDesc.getSettings().setJavaScriptEnabled(true);
                    wvMallDesc.loadDataWithBaseURL("file:///android_asset/",((BaseActivity)getActivity()).getStyleString(theMall.getPageDetail()), "text/html", "UTF-8", null);
                    wvMallDesc.setBackgroundColor(0x00000000);
                    wvMallDesc.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                    wvMallDesc.setBackgroundColor(Color.argb(1, 0, 0, 0));
                    wvMallDesc.setScrollContainer(false);
                    wvMallDesc.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            return (motionEvent.getAction() == MotionEvent.ACTION_MOVE);
                        }
                    });

                    wvMallDesc.setWebViewClient(new WebViewClient(){
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            if(url.contains("tel:")){
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                                startActivity(intent);
                            }else if(url.contains("mailto:")){
                                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                emailIntent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                                emailIntent.setType("text/plain");
                                String strTo=url.replace("file:///android_asset/%22", "")
                                        .substring(url.replace("file:///android_asset/%22", "").indexOf(":")+1,
                                                url.replace("file:///android_asset/%22", "").length());
                                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{strTo});
                                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                            }else{
                                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                                        .putExtra("url",url.replace("file:///android_asset/%22", "").replace("%22", ""))
                                        .putExtra("title","THE MALL")
                                        .putExtra("category", Constant.DONT_SEND));
                                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                            }
                            return true;
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    /*@Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (view.getId()) {
            case R.id.btnOeningHours:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Drawable mDrawable = getActivity().getResources().getDrawable(R.drawable.icon_openinghours);
                    mDrawable.setColorFilter(new PorterDuffColorFilter(0xD53F4B, PorterDuff.Mode.MULTIPLY));
                }
                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    Drawable mDrawable = getActivity().getResources().getDrawable(R.drawable.icon_openinghours);
                    mDrawable.setColorFilter(new PorterDuffColorFilter(0xffffff, PorterDuff.Mode.MULTIPLY));
                }
                break;
        }
        return false;
    }*/
}