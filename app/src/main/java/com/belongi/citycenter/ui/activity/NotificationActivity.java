package com.belongi.citycenter.ui.activity;

import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.fragment.NotificationFragment;

/**
 * Created by webwerks on 14/6/15.
 */
public class NotificationActivity extends BaseActivity {
    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_notification);
    }

    @Override
    protected void initializeUi() {
        fragmentTransaction(ADD_FRAGMENT,NotificationFragment.newInstance(true),R.id.container,false);
    }

    @Override
    public String getScreenName() {
        return "Launch Manage Notification";
    }

}
