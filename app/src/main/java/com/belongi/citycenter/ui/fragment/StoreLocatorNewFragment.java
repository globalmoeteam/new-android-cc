package com.belongi.citycenter.ui.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.jibe.DrawerMenuOject;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.jibe.MapBackgroundTouch;
import com.belongi.citycenter.jibe.WayFindManager;
import com.belongi.citycenter.jibe.overlays.AmenitiesOverlay;
import com.belongi.citycenter.jibe.overlays.AttractionOverlay;
import com.belongi.citycenter.jibe.overlays.MapActionListener;
import com.belongi.citycenter.jibe.overlays.MapStoreDetailOverlay;
import com.belongi.citycenter.jibe.overlays.SearchOverlay;
import com.belongi.citycenter.model.MapLevel;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.jibestream.jibestreamandroidlibrary.elements.Amenity;
import com.jibestream.jibestreamandroidlibrary.elements.Element;
import com.jibestream.jibestreamandroidlibrary.elements.Unit;
import com.jibestream.jibestreamandroidlibrary.elements.UnitLabel;
import com.jibestream.jibestreamandroidlibrary.intentFilters.IntentFilterTouch;
import com.jibestream.jibestreamandroidlibrary.intents.IntentTouch;
import com.jibestream.jibestreamandroidlibrary.main.EngineView;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.main.VenueData;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Category;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Destination;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.MapFull;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Waypoint;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.textDirections.TDInstruction;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyleIcon;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by webwerks on 13/12/16.
 */

public class StoreLocatorNewFragment extends BaseFragment implements View.OnClickListener, MapActionListener {

    LinearLayout llSearch, llAttraction, llAmenities;
    FrameLayout frameFeatures;
    EngineView mapView;
    LinearLayout llLevels;
    Destination toDest, fromDest;
    WayFindManager wayfindManager;
    int destID = 0;
    View mView;
    RectF gfCenterRectF = new RectF(2696.75f, 2636.405f, 2727.14f, 2660.604f);
    ArrayList<HashMap<String, String>> imgUrls = new ArrayList<>();
    Spinner spnrLevel;
    MapLevelAdapter mapLevelAdapter;
    BroadcastReceiver outsideTouchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            IntentTouch intentTouch = (IntentTouch) intent;
            if (intentTouch.element instanceof MapBackgroundTouch) {
                if (isAdded() && isVisible() && (frameFeatures.getChildCount() > 1)) {
                    frameFeatures.removeViewAt(1);
                    llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    resetAll(true);
                }
            }
        }
    };
    //to get text direction
    String finalInstructions;
    ArrayList<String> amenitiesAvailabiltyLevels = new ArrayList<>();
    BroadcastReceiver mapLoadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ((BaseActivity) getActivity()).stopLoading();
            showMap();
            updateLevelMenu();
        }
    };
    Dialog informationAlert = null;
    private IntentFilter mapLoadedFilter = new IntentFilter("MAP_LOADED_INTENT");

    @Override
    public String getScreenName() {
        return "STORE LOCATOR";
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_store_locator_new, null);
    }

    public void showMap() {
        getAmenitiesIcon();
        setTextResponse("broadcastReceiverFrameworkInit", "start " + Constant.LAST_SELECTED_MAP_INDEX);
        MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[Constant.LAST_SELECTED_MAP_INDEX]);
        MSingleton.getI().setCurrentMapIndex(Constant.LAST_SELECTED_MAP_INDEX);

        //// TODO: 2016-06-19 assuming you have checked you have a valid map see todos in splashActivity
        wayfindManager = new WayFindManager(MSingleton.getI(), getActivity().getApplicationContext(),
                StoreLocatorNewFragment.this);

       /* Image image=new Image();
        Bitmap startPin = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_start);
        image.setBitmap(startPin);

        wayfindManager.youAreHere.setShape(image);*/
        wayfindManager.stop();
        wayfindManager.start();

        MSingleton.getI().setEngineView(mapView);
        MSingleton.getI().onResume();
        MSingleton.getI().resetWayfind();

        touchBroadcastReceiver();
        setMapOutsideTouch();
        resetAll(true);

        for (Destination dest : MSingleton.getI().venueData.destinations) {
            final Element[] element = MSingleton.getI().getElementsOfDestination(dest.id);
            if (element.length > 0) {
                if (element[0] instanceof Unit) {
                    Unit unit = (Unit) element[0];

                    RenderStyle styleHighlighted = new RenderStyle(Paint.Style.FILL_AND_STROKE);
                    styleHighlighted.setFillColor(Color.parseColor("#E20303"));
                    styleHighlighted.setStrokeColor(Color.parseColor("#A5A5A5"));
                    styleHighlighted.setStrokeWidth(1);
                    unit.setStyleIdle(styleHighlighted);

                    unit.setHighlightState(false);
                }
            }
        }

        int hotelId = 0, anchorStoreId = 0;
        final VenueData venueData = MSingleton.getI().venueData;
        for (Category cat : venueData.categories) {

            if (cat.name.equalsIgnoreCase("Hotels")) {
                hotelId = cat.id;
            } else if (cat.name.equalsIgnoreCase("Anchor Stores")) {
                anchorStoreId = cat.id;
            }
            //Log.e("Category",cat.name + " ::" + cat.id);
        }

        Element[] elements = MSingleton.getI().getElementsByType(Unit.class);
        for (int i = 0; i < elements.length; i++) {
            Unit unit = (Unit) elements[i];

            RenderStyle renderStyleHighlight = new RenderStyle(Paint.Style.FILL_AND_STROKE);
            renderStyleHighlight.setStrokeColor(Color.parseColor("#A5A5A5"));
            renderStyleHighlight.setStrokeWidth(1);
            renderStyleHighlight.setFillColor(Color.parseColor(/*"#FFDAB9"*/"#F7EEE0"));
            unit.setHighlightState(false);
            unit.setStyleIdle(renderStyleHighlight);

            if (unit.getDestinations() == null) continue;
            if (unit.getDestinations().length == 0) continue;
            Destination[] destinations = unit.getDestinations();
            for (int j = 0; j < destinations.length; j++) {

                Destination destination = destinations[j];
                if (destination == null) continue;
                if (destination.categoryId == null) continue;

                int[] categoryIds = destination.categoryId;
                for (int k = 0; k < categoryIds.length; k++) {
                    int categoryId = categoryIds[k];
                    if (categoryId == hotelId) {
                        renderStyleHighlight.setFillColor(Color.parseColor("#C6AE8F"));
                        unit.setHighlightState(false);
                        unit.setStyleIdle(renderStyleHighlight);
                        break;
                    } else if (categoryId == anchorStoreId) {
                        renderStyleHighlight.setFillColor(Color.parseColor("#E4CBA4"));
                        unit.setHighlightState(false);
                        unit.setStyleIdle(renderStyleHighlight);
                        break;
                    }
                }
            }
        }


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mView = view;
        frameFeatures = (FrameLayout) view.findViewById(R.id.frame_features);
        mapView = (EngineView) view.findViewById(R.id.engineView);
        llLevels = (LinearLayout) view.findViewById(R.id.level_menu);


        //todo  when coming from pin icon, maybe frame the unit
        view.findViewById(R.id.btnInformation).setOnClickListener(this);
        setActionbarTitle("");
        ((BaseActivity) getActivity()).setSearchVisibility(false);
        if (getArguments() != null) {
            String id = getArguments().getString("dest_id");
            if (id != null && !id.equalsIgnoreCase("")) {
                destID = Integer.parseInt(id.trim());
            }
        }

        Log.e("STORE LOCATOR", App.get().isMapEnabled() + "::::");
        if (!App.get().isMapEnabled()) {
            ((BaseActivity) getActivity()).showProgressLoading();
            getActivity().registerReceiver(mapLoadReceiver, mapLoadedFilter);
        } else {

            //todo  when coming from pin icon, maybe frame the unit

            showMap();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (App.get().isMapEnabled()) {
            //showMap();
            updateLevelMenu();
        }
    }

    private void resetAll(boolean resetZoom) {
        M m = MSingleton.getI();
        m.setLevel(MSingleton.getI().venueData.maps[Constant.LAST_SELECTED_MAP_INDEX]);


        Element[] amenList = m.getElementsByType(Amenity.class);
        for (int i = 0; i < amenList.length; i++) {
            Amenity amenity = (Amenity) amenList[i];

            if (amenity.amenityComponent.bean.description.contains("Entrance") ||
                    amenity.amenityComponent.bean.description.contains("Information") ||
                    amenity.amenityComponent.bean.description.contains("Toilets") ||
                    amenity.amenityComponent.bean.description.contains("Prayer") ||
                    amenity.amenityComponent.bean.description.contains("Metro") ||
                    amenity.amenityComponent.bean.description.contains("Parking")) {
                MSingleton.getI().setAmenityVisibility(i, true);
                MSingleton.getI().setAmenityVisibility(amenity, true);
                amenity.setVisible(true);
            } else {
                MSingleton.getI().setAmenityVisibility(i, false);
                MSingleton.getI().setAmenityVisibility(amenity, false);
                amenity.setVisible(false);
            }
        }

       /* for (int i = 0; i < m.venueData.amenities.length; i++) {

            Log.e("Tag", " Venue data amenities  " + m.venueData.amenities[i].bean.description);
            if (m.venueData.amenities[i].bean.description.contains("Entrance") ||
                    m.venueData.amenities[i].bean.description.contains("Information") ||
                    m.venueData.amenities[i].bean.description.contains("Toilets") ||
                    m.venueData.amenities[i].bean.description.contains("Prayer") ||
                    m.venueData.amenities[i].bean.description.contains("Metro") ||
                    m.venueData.amenities[i].bean.description.contains("Parking")
                    ) {
                m.setAmenityVisibility(i, true);
            }

            else
            {
                m.setAmenityVisibility(i, false);
            }
        }*/

        m.resetWayfind();
        m.unHighlightUnits();

        toDest = null;

        m.setLevel(m.venueData.maps[0]);
        m.setCurrentMapIndex(0);
        Constant.LAST_SELECTED_MAP_INDEX = 0;

        if (resetZoom) {
            m.camera.zoomTo(m.defaultFramings[Constant.LAST_SELECTED_MAP_INDEX]);
            //m.camera.zoomTo(m.defaultFramings[1]);
            m.camera.setRoll(0);
        }

        m.setAccessLevel(100);
        updateLevelState();

        //Log.e("PIN VISIBLE","RESET ALL FALSE");
        m.pin.setVisible(false);
        setDefaultAmenities();
    }

    private void setDestinationOnMap(List<Destination> destinations, boolean isFrom) {
        M m = MSingleton.getI();
        final Waypoint[] waypointsOfDestination = m.getWaypointsOfDestination(destinations.get(0));
        if (waypointsOfDestination == null || waypointsOfDestination.length == 0) return;
        Waypoint waypoint = waypointsOfDestination[0];
        if (waypoint == null) return;
        m.unHighlightUnits();

        if (isFrom) {
            fromDest = destinations.get(0);
            m.setFromWaypoint(waypoint);

            for (int l = 0; l < m.venueData.maps.length; l++) {
                if (m.venueData.maps[l].map.mapId == waypoint.mapId) {
                    m.setCurrentMapIndex(l);
                    Constant.LAST_SELECTED_MAP_INDEX = l;
                }
            }
            if (toDest != null) {
                /*final Element[] element = m.getElementsOfDestination(toDest.id);
                if (element.length > 0) {
                    if (element[0] instanceof Unit) {
                        Unit unit = (Unit) element[0];
                        zoomStore(m, unit);
                    }
                }*/
                m.setHighlightOnUnitsByDestinationID(toDest.id, true);
            }
            updateLevelState();
            //getTextDir();
        } else {
            toDest = destinations.get(0);
            m.setToWaypoint(waypoint);

            if (fromDest == null) {
                if (isAdded() && isVisible()) {
                    resetAll(false);
                    toDest = destinations.get(0);

                    List<Search> searchList = new ArrayList<>();

                    if (destinations != null && destinations.size() > 0) {

                        for (Destination dest : destinations) {
                            if (dest != null) {

                                Search search = new Search();
                                search.setLabel("" + Html.fromHtml(dest.name));
                                search.setId(dest.id + " ");
                                MapFull mapFull = m.getMapFullByDestinationId(dest.id);

                                search.setLevel(getShortFormLevel(mapFull.map.floorSequence - 1));
                                search.setFloor(mapFull.map.floorSequence - 1);
                                search.setCategory(getCategories(dest.category));

                                searchList.add(search);
                            }
                        }
                    }

                    if (frameFeatures.getChildCount() > 1) {
                        frameFeatures.removeViewAt(1);
                    }

                    frameFeatures.addView(new MapStoreDetailOverlay(getActivity(), searchList, this),
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                    // set layout default if other overlay is present
                    llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.map_menu_selected));
                    llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                }
            }

            for (int l = 0; l < m.venueData.maps.length; l++) {
                if (m.venueData.maps[l].map.mapId == waypoint.mapId) {
                    m.setCurrentMapIndex(l);
                    Constant.LAST_SELECTED_MAP_INDEX = l;
                }
            }

            if (fromDest != null) {

                /*final Element[] element = m.getElementsOfDestination(fromDest.id);
                Log.e("SET DEST MAP","!=Null " + element.length + "::::" + (element[0] instanceof Unit) );
                if (element.length > 0) {
                    if (element[0] instanceof Unit) {
                        Unit unit = (Unit) element[0];
                        unit.setHighlightState(true);
                        zoomStore(m,unit);
                    }
                }*/
                m.setHighlightOnUnitsByDestinationID(fromDest.id, true);
                //getTextDir();
            }
            //Calling text direction after getting end destination
            updateLevelState();
        }
        /*final Element[] element = m.getElementsOfDestination(destinations.get(0).id);
        if (element.length > 0) {
            if (element[0] instanceof Unit) {
                Unit unit = (Unit) element[0];
                unit.setHighlightState(true);
                zoomStore(m, unit);
            }
        }*/
        zoomStore(m, destinations.get(0));
        m.setHighlightOnUnitsByDestinationID(destinations.get(0).id, true);
        if (waypointsOfDestination[0].mapId == MSingleton.getI().getCurrentMap().map.mapId) {
            MSingleton.getI().pin.getTransform().setTranslationX((float) waypointsOfDestination[0].x);
            MSingleton.getI().pin.getTransform().setTranslationY((float) waypointsOfDestination[0].y);
            MSingleton.getI().pin.getTransform().getChildAt(waypointsOfDestination[0].mapId);

            //Log.e("PIN VISIBLE","SET DEST TRUE");
            MSingleton.getI().pin.setVisible(true);
        }
    }

    private void updateLevelMenu() {
        /*if (llLevels != null) {
            llLevels.removeAllViews();
            for (int k = 0; k < MSingleton.getI().venueData.maps.length; k++) {
                View view = addLevelToMenu(k);
                if (view != null)
                    llLevels.addView(view);
            }
        }*/

        if (llLevels != null) {
            llLevels.removeAllViews();
            List<MapLevel> levelList = new ArrayList<>();
            for (int k = 0; k < MSingleton.getI().venueData.maps.length; k++) {
                levelList.add(new MapLevel(k, false));
            }
            addLevelToMenu(levelList);
        }

        if (toDest != null) {
            final Waypoint[] waypointsOfDestination = MSingleton.getI().getWaypointsOfDestination(toDest);
            if (waypointsOfDestination[0].mapId == MSingleton.getI().getCurrentMap().map.mapId) {
                MSingleton.getI().pin.getTransform().setTranslationX((float) waypointsOfDestination[0].x);
                MSingleton.getI().pin.getTransform().setTranslationY((float) waypointsOfDestination[0].y);
                MSingleton.getI().pin.getTransform().getChildAt(waypointsOfDestination[0].mapId);

                //Log.e("PIN VISIBLE","UPDATE LEVEL TRUE");
                MSingleton.getI().pin.setVisible(true);
                //MSingleton.getI().cameraToUnit(toDest.id);
            }
        }
    }

    public void zoomStore(final M m, Destination dest) {
//        RectF regionToZoomTo = new RectF();
//        if (unit.getLevel() == m.getCurrentMapIndex()) {
//            regionToZoomTo.union(unit.getBBox());
//        }
        zoomAnimation(MSingleton.getI(), null, dest);
    }

    private void zoomAnimation(final M m, RectF regionToZoomTo, Destination destination) {
     /*   m.camera.zoomTo(m.defaultFramings[MSingleton.getI().getCurrentMapIndex()]);
        m.camera.setRoll(0);
        if (destination != null) {
            Log.e("ENTER", "ZOOM DEST");

            final Waypoint[] waypointsOfDestination = Building.getWaypointsOfDestination(m, destination);
            final Waypoint waypoint = waypointsOfDestination[0];
            final int levelIndexOfWaypointWithID = Building.getLevelIndexOfWaypointWithID(m, waypoint.id);
            m.setCurrentMapIndex(levelIndexOfWaypointWithID);
            float x = Float.parseFloat(waypoint.x + "");
            float y = Float.parseFloat(waypoint.y + "");

            RectF rectF = new RectF(x, y, x, y);
            float padding = 100;
            rectF.inset(-padding, -padding);
            m.camera.zoomTo(rectF);
            //m.cameraToUnit(unit.getDestinations()[0].id);
        } else {
            m.camera.zoomTo(regionToZoomTo, 50);
        }*/
        // Zoom Animation
        // Setup the animator

        /*final ValueAnimator valueAnimatorCamZoomStep = new ValueAnimator().setDuration(500);
        valueAnimatorCamZoomStep.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimatorCamZoomStep.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                m.camera.setScale((Float) animation.getAnimatedValue());
            }
        });

        // Call the animator and zoom in
        if (valueAnimatorCamZoomStep.isRunning()) valueAnimatorCamZoomStep.end();
        float scale = m.camera.getScale();
        valueAnimatorCamZoomStep.setFloatValues(scale, scale - scale * 0.8f);
        valueAnimatorCamZoomStep.setStartDelay(300);
        valueAnimatorCamZoomStep.start();*/
    }

    private View addLevelToMenu(List<MapLevel> levelList/*int i*/) {

        if (isAdded() && isVisible()) {
            spnrLevel = (Spinner) LayoutInflater.from(getActivity()).inflate(R.layout.map_level_layout, null);
            spnrLevel.setAdapter(mapLevelAdapter = new MapLevelAdapter(spnrLevel.getContext(), levelList));
            llLevels.addView(spnrLevel);

            if (destID != 0) {
                spnrLevel.setSelection(MSingleton.getI().getCurrentMapIndex());
                spnrLevel.setTag(R.id.pos, (Integer) MSingleton.getI().getCurrentMapIndex());
            } else {
                spnrLevel.setTag(R.id.pos, 100);
            }

            spnrLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    MapLevel level = (MapLevel) parent.getItemAtPosition(position);
                    mapLevelAdapter.selectLevel(position);

                    M m = MSingleton.getI();

                    Log.e("SpnLevel >", "SpnLevel > " + (int) spnrLevel.getTag(R.id.pos) + " != " + position);
                    if ((int) spnrLevel.getTag(R.id.pos) != position) {
                        m.resetWayfind();
                        m.camera.zoomTo(m.defaultFramings[level.getLevel()]);
                        m.camera.setRoll(0);

                        MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[level.getLevel()]);
                        MSingleton.getI().setCurrentMapIndex(level.getLevel());
                        Constant.LAST_SELECTED_MAP_INDEX = level.getLevel();
                    }

                    if (frameFeatures.getChildCount() > 1) {
                        View v = frameFeatures.getChildAt(1);
                        if (v instanceof AmenitiesOverlay) {
                            RectF amenitiesRectF = new RectF();
                            Element[] amenities = m.getElementsByType(Amenity.class);

                            List<Integer> amenityLevel = new ArrayList<>();
                            for (int i = 0; i < amenities.length; i++) {
                                Amenity amenity = (Amenity) amenities[i];
                                if (amenity.amenityComponent.bean.localizedText
                                        .equalsIgnoreCase(((AmenitiesOverlay) v).getSelectedAmenities())) {
                                    amenityLevel.add(amenity.getLevel());
                                    if (amenity.getLevel() == Constant.LAST_SELECTED_MAP_INDEX) {
                                        amenitiesRectF.union(amenity.getBBox());
                                    }
                                    m.camera.zoomTo(amenitiesRectF, 50);
                                }
                            }

                            for (int i = 0; i < amenities.length; i++) {
                                Amenity amenity = (Amenity) amenities[i];

                                if (amenity.amenityComponent.bean.localizedText
                                        .equalsIgnoreCase(((AmenitiesOverlay) v).getSelectedAmenities())
                                        && !amenityLevel.contains(Constant.LAST_SELECTED_MAP_INDEX)) {

                                    amenityLevel.add(amenity.getLevel());
                                    amenitiesAvailabilityLevels(i, amenity.amenityComponent.bean.localizedText);
                                    break;
                                }
                            }
                        } else if (v instanceof AttractionOverlay) {
                            AttractionOverlay overlay = (AttractionOverlay) v;
                            overlay.highLightBasedOnCategory(overlay.selected);
                        }
                    }
                    spnrLevel.setTag(R.id.pos, 100);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            spnrLevel.setVisibility(View.VISIBLE);
            return spnrLevel;

            /*View mView = getActivity().getLayoutInflater().inflate(R.layout.level_text_view, null);
            TextView mtextView = (TextView) mView.findViewById(R.id.lablLevel);
            mtextView.setText(getShortFormLevel(i));
            if (MSingleton.getI().getCurrentMapIndex() == i) {
                mtextView.setBackgroundResource(R.drawable.circular_bg_selected);
            }

            mtextView.setTag(i);
            mtextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int k = (int) view.getTag();
                    M m = MSingleton.getI();
                    m.resetWayfind();
                    m.camera.zoomTo(m.defaultFramings[k]);
                    m.camera.setRoll(0);

                    //m.unHighlightUnits();fg
                    if (frameFeatures.getChildCount() > 1) {
                        View v = frameFeatures.getChildAt(1);
                        frameFeatures.removeViewAt(1);
                    }

                    MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[k]);
                    MSingleton.getI().setCurrentMapIndex(k);
                    Constant.LAST_SELECTED_MAP_INDEX = k;
                    updateLevelState();
                }
            });
            return mView;*/
        }
        return null;
    }

    public String getCategories(String[] strings) {
        String s = " ";
        for (int i = 0; i < strings.length; i++) {
            if (i == strings.length - 1) {
                s = s + strings[i];
            } else {
                s = s + strings[i] + ", ";
            }
        }
        return s.toUpperCase();
    }

    public void setTextResponse(final String callback, final String response) {
        Log.e("callback response", callback + " : " + response);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_store_locator, menu);

        llSearch = (LinearLayout) (menu.findItem(R.id.action_search)).getActionView();
        llAttraction = (LinearLayout) (menu.findItem(R.id.action_attraction)).getActionView();
        llAmenities = (LinearLayout) (menu.findItem(R.id.action_amenities)).getActionView();

        llSearch.setOnClickListener(this);
        llAttraction.setOnClickListener(this);
        llAmenities.setOnClickListener(this);

        if (App.get().isMapEnabled()) {
            final VenueData venueData = MSingleton.getI().venueData;
            if (destID != 0) {

                List<Destination> destinationList = new ArrayList<>();
                Destination[] destination = venueData.destinations;
                for (int j = 0; j < destination.length; j++) {
                    if (destination[j].id == destID) {
                        destinationList.add(destination[j]);
                        setDestinationOnMap(destinationList, false);
                        updateLevelMenu();
                        break;
                    }
                }
            } else {
                zoomAnimation(MSingleton.getI(), gfCenterRectF, null);
            }
            setDefaultAmenities();
        }
    }

    private void setMapOutsideTouch() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        manager.registerReceiver(outsideTouchReceiver, new IntentFilterTouch(MSingleton.getI(), IntentFilterTouch.TYPE_SINGLE));
    }

    private void touchBroadcastReceiver() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final IntentTouch intentTouch = (IntentTouch) intent;
                final int id = intentTouch.id;
                // Get the element based on the id and check its a unit
                final Element element = MSingleton.getI().getElementByID(id);
                if (element instanceof Unit) {
                    Unit unit = (Unit) element;
                    // Get the destinations on the unit
                    final Destination[] destinations = unit.getDestinations();
                    if (destinations != null && destinations.length > 0) {
                        RenderStyle styleHighlighted = new RenderStyle(Paint.Style.FILL_AND_STROKE);
                        styleHighlighted.setFillColor(Color.parseColor("#E40404"));
                        styleHighlighted.setStrokeColor(Color.parseColor("#A5A5A5"));
                        styleHighlighted.setStrokeWidth(1);
                        unit.setStyleHighlighted(styleHighlighted);
                        unit.setHighlightState(true);
                    }

                    if (destinations != null && destinations.length > 0) {
                        final Destination destination = destinations[0];
                        if (destination == null) return;
                        setDestinationOnMap(Arrays.asList(destinations), false);
                        UnitLabel label = new UnitLabel();
                        label.setText(destination.name);

                        //Log.e("PIN VISIBLE","TOUCH UNIT true");
                        MSingleton.getI().pin.setVisible(true);
                        MSingleton.getI().camera.zoomTo(unit.getBBox(), 5);
                    }
                }
            }
        }, new IntentFilterTouch(MSingleton.getI(), IntentFilterTouch.TYPE_SINGLE));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == llSearch.getId()) {
            //getTextDir();
            llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.map_menu_selected));
            llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
            llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));

            if (frameFeatures.getChildCount() <= 1) {
                frameFeatures.addView(new SearchOverlay(getActivity(), this));
            } else {
                View v = frameFeatures.getChildAt(1);
                if (v instanceof SearchOverlay) {
                    llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    frameFeatures.removeViewAt(1);
                } else {
                    frameFeatures.removeViewAt(1);
                    frameFeatures.addView(new SearchOverlay(getActivity(), this));
                }
            }
            resetAll(true);
        } else if (view.getId() == llAttraction.getId()) {

            //getTextDir();
            llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.map_menu_selected));
            llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
            llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));

            if (frameFeatures.getChildCount() <= 1) {
                frameFeatures.addView(new AttractionOverlay(getActivity(), this),
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                //resetAll();
            } else {
                View v = frameFeatures.getChildAt(1);
                if (v instanceof AttractionOverlay) {
                    clearHighLights();

                    llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    frameFeatures.removeViewAt(1);
                } else {
                    frameFeatures.removeViewAt(1);
                    frameFeatures.addView(new AttractionOverlay(getActivity(), this),
                            FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                }
            }
            resetAll(true);
        } else if (view.getId() == llAmenities.getId()) {

            M m = MSingleton.getI();
            //getTextDir();
            llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.map_menu_selected));
            llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
            llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));

            if (frameFeatures.getChildCount() <= 1) {
                frameFeatures.addView(new AmenitiesOverlay(getActivity(), getAmenityList(MSingleton.getI().venueData), this, this),
                        FrameLayout.LayoutParams.MATCH_PARENT, dpToPx(60));
            } else {
                View v = frameFeatures.getChildAt(1);
                if (v instanceof AmenitiesOverlay) {

                    m.setLevel(MSingleton.getI().venueData.maps[Constant.LAST_SELECTED_MAP_INDEX]);
                    llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    frameFeatures.removeViewAt(1);
                } else {
                    frameFeatures.removeViewAt(1);
                    frameFeatures.addView(new AmenitiesOverlay(getActivity(), getAmenityList(MSingleton.getI().venueData), this, this),
                            FrameLayout.LayoutParams.MATCH_PARENT, dpToPx(60));
                }
            }
            resetAll(true);
        } else if (view.getId() == R.id.btnInformation) {
            if (informationAlert == null || (informationAlert != null && !informationAlert.isShowing()))
                showInformationDialog();
        }
    }

    public int dpToPx(int dp) {
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }

    private void getTextDir() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String instructions = "";
                ArrayList<ArrayList<TDInstruction>> textDirectionInstruction = MSingleton.getI().getTextDirectionInstruction(true, 30, 50);
                if (textDirectionInstruction == null) {
                    instructions = "No text direction available ";
                } else {
                    for (int i = 0; i < textDirectionInstruction.size(); i++) {
                        TDInstruction[] textDirectionInstructions = textDirectionInstruction.get(i).toArray(new TDInstruction[textDirectionInstruction.size()]);
                        for (TDInstruction tdInstruction : textDirectionInstructions) {
                            if (tdInstruction != null) {
                                if (!tdInstruction.output.isEmpty()) {
                                    /*int distance_calc = 0;
                                    String distance = "";
                                    distance_calc = (int) Math.floor(tdInstruction.distanceToNextMeters);
                                    distance = Integer.toString(distance_calc);
                                    if (!distance.equals("0")) {
                                        instructions += "<font color=\"red\"> <b>&#8226;</b> </font> " + tdInstruction.output + " for " + distance + " meters \n" + "<br /> <br /> ";
                                    } else {
                                        instructions += "<font color=\"red\"> <b>&#8226;</b> </font>" + tdInstruction.output + "\n" + "<br /> <br /> ";
                                    }*/
                                    instructions += "<font color=\"red\"> <b>&#8226;</b> </font>" + tdInstruction.output + "\n" + "<br /> <br /> ";
                                }
                            }
                        }
                    }
                }
                finalInstructions = instructions;
                Log.e("TEXT INSTRUCTION", "TEXT INSTRUCTION >" + finalInstructions);
            }
        }).start();
    }

    private String getShortFormLevel(int level) {
        String label = "";
        if (level == 0) {
            label = "GF";
        } else if (level < 0) {
            label = "L" + (MSingleton.getI().venueData.maps.length - 1);
        } else
            label = "L" + (level);
        return label;
    }

    public void updateLevelState() {
        if (spnrLevel != null) {
            if (isAdded() && isVisible()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spnrLevel.setSelection(Constant.LAST_SELECTED_MAP_INDEX);
                        spnrLevel.setTag(R.id.pos, (Integer) Constant.LAST_SELECTED_MAP_INDEX);
                    }
                });
            }
        }
    }

    private ArrayList<DrawerMenuOject> getAmenityList(final VenueData venueData/*, final ArrayList<HashMap<String, String>> imgUrls*/) {
        ArrayList<DrawerMenuOject> amenityList = new ArrayList<>();
        for (int k = 0; k < MSingleton.getI().venueData.amenities.length; k++) {
            DrawerMenuOject amenity = new DrawerMenuOject();
            amenity.setTitle(venueData.amenities[k].bean.localizedText);
            amenity.setDescription(venueData.amenities[k].bean.description);
            amenity.setUnderLineVisible(false);
            amenity.setType("amenities");

            if (imgUrls.size() > k)
                amenity.setUrl(/*"https://qamaf2.js-network.co"*/Constant.URL_JMAP
                        + getAmenitiesIconURL(venueData.amenities[k].bean.localizedText));
            /*if (MSingleton.getI().getAmenitiesVisibility()[k]) {
                amenity.setSelected(true);
            }*/
            /*if(amenity.getTitle().equalsIgnoreCase("Telephone")) {
                amenityList.add(amenity);
            }*/

            amenityList.add(amenity);
        }
        return amenityList;
    }

    public String getAmenitiesIconURL(String localizedText) {
        String url = "";
        for (int i = 0; i < imgUrls.size(); i++) {
            if (localizedText.equalsIgnoreCase(imgUrls.get(i).get("localizedText"))) {
                url = imgUrls.get(i).get("iconImagePath");
            }
        }
        return url;
    }

    private String sendGet() throws Exception {
        String url = /*"https://qamaf2.js-network.co/v3/location/5989/amenity";*/Constant.URL_JMAP+"/v3/location/" + App.get().getSelectedMall().getJibeProjectId() + "/amenity";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private void getAmenitiesIcon() {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String response = sendGet();
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            HashMap<String, String> stringHashMap = new HashMap<String, String>();
                            stringHashMap.put("localizedText", (String) ((JSONObject) jsonArray.get(i)).get("localizedText"));
                            stringHashMap.put("iconImagePath", (String) ((JSONObject) jsonArray.get(i)).get("iconImagePath"));
                            imgUrls.add(stringHashMap);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void amenitiesAvailabilityLevels(int j, String name) {
        boolean isOnCurrentLevel = false;
        amenitiesAvailabiltyLevels = new ArrayList<>();

        com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Amenity[] amenities = MSingleton.getI().venueData.amenities;
        for (int i = 0; i < amenities.length; i++) {
            if (amenities[i].bean.localizedText.equalsIgnoreCase(name)) {
                j = i;
            }
        }
        Waypoint waypoint[] = MSingleton.getI().getWaypointsOfAmenity(amenities[j].bean.componentId);
        for (int k = 0; k < waypoint.length; k++) {

            /*MapFull mapFull=MSingleton.getI().getMapFullDataBySequence(MSingleton.getI().getCurrentMapIndex());
            Log.e("Enter","WAYPOINT " + mapFull);*/

            /*MapFull currentMap=null;
            MapFull[] maps = MSingleton.getI().venueData.maps;
            for(MapFull mapFull:maps){
                Log.e("AMENITY AVAILABILITY",mapFull.map.floorSequence +"::::" + Constant.LAST_SELECTED_MAP_INDEX);
                if(mapFull.map.floorSequence==Constant.LAST_SELECTED_MAP_INDEX){
                    currentMap=mapFull;
                    break;
                }
            }*/

            //if(currentMap!=null) {
            if (waypoint[k].mapId == MSingleton.getI().getCurrentMap().map.mapId) {
                isOnCurrentLevel = true;
                break;
            } else {
                for (int m = 0; m < MSingleton.getI().venueData.maps.length; m++) {
                    if (MSingleton.getI().venueData.maps[m].map.mapId == waypoint[k].mapId)
                        if (!amenitiesAvailabiltyLevels.contains(getFullLevel(m)))
                            amenitiesAvailabiltyLevels.add(getFullLevel(m));
                }
            }
            //}
        }

        //Log.e("Enter","WAYPOINT " + isOnCurrentLevel);
        if (!isOnCurrentLevel) {
            if (amenitiesAvailabiltyLevels.size() == 1) {
                showAmenityAvailabilityDialog(amenitiesAvailabiltyLevels/*.toString().replace("[", "").replace("]", "")*/, j);
            } else {
                showAmenityAvailabilityDialog(amenitiesAvailabiltyLevels/*.toString().replace("[", "").replace("]", "")*/, j);
            }
        }
    }

    public String getFullLevel(int level) {
        String label = "";
        if (level == 0) {
            label = "Ground Floor";
        } else if (level < 0) {
            label = "Level " + (MSingleton.getI().venueData.maps.length - 1);
        } else
            label = "Level " + (level);
        return label;
    }

    public void showAmenityAvailabilityDialog(/*final String level*/List<String> unitList, final int position) {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_aminities_availbility, null);
        alertDialog.setContentView(view);

        DisplayMetrics disMat = getResources().getDisplayMetrics();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = (int) (disMat.widthPixels * (0.70));
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        /*((Button) view.findViewById(R.id.btnLevel)).setText(level);
        ((Button) view.findViewById(R.id.btnLevel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                int k=0;
                if(level.equalsIgnoreCase("Ground Floor")){
                    k=0;
                }else if(level.equalsIgnoreCase("Level1")){
                    k=1;
                }else if(level.equalsIgnoreCase("Level2")){
                    k=2;
                }else if(level.equalsIgnoreCase("Level3")){
                    k=3;
                }
                MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[k]);
                MSingleton.getI().setCurrentMapIndex(k);
                Constant.LAST_SELECTED_MAP_INDEX = k;
                updateLevelState();
            }
        });*/
        LinearLayout llLevel = (LinearLayout) view.findViewById(R.id.llLevel);
        String lastLevel = "";
        for (String unit : unitList) {

            if (TextUtils.isEmpty(lastLevel) || !lastLevel.equals(unit/*.getLevel() + ""*/)) {
                lastLevel = /*unit.getLevel() + ""*/unit;
                TextView btnLevel = new TextView(llLevel.getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 20, 0, 0);
                btnLevel.setLayoutParams(params);
                btnLevel.setText(/*getFullLevel(unit.getLevel())*/unit);
                btnLevel.setBackgroundResource(R.drawable.map_btn_bg);
                btnLevel.setTextColor(ContextCompat.getColor(llLevel.getContext(), R.color.white));
                btnLevel.setPadding(20, 15, 25, 10);

                btnLevel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                btnLevel.setAllCaps(true);
                btnLevel.setTag(/*unit.getLevel()*/unit);
                btnLevel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        String level = (String) v.getTag();
                        int k = 0;
                        if (level.equalsIgnoreCase("Ground Floor")) {
                            k = 0;
                        } else if (level.equalsIgnoreCase("Level 1")) {
                            k = 1;
                        } else if (level.equalsIgnoreCase("Level 2")) {
                            k = 2;
                        } else if (level.equalsIgnoreCase("Level 3")) {
                            k = 3;
                        }
                        MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[k]);
                        MSingleton.getI().setCurrentMapIndex(k);
                        Constant.LAST_SELECTED_MAP_INDEX = k;
                        updateLevelState();
                    }
                });
                llLevel.addView(btnLevel);
            }
        }

        ((Button) view.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (frameFeatures.getChildCount() > 1) {
                    View v = frameFeatures.getChildAt(1);
                    if (v instanceof AmenitiesOverlay) {
                        AmenitiesOverlay overlay = (AmenitiesOverlay) v;
                        overlay.changeImg(position, false);
                    }
                }
            }
        });
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#222222")));
        alertDialog.show();
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void showAttractionMissingDialog(/*final int level*/ List<Unit> unitList) {
        //final String selLevel = level == 0 ? "Ground Floor" : "Level"+level;
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_attraction_availbility, null);
        alertDialog.setContentView(view);

        DisplayMetrics disMat = getResources().getDisplayMetrics();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = (int) (disMat.widthPixels * (0.70));
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        /*((Button) view.findViewById(R.id.btnLevel)).setText(selLevel);
        ((Button) view.findViewById(R.id.btnLevel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[level]);
                MSingleton.getI().setCurrentMapIndex(level);
                Constant.LAST_SELECTED_MAP_INDEX = level;
                updateLevelState();
            }
        });*/

        LinearLayout llLevel = (LinearLayout) view.findViewById(R.id.llLevel);
        String lastLevel = "";
        for (Unit unit : unitList) {

            if (TextUtils.isEmpty(lastLevel) || !lastLevel.equals(unit.getLevel() + "")) {
                lastLevel = unit.getLevel() + "";
                TextView btnLevel = new TextView(llLevel.getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 20, 0, 0);
                btnLevel.setLayoutParams(params);
                btnLevel.setText(getFullLevel(unit.getLevel()));
                btnLevel.setBackgroundResource(R.drawable.map_btn_bg);
                btnLevel.setTextColor(ContextCompat.getColor(llLevel.getContext(), R.color.white));
                btnLevel.setPadding(20, 15, 25, 10);

                btnLevel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                btnLevel.setAllCaps(true);
                btnLevel.setTag(unit.getLevel());
                btnLevel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        int level = (int) v.getTag();
                        MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[level]);
                        MSingleton.getI().setCurrentMapIndex(level);
                        Constant.LAST_SELECTED_MAP_INDEX = level;
                        updateLevelState();
                    }
                });
                llLevel.addView(btnLevel);
            }
        }

        ((Button) view.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#222222")));
        alertDialog.show();
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void clearHighLights() {
        M m = MSingleton.getI();
        m.unHighlightUnits();
    }

    @Override
    public void onCloseClick() {
        resetAll(true);
        if (frameFeatures.getChildCount() > 1) {
            frameFeatures.removeViewAt(1);
        }

        llSearch.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
        llAttraction.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
        llAmenities.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
    }

    @Override
    public void plotRoute(/*Destination toDestination, Destination fromDestination*/String toDestinationId, String fromDestinationId) {
        if (/*toDestination != null && fromDestination != null*/
                !TextUtils.isEmpty(toDestinationId) && !TextUtils.isEmpty(fromDestinationId)) {

            App.get().setCurrentSegment(0);
            M m = MSingleton.getI();
            int fromId = Integer.parseInt(fromDestinationId.trim());
            int toId = Integer.parseInt(toDestinationId.trim());

            final Waypoint[] waypointsFromDestination = m.getWaypointsOfDestination(fromId);
            final Waypoint[] waypointsToDestination = m.getWaypointsOfDestination(toId);
            m.setHighlightOnUnitsByDestinationID(fromId, true);
            m.setHighlightOnUnitsByDestinationID(toId, true);

            //try {
            if (waypointsFromDestination == null || waypointsFromDestination.length == 0)
                return;
            Waypoint waypointFrom = waypointsFromDestination[0];
            if (waypointFrom != null)
                m.setFromWaypoint(waypointFrom);

            if (waypointsToDestination == null || waypointsToDestination.length == 0)
                return;
            Waypoint waypointTo = waypointsToDestination[0];
            Log.e("PLOT ", waypointTo + "::::" + m);
            if (waypointTo != null)
                m.setToWaypoint(waypointTo);
//            }catch (Exception e){
//                e.printStackTrace();
//            }

            final Element[] element = m.getElementsOfDestination(toId);
            if (element.length > 0) {
                if (element[0] instanceof Unit) {
                    Unit unit = (Unit) element[0];
                    RenderStyle styleHighlighted = new RenderStyle(Paint.Style.FILL_AND_STROKE);
                    styleHighlighted.setFillColor(Color.parseColor("#E40404"));
                    styleHighlighted.setStrokeColor(Color.parseColor("#BDBBB7"));
                    styleHighlighted.setStrokeWidth(1);
                    unit.setStyleHighlighted(styleHighlighted);
                    unit.setHighlightState(true);
                }
            }
        }
    }

    public void showInformationDialog() {
        informationAlert = new Dialog(getActivity());
        informationAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        informationAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_information_overlay, null);
        informationAlert.setContentView(view);

        DisplayMetrics disMat = getResources().getDisplayMetrics();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(informationAlert.getWindow().getAttributes());
        layoutParams.width = (int) (disMat.widthPixels);
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.BOTTOM;

        ((Button) view.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                informationAlert.dismiss();
            }
        });
        informationAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor(/*"#B3000000"*/"#222222")));
        informationAlert.show();
        informationAlert.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void setFromPoint(Destination fromPoint) {
        List<Destination> destinationList = new ArrayList<>();
        destinationList.add(fromPoint);
        setDestinationOnMap(destinationList, false);
    }

    @Override
    public void changeLevel(String level) {
        int k = 0;
        if (level.equalsIgnoreCase("GF")) {
            k = 0;
        } else if (level.equalsIgnoreCase("L1")) {
            k = 1;
        } else if (level.equalsIgnoreCase("L2")) {
            k = 2;
        } else if (level.equalsIgnoreCase("L3")) {
            k = 3;
        }

        MSingleton.getI().setLevel(MSingleton.getI().venueData.maps[k]);
        MSingleton.getI().setCurrentMapIndex(k);
        Constant.LAST_SELECTED_MAP_INDEX = k;
        updateLevelState();
    }

    public void setDefaultAmenities() {
        M m = MSingleton.getI();
        Element[] amenList = m.getElementsByType(Amenity.class);
        for (int i = 0; i < amenList.length; i++) {
            Amenity amenity = (Amenity) amenList[i];

            RenderStyle styleBG = new RenderStyle(Paint.Style.FILL);
            styleBG.setFillColor(ContextCompat.getColor(getActivity(), R.color.color_black));

            RenderStyle styleFG = new RenderStyle(Paint.Style.FILL);
            styleFG.setFillColor(ContextCompat.getColor(getActivity(), R.color.white));

            RenderStyleIcon renderStyleIcon = new RenderStyleIcon();
            renderStyleIcon.renderStyleBG = styleBG;
            renderStyleIcon.renderStyleFG = styleFG;
            amenity.setStyleIdle(renderStyleIcon);

            //Log.e("TEXT INSTRUCTION", "AMENTITY DESC >" + amenity.amenityComponent.bean.description);

            if (amenity.amenityComponent.bean.description.contains("Entrance") ||
                    amenity.amenityComponent.bean.description.contains("Information") ||
                    amenity.amenityComponent.bean.description.contains("Toilets") ||
                    amenity.amenityComponent.bean.description.contains("Prayer") ||
                    amenity.amenityComponent.bean.description.contains("Metro") ||
                    amenity.amenityComponent.bean.description.contains("Parking")) {
                MSingleton.getI().setAmenityVisibility(i, true);
                MSingleton.getI().setAmenityVisibility(amenity, true);
                amenity.setVisible(true);
            } else {
                MSingleton.getI().setAmenityVisibility(i, false);
                MSingleton.getI().setAmenityVisibility(amenity, false);
                amenity.setVisible(false);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (mapLoadReceiver != null) {
                getActivity().unregisterReceiver(mapLoadReceiver);
            }
        } catch (Exception ex) {

        }

    }

    /*public void setParking(){

        RenderStyle styleL0=new RenderStyle();
        styleL0.setFillColor(ContextCompat.getColor(getActivity(),R.color.p1));

        RenderStyle styleL1=new RenderStyle();
        styleL1.setFillColor(ContextCompat.getColor(getActivity(),R.color.p2));

        RenderStyle styleL2=new RenderStyle();
        styleL2.setFillColor(ContextCompat.getColor(getActivity(),R.color.p3));

        RenderStyle styleL3=new RenderStyle();
        styleL3.setFillColor(ContextCompat.getColor(getActivity(),R.color.p4));

        RenderStyleIcon renderStyleIconLevel0 = new RenderStyleIcon();
        renderStyleIconLevel0.renderStyleBG=styleL0;

        RenderStyleIcon renderStyleIconLevel1 = new RenderStyleIcon();
        renderStyleIconLevel1.renderStyleBG=styleL1;

        RenderStyleIcon renderStyleIconLevel2 = new RenderStyleIcon();
        renderStyleIconLevel2.renderStyleBG=styleL2;

        RenderStyleIcon renderStyleIconLevel3 = new RenderStyleIcon();
        renderStyleIconLevel3.renderStyleBG=styleL3;

        // get all amenities
        M m=MSingleton.getI();

        Element[] amenities = m.getElementsByType(Amenity.class);
        for (int i = 0; i < amenities.length; i++) {
            Amenity amenity = (Amenity) amenities[i];

            //Log.e("AMENITY",amenity.amenityComponent.bean.filePathSVG + "::" + amenity.amenityComponent.bean.filePath + "::::" + amenity.amenityComponent.bean.iconImagePath + ": :::" );
            // Check for Parking Amenities

            if(amenity.amenityComponent.bean.description.equalsIgnoreCase("Parking")){
                if(amenity.getLevel()==0){
                    Log.e("SET VISIBLE AMEN",i + "::::" + amenity.amenityComponent.bean.description + "::" + amenity.isVisible());
                    amenity.setStyleIdle(renderStyleIconLevel0);
                }else if(amenity.getLevel()==1){
                    amenity.setStyleIdle(renderStyleIconLevel1);
                }else if(amenity.getLevel()==2){
                    amenity.setStyleIdle(renderStyleIconLevel2);
                }else if(amenity.getLevel()==3){
                    amenity.setStyleIdle(renderStyleIconLevel3);
                }
            }
        }
    }*/

    public class MapLevelAdapter extends ArrayAdapter<MapLevel> {

        Context mContext;
        List<MapLevel> levelList;

        public MapLevelAdapter(Context context, List<MapLevel> objects) {
            super(context, 0, objects);
            mContext = context;
            levelList = objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.level_text_view, null);
            }
            TextView lblLevel = (TextView) convertView.findViewById(R.id.lablLevel);
            lblLevel.setText(getShortFormLevel(getItem(position).getLevel()));
            lblLevel.setTag(getItem(position));
            return convertView;
        }

        public void selectLevel(int position) {
            for (MapLevel level : levelList) {
                level.setSelected(false);
            }
            levelList.get(position).setSelected(true);
            notifyDataSetChanged();
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.level_text_view, null);
            }
            TextView lblLevel = (TextView) convertView.findViewById(R.id.lablLevel);
            lblLevel.setText(getShortFormLevel(getItem(position).getLevel()));
            lblLevel.setTag(getItem(position));
            lblLevel.setPadding(0, 0, 20, 0);

            if (getItem(position).isSelected()) {
                lblLevel.setBackgroundResource(R.drawable.circular_bg_selected);
            } else {
                lblLevel.setBackgroundResource(R.drawable.circular_bg);
            }
            return convertView;
        }
    }
}
