package com.belongi.citycenter.ui.activity.spinwin;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.utilities.Validation;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.data.entities.QuestionAnswer;
import com.belongi.citycenter.data.entities.RegisterAnswer;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 22/6/15.
 */

public class QuestionActivity extends BaseActivity implements BackgroundJobClient,RadioGroup.OnCheckedChangeListener,View.OnClickListener{

    TextView lblQuestion;
    RadioButton rbtnAnswer1,rbtnAnswer2,rbtnAnswer3,rbtnAnswer4;
    QuestionAnswer questionRespo;
    Campaigns.Compaign contest;
    RegisterAnswer registerAnswer;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_question_answer);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
        contest= App.get().getSelectedContest();
        showProgressLoading();
        SpinAndWinLogic.getQuestionAnswer(this,contest.getCompaignId());
        lblQuestion= (TextView) findViewById(R.id.lblQuestion);
        RadioGroup rgrpAnswer= (RadioGroup) findViewById(R.id.rgrpAnswer);
        findViewById(R.id.btnSubmit).setOnClickListener(this);
        rbtnAnswer1= (RadioButton) findViewById(R.id.rbtnAns1);
        rbtnAnswer2= (RadioButton) findViewById(R.id.rbtnAns2);
        rbtnAnswer3= (RadioButton) findViewById(R.id.rbtnAns3);
        rbtnAnswer4= (RadioButton) findViewById(R.id.rbtnAns4);
        rgrpAnswer.setOnCheckedChangeListener(this);
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        stopLoading();
        if (result != null) {
            switch (requestCode){
                case SpinAndWinLogic.REQ_GET_QUESTION:
                    questionRespo= (QuestionAnswer) ContentDownloader.getObjectFromJson(result.toString(),QuestionAnswer.class);
                    if(TextUtils.isEmpty(questionRespo.getError())){
                        lblQuestion.setText(questionRespo.getQuestion());
                        rbtnAnswer1.setText(questionRespo.getAnswer().getAnswer1());
                        rbtnAnswer1.setTag(questionRespo.getAnswer().getAnswerId1());
                        rbtnAnswer2.setText(questionRespo.getAnswer().getAnswer2());
                        rbtnAnswer2.setTag(questionRespo.getAnswer().getAnswerId2());
                        rbtnAnswer3.setText(questionRespo.getAnswer().getAnswer3());
                        rbtnAnswer3.setTag(questionRespo.getAnswer().getAnswerId3());
                        rbtnAnswer4.setText(questionRespo.getAnswer().getAnswer4());
                        rbtnAnswer4.setTag(questionRespo.getAnswer().getAnswerId4());
                    }else{
                        finish();
                        if(questionRespo.getError().equals("Attempts Exceeded")){
                            Validation.UI.showLongToast(this,"Sorry you have reached the maximum tries for the day. Try again tomorrow.");
                        }else if(questionRespo.getError().equals("Already Answered")){
                            Validation.UI.showLongToast(this,"Sorry, your entry is already submitted with this campaign. Try your Luck with other campaigns.");
                        }
                    }
                    break;

                case SpinAndWinLogic.REQ_REGISTER_ANSWER:
                    registerAnswer= (RegisterAnswer) ContentDownloader.getObjectFromJson(result.toString(),RegisterAnswer.class);
                    getCorrectAnsText();
                    if(TextUtils.isEmpty(registerAnswer.getError())){
                        startActivity(new Intent(this,CorrectAnserActivity.class).putExtra(Constant.SELECTED_CONTEST,contest));
                    }else{
                        startActivity(new Intent(this, WrongAnswerActivity.class).putExtra(Constant.ATTEMPT_LEFT, registerAnswer));
                    }
                    finish();
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    break;
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnSubmit:
                String selectedAnswerId = null;
                if (rbtnAnswer1.isChecked()) {
                    selectedAnswerId = questionRespo.getAnswer().getAnswerId1();
                } else if (rbtnAnswer2.isChecked()) {
                    selectedAnswerId = questionRespo.getAnswer().getAnswerId2();
                } else if (rbtnAnswer3.isChecked()) {
                    selectedAnswerId = questionRespo.getAnswer().getAnswerId3();
                } else if (rbtnAnswer4.isChecked()) {
                    selectedAnswerId = questionRespo.getAnswer().getAnswerId4();
                } else {
                    Validation.UI.showLongToast(this, "Please select your Answer");
                }

                if(selectedAnswerId!=null){
                    SpinAndWinLogic.registerUserRightAnswer(this, contest.getCompaignId(), questionRespo.getQuestionId(), selectedAnswerId, questionRespo.getAttemptId());
                    showProgressLoading();
                }
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN";
    }

    private void getCorrectAnsText(){
        String correctAns=null;
        if(rbtnAnswer1.getTag().equals(questionRespo.getCorrectAnswerId())){
            correctAns=rbtnAnswer1.getText().toString();
        }else if(rbtnAnswer2.getTag().equals(questionRespo.getCorrectAnswerId())){
            correctAns=rbtnAnswer2.getText().toString();
        }else if(rbtnAnswer3.getTag().equals(questionRespo.getCorrectAnswerId())){
            correctAns=rbtnAnswer3.getText().toString();
        }else if(rbtnAnswer4.getTag().equals(questionRespo.getCorrectAnswerId())){
            correctAns=rbtnAnswer4.getText().toString();
        }
        registerAnswer.setCorrectAns(correctAns);
    }
}
