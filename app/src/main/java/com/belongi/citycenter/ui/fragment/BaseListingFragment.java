package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.utilities.DateTimeUtil;
import com.android.utilities.Validation;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.emsp.EmspHelper;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.ui.activity.ShopDetailActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by yashesh on 6/30/2015.
 */
public abstract class BaseListingFragment extends BaseFragment implements LocalDataLoader.QueryClient, AdapterView.OnItemClickListener {

    protected ListView lstContent;
    List items;
    ArrayAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lstContent = (ListView) view.findViewById(R.id.lstMain);
        lstContent.setOnItemClickListener(this);
        // get the listing.
        new LocalDataLoader(this, 0).execute();

        if(this instanceof EventListingFragment ){
            Log.e("Enter","Events");
            showAds("events");
        }else if(this instanceof OfferListingFragment){
            Log.e("Enter","offers");
            showAds("offers");
        }

    }

    public void showAds(String screenName){
        if(App.EMSP_INITIALIZED){
            if(Validation.Network.isConnected(getActivity())){
                // show General Ads
               // EmspHelper.showFullScreenAd(getActivity(), screenName);
                EmspHelper.showOverlayAd(getActivity(),screenName);

                //show Location based Ads
                if(App.EMSP_LOCATION_REGISTERED){
                   // EmspHelper.showLocBasedFullscreenAd(getActivity(), screenName);
                    EmspHelper.showLocBasedOverlayAd(getActivity(),screenName);
                }
            }
        }
    }


    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        if (result != null && ((List) result).size()>0) {
            items = (List) result;
            if(items.size()>0) {
                if (items.get(0) instanceof Event) {
                    setActionbarTitle("EVENTS");
                } else if (items.get(0) instanceof Offer) {
                    setActionbarTitle("OFFERS");
                } else if (items.get(0) instanceof Hotel) {
                    setActionbarTitle("HOTELS");
                }
                sortObjectList();
                lstContent.setAdapter(new ArrayAdapter(getActivity(), 0, 0, items) {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {
                            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.row_offer_item, null);
                        }
                        Object item = getItem(position);
                        final ImageView img = (ImageView) convertView.findViewById(R.id.imgOffer);
                        if (item instanceof Event) {
                            Picasso.with(getActivity()).load(WebApi.BASE_HOAST_URL + ((Event) item).getEvent_Image())
                                    .placeholder(R.drawable.place_holder_event1_6p).into(img, new Callback() {
                                @Override
                                public void onSuccess() {
                                    if(isVisible()) {
                                        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                                        img.setAnimation(anim);
                                        anim.start();
                                    }
                                }

                                @Override
                                public void onError() {
                                }
                            });
                            ((TextView) convertView.findViewById(R.id.lblOfferTitle)).setText(Html.fromHtml(((Event) item).getTitle().toUpperCase()));

                            if (((Event) item).getEvent_Start_Date() != null && ((Event) item).getEvent_End_Date() != null) {
                                ((TextView) convertView.findViewById(R.id.lblOfferDate)).setText(DateTimeUtil.dateFormat(((Event) item).getEvent_Start_Date(), "dd  MMM yyyy","dd/MM/yyyy") + " - " +
                                        DateTimeUtil.dateFormat(((Event) item).getEvent_End_Date(), "dd  MMM yyyy" ,"dd/MM/yyyy"));
                            } else {
                                ((TextView) convertView.findViewById(R.id.lblOfferDate)).setVisibility(View.GONE);
                            }
                           // desc = ((Event) item).getShort_Description();
                        } else if (item instanceof Offer) {
                            Picasso.with(getActivity()).load(WebApi.BASE_HOAST_URL + ((Offer) item).getImage_URL())
                                    .placeholder(R.drawable.place_holder_event1_6p).into(img, new Callback() {
                                @Override
                                public void onSuccess() {
                                    if(isVisible()) {
                                        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                                        img.setAnimation(anim);
                                        anim.start();
                                    }
                                }

                                @Override
                                public void onError() {
                                }
                            });
                            ((TextView) convertView.findViewById(R.id.lblOfferTitle)).setText(Html.fromHtml(((Offer) item).getTitle().toUpperCase()));
                            if (((Offer) item).getStart_Date() != null && ((Offer) item).getEnd_Date() != null) {
                                ((TextView) convertView.findViewById(R.id.lblOfferDate)).setText(DateTimeUtil.dateFormat(((Offer) item).getStart_Date(), "dd  MMM yyyy" ,"dd/MM/yyyy") + " - " +
                                        DateTimeUtil.dateFormat(((Offer) item).getEnd_Date(), "dd  MMM yyyy" ,"dd/MM/yyyy"));
                            } else {
                                ((TextView) convertView.findViewById(R.id.lblOfferDate)).setVisibility(View.GONE);
                            }
                           // desc = ((Offer) item).getDescription();
                        } else if (item instanceof Hotel) {
                            Picasso.with(getActivity()).load(WebApi.BASE_HOAST_URL + ((Hotel) item).getImage())
                                    .placeholder(R.drawable.place_holder_event1_6p).into(img, new Callback() {
                                @Override
                                public void onSuccess() {
                                    if (isVisible()) {
                                        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                                        img.setAnimation(anim);
                                        anim.start();
                                    }
                                }

                                @Override
                                public void onError() {
                                }
                            });
                            ((TextView) convertView.findViewById(R.id.lblOfferTitle)).setText(Html.fromHtml(((Hotel) item).getHotel_Name().toUpperCase()));
                            ((TextView) convertView.findViewById(R.id.lblOfferDate)).setVisibility(View.GONE);
                           // desc = ((Hotel) item).getDescription();
                        }
                       // ((TextView) convertView.findViewById(R.id.lblOfferDesc)).setText(Html.fromHtml(desc));
                        return convertView;
                    }
                });
                lstContent.setLayoutAnimation(new LayoutAnimationController
                        (AnimationUtils.loadAnimation(getActivity(), R.anim.animation_translate_in)));
            }
        }
    }
    private void sortObjectList() {
        Collections.sort(items, new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 instanceof Event) {
                    return ((Event) o1).getTitle().compareTo(((Event) o2).getTitle());
                } else if (o1 instanceof Offer) {
                    return ((Offer) o1).getTitle().compareTo(((Offer) o2).getTitle());
                } else if (o1 instanceof Hotel) {
                    return ((Hotel) o1).getHotel_Name().compareToIgnoreCase(((Hotel) o2).getHotel_Name());
                }else {
                    return 0;
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent detailIntent = new Intent(getActivity(), ShopDetailActivity.class);
        detailIntent.putExtra(Constant.DETAIL_SHOP_TYPE, (Serializable) items.get(position));
        getActivity().startActivity(detailIntent);
        getActivity().overridePendingTransition(0, 0);
    }
}
