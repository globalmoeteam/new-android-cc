package com.belongi.citycenter.ui.activity;

import android.os.Bundle;
import android.util.Log;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.ui.fragment.PreviewWebContentFragment;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;

/**
 * Created by webwerks on 29/6/15.
 */
public class WebContentPreviewActivity extends BaseActivity {


    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_web_content_preview);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();

        if ("STORE LOCATOR".equalsIgnoreCase(getIntent().getStringExtra("title"))) {
            String url = getIntent().getStringExtra("url");
            String[] strings = null;
            if (url.contains("?destId=")) {
                strings = url.split("destId=");
            }
            StoreLocatorNewFragment storeLocatorFragment = new StoreLocatorNewFragment();
            Bundle bundle = new Bundle();
            if (strings != null)
                if (strings.length >= 1) {
                    Log.e("dest_id", "" + strings[1]);
                    bundle.putString("dest_id", strings[1]);
                } else {
                    bundle.putString("dest_id", "");
                }

            Log.e("OnCLICK",bundle.getString("dest_id")+":::::");

            storeLocatorFragment.setArguments(bundle);
            fragmentTransaction(REPLACE_FRAGMENT, storeLocatorFragment, R.id.webContainer, false);
        }else {
            setActionbarTitle(getIntent().getStringExtra("title"));
            fragmentTransaction(REPLACE_FRAGMENT,
                    PreviewWebContentFragment.getInstance(getIntent().getStringExtra("url"), getIntent().getStringExtra("title"), getIntent().getStringExtra("category")), R.id.webContainer, false);
        }
    }

    @Override
    public String getScreenName() {
        return Constant.DONT_SEND;
    }
}
