package com.belongi.citycenter.ui.fragment;

import com.activeandroid.query.Select;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.global.App;

import java.util.List;

/**
 * Created by user on 6/30/2015.
 */
public class HotelListFragment extends BaseListingFragment{


    @Override
    public Object executeQuery() {
        return new Select().from(Hotel.class).where("mallid=?", App.get().getMallId()).execute();
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public String getScreenName() {
        return "Hotels";
    }
}
