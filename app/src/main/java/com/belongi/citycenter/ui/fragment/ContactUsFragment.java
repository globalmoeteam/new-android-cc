package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.activity.themall.MallContentPageActivity;


/**
 * Created by webwerks on 7/4/16.
 */
public class ContactUsFragment extends BaseFragment implements View.OnClickListener{

    TextView lblContactUs,lblFeedback;

    @Override
    public String getScreenName() {
        return "Contact Us";
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("CONTACT US");
        lblContactUs= (TextView) view.findViewById(R.id.lblContactInfo);
        lblFeedback= (TextView) view.findViewById(R.id.lblFeedback);

        lblContactUs.setOnClickListener(this);
        lblFeedback.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us,null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lblContactInfo:
                if(App.get().getArrContentPage()!=null && App.get().getArrContentPage().length>0) {

                    for(MallContentPage page:App.get().getArrContentPage()){
                        if(page.getTitle().toLowerCase().equals("contact details")){
                            getActivity().startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                                    .putExtra("PageModel", page)
                                    .putExtra("from", "contactus"));
                            getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                            break;
                        }
                    }
                }
                break;

            case R.id.lblFeedback:
                String feedbackUrl="";
                if(!TextUtils.isEmpty(App.get().getSelectedMall().getFeedbackUrl())
                        && App.get().getSelectedMall().getFeedbackUrl().startsWith("http")){
                    feedbackUrl=App.get().getSelectedMall().getFeedbackUrl();
                }else if(!TextUtils.isEmpty(App.get().getSelectedMall().getFeedbackUrl()) ){
                    feedbackUrl=App.get().getSelectedMall().getImageUrl()+"/"+App.get().getSelectedMall().getFeedbackUrl();
                }

                //String feedbackUrl=App.get().getSelectedMall().getImageUrl()+"/"+App.get().getSelectedMall().getFeedbackUrl();
                Log.e("Feedback Url ", App.get().getSelectedMall().getImageUrl() + " : " + feedbackUrl);
                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                        .putExtra("url", feedbackUrl)
                        .putExtra("title", "FEEDBACK")
                        .putExtra("category", "Contact Us"));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }
}
