package com.belongi.citycenter.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.android.utilities.ui.base._Fragment;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by webwerks on 14/6/15.
 */
public abstract class BaseFragment extends _Fragment {

    private Tracker mTracker;

    @Override
    public void onInternetAvailable() {
    }

    @Override
    public void onInternetUnavailable() {
    }

    public void setActionbarTitle(String title) {
        if (isAdded() && isVisible())
            ((BaseActivity) getActivity()).setActionbarTitle(title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread() {
            @Override
            public void run() {
                mTracker=App.get().getTracker();
                if (!(getScreenName().equalsIgnoreCase(Constant.DONT_SEND))) {
                    if (App.get().getSelectedMall() != null) {
                        // Set screen name.
                        mTracker.setScreenName(App.get().getSelectedMall().getMallCOde() + " : " + getScreenName());
                        // Send a screen view.
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                    }
                }
            }
        }.start();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public abstract String getScreenName();

}
