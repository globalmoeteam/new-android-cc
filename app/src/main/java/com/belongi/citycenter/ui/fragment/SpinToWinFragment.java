package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.UserLogin;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.spinwin.ForgotPasswordActivity;
import com.belongi.citycenter.ui.activity.spinwin.RegisterNewUserActivity;
import com.belongi.citycenter.ui.activity.spinwin.SpinToWinDashboardActivity;

/**
 * Created by webwerks on 15/6/15.
 */
public class SpinToWinFragment extends BaseFragment implements View.OnClickListener,BackgroundJobClient {

    EditText txtEmail,txtPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_spin_to_win,null);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("SPIN & WIN");
        txtEmail= (EditText) view.findViewById(R.id.txtEmail);
        txtPassword= (EditText) view.findViewById(R.id.txtPassword);
        view.findViewById(R.id.btnLogin).setOnClickListener(this);
        view.findViewById(R.id.btnRegister).setOnClickListener(this);
        view.findViewById(R.id.btnForgotPwd).setOnClickListener(this);

        /*((BaseActivity)getActivity()).buttonEffect(view.findViewById(R.id.btnLogin));
        ((BaseActivity)getActivity()).buttonEffect(view.findViewById(R.id.btnRegister));
        ((BaseActivity)getActivity()).buttonEffect(view.findViewById(R.id.btnForgotPwd));*/
    }

    @Override
    public void onResume() {
        super.onResume();
        txtEmail.setText("");
        txtPassword.setText("");
        txtEmail.requestFocus();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnLogin:
                if (!TextUtils.isEmpty(txtEmail.getText().toString().trim()) &&
                        Validation.isValidEmail(txtEmail.getText().toString().trim())) {

                    if (!TextUtils.isEmpty(txtPassword.getText().toString().trim())) {
                        SpinAndWinLogic.userLogin(SpinToWinFragment.this,txtEmail.getText().toString().trim(),
                                txtPassword.getText().toString().trim());
                        ((BaseActivity)getActivity()).showProgressLoading();
                    }else{
                        txtPassword.setError("Password enter password");
                        txtPassword.requestFocus();
                    }
                } else {
                    txtEmail.setError("Please enter a valid email address");
                    txtEmail.requestFocus();
                }
                break;

            case R.id.btnRegister:
                startActivity(new Intent(getActivity(), RegisterNewUserActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnForgotPwd:
                startActivity(new Intent(getActivity(), ForgotPasswordActivity.class)
                         .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        ((BaseActivity)getActivity()).stopLoading();
        if(result!=null) {
            UserLogin loginRespo= (UserLogin) ContentDownloader.getObjectFromJson(result.toString(), UserLogin.class);
            if(TextUtils.isEmpty(loginRespo.getError())){
                App.get().setLoginResponse(loginRespo.getLoginRespo());
                Validation.UI.showLongToast(getActivity(),"You have a maximum of 3 chances per day to take part in the draw");
                startActivity(new Intent(getActivity(), SpinToWinDashboardActivity.class));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }else{
                Validation.UI.showLongToast(getActivity(),loginRespo.getError());
                //Toast.makeText(getActivity(),loginRespo.getError(),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN Login";
    }
}