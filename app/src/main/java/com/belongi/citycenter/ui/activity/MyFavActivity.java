package com.belongi.citycenter.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.global.App;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yashesh on 7/13/2015.
 */
public class MyFavActivity extends BaseActivity implements View.OnClickListener{


    ListView lstItems;
    TextView txvNoData;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_my_fav);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("MY FAVORITES");
        lstItems= (ListView) findViewById(R.id.lstSearchResults);
        txvNoData = (TextView) findViewById(R.id.txv_no_data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<FavouriteItem> search= new Select().from(FavouriteItem.class).where("mallid=?", App.get().getMallId()).execute();
        if(search!=null && search.size()>0){
            lstItems.setAdapter(new ArrayAdapter<FavouriteItem>(this, 0, 0, search) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    if (convertView == null) {
                        convertView = LayoutInflater.from(MyFavActivity.this).inflate(R.layout.row_search_result, null);
                        convertView.findViewById(R.id.imgFavIncon).setVisibility(View.VISIBLE);
                    }
                    convertView.findViewById(R.id.imgFavIncon).setTag(getItem(position));
                    convertView.findViewById(R.id.imgFavIncon).setOnClickListener(MyFavActivity.this);
                    TextView lbl = (TextView) convertView.findViewById(R.id.lblSearchResult);
                    lbl.setText(getItem(position).getLabel());
                    Log.e("Category", getItem(position).getType());
                    int icon = 0;
                    if (getItem(position).getType().equalsIgnoreCase("HOTEL")) {
                        icon = R.drawable.icon_search_hotel;
                        lbl.setCompoundDrawablePadding(20);
                    } else if (getItem(position).getType().equalsIgnoreCase("DINING")) {
                        icon = R.drawable.icon_search_dine;
                        lbl.setCompoundDrawablePadding(20);
                    } else if (getItem(position).getType().equalsIgnoreCase("SHOPPING")) {
                        icon = R.drawable.icon_search_shopping;
                        lbl.setCompoundDrawablePadding(30);
                    } else if (getItem(position).getType().equalsIgnoreCase("EVENT")) {
                        icon = R.drawable.icon_search_event;
                        lbl.setCompoundDrawablePadding(20);
                    } else if (getItem(position).getType().equalsIgnoreCase("OFFER")) {
                        icon = R.drawable.icon_search_event;
                        lbl.setCompoundDrawablePadding(20);
                    } else if (getItem(position).getType().equalsIgnoreCase("ENTERTAINMENT")) {
                        icon = R.drawable.icon_search_entertainment;
                        lbl.setCompoundDrawablePadding(20);
                    } else if (getItem(position).getType().equalsIgnoreCase("SERVICE")) {
                        icon = R.drawable.icon_search_service;
                        lbl.setCompoundDrawablePadding(20);
                    } else if (getItem(position).getType().equalsIgnoreCase("ATM")) {
                        icon = R.drawable.icon_search_atm;
                        lbl.setCompoundDrawablePadding(20);
                    }
                    lbl.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
                    lbl.setTag(getItem(position));
                    lbl.setOnClickListener(MyFavActivity.this);
                    return convertView;
                }
            });
            lstItems.setLayoutAnimation(new LayoutAnimationController
                    (AnimationUtils.loadAnimation(MyFavActivity.this, R.anim.animation_translate_in)));
        }else{
            lstItems.setAdapter(null);
            //Toast.makeText(this,"No Data Found",Toast.LENGTH_SHORT).show();
            txvNoData.setVisibility(View.VISIBLE);
            txvNoData.setText("No Data Found");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        FavouriteItem search= (FavouriteItem) view.getTag();
        List detailObjects=null;

            switch(view.getId()){
                case R.id.lblSearchResult:
                    if (search.getType().equalsIgnoreCase("HOTEL")) {
                     detailObjects=   new Select().from(Hotel.class).where("identifier = ?",new String[]{search.getIdentifier()}).execute();
                    } else if (search.getType().equalsIgnoreCase("DINING")) {
                        detailObjects=    new Select().from(DiningShop.class).where("identifier = ?",new String[]{search.getIdentifier()}).execute();
                        for(DiningShop dine:(List<DiningShop>)detailObjects){
                            dine.setFavourited(true);
                        }
                    } else if (search.getType().equalsIgnoreCase("SHOPPING")) {
                        detailObjects=   new Select().from(ShoppingShop.class).where("identifier = ?",new String[]{search.getIdentifier()}).execute();
                        for(ShoppingShop shopping:(List<ShoppingShop>)detailObjects){
                            shopping.setFavourited(true);
                        }
                    }  else if (search.getType().equalsIgnoreCase("ENTERTAINMENT")) {
                        detailObjects=   new Select().from(Entertainment.class).where("identifier = ?",new String[]{search.getIdentifier()}).execute();
                        for(Entertainment entertainment:(List<Entertainment>)detailObjects){
                            entertainment.setFavourited(true);
                        }
                    }
                    if(detailObjects!=null && detailObjects.size()>0){
                        Intent detailInent=new Intent(this,ShopDetailActivity.class);
                        detailInent.putExtra(Constant.DETAIL_SHOP_TYPE,(Serializable)detailObjects.get(0));
                        startActivity(detailInent);
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    }
                    break;

                case R.id.imgFavIncon:
                    ((ArrayAdapter) lstItems.getAdapter() ).remove(search);
                    ((ArrayAdapter) lstItems.getAdapter() ).notifyDataSetChanged();
                    new Delete().from(FavouriteItem.class).where("identifier = ?",new String[]{search.getIdentifier()}).execute();
                    break;
            }
    }

    @Override
    public String getScreenName() {
        return "Favorites";
    }
}
