package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;

/**
 * Created by webwerks on 29/6/15.
 */
public class GiftCardFragment extends BaseFragment implements View.OnClickListener{

    private ImageView ic_gift_card;
    private String knowMoreURL= Constant.KNOW_MORE_URL;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gift_card,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("MALL GIFT CARD");
        ic_gift_card= (ImageView) view.findViewById(R.id.imgGiftCard);
        ic_gift_card.setOnClickListener(this);

        view.findViewById(R.id.btnKnowMore).setOnClickListener(this);
        view.findViewById(R.id.btnChkBal).setOnClickListener(this);

        String description = "One convenient card allows recipients to buy what they want when they want. They will enjoy shopping at over 2100 stores at all of their favourite shopping malls in a number of different countries. See <a href='https://www.mallgiftcard.ae/card-acceptance-in-malls/'>Where To Buy</a> for a list of shopping malls.";
        switch (App.get().getSelectedMall().getIdentifier()) {
            case BEIRUT:
                description = "Introducing our C Gift Cards:</br>" +
                        "</br>" +
                        "Enjoy a selection of six amazing “C Gift Cards” for the year’s most celebrated occasions.";

                view.findViewById(R.id.btnChkBal).setVisibility(View.GONE);
                ic_gift_card.setImageResource(R.drawable.ic_gift_card_beirut);
                knowMoreURL=Constant.KNOW_MORE_URL_BERUIT;
                break;
        }

        WebView wvGiftCardDesc= (WebView) view.findViewById(R.id.wvGiftCardDesc);

        wvGiftCardDesc.getSettings().setJavaScriptEnabled(true);
        wvGiftCardDesc.loadData(description, "text/html", "UTF-8");
        wvGiftCardDesc.loadDataWithBaseURL("file:///android_asset/",((BaseActivity)getActivity()).getStyleString(description),"text/html","UTF-8",null);
        wvGiftCardDesc.setBackgroundColor(0x00000000);
        wvGiftCardDesc.setBackgroundColor(Color.argb(1, 0, 0, 0));
        wvGiftCardDesc.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                startActivity(new Intent(getActivity(),WebContentPreviewActivity.class)
                        .putExtra("url",url)
                        .putExtra("title","MALL GIFT CARD")
                        .putExtra("category","Gift Cards"));
                return true;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgGiftCard:
                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                        .putExtra("url", Constant.GIFT_CARD_URL)
                        .putExtra("title","MALL GIFT CARD")
                        .putExtra("category","Gift Cards"));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnKnowMore:
                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                        .putExtra("url", knowMoreURL)
                        .putExtra("title", "MALL GIFT CARD")
                        .putExtra("category", "Gift Cards"));
                /*if(App.get().getArrContentPage()!=null && App.get().getArrContentPage().length>0){
                    for(MallContentPage page:App.get().getArrContentPage()){
                        if(page.getTitle().toLowerCase().equals("know more")){
                            startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                                    .putExtra("url", App.get().getArrContentPage()[5].getPageDetail())
                                    .putExtra("title", App.get().getArrContentPage()[5].getTitle().toUpperCase())
                                    .putExtra("category", "Gift Cards"));
                            getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                            break;
                        }
                    }
                }*/
                break;

            case R.id.btnChkBal:
                startActivity(new Intent(getActivity(), WebContentPreviewActivity.class)
                        .putExtra("url", App.get().getSelectedMall().getImageUrl()+"/balance-enquiry")
                        .putExtra("title","MALL GIFT CARD")
                        .putExtra("category","Gift Cards"));
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Gift Cards";
    }
}
