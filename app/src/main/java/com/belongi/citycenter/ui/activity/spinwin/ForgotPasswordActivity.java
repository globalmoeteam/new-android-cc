package com.belongi.citycenter.ui.activity.spinwin;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.ForgotPassword;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 15/6/15.
 */
public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener,BackgroundJobClient {

    EditText txtEmail;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_forgot_password);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("FORGOT PASSWORD");
        findViewById(R.id.btnSubmit).setOnClickListener(this);
        findViewById(R.id.btnClickHere).setOnClickListener(this);
        txtEmail= (EditText) findViewById(R.id.txtEmail);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnSubmit:
                if(Validation.isValidEmail(txtEmail.getText().toString().trim())){
                    SpinAndWinLogic.forgotPassword(this,txtEmail.getText().toString().trim());
                    showProgressLoading();
                }else{
                    txtEmail.setError("Please enter a valid email address");
                    txtEmail.requestFocus();
                }
                break;

            case R.id.btnClickHere:
                startActivity(new Intent(ForgotPasswordActivity.this,ResetPasswordActivity.class));
                finish();
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN ForgotPassword";
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        stopLoading();
        if(result!=null) {
            ForgotPassword respo= (ForgotPassword) ContentDownloader.getObjectFromJson(result.toString(),ForgotPassword.class);
            if(TextUtils.isEmpty(respo.getError())){
                Validation.UI.showLongToast(this,"Your Password reset request is processed and a security code had been sent to your email address. use the code to reset or change your password.");
                /*startActivity(new Intent(this,ResetPasswordActivity.class));
                finish();*/
            }else{
                Validation.UI.showLongToast(this,respo.getError());
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }
}
