package com.belongi.citycenter.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.activeandroid.query.Select;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.ShopDetailActivity;
import com.belongi.citycenter.ui.activity.SpotlightDetailsActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.belongi.citycenter.global.WebApi.BASE_HOAST_URL;

/**
 * Created by yashesh on 6/23/2015.
 */
public class Utils {

    private static final String TAG = Utils.class.getSimpleName();
    private static int CURRENT_INDEX = 0;
    static String linkUrl = null;

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void setTopViews(ViewGroup layout, final Activity activity) {

        layout.removeAllViews();
        layout.invalidate();

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int height = layout.getMeasuredHeight();
        LinearLayout.LayoutParams spotLightParams = new LinearLayout.LayoutParams((int) (0.85 * height), height);
        ImageView imgSpotLight = new ImageView(activity);
        imgSpotLight.setLayoutParams(spotLightParams);
        imgSpotLight.setScaleType(ImageView.ScaleType.FIT_XY);
        imgSpotLight.setPadding(1, 1, 1, 1);
        // add spotlight image into scroll container
        layout.addView(imgSpotLight);
        // schedule timely loading of spotlight images
        scheduleLoading(activity, imgSpotLight);

        imgSpotLight.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // int spotlightBannerIndex = (int) imgSpotLight.getTag();
                        Intent spotLightDetailIntent = new Intent(activity, SpotlightDetailsActivity.class);
                        //  spotLightDetailIntent.putExtra(SpotlightDetailsActivity.SPOTLIGHT_INDEX, spotlightBannerIndex);
                        ((Activity) activity).startActivity(spotLightDetailIntent);
                        ((Activity) activity).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    }
                }
        );

        GridLayout gridOffersEvents = new GridLayout(activity);
        gridOffersEvents.setPadding(1, 1, 1, 1);
        gridOffersEvents.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
        gridOffersEvents.setRowCount(2);
        //gridOffersEvents.setColumnCount((App.get().getHomeScreenData().
        //
        // getOffersAndEvents().size() / 2));

        Log.e(TAG, "Offer and Event Count=" + App.get().getHomeScreenData().getOffersAndEvents().size());
        if (App.get().getHomeScreenData().getOffersAndEvents().size() > 0) {
            int columnCount = 1;

            if (App.get().getHomeScreenData().getOffersAndEvents().size() > 1) {
                columnCount = App.get().getHomeScreenData().getOffersAndEvents().size() / 2;
            }

            gridOffersEvents.setColumnCount(columnCount);
        }

        // add gridlayout into scroll container
        layout.addView(gridOffersEvents);
        int c = 0;
        int r = 0;
        Log.e("OFFRE DATA", App.get().getHomeScreenData().getOffersAndEvents().size() + ":::::" + gridOffersEvents.getColumnCount());

        int totalTileCount = gridOffersEvents.getColumnCount();

        if (App.get().getHomeScreenData().getOffersAndEvents().size() > 1) {
            totalTileCount = totalTileCount * 2;
        }

        for (int i = 0; i < totalTileCount; i++) {
            //for (final Object item : App.get().getHomeScreenData().getOffersAndEvents()) {

            final Object item = App.get().getHomeScreenData().getOffersAndEvents().get(i);

            int imageheight = layout.getMeasuredHeight() / 2;
            int width = (int) (1.33 * (imageheight));

            GridLayout.LayoutParams imageParams = new GridLayout.LayoutParams();
            imageParams.width = width;
            imageParams.height = imageheight;

            imageParams.columnSpec = GridLayout.spec(c);


            imageParams.rowSpec = GridLayout.spec(r);
            Log.e("ROW COL", r + " " + c + " " + gridOffersEvents.getColumnCount());

            RelativeLayout catgoryRowLayout = new RelativeLayout(activity);
            catgoryRowLayout.setLayoutParams(imageParams);
            ImageView imgIndicator = new ImageView(activity);
            imgIndicator.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
            imgIndicator.setScaleType(ImageView.ScaleType.FIT_XY);

            final ImageView img = new ImageView(activity);
            img.setLayoutParams(new RelativeLayout.LayoutParams(width, imageheight));
            img.setScaleType(ImageView.ScaleType.FIT_XY);
            img.setPadding(1, 1, 1, 1);
            if (item instanceof Offer) {
                //  Log.w("OFFER IMG",".."+((Offer) item).getImage_URL());
                String url = "";
                if (!(((Offer) item).getImage_URL().startsWith("http"))) {
                    url = BASE_HOAST_URL + ((Offer) item).getImage_URL();
                } else {
                    url = ((Offer) item).getImage_URL();
                }

                Log.e("URL", url + ":::");

                Picasso.with(activity).load(url).placeholder(R.drawable.place_holder_event1_6p).into(img);
                imgIndicator.setBackgroundResource(R.drawable.ico_cat_offers6);


            } else if (item instanceof Event) {

                //  Log.w("EVENT IMG",".."+((Event) item).getEvent_Image());

                String url = "";
                if (!(((Event) item).getEvent_Image().startsWith("http"))) {
                    url = BASE_HOAST_URL + ((Event) item).getEvent_Image();
                } else {
                    url = ((Event) item).getEvent_Image();
                }
                if (url.length() > 0) {
                    Picasso.with(activity).load(url).placeholder(R.drawable.place_holder_event1_6p).fit().into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                            Animation anim = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
                            img.setAnimation(anim);
                            anim.start();
                        }

                        @Override
                        public void onError() {
                            img.setImageResource(R.drawable.place_holder_event1_6p);
                        }
                    });
                }
                imgIndicator.setBackgroundResource(R.drawable.ico_cat_event6);
            }

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ShopDetailActivity.class);
                    intent.putExtra(Constant.DETAIL_SHOP_TYPE, (Serializable) item);
                    activity.startActivity(intent);
                    ((Activity) activity).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
            });

            catgoryRowLayout.addView(img);
            catgoryRowLayout.addView(imgIndicator);

            //gridOffersEvents.addView(img);
            gridOffersEvents.addView(catgoryRowLayout);
            c++;
            if (c > (gridOffersEvents.getColumnCount() - 1)) {
                r++;
                c = 0;
            }
        }
    }

    public static void scheduleLoading(final Context context, final ImageView imageView) {
        CURRENT_INDEX = 0;
        Log.e("scheduleLoading", "scheduleLoading");

        Timer timer = new Timer();

        final Handler handle = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (App.get().getHomeScreenData().getSpotlightBanners() != null && App.get().getHomeScreenData().getSpotlightBanners().length > 0) {
                    if (CURRENT_INDEX < App.get().getHomeScreenData().getSpotlightBanners().length) {
                        final String url = BASE_HOAST_URL + App.get().getHomeScreenData().getSpotlightBanners()[CURRENT_INDEX].getImage();
                        imageView.setTag(CURRENT_INDEX);

                        Picasso.with(context).load(url).placeholder(R.drawable.place_holder_spotlight6p).into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                Animation anim = AnimationUtils.loadAnimation(context, R.anim.fade_in);
                                imageView.setAnimation(anim);
                                anim.start();
                            }

                            @Override
                            public void onError() {
                            }
                        });
                    }

                    if (CURRENT_INDEX < (App.get().getHomeScreenData().getSpotlightBanners().length - 1)) {
                        CURRENT_INDEX += 1;
                    } else {
                        CURRENT_INDEX -= (App.get().getHomeScreenData().getSpotlightBanners().length - 1);
                    }
                }
            }
        };
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handle.sendEmptyMessage(0);
            }
        }, 0, 8000);
    }

    public static void setBottomViews(ViewGroup layout, final Activity activity) {
        layout.removeAllViews();
        layout.invalidate();

        int height = layout.getMeasuredHeight();
        LinearLayout.LayoutParams spotLightParams = new LinearLayout.LayoutParams((int) (0.66 * height), height);
        RelativeLayout innerLayout = new RelativeLayout(activity);

        ImageView imgIndicatorMovie = new ImageView(activity);
        imgIndicatorMovie.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
        imgIndicatorMovie.setScaleType(ImageView.ScaleType.FIT_XY);
        imgIndicatorMovie.setBackgroundResource(R.drawable.ico_cat_entertainment6);

        final ImageView imgMovie = new ImageView(activity);
        imgMovie.setLayoutParams(spotLightParams);
        imgMovie.setScaleType(ImageView.ScaleType.FIT_XY);
        imgMovie.setPadding(1, 1, 1, 1);
        imgMovie.setBackgroundResource(R.drawable.place_holder_movie1_6p);

        innerLayout.addView(imgMovie);
        innerLayout.addView(imgIndicatorMovie);
        layout.addView(innerLayout);
        String url = "";

        if (App.get().getSelectedMall().getMovieCount() == 0) {
            imgMovie.setVisibility(View.GONE);
            imgIndicatorMovie.setVisibility(View.GONE);
        } else {
            if (App.get().getHomeScreenData().getMovies() != null && App.get().getHomeScreenData().getMovies().length > 0 && App.get().getHomeScreenData().getMovies()[0].getImage() != null) {
                if (!(App.get().getHomeScreenData().getMovies()[0].getImage().startsWith("http"))) {
                    url = BASE_HOAST_URL + App.get().getHomeScreenData().getMovies()[0].getImage();
                } else {
                    url = App.get().getHomeScreenData().getMovies()[0].getImage();
                }

                Picasso.with(activity).load(url).placeholder(R.drawable.place_holder_movie1_6p).into(imgMovie, new Callback() {
                    @Override
                    public void onSuccess() {
                        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
                        imgMovie.setAnimation(anim);
                        anim.start();
                    }

                    @Override
                    public void onError() {
                    }
                });

                linkUrl = App.get().getHomeScreenData().getMovies()[0].getURL();
                if (linkUrl != null && !linkUrl.startsWith("http")) {
                    linkUrl = BASE_HOAST_URL + App.get().getHomeScreenData().getMovies()[0].getURL();
                } else if (TextUtils.isEmpty(linkUrl)) {
                    linkUrl = App.get().getHomeScreenData().getMovies()[0].getURL();
                } else {
                    linkUrl = getVoxUrl(Integer.parseInt(App.get().getSelectedMall().getSiteId()));
                }
            } else {
                Picasso.with(activity).load(R.drawable.vox_static).placeholder(R.drawable.place_holder_movie1_6p).into(imgMovie);
                linkUrl = getVoxUrl(Integer.parseInt(App.get().getSelectedMall().getSiteId()));
            }
        }

        if (!TextUtils.isEmpty(linkUrl)) {

            imgMovie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, WebContentPreviewActivity.class);
                    intent.putExtra("title", "MOVIES@VOX");
                    intent.putExtra("url", linkUrl);
                    intent.putExtra("category", "MOVIE");
                    activity.startActivity(intent);
                    ((Activity) activity).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
            });
        }


        if (App.get().getHomeScreenData().getEntertainments() != null && App.get().getHomeScreenData().getEntertainments().size() > 0) {
            if (App.get().getHomeScreenData().getEntertainments().size() < 2) {
                for (int i = 0; i < 2 - App.get().getHomeScreenData().getEntertainments().size(); i++) {
                    App.get().getHomeScreenData().getEntertainments().add(new Entertainment());
                }
            }

            for (final Entertainment item : App.get().getHomeScreenData().getEntertainments()) {
                if (item != null) {
                    LinearLayout.LayoutParams entertainmentParams = new LinearLayout.LayoutParams((int) (1.33 * height), height);
                    RelativeLayout relativeLayout = new RelativeLayout(activity);

                    ImageView imgIndicator = new ImageView(activity);
                    imgIndicator.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
                    imgIndicator.setScaleType(ImageView.ScaleType.FIT_XY);
                    imgIndicator.setBackgroundResource(R.drawable.ico_cat_entertainment6);

                    final ImageView imgEntertainment = new ImageView(activity);
                    imgEntertainment.setLayoutParams(entertainmentParams);
                    imgEntertainment.setScaleType(ImageView.ScaleType.FIT_XY);
                    imgEntertainment.setPadding(1, 1, 1, 1);
                    imgEntertainment.setBackgroundResource(R.drawable.place_holder_event1_6p);

                    relativeLayout.addView(imgEntertainment);
                    relativeLayout.addView(imgIndicator);
                    //layout.addView(imgEntertainment);
                    layout.addView(relativeLayout);
                    String url1 = "";
                    final Entertainment entertainment = getEntertainmentById(item.getIdentifier());
                    if (entertainment != null) {
                        if (entertainment.getThumbnail() != null) {
                            if (!(entertainment.getThumbnail().startsWith("http"))) {
                                url1 = BASE_HOAST_URL + entertainment.getThumbnail();
                            } else {
                                url1 = item.getThumbnail();
                            }

                            List<FavouriteItem> favList = new Select().from(FavouriteItem.class).where("identifier = ?", new String[]{entertainment.getIdentifier()}).execute();
                            if (favList.size() > 0) {
                                entertainment.setFavourited(true);
                            } else {
                                entertainment.setFavourited(false);
                            }

                            imgEntertainment.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(activity, ShopDetailActivity.class);
                                    intent.putExtra(Constant.DETAIL_SHOP_TYPE, entertainment);
                                    activity.startActivity(intent);
                                    ((Activity) activity).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                                }
                            });

                            if (url1.length() > 0) {
                                Picasso.with(activity).load(url1).placeholder(R.drawable.place_holder_event1_6p).into(imgEntertainment, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
                                        imgEntertainment.setAnimation(anim);
                                        anim.start();
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    }

    public static Entertainment getEntertainmentById(String id) {
        if (!TextUtils.isEmpty(id)) {
            List<Entertainment> entrList = new Select().from(Entertainment.class).where("identifier=?", id).execute();
            if (entrList != null && entrList.size() > 0)
                return entrList.get(0);
        }
        return null;
    }

    public static String getVoxUrl(int siteId) {
        String voxUrl = null;
        switch (siteId) {
            case 1050:
                voxUrl = "https://www.citycentredeira.com/vox-cinemas.aspx";
                break;

            case 5329:
                voxUrl = "https://www.citycentremirdif.com/vox-cinemas.aspx";
                break;

            case 5231:
                voxUrl = "https://www.citycentreajman.com/vox-cinemas.aspx";
                break;

            case 9164:
                voxUrl = "https://www.citycentrefujairah.com/vox-cinemas.aspx";
                break;

            case 8560:
                voxUrl = "https://www.citycentrequrum.com/vox-cinemas.aspx";
                break;

            case 7352:
                voxUrl = "https://www.citycentremuscat.com/vox-cinemas.aspx";
                break;

            case 2197:
                voxUrl = "https://www.citycentremallbeirut.com/vox-cinemas.aspx";
                break;

            case 44517:
                voxUrl = "https://www.citycentreshindagha.com/entertainment/vox-cinemas.aspx";
                break;


        }
        return voxUrl;
    }
}
