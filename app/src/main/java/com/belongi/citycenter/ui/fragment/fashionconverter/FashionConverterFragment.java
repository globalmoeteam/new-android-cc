package com.belongi.citycenter.ui.fragment.fashionconverter;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.fragment.BaseFragment;
import com.belongi.citycenter.ui.view.spinnerwheel.AbstractWheelTextAdapter;
import com.belongi.citycenter.ui.view.spinnerwheel.OnWheelScrollListener;
import com.belongi.citycenter.ui.view.spinnerwheel.WheelView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by webwerks on 15/6/15.
 */
public class FashionConverterFragment extends BaseFragment {

    public static FashionConverterFragment newInstance(String type,String data) {
        FashionConverterFragment fragment = new FashionConverterFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        bundle.putString("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    private boolean scrolling = false;
    String[] regions = {"UK", "EUROPE", "US", "Japan"};
    String[] eurVal, japVal, usVal, ukVal ;
    TextView lblEuVal, lblJpVal, lblUsVal, lblUkVal;
    WheelView whRegion,whSize;
    View vwCategory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shoe_size_converter,null);
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lblEuVal = (TextView)view.findViewById(R.id.lblEuVal);
        lblJpVal = (TextView)view.findViewById(R.id.lblJpVal);
        lblUsVal = (TextView)view.findViewById(R.id.lblUsVal);
        lblUkVal = (TextView)view.findViewById(R.id.lblUkVal);

        whRegion = (WheelView)view.findViewById(R.id.whRegion);
        whSize = (WheelView)view.findViewById(R.id.whSize);

        vwCategory=view.findViewById(R.id.vwCategory);

        setActionbarTitle(getArguments().getString("data").toUpperCase());

        if(getArguments().getString("type").equals("Men")){
            vwCategory.setBackgroundResource(R.drawable.btn_f_men);
        }else if(getArguments().getString("type").equals("Women")){
            vwCategory.setBackgroundResource(R.drawable.btn_f_women);
        }else if(getArguments().getString("type").equals("Children")){
            vwCategory.setBackgroundResource(R.drawable.btn_f_kids);
        }
        new GetDetails(getActivity()).execute();
    }

    @Override
    public String getScreenName() {
        if(getArguments().getString("type").equals("Men")) {
            return "Utilities - Men  Fashion Converter - " + getArguments().getString("data");
        }else if(getArguments().getString("type").equals("Women")) {
            return "Utilities - Women Fashion Converter - " + getArguments().getString("data");
        }else if(getArguments().getString("type").equals("Children")) {
            return "Utilities - Kids Fashion Converter - " + getArguments().getString("data");
        }else{
            return Constant.DONT_SEND;
        }
    }

    class GetDetails extends AsyncTask<Void, Void, Void> {
        protected Context mContext;
        public GetDetails(Context context) {
            this.mContext = context;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            StringBuffer sb = new StringBuffer();
            BufferedReader br=null;

            try {
                br = new BufferedReader(new InputStreamReader(getActivity().getAssets().open("fashion_converter.json")));
                String temp;

                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }


            try {
                JSONObject obj = new JSONObject(new JSONTokener(sb.toString()));
                JSONObject sizes = obj.getJSONObject("FashionSizeConvertor");
                JSONArray sizeArray = sizes.getJSONObject(getArguments().getString("type")).
                        getJSONArray(getArguments().getString("data"));

                eurVal = new String[sizeArray.length()];
                usVal = new String[sizeArray.length()];
                ukVal = new String[sizeArray.length()];
                japVal = new String[sizeArray.length()];

                for(int i = 0; i < sizeArray.length(); i++)
                {
                    eurVal[i] = sizeArray.getJSONObject(i).getString("Europe");
                    usVal[i] = sizeArray.getJSONObject(i).getString("US");
                    ukVal[i] = sizeArray.getJSONObject(i).getString("UK");
                    japVal[i] = sizeArray.getJSONObject(i).getString("Japan");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            whRegion.setVisibleItems(4);
            whRegion.setViewAdapter(new RegionAdapter(getActivity()));
            whRegion.setCurrentItem(0);

            whSize.setVisibleItems(4);
            whSize.setViewAdapter(new SizeAdapter(getActivity()));
            whSize.setCurrentItem(0);

            whRegion.addScrollingListener( new OnWheelScrollListener() {
                public void onScrollingStarted(WheelView wheel) {
                    scrolling = true;
                }
                public void onScrollingFinished(WheelView wheel) {
                    scrolling = false;
                    whSize.setViewAdapter(new SizeAdapter(getActivity()));
                    whSize.setCurrentItem(0);

                    lblEuVal.setText(eurVal[whSize.getCurrentItem()]);
                    lblJpVal.setText(japVal[whSize.getCurrentItem()]);
                    lblUsVal.setText(usVal[whSize.getCurrentItem()]);
                    lblUkVal.setText(ukVal[whSize.getCurrentItem()]);
                }
            });

            whSize.addScrollingListener( new OnWheelScrollListener() {
                public void onScrollingStarted(WheelView wheel) {
                    scrolling = true;
                }
                public void onScrollingFinished(WheelView wheel) {
                    scrolling = false;

                    lblEuVal.setText(eurVal[whSize.getCurrentItem()]);
                    lblJpVal.setText(japVal[whSize.getCurrentItem()]);
                    lblUsVal.setText(usVal[whSize.getCurrentItem()]);
                    lblUkVal.setText(ukVal[whSize.getCurrentItem()]);
                }
            });

            lblEuVal.setText(eurVal[whSize.getCurrentItem()]);
            lblJpVal.setText(japVal[whSize.getCurrentItem()]);
            lblUsVal.setText(usVal[whSize.getCurrentItem()]);
            lblUkVal.setText(ukVal[whSize.getCurrentItem()]);
        }
    }

    private class RegionAdapter extends AbstractWheelTextAdapter {

        protected RegionAdapter(Context context) {
            super(context, android.R.layout.simple_list_item_1, NO_RESOURCE);
            setItemTextResource(android.R.id.text1);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            return view;
        }

        @Override
        public int getItemsCount() {
            return regions.length;
        }

        @Override
        protected CharSequence getItemText(int index) {
            return regions[index];
        }
    }

    private class SizeAdapter extends AbstractWheelTextAdapter {

        protected SizeAdapter(Context context) {
            super(context, android.R.layout.simple_list_item_1, NO_RESOURCE);
            setItemTextResource(android.R.id.text1);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);

            return view;
        }

        @Override
        public int getItemsCount() {
            if(whRegion.getCurrentItem() == 0)
                return ukVal.length;
            else if(whRegion.getCurrentItem() == 1)
                return eurVal.length;
            else if(whRegion.getCurrentItem() == 2)
                return usVal.length;
            else if(whRegion.getCurrentItem() == 3)
                return japVal.length;
            else
                return eurVal.length;
        }

        @Override
        protected CharSequence getItemText(int index) {
            if(whRegion.getCurrentItem() == 0)
                return ukVal[index];
            else if(whRegion.getCurrentItem() == 1)
                return eurVal[index];
            else if(whRegion.getCurrentItem() == 2)
                return usVal[index];
            else if(whRegion.getCurrentItem() == 3)
                return japVal[index];
            else
                return eurVal[index];
        }
    }
}
