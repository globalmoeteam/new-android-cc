package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belongi.citycenter.R;

import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class CountryAdapter extends ArrayAdapter<String> {

    Context mContext;

    public CountryAdapter(Context context, String[] objects) {
        super(context, 0, objects);
        mContext=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1,null);
        TextView lblTitle= (TextView) convertView.findViewById(android.R.id.text1);
        lblTitle.setText(getItem(position));
        lblTitle.setTextColor(mContext.getResources().getColor(R.color.text_color));
        lblTitle.setPadding(10,0,0,0);
        lblTitle.setTextSize(14);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1,null);
        TextView lblTitle= (TextView) convertView.findViewById(android.R.id.text1);
        lblTitle.setText(getItem(position));
        lblTitle.setTextSize(14);
        return convertView;
    }
}
