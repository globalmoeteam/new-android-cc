package com.belongi.citycenter.ui.activity;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import com.android.utilities.Validation;
import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.PermissionUtils;
import com.belongi.citycenter.ui.fragment.EventListingFragment;
import com.belongi.citycenter.ui.fragment.GiftCardFragment;
import com.belongi.citycenter.ui.fragment.OfferListingFragment;
import com.belongi.citycenter.ui.fragment.PreviewWebContentFragment;
import com.belongi.citycenter.ui.fragment.SpinToWinFragment;
import com.belongi.citycenter.ui.fragment.categories.StoreListingFragment;
import com.belongi.citycenter.ui.fragment.home.InsideMallFragment;
import com.belongi.citycenter.ui.fragment.home.OutsidemallFragment;
import com.belongi.citycenter.ui.fragments.home.AbstractHomeFragment;
import com.belongi.citycenter.ui.view.residemenu.ResideMenu;
import com.belongi.citycenter.ui.view.residemenu.ResideMenuManager;


public class HomeActivity extends BaseActivity implements PermissionUtils.PermissionProcessor {

    private ResideMenu resideMenu;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        resideMenu = ResideMenuManager.getInstance().setResideMenu(this,this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.sidebar6);
        setActionbarTitle(App.get().getSelectedMall().getMallName().toUpperCase());

       /* Fragment fragment = null;
        if (App.get().isInsideTheMall()) {
            fragment = new InsideMallFragment();
        } else {
            fragment = new OutsidemallFragment();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && hasSoftKeys()) {
            int navBarHeight = getNavigationBarHeight();
            findViewById(R.id.container).setPadding(0, 0, 0, navBarHeight);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();*/

        moveToFragment();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        moveToFragment();
    }


    private void moveToFragment() {
        Fragment fragment = null;

        switch (App.get().getNotificationKey()) {
            case Constant.NotificationKey.ALL:
                App.get().setNotificationKey("");
                if (App.get().isInsideTheMall()) {
                    fragment = new InsideMallFragment();
                } else {
                    fragment = new OutsidemallFragment();
                }
                break;

            case Constant.NotificationKey.FASHION_LADIES:
                App.get().setNotificationKey("");
                setActionbarTitle("SHOP & DINE");
                fragment = StoreListingFragment.getInstance(StoreListingFragment.CategoryType.SHOPPING);
                break;
            case Constant.NotificationKey.FASHION_MEN:
                App.get().setNotificationKey("");
                setActionbarTitle("SHOP & DINE");
                fragment = StoreListingFragment.getInstance(StoreListingFragment.CategoryType.SHOPPING);
                break;

            case Constant.NotificationKey.DINING:
                App.get().setNotificationKey("");
                setActionbarTitle("SHOP & DINE");
                fragment = StoreListingFragment.getInstance(StoreListingFragment.CategoryType.DINING);
                break;

            case Constant.NotificationKey.OFFERS:
                App.get().setNotificationKey("");
                setActionbarTitle("OFFERS");
                fragment = new OfferListingFragment();
                break;

            case Constant.NotificationKey.EVENTS:
                App.get().setNotificationKey("");
                setActionbarTitle("EVENTS");
                fragment = new EventListingFragment();

                break;

            case Constant.NotificationKey.HOMESCREEN:
                App.get().setNotificationKey("");

                if (App.get().isInsideTheMall()) {
                    fragment = new InsideMallFragment();
                } else {
                    fragment = new OutsidemallFragment();
                }

                break;

            case Constant.NotificationKey.GIFT_CARD:
                App.get().setNotificationKey("");
                fragment = new GiftCardFragment();
                setActionbarTitle("MALL GIFT CARD");
                break;

           /* case Constant.NotificationKey.HANDS_FREE:
                App.get().setNotificationKey("");

                setActionbarTitle(getResources().getString(R.string.title_handsfree));

                if ((Preferences.getBoolean(Constant.PREF_NAME, App.get(), Constant.PREF_KEY_IS_HANDSFREE_REGISTER, false))) {
                    fragment = new HandsfreeFragment();
                } else {
                    if ((Preferences.getBoolean(Constant.PREF_NAME, App.get(), Constant.PREF_HANDSFREE_WLCOME_SCREEN_NOT_SHOW, false))) {
                        fragment = RegisterMobileFragment.getInstance("0");
                    } else {
                        fragment = new HandsFreeWelcomeFragment();
                    }
                }

                break;*/

            case Constant.NotificationKey.SPIN_WIN:
                App.get().setNotificationKey("");
                setActionbarTitle("SPIN & WIN");
                fragment= new SpinToWinFragment();

                break;

            case Constant.NotificationKey.MALL_PROMOTIONS:
                App.get().setNotificationKey("");

                String title =getResources().getString(R.string.title_mall_promotions);
                setActionbarTitle(title);
                fragment= PreviewWebContentFragment.getInstance(App.get().getSelectedMall().getMallPromotionUrl(), title, title);

                break;


            default:
                if (App.get().isInsideTheMall()) {
                    fragment = new InsideMallFragment();
                } else {
                    fragment = new OutsidemallFragment();
                }

                break;
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && hasSoftKeys()) {
            int navBarHeight = getNavigationBarHeight();
            findViewById(R.id.container).setPadding(0, 0, 0, navBarHeight);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

    }

    private int getNavigationBarHeight() {
        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (EMSP.getLocation() != null)
            EMSP.getLocation().registerDeviceAtCurrentLocation(new EMSPLocationRegisterListener() {
                @Override
                public void onRegisterSuccess(WiFiLocation wiFiLocation) {
                    //Toast.makeText(HomeActivity.this, " onRegisterSuccess ", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onRegisterFailure(StatusHandle.Status status) {
                    //Toast.makeText(HomeActivity.this, " onRegisterFailure ", Toast.LENGTH_LONG).show();
                }
            });*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            Validation.UI.hideKeyboard(HomeActivity.this);
            resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof AbstractHomeFragment) {
            finish();
        } else {
            Fragment fragment = null;
            if (App.get().isInsideTheMall()) {
                fragment = new InsideMallFragment();
            } else {
                fragment = new OutsidemallFragment();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        }
    }

    @Override
    public String getScreenName() {
        return Constant.DONT_SEND;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ResideMenuManager.req_shopping) {
            setActionbarTitle("SHOPPING");
            // fragmentTransaction(REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.SHOPPING), R.id.container, false);
            fragmentTransaction(_Activity.REPLACE_FRAGMENT, StoreListingFragment.getInstance(StoreListingFragment.CategoryType.SHOPPING), R.id.container, false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
	    super.onRequestPermissionsResult( requestCode, permissions, grantResults );

	    boolean granted = false;
        switch (requestCode) {
            case 0:
               /* if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    notifyPermissionResult(0, true);
                } else {
                    notifyPermissionResult(0, false);
                }*/
                for (int i : grantResults) {
                    if (i == PackageManager.PERMISSION_GRANTED) {
                        granted = true;
                    } else {
                        granted = false;
                        break;
                    }
                }
                notifyPermissionResult(0, granted);
                return;

            /*default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);*/
        }

    }

    public boolean hasSoftKeys() {
        boolean hasSoftwareKeys = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display d = getWindowManager().getDefaultDisplay();

            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);

            int realHeight = realDisplayMetrics.heightPixels;
            int realWidth = realDisplayMetrics.widthPixels;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);

            int displayHeight = displayMetrics.heightPixels;
            int displayWidth = displayMetrics.widthPixels;

            hasSoftwareKeys = (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
        } else {
            boolean hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            hasSoftwareKeys = !hasMenuKey && !hasBackKey;
        }
        return hasSoftwareKeys;
    }

    PermissionUtils.PermissionCallback mCallback;

    @Override
    public void setPermissionCallback(PermissionUtils.PermissionCallback callback) {
        mCallback = callback;
    }

    @Override
    public void notifyPermissionResult(int requestCode, boolean granted) {
        if (mCallback != null)
        mCallback.onPermissionResult(requestCode, granted);
    }
}
