package com.belongi.citycenter.ui.activity.services;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by webwerks on 27/6/15.
 */
public class ServiceDetailActivity extends BaseActivity {
    BaseService serviceObj;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_service_detail);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        /*if(TextUtils.isEmpty(getIntent().getStringExtra("title"))){
            setActionbarTitle("SERVICE");
        }else{
            setActionbarTitle(getIntent().getStringExtra("title"));
        }*/

        final ImageView imgService = (ImageView) findViewById(R.id.imgService);
        WebView wvServiceDesc = (WebView) findViewById(R.id.wvServiceDesc);

        serviceObj = (BaseService) getIntent().getSerializableExtra("Model");
        setActionbarTitle(serviceObj.getServicePageTitle().toUpperCase());
        Picasso.with(this).load(WebApi.IMAGE_BASE_URL + serviceObj.getServiceImage())
                .placeholder(R.drawable.place_holder_event1_6p).into(imgService, new Callback() {
            @Override
            public void onSuccess() {
                Animation scaleAnim = AnimationUtils.loadAnimation(ServiceDetailActivity.this, R.anim.scale_animation);
                imgService.startAnimation(scaleAnim);
            }

            @Override
            public void onError() {
            }
        });

        String desc = serviceObj.getServicePageDetail().replaceAll("&quot;", "\"");
        wvServiceDesc.getSettings().setJavaScriptEnabled(true);
        Log.e("Service Detail", getStyleString(desc));
        Log.e("wvServiceDesc", "" + App.get().getSelectedMall().getMallName());
        wvServiceDesc.loadDataWithBaseURL("file:///android_asset/", getStyleString(desc), "text/html", "UTF-8", null);
        //wvServiceDesc.setBackgroundColor(0x00000000);
        //wvServiceDesc.setScrollContainer(false);
        wvServiceDesc.setBackgroundColor(Color.argb(1, 0, 0, 0));
       /* wvServiceDesc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return (motionEvent.getAction() == MotionEvent.ACTION_MOVE);
            }
        });*/
        wvServiceDesc.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                    startActivity(intent);
                } else if (url.contains("mailto:")) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                    emailIntent.setType("text/plain");
                    //emailIntent.putExtra(Intent.EXTRA_EMAIL,);
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } else {
                    startActivity(new Intent(ServiceDetailActivity.this, WebContentPreviewActivity.class)
                            .putExtra("url", url)
                            .putExtra("title", serviceObj.getServicePageTitle())
                            .putExtra("category", "Service - " + serviceObj.getServicePageTitle()));
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
                return true;
            }
        });
    }

    @Override
    public String getScreenName() {
        return "Services - " + serviceObj.getServicePageTitle();
    }
}
