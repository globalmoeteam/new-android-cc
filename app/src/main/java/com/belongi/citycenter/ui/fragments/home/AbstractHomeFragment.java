package com.belongi.citycenter.ui.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.android.utilities.Validation;
import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.data.entities.Malls;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.SpotlightBanner;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.data.entities.utils.Movie;
import com.belongi.citycenter.emsp.EmspHelper;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.Utils;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.activity.MallSelectionActivity;
import com.belongi.citycenter.ui.activity.ShoppingCoachActivity;
import com.belongi.citycenter.ui.activity.services.ServiceDetailActivity;
import com.belongi.citycenter.ui.activity.themall.MallContentPageActivity;
import com.belongi.citycenter.ui.fragment.BaseFragment;
import com.belongi.citycenter.ui.fragment.GiftCardFragment;
import com.belongi.citycenter.ui.fragment.MyParkingFragment;
import com.belongi.citycenter.ui.fragment.PreviewWebContentFragment;
import com.belongi.citycenter.ui.fragment.SpinToWinFragment;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;
import com.belongi.citycenter.ui.fragment.UtilitiesFragment;
import com.belongi.citycenter.ui.fragment.categories.StoreListingFragment;
import com.belongi.citycenter.ui.view.residemenu.ResideMenuManager;
import com.july.emsp.EMSPAppXPushDown;
import com.july.emsp.EMSPAppXViewListner;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by yashesh on 6/19/2015.
 */
public abstract class AbstractHomeFragment extends BaseFragment implements View.OnClickListener, LocalDataLoader.QueryClient, EMSPAppXViewListner {

    public static final int REQ_HOME_DATA = 50;
    protected View root;
    protected ViewGroup included, containerTop, containerBottom, containerMiddle, containerMiddleLoc;
    TextView lblGiftCard;
    List<Service> giftVoucher;
    EMSPAppXViewListner emspAppXViewListnerNormal = new EMSPAppXViewListner() {
        @Override
        public void onReceiveCustomView(String s, JSONObject jsonObject) {

        }

        @Override
        public void onReceiveCustomView(String s, String s1) {

        }

        @Override
        public void onFinishLoading(View view) {
            containerMiddle.setVisibility(View.VISIBLE);
        }

        @Override
        public void onFailure(String s, Exception e) {

        }
    };
    EMSPAppXViewListner emspAppXViewListnerLoc = new EMSPAppXViewListner() {
        @Override
        public void onReceiveCustomView(String s, JSONObject jsonObject) {

        }

        @Override
        public void onReceiveCustomView(String s, String s1) {

        }

        @Override
        public void onFinishLoading(View view) {
            containerMiddleLoc.setVisibility(View.VISIBLE);
        }

        @Override
        public void onFailure(String s, Exception e) {

        }
    };

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root = view;
        included = (ViewGroup) root.findViewById(R.id.included);
        containerTop = (ViewGroup) root.findViewById(R.id.containerTop);
        containerBottom = (ViewGroup) root.findViewById(R.id.containerBottom);
        containerMiddle = (LinearLayout) root.findViewById(R.id.container_middle);
        containerMiddleLoc = (LinearLayout) root.findViewById(R.id.container_middle_loc);
        lblGiftCard = (TextView) root.findViewById(R.id.btnGiftCard);
        included.post(new Runnable() {
            public void run() {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) containerTop.getLayoutParams();
                layoutParams.height = included.getHeight() / 2;
                containerTop.setLayoutParams(layoutParams);
                FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) containerBottom.getLayoutParams();
                layoutParams1.height = included.getHeight() / 2;
                containerBottom.setLayoutParams(layoutParams1);
                Log.e("included", " ::" + included.getHeight() / 2 + " -- " + containerTop.getHeight());
            }
        });
        List<MallContentPage> contentPage = new Select().from(MallContentPage.class).where("mallid=?", App.get().getMallId()).execute();
        App.get().setArrContentPage(((List<MallContentPage>) contentPage).toArray(new MallContentPage[contentPage.size()]));
        giftVoucher = EntityUtils.containsGiftVoucher();
        if (App.EMSP_INITIALIZED) {
            if (Validation.Network.isConnected(getActivity())) {
                // show pushdown Ads
                setUpAds();
                // setUpLocAds();
                //setUpLocAds();
                // show General Ads
                EmspHelper.showOverlayAd(getActivity(), "home");
                //EmspHelper.showFullScreenAd(getActivity(), "home");
                //show Location based Ads
                if (App.EMSP_LOCATION_REGISTERED) {
                    setUpLocAds();
                    EmspHelper.showLocBasedOverlayAd(getActivity(), "home");
                    //EmspHelper.showLocBasedFullscreenAd(getActivity(), "home");
                }
            }
        }


        if (App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.MUSCAT) ||
                App.get().getSelectedMall().getIdentifier() == MallSelectionActivity.Mall.QURUM ||
                App.get().getSelectedMall().getIdentifier() == MallSelectionActivity.Mall.ALEXANDRIA ||
                App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.MAADI)/* ||
                App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.BEIRUT) ||
                App.get().getSelectedMall().getIdentifier().equals( MallSelectionActivity.Mall.SHINDAGHA )*/) {
            if (giftVoucher != null && giftVoucher.size() == 0) {

                lblGiftCard.setVisibility(View.GONE);
            }

        }


    }

    public void setUpAds() {
        try {
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", getActivity().getPackageName());
            if (App.EMSP_INITIALIZED) {
                if (!Constant.CISCO_MALL_TAG.equalsIgnoreCase("")) {
                    if (Validation.Network.isConnected(getActivity())) {
                        Log.e("Validation", "" + Constant.CISCO_MALL_TAG + "_home_pushdownx");
                        String tag = "";
	                   /* if (App.EMSP_LOCATION_REGISTERED) {
                            tag = Constant.CISCO_LOCATION_TAG + "DCC_HOME_PUSHDOWN_CENTERLOCATION";
                        } else {*/
                        tag = Constant.CISCO_MALL_TAG + "_home_pushdownx";
                        /*}*/
                        Log.e("Validation", tag);
                        EMSPAppXPushDown mEMSPAppXPushDown = new EMSPAppXPushDown(getActivity(), tag, appaxParams, emspAppXViewListnerNormal);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        // mEMSPAppXPushDown.setAppXViewListner(this);
                        containerMiddle.addView(mEMSPAppXPushDown, lp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUpLocAds() {
        containerMiddleLoc.setVisibility(View.VISIBLE);
        try {
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", getActivity().getPackageName());
            if (App.EMSP_INITIALIZED) {
               /* if (!Constant.CISCO_LOCATION_TAG.equalsIgnoreCase("")) {*/
                if (Validation.Network.isConnected(getActivity())) {
                    EMSPAppXPushDown mEMSPAppXPushDown = new EMSPAppXPushDown(getActivity(), Constant.CISCO_LOCATION_TAG + "_HOME_PUSHDOWN_CENTERLOCATION", appaxParams, emspAppXViewListnerLoc);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    //mEMSPAppXPushDown.setAppXViewListner(this);
                    containerMiddleLoc.addView(mEMSPAppXPushDown, lp);
                    // Toast.makeText(getActivity(), " Push down : " + Constant.CISCO_LOCATION_TAG + "_HOME_PUSHDOWN_CENTERLOCATION", Toast.LENGTH_LONG).show();
                }
                /*}*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*public void showFullScreenAndOverlayAds(){
        try {
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", getActivity().getPackageName());

            if(!(this instanceof AbstractHomeFragment))
                EMSP.loadAppXFullPage(getActivity(), Constant.CISCO_MALL_TAG + "_home_fullpage");
            EMSPAppXOverlay mAppXOverlay = new EMSPAppXOverlay(getActivity(), Constant.CISCO_MALL_TAG + "_home_overlay", R.drawable.appx_overlay_closebutton, true, appaxParams);
            mAppXOverlay.setAnimationStyle(R.style.APPX_ANIMATION);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showFullScreenAndOverlayAdsLoc(){
        try {
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", getActivity().getPackageName());

            if(!(this instanceof AbstractHomeFragment))
                EMSP.loadAppXFullPage(getActivity(), Constant.CISCO_LOCATION_TAG+"_home_fullpage");

            EMSPAppXOverlay mAppXOverlay = new EMSPAppXOverlay(getActivity(), Constant.CISCO_LOCATION_TAG + "_home_overlay", R.drawable.appx_overlay_closebutton, true, appaxParams);
            mAppXOverlay.setAnimationStyle(R.style.APPX_ANIMATION);
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    public void setLandingScreen() {
        Log.e("HOMEFRAGEENT", App.get().getSelectedMall().getMallName() + " : " + App.get().getMallId());
        SpotlightBanner[] spotlightBanners = App.get().getHomeScreenData().getSpotlightBanners();
        if (spotlightBanners != null && spotlightBanners.length > 0) {
            Log.e("HOMEFRAGMENT", spotlightBanners[0].getTitle());
        }
        List offerEvent = App.get().getHomeScreenData().getOffersAndEvents();
        if (offerEvent != null && offerEvent.size() > 0) {
            Log.e("HOMEFRAGMENT", offerEvent.get(0).getClass().getName());
        }
        new LocalDataLoader(this, REQ_HOME_DATA).execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.MUSCAT) ||
                App.get().getSelectedMall().getIdentifier() == MallSelectionActivity.Mall.QURUM ||
                App.get().getSelectedMall().getIdentifier() == MallSelectionActivity.Mall.ALEXANDRIA ||
                App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.MAADI) /*||
                App.get().getSelectedMall().getIdentifier().equals(MallSelectionActivity.Mall.BEIRUT) ||
				App.get().getSelectedMall().getIdentifier().equals( MallSelectionActivity.Mall.SHINDAGHA )*/) {
            lblGiftCard.setText("GIFT VOUCHER");
        }
        if (App.get().getHomeScreenData() != null) {
            setLandingScreen();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLocateStore:
				/*( ( BaseActivity ) getActivity() ).fragmentTransaction( _Activity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance( Constant.STORE_LOCATOR, "STORE LOCATOR", "Store Locator" ),
				                                                        R.id.container, false );*/
                if (/*Validation.Network.isConnected(mActivity)*/
                        App.get().isMapEnabled()) {
                    ((HomeActivity) getActivity()).setActionbarTitle("STORE LOCATOR");
                    ((HomeActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, new StoreLocatorNewFragment(), R.id.container, false);
                    //mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new StoreLocatorFragment(), R.id.container, false);
                } else {
                    Toast.makeText(getActivity(), "Map not available", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnShopping:
                if (App.get().getSelectedMall().getShoppingCoach().equals("true")) {
                    Malls mall = new Malls();
                    mall.setMallName(App.get().getSelectedMall().getMallName());
                    mall.setMallUrl(App.get().getSelectedMall().getMallUrl());
                    mall.setImageUrl(App.get().getSelectedMall().getImageUrl());
                    mall.setId(App.get().getSelectedMall().getIdentifier());
                    mall.setMallCOde(App.get().getSelectedMall().getMallCOde());
                    mall.setFirstLaunch(App.get().getSelectedMall().getFirstLaunch());
                    mall.setShoppingCoach("false");

                    EntityUtils.updateMall(mall);
                    getActivity().startActivityForResult(new Intent(getActivity(), ShoppingCoachActivity.class), ResideMenuManager.req_shopping);
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else {
                    ((BaseActivity) getActivity()).setActionbarTitle("SHOPPING");
                    // ((HomeActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.SHOPPING), R.id.container, false);
                    ((HomeActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, StoreListingFragment.getInstance(StoreListingFragment.CategoryType.SHOPPING), R.id.container, false);
                }
                break;

            case R.id.btnDining:
                ((BaseActivity) getActivity()).setActionbarTitle("DINING");
                //((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.DINING), R.id.container, false);
                ((HomeActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, StoreListingFragment.getInstance(StoreListingFragment.CategoryType.DINING), R.id.container, false);
                break;

            case R.id.btnEntertainment:
                ((BaseActivity) getActivity()).setActionbarTitle("ENTERTAINMENT");
                //((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.ENTERTAINMENT), R.id.container, false);
                ((HomeActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, StoreListingFragment.getInstance(StoreListingFragment.CategoryType.ENTERTAINMENT), R.id.container, false);
                break;

            case R.id.btnGiftCard:
                if (lblGiftCard.getText().toString().equalsIgnoreCase("GIFT VOUCHER")) {
                    if (giftVoucher != null && giftVoucher.size() > 0) {
                        startActivity(new Intent(getActivity(), ServiceDetailActivity.class)
                                .putExtra("Model", giftVoucher.get(0))
                                .putExtra("title", "GIFT VOUCHER"));
                        getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    } else {
                        Validation.UI.showLongToast(getActivity(), "This Service is not available at this mall");
                    }
                } else {
                    ((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, new GiftCardFragment(), R.id.container, false);
                }
                break;

            case R.id.btnSocialWall:
                ((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance(Constant.SOCIAL_WALL, "SOCIAL WALL", "Social Wall"),
                        R.id.container, false);
                break;

            case R.id.btnUtilities:
                ((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, new UtilitiesFragment(), R.id.container, false);
                break;

            case R.id.btnFindCar:
                ((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, new MyParkingFragment(), R.id.container, false);
                break;

            case R.id.btnSpinandWin:
                SpinAndWinLogic.getCampaigns(new BackgroundJobClient() {
                    @Override
                    public void onBackgroundJobComplete(int requestCode, Object result) {
                        ((BaseActivity) getActivity()).stopLoading();
                        if (result != null) {
                            switch (requestCode) {
                                case SpinAndWinLogic.REQ_GET_COMPAIGNS:
                                    Campaigns compaigns = (Campaigns) ContentDownloader.getObjectFromJson(result.toString(), Campaigns.class);
                                    if (compaigns.getListCompagins() != null && compaigns.getListCompagins().size() > 0) {
                                        App.get().setCompaignList(compaigns.getListCompagins());
                                        ((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, new SpinToWinFragment(), R.id.container, false);
                                    } else {
                                        Validation.UI.showToast(getActivity(), "No campaigns available in the selected Mall.");
                                    }
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onBackgroundJobAbort(int requestCode, Object reason) {
                    }

                    @Override
                    public void onBackgroundJobError(int requestCode, Object error) {
                        ((BaseActivity) getActivity()).stopLoading();
                        Toast.makeText(getActivity(), "No campaigns available in the selected Mall.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public boolean needAsyncResponse() {
                        return true;
                    }

                    @Override
                    public boolean needResponse() {
                        return isAdded() && isVisible();
                    }
                });
                ((BaseActivity) getActivity()).showProgressLoading();
                break;


            case R.id.btnGettingHere:
                MallContentPage gettingHere = null;
                for (MallContentPage mall : App.get().getArrContentPage()) {
                    if (mall.getTitle().equals("Getting Here")) {
                        gettingHere = mall;
                    }
                }

                if (gettingHere != null) {
                    startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                            .putExtra("PageModel", gettingHere));
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
                break;

            case R.id.btnBookTaxi:
                switch (App.get().getSelectedMall().getIdentifier()){
                    case MAADI:
                    case ALEXANDRIA:
                    case BAHRAIN:
                        if (App.get().getArrContentPage() != null && App.get().getArrContentPage().length >= 1) {
                            for(MallContentPage mallContent:App.get().getArrContentPage()){
                                if(mallContent.getTitle().equals("Opening Hours")){
                                    startActivity(new Intent(getActivity(), MallContentPageActivity.class)
                                            .putExtra("PageModel", mallContent));
                                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                                }
                            }
                        }
                        break;

                    default:
                        String url = "";
                        if (TextUtils.isEmpty(Constant.BOOKATAXI)) {
                            url = App.get().getSelectedMall().getImageUrl();
                        } else {
                            url = Constant.BOOKATAXI;
                        }
                        ((BaseActivity) getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance(url, "CALL A TAXI", "Call A Taxi"),
                                R.id.container, false);
                        break;
                }
                break;
        }
    }

    @Override
    public Object executeQuery() {
        Object[] result = new Object[5];

        List<Entertainment> entertainmentList = new Select().from(Entertainment.class).where("forHome=?", 0)
                .where("mallid=?", App.get().getMallId()).execute();

        List<Movie> movieList = new Select().from(Movie.class)
                .where("mallid=?", App.get().getMallId()).execute();

        List<SpotlightBanner> spotlightBannerList = new Select().from(SpotlightBanner.class)
                .where("mallid=?", App.get().getMallId()).execute();

        List<Offer> offerList = new Select().from(Offer.class).where("forHome=?", 0)
                .where("mallid=?", App.get().getMallId()).execute();

        List<Event> eventList = new Select().from(Event.class).where("forHome=?", 0)
                .where("mallid=?", App.get().getMallId()).execute();

        result[0] = entertainmentList;
        result[1] = movieList;
        result[2] = spotlightBannerList;
        result[4] = offerList;
        result[3] = eventList;

        return result;
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Log.e("HOME ENTR", " " + result);
        if (result != null) {
            switch (requestCode) {
                case REQ_HOME_DATA:
                    Object[] resultData = (Object[]) result;

                    if (resultData[0] != null) {
                        App.get().getHomeScreenData().setEntertainments((ArrayList<Entertainment>) resultData[0]);
                    }

                    if (resultData[2] != null) {
                        SpotlightBanner[] spotlightBanners = new SpotlightBanner[((ArrayList<SpotlightBanner>) resultData[2]).size()];
                        ((ArrayList<SpotlightBanner>) resultData[2]).toArray(spotlightBanners);
                        App.get().getHomeScreenData().setSpotlightBanners(spotlightBanners);
                    }

                    if (resultData[1] != null) {
                        Movie[] movie = new Movie[((ArrayList<Entertainment>) resultData[1]).size()];
                        ((ArrayList<Entertainment>) resultData[1]).toArray(movie);
                        App.get().getHomeScreenData().setMovies(movie);
                    }

                    if (resultData[4] != null) {
                        List<Offer> offerList = (List<Offer>) resultData[4];

                        for (Offer offer : offerList) {
                            Log.e("offerList", offer.getTitle());
                        }
                        App.get().getHomeScreenData().setOffersAndEvents(offerList);
                    }

                    if (resultData[3] != null) {
                        List<Event> eventList = (List<Event>) resultData[3];

                        for (Event offer : eventList) {
                            Log.e("eventList", offer.getTitle());
                        }
                        App.get().getHomeScreenData().getOffersAndEvents().addAll(eventList);
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.setTopViews(containerTop, getActivity());
                            Utils.setBottomViews(containerBottom, getActivity());
                        }
                    }, 100);

                    break;
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public void onReceiveCustomView(String s, JSONObject jsonObject) {
        Log.e("callback", "onReceiveCustomView" + jsonObject + " :: " + s);
    }

    @Override
    public void onReceiveCustomView(String s, String s1) {
        Log.e("callback", "onReceiveCustomView" + s + " :: " + s1);
    }

    @Override
    public void onFinishLoading(View view) {
        Log.e("callback", "onFinishLoading" + view);
    }

    @Override
    public void onFailure(String s, Exception e) {
        Log.e("callback", "onFailure" + s + " :: " + e);
    }
}
