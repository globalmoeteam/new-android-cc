package com.belongi.citycenter.ui.activity.themall;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.MallContentPage;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;

/**
 * Created by webwerks on 16/6/15.
 */
public class MallContentPageActivity extends BaseActivity implements View.OnClickListener{

    MallContentPage[] arrContentPage;
    MallContentPage contentPage;
    MallContentPage openingHour,contactDetails,facilities,gettingHere;

    ImageView imgOpening,imgContact,imgFacilities,imgGetting;
    TextView txtOpening,txtContact,txtFacilities,txtGetting;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_mall_content_page);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        WebView wvContentDesc= (WebView) findViewById(R.id.wvContentDesc);
        WebView wvMap= (WebView) findViewById(R.id.wvMap);
        contentPage= (MallContentPage) getIntent().getSerializableExtra("PageModel");

        if(!TextUtils.isEmpty(getIntent().getStringExtra("from"))){
            findViewById(R.id.layout_bottom).setVisibility(View.GONE);
        }else{
            findViewById(R.id.layout_bottom).setVisibility(View.VISIBLE);
        }

        imgOpening= (ImageView)findViewById(R.id.imgOpening);
        imgContact= (ImageView) findViewById(R.id.imgContact);
        imgFacilities= (ImageView) findViewById(R.id.imgFacilities);
        imgGetting= (ImageView) findViewById(R.id.imgGetting);

        txtOpening= (TextView) findViewById(R.id.txtOpening);
        txtContact= (TextView) findViewById(R.id.txtContact);
        txtFacilities= (TextView) findViewById(R.id.txtFacilities);
        txtGetting= (TextView) findViewById(R.id.txtGetting);

        if(contentPage!=null){
            if(contentPage.getTitle().equals("Contact Details")){
                setActionbarTitle("CONTACT INFO");
                contactClickEffect();
            }else if(contentPage.getTitle().equals("Opening Hours")){
                setActionbarTitle("OPENING HOURS");
                openingHourClickEffect();
            }else if(contentPage.getTitle().equals("Facilities")){
                setActionbarTitle("FACILITIES");
                facilitiesClickEffect();
            }else if(contentPage.getTitle().equalsIgnoreCase("Getting Here")){
                setActionbarTitle("GETTING HERE");
                gettingHereClickEffect();
                wvMap.setVisibility(View.VISIBLE);
                DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;
                wvMap.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,(int)(height*0.60)));
                String mapFrame=App.get().getSelectedMall().getGettingHere();

                //Log.e("MALLCONTENTPAGE",mapFrame);

                wvMap.getSettings().setJavaScriptEnabled(true);
                wvMap.loadData(mapFrame, "text/html", "UTF-8");
                wvMap.setBackgroundColor(0x00000000);
                wvMap.getSettings().setSupportMultipleWindows(true);
                wvMap.setWebChromeClient(new WebChromeClient(){
                    @Override
                    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                        WebView newWebView = new WebView(MallContentPageActivity.this);
                        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                        transport.setWebView(newWebView);
                        resultMsg.sendToTarget();
                        return true;
                    }
                });
            }
            arrContentPage= App.get().getArrContentPage();
            for(MallContentPage mallContent:arrContentPage){
                if(mallContent.getTitle().equals("Contact Details")){
                    contactDetails=mallContent;
                }else if(mallContent.getTitle().equals("Opening Hours")){
                    openingHour=mallContent;
                }else if(mallContent.getTitle().equals("Facilities")){
                    facilities=mallContent;
                }else if(mallContent.getTitle().equals("Getting Here")){
                    gettingHere=mallContent;
                }
            }

            String desc= contentPage.getPageDetail().replace("&quot;", "\"");
	        Log.e( "abc", desc);
            wvContentDesc.getSettings().setJavaScriptEnabled(true);
            wvContentDesc.loadDataWithBaseURL("file:///android_asset/",getStyleString(desc), "text/html", "UTF-8", null);
            wvContentDesc.setBackgroundColor(0x00000000);
            wvContentDesc.setBackgroundColor(Color.argb(1, 0, 0, 0));

            wvContentDesc.setWebViewClient(new WebViewClient(){
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if(url.contains("tel:")){
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                        startActivity(intent);
                    }else if(url.contains("mailto:")){
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                        emailIntent.setType("text/plain");
                        String strTo=url.replace("file:///android_asset/%22", "")
                                .substring(url.replace("file:///android_asset/%22", "").indexOf(":")+1,
                                        url.replace("file:///android_asset/%22", "").length());
                        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{strTo});
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    }else{
                        startActivity(new Intent(MallContentPageActivity.this, WebContentPreviewActivity.class)
                                .putExtra("url",url)
                                .putExtra("title",contentPage.getTitle().toUpperCase())
                                .putExtra("category",Constant.DONT_SEND));
                        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    }
                    return true;
                }
            });

            wvContentDesc.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            /*wvContentDesc.setScrollContainer(false);
            wvContentDesc.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return (motionEvent.getAction() == MotionEvent.ACTION_MOVE);
                }
            });*/
        }

        findViewById(R.id.btnOeningHours).setOnClickListener(this);
        findViewById(R.id.btnFacilities).setOnClickListener(this);
        findViewById(R.id.btnContactUs).setOnClickListener(this);
        findViewById(R.id.btnGettingHere).setOnClickListener(this);
    }

    public void openingHourClickEffect(){
        imgOpening.setColorFilter(Color.rgb (129, 207, 255));
        imgContact.setColorFilter(Color.rgb (255, 255, 255));
        imgFacilities.setColorFilter(Color.rgb (255, 255, 255));
        imgGetting.setColorFilter(Color.rgb(255, 255, 255));

        txtOpening.setTextColor(Color.rgb(129, 207, 255));
        txtContact.setTextColor(Color.rgb(255, 255, 255));
        txtFacilities.setTextColor(Color.rgb(255, 255, 255));
        txtGetting.setTextColor(Color.rgb(255, 255, 255));
    }

    public void facilitiesClickEffect(){
        imgOpening.setColorFilter(Color.rgb (255, 255, 255));
        imgFacilities.setColorFilter(Color.rgb (129, 207, 255));
        imgContact.setColorFilter(Color.rgb (255, 255, 255));
        imgGetting.setColorFilter(Color.rgb(255, 255, 255));

        txtFacilities.setTextColor(Color.rgb(129, 207, 255));
        txtContact.setTextColor(Color.rgb(255, 255, 255));
        txtOpening.setTextColor(Color.rgb(255, 255, 255));
        txtGetting.setTextColor(Color.rgb(255, 255, 255));
    }

    public void contactClickEffect(){
        imgOpening.setColorFilter(Color.rgb (255, 255, 255));
        imgContact.setColorFilter(Color.rgb (129, 207, 255));
        imgFacilities.setColorFilter(Color.rgb (255, 255, 255));
        imgGetting.setColorFilter(Color.rgb(255, 255, 255));

        txtContact.setTextColor(Color.rgb(129, 207, 255));
        txtOpening.setTextColor(Color.rgb(255, 255, 255));
        txtFacilities.setTextColor(Color.rgb(255, 255, 255));
        txtGetting.setTextColor(Color.rgb(255, 255, 255));
    }

    public void gettingHereClickEffect(){
        imgOpening.setColorFilter(Color.rgb (255, 255, 255));
        imgContact.setColorFilter(Color.rgb (255, 255, 255));
        imgGetting.setColorFilter(Color.rgb (129, 207, 255));
        imgFacilities.setColorFilter(Color.rgb(255, 255, 255));

        txtGetting.setTextColor(Color.rgb(129, 207, 255));
        txtContact.setTextColor(Color.rgb(255, 255, 255));
        txtFacilities.setTextColor(Color.rgb(255, 255, 255));
        txtOpening.setTextColor(Color.rgb(255, 255, 255));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        //Log.e("MALLCONTENT",view.toString());

        switch (view.getId()){
            case R.id.btnOeningHours:
                openingHourClickEffect();

                if(openingHour!=null) {
                  //  Log.e("MALLCONTENT", Html.fromHtml(openingHour.getPageDetail()) + "");
                    startActivity(new Intent(this, MallContentPageActivity.class)
                            .putExtra("PageModel", openingHour));
                }
                break;

            case R.id.btnFacilities:
                facilitiesClickEffect();

                if(facilities!=null) {
                    //Log.e("MALLCONTENT", Html.fromHtml(facilities.getPageDetail()) + "");
                    startActivity(new Intent(this, MallContentPageActivity.class)
                            .putExtra("PageModel", facilities));
                }
                break;

            case R.id.btnContactUs:

                contactClickEffect();

                if(contactDetails!=null){
                    //Log.e("MALLCONTENT",Html.fromHtml(contactDetails.getPageDetail())+"");
                    startActivity(new Intent(this, MallContentPageActivity.class)
                            .putExtra("PageModel", contactDetails));
                }
                break;

            case R.id.btnGettingHere:
                gettingHereClickEffect();

                if(gettingHere!=null) {
                    //Log.e("MALLCONTENT", Html.fromHtml(gettingHere.getPageDetail()) + "");
                    startActivity(new Intent(this, MallContentPageActivity.class)
                            .putExtra("PageModel", gettingHere));
                }
                break;
        }
        finish();
        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    @Override
    public String getScreenName() {
        if(contentPage!=null){
            if(contentPage.getTitle().equals("Contact Details")){
                return "The Mall - Contact US";
            }else if(contentPage.getTitle().equals("Opening Hours")){
                return "The Mall - Opening Hours";
            }else if(contentPage.getTitle().equals("Facilities")){
                return "The Mall - Facilities";
            }else if(contentPage.getTitle().equalsIgnoreCase("Getting Here")){
                return "The Mall - Getting Here";
            }else{
                return Constant.DONT_SEND;
            }
        }else{
            return Constant.DONT_SEND;
        }
    }
}