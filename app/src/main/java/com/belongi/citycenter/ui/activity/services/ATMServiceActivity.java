package com.belongi.citycenter.ui.activity.services;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.adapters.ATMServiceAdapter;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.logic.ATMServiceLogic;
import com.belongi.citycenter.ui.activity.BaseActivity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by webwerks on 29/6/15.
 */
public class ATMServiceActivity extends BaseActivity implements LocalDataLoader.QueryClient,AdapterView.OnItemClickListener{

    ListView lstAtms;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_atm_service);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("ATM'S");

        lstAtms= (ListView) findViewById(R.id.lstAtms);
        //lstAtms.setOnItemClickListener(this);
    }

    @Override
    public String getScreenName() {
        return "ATM'S List";
    }

    @Override
    protected void onResume() {
        super.onResume();
        new LocalDataLoader(this, ATMServiceLogic.REQ_GET_ATMS).execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        /*BaseService atm= (BaseService) parent.getItemAtPosition(position);
        startActivity(new Intent(this, ServiceDetailActivity.class).putExtra("Model",atm));
        overridePendingTransition(0,0);*/
    }

    @Override
    public Object executeQuery() {
        return EntityUtils.getAllATMService();
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        List<BaseService> atmList= (List<BaseService>) result;
        Collections.sort(atmList, new Comparator<BaseService>() {
            @Override
            public int compare(BaseService baseService, BaseService baseService1) {
                return baseService.getServicePageTitle().compareToIgnoreCase(baseService1.getServicePageTitle());
            }
        });
        lstAtms.setAdapter(new ATMServiceAdapter(this, atmList));
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }
}
