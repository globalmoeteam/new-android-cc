package com.belongi.citycenter.ui.activity.spinwin;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.utilities.Validation;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.ui.adapters.ContestAdapter;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by webwerks on 22/6/15.
 */
public class SpinToWinDashboardActivity extends BaseActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener,CompoundButton.OnCheckedChangeListener{

    CheckBox chkTermsCondition;
    ImageView imgContest;
    Campaigns.Compaign selectedCompaign;
    boolean[] arrTermsCondition;
    int contestPosition=0;
    TextView lblContestSummary;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_spintowin_dashboard);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");

        findViewById(R.id.btnPlayNow).setOnClickListener(this);
        findViewById(R.id.btnDateOfDraw).setOnClickListener(this);
        findViewById(R.id.btnPastWinners).setOnClickListener(this);
        findViewById(R.id.btnHowToPlay).setOnClickListener(this);
        findViewById(R.id.lblTermsCondition).setOnClickListener(this);

        lblContestSummary= (TextView) findViewById(R.id.lblContestSummary);
        Spinner spnrContestName= (Spinner) findViewById(R.id.spnrContestName);
        chkTermsCondition= (CheckBox) findViewById(R.id.chkTermsCondition);
        imgContest= (ImageView) findViewById(R.id.imgContest);

        chkTermsCondition.setOnCheckedChangeListener(this);
        spnrContestName.setOnItemSelectedListener(this);
        spnrContestName.setAdapter(new ContestAdapter(this, App.get().getCompaignList()));

        arrTermsCondition=new boolean[App.get().getCompaignList().size()];
        if(arrTermsCondition[contestPosition]){
            chkTermsCondition.setChecked(true);
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnPlayNow:
                //if(chkTermsCondition.isChecked()){
                if (arrTermsCondition[contestPosition]) {
                    startActivity(new Intent(this, PlayContestActivity.class)
                            .putExtra(Constant.TYPE_CLICKED, Constant.CONTEST));
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else {
                    Validation.UI.showLongToast(this,"Please accept the terms and conditions to play the game.");
                }
                break;

            case R.id.btnDateOfDraw:
                startActivity(new Intent(this, DateOfDrawAndWinnerActivity.class)
                        .putExtra(Constant.TYPE_CLICKED,Constant.DATE_OF_DRAW));
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnPastWinners:
                startActivity(new Intent(this, DateOfDrawAndWinnerActivity.class)
                        .putExtra(Constant.TYPE_CLICKED,Constant.PAST_WINNERS));
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.btnHowToPlay:
                startActivity(new Intent(this, ContestInfoActivity.class)
                        .putExtra(Constant.TYPE_CLICKED,Constant.HOW_TO_PLAY));
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;

            case R.id.lblTermsCondition:
                startActivity(new Intent(this, ContestInfoActivity.class)
                        .putExtra(Constant.TYPE_CLICKED,Constant.TERMS_CONDITIONS));
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN Dashboard";
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        selectedCompaign= (Campaigns.Compaign) parent.getItemAtPosition(position);
        App.get().setSelectedContest(selectedCompaign);
        Picasso.with(this).load(selectedCompaign.getCampaignThmb())
                .placeholder(R.drawable.place_holder_event1_6p).fit().into(imgContest);

        lblContestSummary.setText(selectedCompaign.getSummary());
        contestPosition=position;
       /* for(int i=0;i<arrTermsCondition.length;i++){
            arrTermsCondition[i]=false;
        }*/
        //chkTermsCondition.setChecked(false);
        if(arrTermsCondition[position]){
            chkTermsCondition.setChecked(true);
        }else{
            chkTermsCondition.setChecked(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b){
            arrTermsCondition[contestPosition]=true;
        }else{
            arrTermsCondition[contestPosition]=false;
        }
    }
}
