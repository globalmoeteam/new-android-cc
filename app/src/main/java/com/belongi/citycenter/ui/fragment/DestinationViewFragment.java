package com.belongi.citycenter.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.logic.ValidationUtils;

/**
 * Created by yashesh on 6/25/2015.
 */
public class DestinationViewFragment extends BaseFragment{

WebView rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_destination_view,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView= (WebView) view;
       if(getArguments()!=null) {
        String id=   getArguments().getString(Constant.DEST_ID);
           if(ValidationUtils.isNotNullOrBlank(id)) {
               rootView.loadUrl(WebApi.DESTINATION_HOST + "?destId=" + id);
           }else{
               rootView.loadUrl(WebApi.DESTINATION_HOST );
           }
       }

    }

    @Override
    public String getScreenName() {
        return Constant.DONT_SEND;
    }
}
