package com.belongi.citycenter.ui.view;

import android.content.Context;
import android.util.AttributeSet;

import com.nirhart.parallaxscroll.views.ParallaxScrollView;

/**
 * Created by webwerks on 7/7/15.
 */
public class ScrollViewParax extends ParallaxScrollView {

    private ParaxScroll listner;

    public ScrollViewParax(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ScrollViewParax(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollViewParax(Context context) {
        super(context);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if(listner != null)
            listner.onParaxScroll();
    }

    public interface ParaxScroll {

        public void onParaxScroll();
    }

    public void setListner(ParaxScroll listner) {

        this.listner = listner;
    }
}
