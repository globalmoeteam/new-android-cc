package com.belongi.citycenter.ui.activity.spinwin;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.adapters.CountryAdapter;
import com.belongi.citycenter.data.entities.Registration;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 15/6/15.
 */
public class RegisterNewUserActivity extends BaseActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener,BackgroundJobClient{

    EditText txtFirstNm,txtLastNm,txtMobileNo,txtEmail,txtPassword,txtConfirmPwd;
    RadioButton rbtnMr,rbtnMs;
    Spinner spnrNationality,spnrResidenttype;
    Registration registrationModel=null;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_register_new_user);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("REGISTER");
        findViewById(R.id.btnRegister).setOnClickListener(this);
        txtFirstNm= (EditText) findViewById(R.id.txtFirstNm);
        txtLastNm= (EditText) findViewById(R.id.txtLastNm);
        txtMobileNo= (EditText) findViewById(R.id.txtMobileNo);
        txtEmail= (EditText) findViewById(R.id.txtEmail);
        txtPassword= (EditText) findViewById(R.id.txtPassword);
        txtConfirmPwd= (EditText) findViewById(R.id.txtConfirmPwd);
        rbtnMr= (RadioButton) findViewById(R.id.rbtnMr);
        rbtnMs= (RadioButton) findViewById(R.id.rbtnMs);
        spnrNationality= (Spinner) findViewById(R.id.spnrNationality);
        spnrResidenttype= (Spinner) findViewById(R.id.spnrResidenttype);
        findViewById(R.id.btnRegister).setOnClickListener(this);

        /*String[] arrNationality= getResources().getStringArray(R.array.arr_country);
        String[] arrResident=getResources().getStringArray(R.array.arr_resident);
        //arrNationality[0]="Resident Type";*/

        spnrNationality.setAdapter(new CountryAdapter(this,getResources().getStringArray(R.array.arr_nationality)));
        spnrResidenttype.setAdapter(new CountryAdapter(this,getResources().getStringArray(R.array.arr_resident)));

        spnrNationality.setOnItemSelectedListener(this);
        spnrResidenttype.setOnItemSelectedListener(this);

        if(registrationModel==null){
            registrationModel=new Registration();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnRegister:
                if(validate()){
                    if(rbtnMr.isChecked()){
                        registrationModel.setTitle("Mr");
                    }else{
                        registrationModel.setTitle("Ms");
                    }
                    registrationModel.setName(txtFirstNm.getText().toString() + " " + txtLastNm.getText().toString());
                    registrationModel.setEmail(txtEmail.getText().toString().trim());
                    registrationModel.setMobile(txtMobileNo.getText().toString());
                    registrationModel.setPassword(txtPassword.getText().toString().trim());

                    SpinAndWinLogic.userRegistration(this, registrationModel);
                    showProgressLoading();
                }
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN Registration";
    }

    public boolean validate(){
        if(!rbtnMr.isChecked() && !rbtnMs.isChecked() ){
            Validation.UI.showToast(this,"Please select title");
            return false;
        }

        if(TextUtils.isEmpty(txtFirstNm.getText().toString().trim())){
            txtFirstNm.setError("Please provide First name");
            txtFirstNm.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(txtLastNm.getText().toString().trim())){
            txtLastNm.setError("Please provide Last name");
            txtLastNm.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(registrationModel.getNationality())){
            Validation.UI.showToast(this,"Please select Nationality");
            return false;
        }

        if(TextUtils.isEmpty(registrationModel.getCountryRes())){
            Validation.UI.showToast(this,"Please select Country of Resident");
            return false;
        }

        if(TextUtils.isEmpty(txtMobileNo.getText().toString().trim())){
            txtMobileNo.requestFocus();
            txtMobileNo.setError("Please enter valid mobile number");
            return false;
        }

        /*if(!Validation.isValidMobNumber(txtMobileNo.getText().toString().trim())){
            txtMobileNo.requestFocus();
            txtMobileNo.setError("Please enter valid mobile number");
            return false;
        }*/

        if(!Validation.isValidEmail(txtEmail.getText().toString().trim())){
            txtEmail.requestFocus();
            txtEmail.setError("Please enter a valid email address.");
            return false;
        }

        if(TextUtils.isEmpty(txtPassword.getText().toString().trim())){
            txtPassword.requestFocus();
            txtPassword.setError("Please enter password");
            return false;
        }

        if(!txtPassword.getText().toString().trim().equals(txtConfirmPwd.getText().toString().trim())){
            txtConfirmPwd.requestFocus();
            txtConfirmPwd.setError("The Confirm password does not match the Password");
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        if(parent.equals(spnrNationality)){
            if(position==0){
                registrationModel.setNationality("");
            }else{
                registrationModel.setNationality((String) parent.getItemAtPosition(position));
            }
        }else if(parent.equals(spnrResidenttype)){
            if(position==0){
                registrationModel.setCountryRes("");
            }else{
                registrationModel.setCountryRes((String) parent.getItemAtPosition(position));
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        stopLoading();
        if(result!=null) {
           /* String respString = result.toString();
            respString = respString.substring(0, respString.indexOf("<?xml version=\"1.0\""));
            Log.e("RegistrationRESULT", respString);*/
            Registration registrationRespo= (Registration) ContentDownloader.getObjectFromJson(result.toString(),Registration.class);
            if(TextUtils.isEmpty(registrationRespo.getError())){
                startActivity(new Intent(RegisterNewUserActivity.this,RegistrationSuccessActivity.class));
                finish();
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }else{
                Validation.UI.showLongToast(this,registrationRespo.getError());
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }
}