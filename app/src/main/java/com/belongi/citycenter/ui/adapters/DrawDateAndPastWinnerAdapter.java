package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.utilities.DateTimeUtil;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DrawDatesAndPastWinner;

import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class DrawDateAndPastWinnerAdapter extends ArrayAdapter<DrawDatesAndPastWinner> {

    Context mContext;
    int typeClicked;

    public DrawDateAndPastWinnerAdapter(Context context, List<DrawDatesAndPastWinner> objects,int typeClicked) {
        super(context, 0, objects);
        mContext=context;
        this.typeClicked=typeClicked;
    }

    class ViewHolder{
        TextView lblDate,lblContest,lblWinner;

        public ViewHolder(View convertView){
            lblDate= (TextView) convertView.findViewById(R.id.lblDate);
            lblContest= (TextView) convertView.findViewById(R.id.lblContest);
            lblWinner= (TextView) convertView.findViewById(R.id.lblWinner);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.row_contest,null);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }

        if(typeClicked== Constant.DATE_OF_DRAW){
            holder.lblWinner.setVisibility(View.GONE);
           // holder.lblDate.setText(holder.lblDate.getText() + " " + getItem(position).getDate());
            holder.lblDate.setText(holder.lblDate.getText() + " " +
                    DateTimeUtil.dateFormat(getItem(position).getDate(), "dd  MMM yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss"));
            holder.lblContest.setText(holder.lblContest.getText() + " " +getItem(position).getDesc());
        }else{
            holder.lblWinner.setVisibility(View.VISIBLE);
            holder.lblWinner.setText(holder.lblWinner.getText() + " " + getItem(position).getUserName());
            holder.lblDate.setText(holder.lblDate.getText() + " "  +
                    DateTimeUtil.dateFormat(getItem(position).getDrawDate(), "dd  MMM yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss"));
            holder.lblContest.setText(holder.lblContest.getText() + " " +getItem(position).getCompaignTitle());
        }

        //Log.e("DrawDates", DateTimeUtil.dateFormat(getItem(position).getDate(), "dd  MMM yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss"));
        return convertView;
    }
}
