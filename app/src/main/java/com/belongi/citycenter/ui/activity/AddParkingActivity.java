package com.belongi.citycenter.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.FileUtils;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Parkings;
import com.belongi.citycenter.data.entities.utils.ParkingDAO;
import com.belongi.citycenter.global.App;

import java.util.Calendar;

/**
 * Created by webwerks on 24/6/15.
 */
public class AddParkingActivity extends BaseActivity implements View.OnClickListener/*,PermissionUtils.PermissionCallback,
        PermissionUtils.PermissionProcessor*/{

    EditText txtTitle;
    ImageView imgPic;
    Parkings model;
    TextView btnDelete;
    //private PermissionUtils.PermissionCallback permissionCallback;

    @Override
    protected void releaseUi() {
    }

    /*private static final String[] CAMERA_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };*/

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_add_parking);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("MY PARKING");
        findViewById(R.id.btnRetake).setOnClickListener(this);
        btnDelete= (TextView) findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);
        findViewById(R.id.btnDone).setOnClickListener(this);
        txtTitle= (EditText) findViewById(R.id.txtTitle);
        //tDesc= (EditText) findViewById(R.id.txtDesc);
        imgPic= (ImageView) findViewById(R.id.imgPic);

        model= (Parkings) getIntent().getSerializableExtra("Model");
        loadUI();
    }

    Uri fileUri;

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btnRetake:
                //PermissionUtils.requestIfNotGranted(this,CAMERA_PERMISSIONS,0,this);
                if(FileUtils.isDeviceSupportCamera(AddParkingActivity.this)){
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = FileUtils.getOutputMediaFileUri(1,"MOE");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
                    startActivityForResult(intent,0);
                }
                break;

            case R.id.btnDelete:
                ParkingDAO.deleteParking(model);
                finish();
                break;

            case R.id.btnDone:
                /*boolean update=false;
                if(!TextUtils.isEmpty(model.getTitle())) {
                    update=true;
                }

                if(TextUtils.isEmpty(txtTitle.getText().toString())){
                    Validation.UI.showToast(AddParkingActivity.this,"Please fill complete details");
                }else{
                   // model.setDesc(txtDesc.getText().toString());
                    model.setTitle(txtTitle.getText().toString());
                    model.setCreatedDate(Calendar.getInstance().getTimeInMillis()+"");
                    model.setMallId(App.get().getMallId());
                    if(update){
                        ParkingDAO.update(model);
                    }else{
                        ParkingDAO.insertParking(model);
                    }
                    finish();
                }*/

                if(!TextUtils.isEmpty(txtTitle.getText().toString()))
                    model.setTitle(txtTitle.getText().toString());
                else
                    model.setTitle("");
                model.setCreatedDate(Calendar.getInstance().getTimeInMillis()+"");
                model.setMallId(App.get().getMallId());
                ParkingDAO.insertOrUpdate(model);
                finish();
                break;
        }
    }

    /*public void callCamera(){
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = FileUtils.getOutputMediaFileUri(1,"CityCenter");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
        startActivityForResult(intent, 0);
    }*/

    @Override
    public String getScreenName() {
        return "My Parking";
    }

    public void loadUI(){
        if(model!=null){
            if(!TextUtils.isEmpty(model.getTitle())){
                //txtDesc.setText(model.getDesc());
                txtTitle.setText(model.getTitle());
                //btnDelete.setVisibility(View.VISIBLE);
            } /*else {
                btnDelete.setVisibility(View.GONE);
            }*/

            if(model.getIdentifier()!=null){
                btnDelete.setVisibility(View.VISIBLE);
            }else{
                btnDelete.setVisibility(View.GONE);
            }

            if(model.getImagePath()!=null) {
                imgPic.setImageBitmap(FileUtils.decodeBitmapFromFile(model.getImagePath(), 500, 400));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK ) {
            model.setImagePath(fileUri.getPath());
            imgPic.setImageBitmap(FileUtils.decodeBitmapFromFile(model.getImagePath(),500,400));
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.msgCancel), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.msgFailed), Toast.LENGTH_SHORT).show();
        }
    }

    /*@Override
    public void onPermissionResult(int requestCode, boolean granted) {
        if(granted){
            callCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean granted=false;
        switch (requestCode) {
            //constant for write external storeage permision for Content Fragment.
            case 0: {
                // If request is cancelled, the result arrays are empty.
                for(int i:grantResults){
                    if(i== PackageManager.PERMISSION_GRANTED) {
                        granted = true;
                    }else {
                        granted = false;
                        break;
                    }
                }
                notifyPermissionResult(0,granted);

               *//* if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    notifyPermissionResult(0, true);
                } else {
                    notifyPermissionResult(0, false);
                }*//*
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void setPermissionCallback(PermissionUtils.PermissionCallback callback) {
        permissionCallback=callback;
    }

    @Override
    public void notifyPermissionResult(int requestCode, boolean granted) {
        permissionCallback.onPermissionResult(requestCode,granted);
    }*/
}
