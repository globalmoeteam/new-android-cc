package com.belongi.citycenter.ui.fragment.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.fragments.home.AbstractHomeFragment;

import org.json.JSONObject;

/**
 * Created by yashesh on 6/22/2015.
 */
public class InsideMallFragment extends AbstractHomeFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public String getScreenName() {
        return "Home Inside Mall";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_inside_mall, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle(App.get().getSelectedMall().getMallName().toUpperCase());
        if (App.get().checkMall()) {
            view.findViewById(R.id.btnLocateStore).setOnClickListener(this);
        } else {
            view.findViewById(R.id.btnLocateStore).setVisibility(View.GONE);
        }
        view.findViewById(R.id.btnShopping).setOnClickListener(this);
        view.findViewById(R.id.btnDining).setOnClickListener(this);
        view.findViewById(R.id.btnEntertainment).setOnClickListener(this);
        view.findViewById(R.id.btnGiftCard).setOnClickListener(this);
        view.findViewById(R.id.btnSocialWall).setOnClickListener(this);
        view.findViewById(R.id.btnUtilities).setOnClickListener(this);
        view.findViewById(R.id.btnFindCar).setOnClickListener(this);
        containerMiddle.setVisibility(View.GONE);
        containerMiddleLoc.setVisibility(View.GONE);
    }


}
