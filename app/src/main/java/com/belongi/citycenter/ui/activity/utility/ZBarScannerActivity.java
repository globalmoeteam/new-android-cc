package com.belongi.citycenter.ui.activity.utility;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.logic.QRLogic;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by webwerks on 16/6/15.
 */
public class ZBarScannerActivity extends BaseActivity implements ZBarScannerView.ResultHandler{

    private ZBarScannerView mScannerView;
    private static final String TAG="ZBarScannerActivity";


    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        mScannerView = new ZBarScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    protected void initializeUi() {
        if(isCameraAvailable()){
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
            mScannerView.setFlash(false);
            mScannerView.setAutoFocus(true);
        }
    }

    @Override
    public String getScreenName() {
        return "Utilities - QR Scanning";
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    @Override
    public void handleResult(Result result) {
        //Log.e(TAG, result.getContents());
        //Log.e(TAG, result.getBarcodeFormat().getName());
        mScannerView.stopCamera();

        if(result.getBarcodeFormat().getName().equals("QRCODE")){
            if(result.getContents().contains("BEGIN:VCARD")){
                QRLogic.parseVcard(this, result.getContents());
            }else if(result.getContents().contains("SMSTO")){
                QRLogic.sendSms(this,result.getContents());
            }else if(result.getContents().contains("MATMSG")){
                QRLogic.sendEmail(this,result.getContents());
            }else if(result.getContents().startsWith("tel:")){
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(result.getContents()));
                startActivity(intent);
            }else{

                if(result.getContents().startsWith("www") || result.getContents().startsWith("http")){
                    if(result.getContents().startsWith("http")){
                        startActivity(new Intent(ZBarScannerActivity.this, WebContentPreviewActivity.class)
                                .putExtra("url",result.getContents())
                                .putExtra("title","UTILITIES")
                                .putExtra("category", Constant.DONT_SEND));
                    }else{
                        startActivity(new Intent(ZBarScannerActivity.this, WebContentPreviewActivity.class)
                                .putExtra("url", "https://"+result.getContents())
                                .putExtra("title", "UTILITIES")
                                .putExtra("category", Constant.DONT_SEND));
                    }

                }else{
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }

              /*  Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(result.getContents()));
                PackageManager manager = this.getPackageManager();
                List<ResolveInfo> infos = manager.queryIntentActivities(webIntent, 0);
                if (infos.size() > 0) {
                    startActivity(new Intent(ZBarScannerActivity.this, WebContentPreviewActivity.class)
                            .putExtra("url",result.getContents())
                            .putExtra("title","UTILITIES"));
                    //startActivity(Intent.createChooser(webIntent, "Open with"));
                }else{
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
*/
            }
        }else{
            Toast.makeText(this,"Please try again ...",Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
