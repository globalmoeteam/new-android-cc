package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.ShopCategory;

import java.util.List;

/**
 * Created by webwerks on 3/7/15.
 */
public class CategoryAdapter extends ArrayAdapter<ShopCategory> {

    Context mContext;

    public CategoryAdapter(Context context, List<ShopCategory> objects) {
        super(context, 0, objects);
        mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, null);
        TextView lblTitle = (TextView) convertView.findViewById(android.R.id.text1);
        lblTitle.setText(getItem(position).getCategoryName());
        lblTitle.setTextColor(Color.parseColor("#E4F7FD"));
        lblTitle.setTextSize(13);
        lblTitle.setSingleLine(true);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, null);
        TextView lblTitle = (TextView) convertView.findViewById(android.R.id.text1);
        lblTitle.setText(getItem(position).getCategoryName());
        lblTitle.setSingleLine(true);
        return convertView;
    }
}
