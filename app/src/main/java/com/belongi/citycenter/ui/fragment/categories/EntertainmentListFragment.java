package com.belongi.citycenter.ui.fragment.categories;

import android.util.Log;

import com.activeandroid.query.Select;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.global.App;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 6/25/2015.
 */
public class EntertainmentListFragment extends BaseCategoryListFragment {

    public EntertainmentListFragment() {
        super();
    }

    @Override
    public Object executeQuery() {
        List<Object> result=new ArrayList<>();

        List<FavouriteItem> favs = new Select().from(FavouriteItem.class).where("type =?", new String[]{"entertainment"}).execute();
        List<Entertainment> items = new Select().from(Entertainment.class)
                .where("forHome=?", 1)
                .where("mallid=?", App.get().getMallId()).execute();
        Log.w("Entertainment list", items.size() + "entertainment size");

        for (Entertainment dining : items) {
            for (FavouriteItem fav : favs) {
                if (dining.getIdentifier().equals(fav.getIdentifier())) {
                    dining.setFavourited(true);
                    break;
                } else {
                    dining.setFavourited(false);
                }
            }
        }

        result.add(items);
        return result;
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
        Log.e("error", error + "");
    }

    @Override
    public String getScreenName() {
        return "Entertainment";
    }
}
