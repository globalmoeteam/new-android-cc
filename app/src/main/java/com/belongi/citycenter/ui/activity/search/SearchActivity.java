package com.belongi.citycenter.ui.activity.search;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.android.utilities.Validation;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.ATMService;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.data.entities.Category;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.ShopDetailActivity;
import com.belongi.citycenter.ui.activity.services.ServiceDetailActivity;
import com.belongi.citycenter.ui.adapters.SearchResultsAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 6/18/2015.
 */
public class SearchActivity extends BaseActivity implements LocalDataLoader.QueryClient, TextWatcher, View.OnClickListener, AdapterView.OnItemClickListener {

    EditText txtSearch;
    RelativeLayout searchHeader;
    ListView lstSearchResult;
    SearchResultsAdapter adapter;
    TextView labelNoData;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_search);
    }

    @Override
    protected void initializeUi() {
        //cmdSearch=findViewById(R.id.cmdSearch);
        findViewById(R.id.btnCancel).setOnClickListener(this);
        findViewById(R.id.btnClose).setOnClickListener(this);
        searchHeader = (RelativeLayout) findViewById(R.id.searchHeader);
        labelNoData = (TextView) findViewById(R.id.label_no_data);
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        lstSearchResult = (ListView) findViewById(R.id.lstSearchResults);
        adapter = new SearchResultsAdapter(this, new ArrayList<Search>());
        lstSearchResult.setAdapter(adapter);
        txtSearch.addTextChangedListener(this);
        searchHeader.setOnClickListener(this);
        lstSearchResult.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtSearch.clearFocus();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnClose:
                txtSearch.setText("");
                txtSearch.requestFocus();
                break;

            case R.id.searchHeader:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtSearch, InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.btnCancel:
                Validation.UI.hideKeyboard(SearchActivity.this);
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Search";
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        adapter.clear();
        if (result != null && ((List<Search>) result).size() > 0) {
            labelNoData.setVisibility(View.GONE);
        } else {
            labelNoData.setVisibility(View.VISIBLE);
        }
        adapter.addAll((List<Search>) result);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        // get response only if not finishing.
        return (!isFinishing());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            //perform search
            labelNoData.setVisibility(View.GONE);
            new LocalDataLoader(this, 0).execute();
        } else {
            //clear the adapter
            labelNoData.setVisibility(View.VISIBLE);

            adapter.clear();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public Object executeQuery() {
        return EntityUtils.localSearchForKeyword(txtSearch.getText().toString());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Search item = adapter.getItem(position);
        Serializable object = null;
        Log.w("CATEGORY :", item.getIdentifier() + "..." + item.getCategory());
        if (item.getCategory().equals("HOTEL")) {
            List<Hotel> hotelShops = new Select().from(Hotel.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (hotelShops != null && hotelShops.size() > 0) {
                object = hotelShops.get(0);
            }
            List<FavouriteItem> items = new Select().from(FavouriteItem.class).where("identifier = ?", new String[]{((Hotel) object).getIdentifier()}).execute();
            if (items.size() > 0) {
                ((Hotel) object).setIsFavourited(true);
            }
        } else if (item.getCategory().equals("DINING")) {
            List<DiningShop> diningShops = new Select().from(DiningShop.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (diningShops != null && diningShops.size() > 0) {
                object = diningShops.get(0);

                List<FavouriteItem> items = new Select().from(FavouriteItem.class).where("identifier = ?", new String[]{((DiningShop) object).getIdentifier()}).execute();
                if (items.size() > 0) {
                    ((DiningShop) object).setFavourited(true);
                }
            }
        } else if (item.getCategory().equals("SHOPPING")) {
            Log.e("GETTING DATA", "" + item.getIdentifier());
            List<ShoppingShop> shoppingShops = new Select().from(ShoppingShop.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (shoppingShops != null && shoppingShops.size() > 0) {
                object = shoppingShops.get(0);

                List<FavouriteItem> items = new Select().from(FavouriteItem.class).where("identifier = ?", new String[]{((ShoppingShop) object).getIdentifier()}).execute();
                if (items.size() > 0) {
                    ((ShoppingShop) object).setFavourited(true);
                }
            }
        } else if (item.getCategory().equals("EVENT")) {
            List<Event> events = new Select().from(Event.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (events != null && events.size() > 0) {
                object = events.get(0);
            }
        } else if (item.getCategory().equals("OFFER")) {
            List<Offer> offers = new Select().from(Offer.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (offers != null && offers.size() > 0) {
                object = offers.get(0);
            }
        } else if (item.getCategory().equals("ENTERTAINMENT")) {
            List<Entertainment> entertainments = new Select().from(Entertainment.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (entertainments != null && entertainments.size() > 0) {
                object = entertainments.get(0);
                List<FavouriteItem> items = new Select().from(FavouriteItem.class).where("identifier = ?", new String[]{((Entertainment) object).getIdentifier()}).execute();
                if (items.size() > 0) {
                    ((Entertainment) object).setFavourited(true);
                }
            }
        } else if (item.getCategory().equals("SERVICE")) {
            List<BaseService> service = new Select().from(Service.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (service != null && service.size() > 0) {
                object = service.get(0);
            }
        } else if (item.getCategory().equals("ATM")) {
            List<BaseService> service = new Select().from(ATMService.class).where("identifier = ?", new String[]{item.getIdentifier()}).execute();
            if (service != null && service.size() > 0) {
                object = service.get(0);
            }
        }

        Intent detailIntent;
        if (item.getCategory().equals("SERVICE") || item.getCategory().equals("ATM")) {
            detailIntent = new Intent(this, ServiceDetailActivity.class);
            detailIntent.putExtra("Model", object);
        } else {
            detailIntent = new Intent(this, ShopDetailActivity.class);
            detailIntent.putExtra(Constant.DETAIL_SHOP_TYPE, object);

        }
        //Intent detailIntent=new Intent(this, ShopDetailActivity.class);
        startActivity(detailIntent);
    }
}
