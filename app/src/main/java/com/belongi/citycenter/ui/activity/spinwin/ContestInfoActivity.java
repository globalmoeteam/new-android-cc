package com.belongi.citycenter.ui.activity.spinwin;

import android.text.Html;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 17/6/15.
 */
public class ContestInfoActivity extends BaseActivity {

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_contest_info);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
        TextView lblHeader= (TextView) findViewById(R.id.lblHeader);
        TextView lblContent= (TextView) findViewById(R.id.lblContent);
        int typeClicked=getIntent().getIntExtra(Constant.TYPE_CLICKED,0);
        Campaigns.Compaign contest= App.get().getSelectedContest();
        if(typeClicked==Constant.HOW_TO_PLAY){
            lblHeader.setText("How To Play");
            lblContent.setText(Html.fromHtml(contest.getCampaignDetailDesc()));
        }else if(typeClicked==Constant.TERMS_CONDITIONS){
            lblHeader.setText("Terms & Conditions");
            lblContent.setText(Html.fromHtml(contest.getTermsConditions()));
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN";
    }
}
