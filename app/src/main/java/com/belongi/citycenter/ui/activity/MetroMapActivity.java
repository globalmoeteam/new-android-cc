package com.belongi.citycenter.ui.activity;

import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;

/**
 * Created by webwerks on 1/7/15.
 */
public class MetroMapActivity extends BaseActivity {
    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_metro_map);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("METRO MAP");
    }

    @Override
    public String getScreenName() {
        return "Book A Taxi - Metro Map";
    }
}
