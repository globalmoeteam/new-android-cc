package com.belongi.citycenter.ui.fragment.categories;

import android.util.Log;

import com.activeandroid.query.Select;
import com.belongi.citycenter.data.entities.Category;
import com.belongi.citycenter.data.entities.ShopCategory;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 6/25/2015.
 */
public class ShoppingListFragment extends  BaseCategoryListFragment implements LocalDataLoader.QueryClient{

    public ShoppingListFragment() {
        super();
    }

    @Override
    public Object executeQuery() {
        List<Object> result=new ArrayList<>();

        List<FavouriteItem> favs= new  Select().from(FavouriteItem.class).where("type =?",new String[]{"shopping"}).execute();

        List<ShoppingShop> items=new Select().from(ShoppingShop.class).where("mallid=?", App.get().getMallId()).execute();

        Log.e("ShopSizeQuery",App.get().getMallId()+" : "+ new Select().from(ShoppingShop.class).where("mallid=?", App.get().getMallId()).toSql());
        for(ShoppingShop dining : items){
            for(FavouriteItem fav:favs){
                if(dining.getIdentifier().equals(fav.getIdentifier())){
                    dining.setFavourited(true);
                    break;
                }else{
                    dining.setFavourited(false);
                }
            }
        }

        List<ShopCategory> shopCategories=new Select().from(ShopCategory.class)
                .where("type =?", Category.SHOPPING)
                .where("mallid=?", App.get().getMallId()).execute();

        result.add(items);

        if(shopCategories!=null){
            result.add(shopCategories);
        }
        //return items;
        return result;
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public String getScreenName() {
        return "Shopping";
    }
}
