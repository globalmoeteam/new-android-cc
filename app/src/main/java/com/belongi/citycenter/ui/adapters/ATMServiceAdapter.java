package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.BaseService;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.services.ServiceDetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 29/6/15.
 */
public class ATMServiceAdapter extends ArrayAdapter<BaseService> implements SectionIndexer,View.OnClickListener {


    Context mContext;
    public String sections;
    List<BaseService> atmList=null;

    public ATMServiceAdapter(Context context, List<BaseService> objects) {
        super(context, 0, objects);
        mContext=context;
        atmList=objects;
        sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(R.layout.row_atm_list,null);
        TextView lblTitle= (TextView) convertView.findViewById(R.id.lblTitle);
        lblTitle.setText(atmList.get(position).getServicePageTitle());
        lblTitle.setTag(getItem(position));
        lblTitle.setOnClickListener(this);
        return convertView;
    }

    @Override
    public Object[] getSections() {
        String[] sectionsArray = new String[sections.length()];
        for (int i=0; i < sections.length(); i++)
            sectionsArray[i] = "" + sections.charAt(i);
        return sectionsArray;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        for (int i=0; i < atmList.size() ; i++) {
            String item = atmList.get(i).getServicePageTitle();
            if (item.charAt(0) == sections.charAt(sectionIndex))
                return i;
        }
        return 0;
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }

    @Override
    public void onClick(View view) {
        BaseService atm= (BaseService) view.getTag();
        mContext.startActivity(new Intent(mContext, ServiceDetailActivity.class).putExtra("Model", atm));
        ((BaseActivity)mContext).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }
}
