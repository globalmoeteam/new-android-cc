package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Malls;
import java.util.List;

/**
 * Created by webwerks on 28/7/15.
 */
public class MallSelectionAdapter extends ArrayAdapter<Malls> {

    Context mContext;

    public MallSelectionAdapter(Context context, List<Malls> objects) {
        super(context, 0, objects);
        mContext=context;
    }


    class ViewHolder{
        TextView lblName;
        LinearLayout llMall;
        public ViewHolder(View convertView){
            lblName= (TextView) convertView.findViewById(R.id.lblMall);
            llMall= (LinearLayout) convertView.findViewById(R.id.layout_mall);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.row_mall_selection,null);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }

        Log.e("SiteId", getItem(position).getMallName() + " : " + getItem(position).getSiteId());

        holder.lblName.setText(getItem(position).getMallName());

        String iconName= getItem(position).getMallName().replace("’", "").replaceAll(" ", "_").toLowerCase()+"_bg";
        Log.e("Incon name",iconName);
        if(iconName!=null){
            int iconId=mContext.getResources().getIdentifier(iconName,"drawable",mContext.getPackageName());
            holder.llMall.setBackgroundResource(iconId);
        }
       // holder.llMall.setBackgroundResource(getItem(position).getMallBg());

        return convertView;
    }
}
