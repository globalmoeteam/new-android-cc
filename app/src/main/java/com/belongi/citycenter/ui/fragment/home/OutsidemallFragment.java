package com.belongi.citycenter.ui.fragment.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.fragments.home.AbstractHomeFragment;

/**
 * Created by yashesh on 6/23/2015.
 */
public class OutsidemallFragment extends AbstractHomeFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_outside_mall, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle(App.get().getSelectedMall().getMallName().toUpperCase());

        if(Constant.BOOKATAXI !=null && Constant.BOOKATAXI.equals("NOTACTIVE")){
            view.findViewById(R.id.llBookATaxi).setVisibility(View.GONE);
        }

        switch (App.get().getSelectedMall().getIdentifier()){
            case MAADI:
            case ALEXANDRIA:
            /*case BEIRUT:*/
                ((TextView)view.findViewById(R.id.btnGiftCard)).setText("GIFT VOUCHER");
                view.findViewById(R.id.btnBookTaxi).setBackgroundResource(R.drawable.ic_opening_hours);
                break;

            case MUSCAT:
            case QURUM:
                ((TextView)view.findViewById(R.id.btnGiftCard)).setText("GIFT VOUCHER");
                break;
            case BAHRAIN:
                view.findViewById(R.id.btnBookTaxi).setBackgroundResource(R.drawable.ic_opening_hours);
                break;
            case BEIRUT:
                view.findViewById(R.id.llBookATaxi).setVisibility(View.GONE);
                view.findViewById(R.id.btnGiftCard).setVisibility(View.GONE);
                break;
        }

        if(App.get().isHasCampaigns()){
            view.findViewById(R.id.viewSpinWin).setVisibility(View.VISIBLE);
            view.findViewById(R.id.btnSpinandWin).setVisibility(View.VISIBLE);
            view.findViewById(R.id.btnSpinandWin).setOnClickListener(this);
        }else{
            view.findViewById(R.id.viewSpinWin).setVisibility(View.GONE);
            view.findViewById(R.id.btnSpinandWin).setVisibility(View.GONE);
        }

        view.findViewById(R.id.btnGiftCard).setOnClickListener(this);
        view.findViewById(R.id.btnSocialWall).setOnClickListener(this);
        view.findViewById(R.id.btnUtilities).setOnClickListener(this);
        view.findViewById(R.id.btnBookTaxi).setOnClickListener(this);
        view.findViewById(R.id.btnGettingHere).setOnClickListener(this);
        containerMiddle.setVisibility(View.GONE);
        containerMiddleLoc.setVisibility(View.GONE);

       /* HorizontalScrollView top= (HorizontalScrollView) view.findViewById(R.id.gridTop);
        LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0);
        params.weight=1.70f;
        top.setLayoutParams(params);

        HorizontalScrollView bottom= (HorizontalScrollView) view.findViewById(R.id.gridBottom);
        LinearLayout.LayoutParams params1=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0);
        params1.weight=1.05f;
        bottom.setLayoutParams(params1);*/
        /*containerTop
                containerBottom*/

    }

    @Override
    public String getScreenName() {
        return "Home Outside Mall";
    }
}
