package com.belongi.citycenter.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.Validation;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.NotificationImage;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.global.ContentUploader;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.logic.NotificationLogic;
import com.belongi.citycenter.model.Notification;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.DashboardCoachActivity;
import com.belongi.citycenter.ui.adapters.CountryAdapter;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;

/**
 * Created by webwerks on 14/6/15.
 */
public class NotificationFragment extends BaseFragment implements View.OnClickListener, BackgroundJobClient, OneSignal.IdsAvailableHandler /*GcmUtility.GcmListner*/ {


    public static NotificationFragment newInstance(boolean isSkipVisible) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isSkipVisible", isSkipVisible);
        fragment.setArguments(bundle);
        return fragment;
    }

    private SwitchCompat mSwitchFashionLadies, mSwitchFashionMens, mSwitchDining,
            mSwitchOffers, mSwitchEvents;
    private ImageView mImgNotification;
    private TextView mLblInterestedCategories, mLblWeekly;
    private EditText mEdtName, mEdtEmailAddress, mEdtMobNo;
    private Spinner mSpnFrequency;
    private Notification mNotification = null;
    RadioButton rbtnMr, rbtnMs;

    boolean skipIndicator = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manage_notification, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setActionbarTitle("MANAGE NOTIFICATIONS");
        ContentDownloader.getNotificationImage(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int navBarHeight = getNavigationBarHeight();
            view.findViewById(R.id.layout_notification).setPadding(0, 0, 0, navBarHeight);
        }

        mEdtMobNo = (EditText) view.findViewById(R.id.edtMobNumber);
        setMobPlaceholder();
        mSwitchFashionLadies = (SwitchCompat) view.findViewById(R.id.switchFashionLadies);
        mSwitchFashionMens = (SwitchCompat) view.findViewById(R.id.switchFashionMens);
        mSwitchDining = (SwitchCompat) view.findViewById(R.id.switchDining);
        mSwitchOffers = (SwitchCompat) view.findViewById(R.id.switchOffers);
        mSwitchEvents = (SwitchCompat) view.findViewById(R.id.switchEvents);

        mImgNotification = (ImageView) view.findViewById(R.id.imgNotification);
        mLblInterestedCategories = (TextView) view.findViewById(R.id.lblInterestedCategories);
        mLblWeekly = (TextView) view.findViewById(R.id.lblWeekly);

        rbtnMr = (RadioButton) view.findViewById(R.id.rbtnMr);
        rbtnMs = (RadioButton) view.findViewById(R.id.rbtnMs);
        mEdtName = (EditText) view.findViewById(R.id.edtName);
        mEdtEmailAddress = (EditText) view.findViewById(R.id.edtEmail);

        mSpnFrequency = (Spinner) view.findViewById(R.id.spnFrequency);
        mSpnFrequency.setAdapter(new CountryAdapter(getActivity(), getResources().getStringArray(R.array.arr_frequency)));

        view.findViewById(R.id.btnSubmit).setOnClickListener(this);
        if (getArguments().getBoolean("isSkipVisible")) {
            view.findViewById(R.id.btnSkip).setOnClickListener(this);
        } else {
            view.findViewById(R.id.btnSkip).setVisibility(View.GONE);
        }
        setupUI(view);
        populateNotification();

    }

    private void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
    }

    private int getNavigationBarHeight() {
        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public void setMobPlaceholder() {
        switch (App.get().getSelectedMall().getIdentifier()) {
            case DEIRA:
            case MIRDIF:
            case SHARJAH:
            case AJMAN:
            case FUJAIRAH:
            case MEAISEM:
            case SHINDAGHA:
                mEdtMobNo.setHint("00971 00 0000000");
                break;

            case BAHRAIN:
                mEdtMobNo.setHint("00973 00 0000000");
                break;

            case MUSCAT:
            case QURUM:
                mEdtMobNo.setHint("00968 00000000");
                break;

            case MAADI:
            case ALEXANDRIA:
                mEdtMobNo.setHint("0020 00 00000000");
                break;

            case BEIRUT:
                mEdtMobNo.setHint("00961 00 000000");
                break;
        }

    }

    private void populateNotification() {
        mNotification = NotificationLogic.getNotification(getActivity());
        if (mNotification != null) {
            mSwitchFashionLadies.setChecked(mNotification.isFashionLadies());
            mSwitchFashionMens.setChecked(mNotification.isFashionMens());
            mSwitchDining.setChecked(mNotification.isDining());
            mSwitchOffers.setChecked(mNotification.isOffers());
            mSwitchEvents.setChecked(mNotification.isEvents());
            mEdtName.setText(mNotification.getName());
            mEdtEmailAddress.setText(mNotification.getEmailAddress());
            mEdtMobNo.setText(mNotification.getMobNumber());
            if (mNotification.isWeekly1())
                mSpnFrequency.setSelection(0);
            else
                mSpnFrequency.setSelection(1);

            Log.e("mNotification", mNotification.getGender() + "");


            if (mNotification.getGender() == 1) {
                rbtnMr.setChecked(true);
            } else if (mNotification.getGender() == 0) {
                rbtnMs.setChecked(true);
            } else {
                rbtnMr.setChecked(false);
                rbtnMs.setChecked(false);
            }
        }
    }

    private boolean validate() {
        /*if(!mSwitchFashionLadies.isChecked() && !mSwitchFashionMens.isChecked() &&
                !mSwitchDining.isChecked()&&!mSwitchOffers.isChecked() &&
                !mSwitchEvents.isChecked()) {
            mLblInterestedCategories.clearFocus();
            mLblInterestedCategories.requestFocus();

            Validation.UI.showToast(getActivity(),"Please select one category");

            return false;
        }

        if(!mChkWeekly1.isChecked() && !mChkWeekly2.isChecked()) {
            mLblWeekly.clearFocus();
            mLblWeekly.requestFocus();

            Validation.UI.showToast(getActivity(),"Please select frequency");

            return false;
        }*/

        if (!rbtnMr.isChecked() && !rbtnMs.isChecked()) {
            Validation.UI.showToast(getActivity(), "Please select Gender");
            return false;
        }

        if (TextUtils.isEmpty(mEdtName.getText().toString().trim())) {
            mEdtName.clearFocus();
            mEdtName.setError("Please enter name");
            mEdtName.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(mEdtEmailAddress.getText().toString().trim())) {
            mEdtEmailAddress.clearFocus();
            mEdtEmailAddress.setError("Please enter a valid email address");
            mEdtEmailAddress.requestFocus();
            return false;
        }

        if (!Validation.isValidEmail(mEdtEmailAddress.getText().toString().trim())) {
            mEdtEmailAddress.clearFocus();
            mEdtEmailAddress.setError("Please enter a valid email address");
            mEdtEmailAddress.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(mEdtMobNo.getText().toString().trim())) {
            mEdtMobNo.clearFocus();
            mEdtMobNo.setError("Please enter mobile number");
            mEdtMobNo.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(mEdtMobNo.getText().toString().trim())) {
            /*if(!mEdtMobNo.getText().toString().startsWith("+")){
                mEdtMobNo.clearFocus();
                mEdtMobNo.setError("Please enter country code to proceed");
                mEdtMobNo.requestFocus();
                return false;
            }else */
            if (/*mEdtMobNo.getText().length()<0 || mEdtMobNo.getText().toString().replace("+","").length()>8 ||*/ mEdtMobNo.getText().toString().replace("+", "").length() < 8) {
                mEdtMobNo.clearFocus();
                mEdtMobNo.setError("Please enter valid Mobile number to proceed");
                mEdtMobNo.requestFocus();
                return false;
            }
        }


       /* if(!Validation.isValidMobNumber(mEdtMobNo.getText().toString().trim())) {
            mEdtMobNo.clearFocus();
            mEdtMobNo.setError("Please enter valid mobile number");
            mEdtMobNo.requestFocus();
            return false;
        }
*/
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (validate()) {
                    if (mNotification == null) {
                        mNotification = new Notification();
                        //  mNotification.setMallId(App.get().getMallId());
                    }

                    if (rbtnMr.isChecked()) {
                        mNotification.setGender(1);
                    } else if (rbtnMs.isChecked()) {
                        mNotification.setGender(0);
                    } else {
                        mNotification.setGender(-1);
                    }
                    //mNotification.setGender(1);
                    mNotification.setIsFashionLadies(mSwitchFashionLadies.isChecked());
                    mNotification.setIsFashionMens(mSwitchFashionMens.isChecked());
                    mNotification.setIsDining(mSwitchDining.isChecked());
                    mNotification.setIsOffers(mSwitchOffers.isChecked());
                    mNotification.setIsEvents(mSwitchEvents.isChecked());
                    mNotification.setName(mEdtName.getText().toString().trim());
                    mNotification.setEmailAddress(mEdtEmailAddress.getText().toString().trim());
                    mNotification.setMobNumber(mEdtMobNo.getText().toString().trim());
                    if (mSpnFrequency.getSelectedItemPosition() == 0) {
                        mNotification.setIsWeekly1(true);
                        mNotification.setIsWeekly2(false);
                    } else {
                        mNotification.setIsWeekly1(false);
                        mNotification.setIsWeekly2(true);
                    }
                    if (isVisible() && isAdded()) {
                        ((BaseActivity) getActivity()).showProgressLoading();
                    }
                    OneSignal.idsAvailable(this);
                    /*GcmUtility.getGcmId(getActivity(), this,
                            Constant.GCM_SENDER_ID);*/
                    //make webcall
                }
                break;

            case R.id.btnSkip:

                skipIndicator = true;

                mNotification = new Notification();
                //mNotification.setMallId(App.get().getMallId());
                mNotification.setIsFashionLadies(true);
                mNotification.setIsFashionMens(true);
                mNotification.setIsDining(true);
                mNotification.setIsOffers(true);
                mNotification.setIsEvents(true);
                mNotification.setName("");
                mNotification.setEmailAddress("");
                mNotification.setMobNumber("");
                mNotification.setIsWeekly1(false);
                mNotification.setIsWeekly2(true);

                if (isVisible() && isAdded()) {
                    ((BaseActivity) getActivity()).showProgressLoading();
                }

                OneSignal.idsAvailable(this);

                /*GcmUtility.getGcmId(getActivity(), this, Constant.GCM_SENDER_ID);*/
                break;
        }
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        Log.e("", result + "::RESULT");

        if (isVisible() && isAdded()) {
            ((BaseActivity) getActivity()).stopLoading();
        }
        switch (requestCode) {
            case WebApi.REQ_NOTIFICATION_IMAGE:
                NotificationImage[] notificationImages = (NotificationImage[]) ContentDownloader.getObjectFromJson(result.toString(), NotificationImage[].class);
                if (notificationImages.length > 0)
                    Picasso.with(getActivity()).load(WebApi.IMAGE_BASE_URL + notificationImages[0].getImageUrl()).fit()
                            .placeholder(R.drawable.notification_placeholder).into(mImgNotification);
                break;


            case ContentUploader.UPLOAD_NOTOFICATION:
                Log.e("UpdateNotificaton", result.toString());

                if (Boolean.parseBoolean(result.toString())) {

                    if (skipIndicator == false) {
                        Toast.makeText(getActivity(), "Thanks for Registration", Toast.LENGTH_SHORT).show();
                    }
                    NotificationLogic.saveNotification(mNotification, getActivity());
                    if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                    } else {
                        startActivity(new Intent(getActivity(), DashboardCoachActivity.class));
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    }
                }
                break;
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

   /* @Override
    public void onGcmRegistered(String gcmKey, int code) {
        mNotification.setGcmId(gcmKey);
        ContentUploader.updateUserPushNotifications(mNotification, this);
    }*/

    @Override
    public String getScreenName() {
        if (getArguments().getBoolean("isSkipVisible")) {
            return "First Notification management";
        } else {
            return "Inside Application Notification management";
        }
    }

    @Override
    public void idsAvailable(String userId, String registrationId) {

        if (registrationId != null) {
            App.get().setDeviceId(registrationId);
            mNotification.setGcmId(registrationId);
            ContentUploader.updateUserPushNotifications(mNotification, this);
        }
    }
}
