package com.belongi.citycenter.ui.activity;

import android.content.Intent;
import android.view.View;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;

/**
 * Created by webwerks on 12/8/15.
 */
public class NotificationCoachActivity extends BaseActivity implements View.OnClickListener {

    @Override
    public String getScreenName() {
        return Constant.DONT_SEND;
    }

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_coach);
    }

    @Override
    protected void initializeUi() {
        //super.initializeUi();
        findViewById(R.id.layout_coach).setBackgroundResource(R.drawable.notification_coach);
        findViewById(R.id.btnSkip).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        startActivity(new Intent(NotificationCoachActivity.this, NotificationActivity.class));
        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        finish();
    }
}
