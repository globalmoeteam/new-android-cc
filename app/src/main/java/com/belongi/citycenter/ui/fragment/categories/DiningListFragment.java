package com.belongi.citycenter.ui.fragment.categories;

import com.activeandroid.query.Select;
import com.belongi.citycenter.data.entities.Category;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.ShopCategory;
import com.belongi.citycenter.global.App;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 6/25/2015.
 */
public class DiningListFragment extends BaseCategoryListFragment {

    public DiningListFragment() {
        super();
    }

    @Override
    public Object executeQuery() {
        List<Object> result=new ArrayList<>();

        List<FavouriteItem> favs = new Select().from(FavouriteItem.class).where("type =?", new String[]{"dining"}).execute();
        List<DiningShop> items = new Select().from(DiningShop.class).where("mallid=?", App.get().getMallId()).execute();
        for (DiningShop dining : items) {
            for (FavouriteItem fav : favs) {
                if (dining.getIdentifier().equals(fav.getIdentifier())) {
                    dining.setFavourited(true);
                    break;
                } else {
                    dining.setFavourited(false);
                }
            }
        }

        List<ShopCategory> dineCategories=new Select().from(ShopCategory.class)
                .where("type =?", Category.DINING)
                .where("mallid=?", App.get().getMallId()).execute();

        result.add(items);
        result.add(dineCategories);
        return result;
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public String getScreenName() {
        return "Dining";
    }
}
