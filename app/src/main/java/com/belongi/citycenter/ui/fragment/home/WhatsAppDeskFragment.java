package com.belongi.citycenter.ui.fragment.home;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.PermissionUtils;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.fragment.BaseFragment;

import java.util.ArrayList;

/**
 * Created by webwerks on 18/4/16.
 */
public class WhatsAppDeskFragment extends BaseFragment implements View.OnClickListener,PermissionUtils.PermissionCallback {

    EditText txtMessage;
    TextView lblMessage;
    public static final String[] CONTACT_PERMISSION = {
            Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS
    };
    LinearLayout layout;

    @Override
    public String getScreenName() {
        return "Whats App Desk";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_whats_app,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HomeActivity)getActivity()).setPermissionCallback(this);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setActionbarTitle("WHATSAPP CS DESK");
        txtMessage= (EditText) view.findViewById(R.id.txtMessage);
        lblMessage= (TextView) view.findViewById(R.id.lblMessage);

        view.findViewById(R.id.btnSendMsg).setOnClickListener(this);
        lblMessage.setText("Select " + App.get().getSelectedMall().getMallName() + " CS Desk from whatsapp contact");
        layout= (LinearLayout) view.findViewById(R.id.layout);

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard();
                return false;
            }
        });
    }

    public void hideKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSendMsg:
                if(!TextUtils.isEmpty(App.get().getSelectedMall().getWhatsapp())/*App.get().getSelectedMall().getWhatsapp()!=null*/)
                    PermissionUtils.requestIfNotGranted(getActivity(),CONTACT_PERMISSION,0,WhatsAppDeskFragment.this);
                else
                    Toast.makeText(getActivity(),"No contact details found",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void showDialog(){
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(App.get().getSelectedMall().getMallName());
        alertDialog.setMessage("You must give the app permission to add the contact first. ");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "ADD", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.e("Enter",App.get().getSelectedMall().getMallName() + " CS Desk" + ": " + App.get().getSelectedMall().getWhatsapp());

                addContact(App.get().getSelectedMall().getMallName() + " CS Desk", App.get().getSelectedMall().getWhatsapp());
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"CANCEL",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.cancel();
            }
        });
        alertDialog.show();
    }




    public void openWhatsApp(){
        /*Uri uri = Uri.parse("smsto:"+ App.get().getSelectedMall().getWhatsapp());
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(i);*/

        /*Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setPackage("com.whatsapp");
        sendIntent.setType("text/plain");*/
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.whatsapp");
            intent.putExtra(Intent.EXTRA_TEXT, txtMessage.getText().toString().trim());
            getActivity().startActivity(intent);
            ((BaseActivity) getActivity()).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }catch (Exception e){
            Toast.makeText(getActivity(), "Whats App application not installed on your device", Toast.LENGTH_SHORT).show();
        }

        /*final ComponentName name = new ComponentName("com.whatsapp", "com.whatsapp.ContactPicker");
        Intent oShareIntent = new Intent();
        oShareIntent.setComponent(name);
        oShareIntent.setType("text/plain");
        oShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Your Message");
        startActivity(oShareIntent);*/

    }

    private void addContact(String name, String phone) {
        Log.e("Contact info", name + " : " + phone);
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = ops.size();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name) // Name of the person
                .build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone) // Number of the person
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build()); // Type of mobile number
        try {
            ContentProviderResult[] res = getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }

        openWhatsApp();
    }

    public boolean contactExists(Context context, String number,String name) {

        //Uri uri=ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
       // String selectionClause = null;
       /* String[] selectionArgs = null;
        if (!TextUtils.isEmpty(name)) {
            selectionClause = *//*ContactsContract.PhoneLookup.NUMBER + " = ? AND " +*//* ContactsContract.PhoneLookup.DISPLAY_NAME;
            selectionArgs = new String[]{ *//*number ,*//*name };
        }*/
        String[] mPhoneNumberProjection = { ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME };
        Cursor cur = context.getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);

        try {
            if (cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }


       /* ContentResolver  cr=getActivity().getContentResolver();
        Cursor c=cr.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
        if(c.getCount()>0){
            while (c.moveToNext()){
                Cursor cursor=cr.query()
            }
        }*/


        return false;
    }

    @Override
    public void onPermissionResult(int requestCode, boolean granted) {
        if(granted){
            Log.e("Contact",App.get().getSelectedMall().getWhatsapp());
            Log.e("COntact is there",contactExists(getActivity(), App.get().getSelectedMall().getWhatsapp(),
                    App.get().getSelectedMall().getMallName() + " CS Desk") +" ::::");
            if(contactExists(getActivity(), App.get().getSelectedMall().getWhatsapp(),
                    App.get().getSelectedMall().getMallName() + " CS Desk")){
                openWhatsApp();
            }else{
                showDialog();
            }
        }
    }
}
