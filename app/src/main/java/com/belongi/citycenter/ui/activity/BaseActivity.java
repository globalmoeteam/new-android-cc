package com.belongi.citycenter.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.search.SearchActivity;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by webwerks on 14/6/15.
 */
public abstract class BaseActivity extends _Activity implements View.OnClickListener {

    TextView actionbarTitle;
    ImageView searchIcon;

    @Override
    protected void initializeUi() {
        if (findViewById(R.id.actionToolbar) != null)
            setSupportActionBar((Toolbar) findViewById(R.id.actionToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_previous_item);

        searchIcon = (ImageView) findViewById(R.id.btnSearch);
        actionbarTitle = (TextView) findViewById(R.id.lblTitle);
        searchIcon.setOnClickListener(this);

    }

    public void setActionbarTitle(String title) {
        if (actionbarTitle != null) {
            actionbarTitle.setText(title);
        }
    }

    public String getActionbarTitle() {
        return actionbarTitle.getText().toString();
    }

    public void setSearchVisibility(boolean visibility) {
        if (visibility) {
            searchIcon.setVisibility(View.VISIBLE);
        } else {
            searchIcon.setVisibility(View.GONE);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                startActivity(new Intent(this, SearchActivity.class));
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                break;
        }
    }

    public String getStyleString(String desc) {

        String styleString = "<html><head><style type=\"text/css\">"
                + "a {color:#005CAB; text-decoration:underline}"
                + " table{border:2px solid #323232; width:100%%; margin:0px 0;}"
                + " td{border-right:2px solid #323232; padding:5px 8px; border-bottom:2px solid #323232;}"
                + " table tr td:last-child{border-right:none;}"
                + " table tr:last-child td{border-bottom:none;}"
                + " table{width:100%% !important;}"
                + "@font-face {"
                + "font-family: RegularFont;" + "src: url(\"file:///android_asset/fonts/GothamMedium.ttf\");}"
                + "@font-face {"
                + "font-family: BoldFont;"
                + "src: url(\"file:///android_asset/fonts/GothamBold.ttf\");}"
                + " body {font-family:RegularFont;font-weight:inherit; color: #323232;font-size:14px; align-content:left;}"
                + " h3 {font-family:BoldFont;font-size:18px;color:#323232;}"
                + " li { align-content: left; margin:0px}"
                + "</style>"
       /* +"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"*/
                + "<meta name='viewport' content='target-densityDpi=device-dpi'/>"
                + "</head>"
                + "<body>"
                + desc
                + "</body>"
                + "</html>";

        return styleString;
    }

    Dialog alertDialog;

    public void showProgressLoading() {
        alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_loading);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    public void stopLoading() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    @Override
    public void overridePendingTransition(int enterAnim, int exitAnim) {
        getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        super.overridePendingTransition(enterAnim, exitAnim);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        new Thread() {
            @Override
            public void run() {
                mTracker = App.get().getTracker();
                if (!(getScreenName().equalsIgnoreCase(Constant.DONT_SEND))) {
                    if (App.get().getSelectedMall() != null) {
                        Log.e("ScreenNAme", "Sending " + App.get().getSelectedMall().getMallCOde() + " : " + getScreenName());
                        //Set screen name.
                        mTracker.setScreenName(App.get().getSelectedMall().getMallCOde() + " : " + getScreenName());
                        // Send a screen view.
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                    }
                }
            }
        }.start();
    }

    public abstract String getScreenName();

    /*@Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (hasFocus) {
                getWindow().getDecorView()
                        .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        }
    }*/
}
