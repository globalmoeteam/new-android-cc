package com.belongi.citycenter.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Malls;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.jibe.MSingleton;
import com.belongi.citycenter.jibe.MoverIcon;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.network.Validator;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.jibestream.jibestreamandroidlibrary.elements.Element;
import com.jibestream.jibestreamandroidlibrary.elements.Pin;
import com.jibestream.jibestreamandroidlibrary.elements.UnitLabel;
import com.jibestream.jibestreamandroidlibrary.http.BasicAuthentication;
import com.jibestream.jibestreamandroidlibrary.intentFilters.IntentFilterEngine;
import com.jibestream.jibestreamandroidlibrary.main.Building;
import com.jibestream.jibestreamandroidlibrary.main.M;
import com.jibestream.jibestreamandroidlibrary.main.VenueDataService;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.PathType;
import com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Waypoint;
import com.jibestream.jibestreamandroidlibrary.shapes.IconShape;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyle;
import com.jibestream.jibestreamandroidlibrary.styles.RenderStyleIcon;
import com.july.emsp.EMSP;
import com.july.emsp.EMSPLocation;
import com.july.emsp.listeners.EMSPLocationRegisterListener;
import com.july.emsp.listeners.EMSPSDKRegistrationListener;
import com.july.emsp.listeners.EMSPWiFiLocationsListener;
import com.july.emsp.model.StatusHandle;
import com.july.emsp.model.WiFiLocation;
import com.onesignal.OneSignal;

import java.util.List;

public class SplashActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, EMSPLocationRegisterListener {

    private final String TAG = SplashActivity.class.getSimpleName();
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    GoogleApiClient googleApiClient;
    ProgressBar progressBar;
    TextView lblProgress;
    List<Malls> mallList = null;
    int totalMalls, count = 0, counter = 0;
    BroadcastReceiver progressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            uihandler.sendEmptyMessage(0);
        }
    };
    IntentFilter progressFilter = new IntentFilter(ContentDownloader.DOWNLOAD_PROGRESS);
    private boolean isGCMsent = false;
    Handler uihandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            counter++;
            progressBar.setProgress(Math.round(((float) counter / 5) * 100));
            lblProgress.setText(Math.round(((float) counter / 5) * 100) + "%");
            if (progressBar.getProgress() >= 100) {
                navigateNext();
            }
        }
    };
    private String bearerToken = "jMz3IyozeZ8arjUC9oP0hRaaDYca";

    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        /*new Thread(){
            @Override
            public void run() {
                App.get().getTracker().setScreenName(App.get().getSelectedMall().getMallCOde()+" : " +"Splash View");
                App.get().getTracker().send(new HitBuilders.ScreenViewBuilder().setNewSession().build());
            }
        }.start();*/

        ImageView imgLogo = (ImageView) findViewById(R.id.imgLogo);
        String iconName = App.get().getSelectedMall().getMallName().replace("’", "").replaceAll(" ", "_").toLowerCase();
        if (iconName != null) {
            int iconId = getResources().getIdentifier(iconName, "drawable", getPackageName());
            imgLogo.setImageResource(iconId);
        }
        progressBar = (ProgressBar) findViewById(R.id.progressDownload);
        progressBar.setMax(100);
        progressBar.setScaleY(10f);
        lblProgress = (TextView) findViewById(R.id.lblProgress);

        if (Validator.isConnected()) {

            ContentDownloader.getSiteID(App.get().getSelectedMall(), SplashActivity.this);

            registerDeviceID(null);
            connectGoogleApiClient();

           /* new GcmUtility().getGcmId(App.get(), new GcmUtility.GcmListner() {
                @Override
                public void onGcmRegistered(String gcmKey, int code) {
                    Log.e("onGcmRegistered", "enter on register");
                    //EMSP.registerForNotifications(SplashActivity.this, gcmKey);
                    App.get().setDeviceId(gcmKey);
                    connectGoogleApiClient();
                    ContentDownloader.getSiteID(App.get().getSelectedMall(),SplashActivity.this);
                }
            }, Constant.GCM_SENDER_ID);*/
        } else {
            navigateNext();
        }

        registerReceiver(progressReceiver, progressFilter);

        try {
            EMSP.initGCMRegistration(SplashActivity.this, Constant.GCM_SENDER_ID);
            EMSP.registerForNotifications(this, Constant.GCM_SENDER_ID);
            EMSP.handleNotification(this, getIntent());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //<<<<<<<<<<<<<<<<<<<<<<

        if (!TextUtils.isEmpty(App.get().getSelectedMall().getJibeProjectId())) {
            Context applicationContext = getApplicationContext();
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(applicationContext);

            localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(VenueDataService.DONE));
            localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(VenueDataService.ERROR));
            //todo starting jibes map basicAuthent not required yet pass it in instead of null when activated
            final BasicAuthentication basicAuthentication = new BasicAuthentication("", "", "", "en");
            VenueDataService.getData(applicationContext, Constant.URL_JMAP
                    /*"https://qamaf2.js-network.co"*/, Integer.parseInt(App.get().getSelectedMall().getJibeProjectId()), null);
            ;

            VenueDataService.setOnVenueDataServiceReadyCallback(new VenueDataService.OnVenueDataServiceReadyCallback() {
                @Override
                public void onVenueDataReady(String s) {

                }

                @Override
                public void onVenueDataError() {

                }
            });
        }
        //<<<<<<<<<<<<<<<<<<<<<<
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
            String action = intent.getAction();
            switch (action) {
                case VenueDataService.DONE:
                    // Get the filename with the data
                    String filename = intent.getStringExtra(VenueDataService.KEY_FILENAME);
                    Log.d("Location", " DONE " + filename);
                    // Get your M instance
                    Context applicationContext = getApplicationContext();
                    MSingleton.initialize(applicationContext);
                    M m = MSingleton.getI();
                   /* m=null;

                    Log.e("CREATE ","M Valte" + MSingleton.getI());

                    m=MSingleton.getI();*/

                    //m.setEngineView(new EngineView(SplashActivity.this));

                    // Register your custom receiver that hooks onto the complete event
                    LocalBroadcastManager.getInstance(applicationContext).
                            registerReceiver(broadcastReceiverFrameworkInit, new IntentFilterEngine(m, IntentFilterEngine.COMPLETE));
                    LocalBroadcastManager.getInstance(context)
                            .registerReceiver(broadcastReceiverFrameworkResume, new IntentFilterEngine(m, IntentFilterEngine.RESUME));
                    // Start your M instance
                    m.start(filename, R.raw.config_old);
                    break;
                case VenueDataService.ERROR:
                    // handle error
                    //todo disable the ability of the user selecting map fragment
                    App.get().setMapEnabled(false);
                    Log.e("Location", " ERROR");
                    break;
            }
        }
    };


    private BroadcastReceiver broadcastReceiverFrameworkResume = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiverFrameworkResume);
            // React to the resume event such as resuming an activity

            //MSingleton.getI().resetWayfind();


        }
    };

    private BroadcastReceiver broadcastReceiverFrameworkInit = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LocalBroadcastManager.getInstance(context.getApplicationContext()).unregisterReceiver(broadcastReceiverFrameworkInit);
            // TODO: remove and add all unitlabels so they will be on top of all other elements <<<<<<<<<<
            M m = MSingleton.getI();
            Element[] elementsOfunitLabel = m.getElementsByType(UnitLabel.class);
            int size = elementsOfunitLabel.length;
            /*for (int i = 0; i < size; i++) {
                UnitLabel unitLabel = (UnitLabel) elementsOfunitLabel[i];
                m.removeFromMap(unitLabel);
                m.addToMap(unitLabel);
            }*/

            //change pin color
            Pin ele = new Pin();
            // Create a shape for it
            final com.jibestream.jibestreamandroidlibrary.shapes.Image pin = new com.jibestream.jibestreamandroidlibrary.shapes.Image();
            Bitmap pinIcon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.ic_map_pin);
            pin.setBitmap(pinIcon);
            pin.setOffsetY(-10);

            // Set its shape and other properties
            ele.setShape(pin);
            ele.setHeadsUp(true);
            MSingleton.getI().addToMap(ele);
            m.pin = ele;

            RenderStyle escalatorStyle = new RenderStyle(Paint.Style.FILL);
            escalatorStyle.paintFill.setColor(Color.parseColor("#E6E6E6"));
            m.cssStyles.setEscalators(escalatorStyle, true);

            ///<<<<<<<<<
            // TODO: add movers as icons
            for (com.jibestream.jibestreamandroidlibrary.mapBuilderV3.dataObjects.Path path : m.venueData.paths) {
                final int type = path.type;
                if (type == PathType.PATH_TYPE_NORMAL_PATH) continue;
                for (int wpID : path.waypoints) {
                    final Waypoint waypoint = Building.getWaypointsById(m, wpID);
                    final int level = Building.getLevelIndexOfWaypointWithID(m, waypoint.id);
                    final MoverIcon moverIcon;
                    moverIcon = new MoverIcon();
                    moverIcon.setHeadsUp(true);
                    moverIcon.setLevel(level);
                    //moverIcon.getTransform().setScale(0.3f);
                    moverIcon.getTransform().setTranslationX((float) waypoint.x);
                    moverIcon.getTransform().setTranslationY((float) waypoint.y);
                    m.addToMap(moverIcon);
                    size = m.venueData.pathTypes.length;
                    for (int i = 0; i < size; i++) {
                        final PathType pathType = m.venueData.pathTypes[i];
                        if (pathType.pathTypeId == path.type) {
                            IconShape iconShape = m.iconShapeLib.getIcon(pathType.typeName);
                            moverIcon.setShape(iconShape);
                            break;
                        }
                    }
                }
            }
            // reorder movers of route (these show when a route has multiple floors)
            // to be on top of the above icons
            m.removeFromMap(m.moverA);
            m.removeFromMap(m.moverB);
            RenderStyleIcon renderStyleIcon = new RenderStyleIcon();
            renderStyleIcon.renderStyleBG = new RenderStyle(Paint.Style.FILL);
            renderStyleIcon.renderStyleMG = new RenderStyle(Paint.Style.FILL);
            renderStyleIcon.renderStyleFG = new RenderStyle(Paint.Style.FILL);
            renderStyleIcon.renderStyleBG.paintFill.setColor(Color.BLACK);
            renderStyleIcon.renderStyleMG.paintFill.setColor(Color.WHITE);
            renderStyleIcon.renderStyleFG.paintFill.setColor(Color.WHITE);
            m.moverA.setStyleIdle(renderStyleIcon);
            m.moverB.setStyleIdle(renderStyleIcon);
            RenderStyle renderStyleBG = new RenderStyle(Paint.Style.FILL);
            renderStyleBG.paintFill.setColor(Color.parseColor("#B5C0C9"));
            m.cssStyles.setBackground(renderStyleBG, true);
            //<<<<<<<<<<<<<<<
            // todo can show map! set a boolean somewhere to enable map functionality

            Element[] amenityList = m.getElementsByType(com.jibestream.jibestreamandroidlibrary.elements.Amenity.class);
            for (int i = 0; i < amenityList.length; i++) {
                com.jibestream.jibestreamandroidlibrary.elements.Amenity amenity
                        = (com.jibestream.jibestreamandroidlibrary.elements.Amenity) amenityList[i];
                m.removeFromMap(amenity);
                m.addToMap(amenity);
            }

            App.get().setMapEnabled(true);
            sendBroadcast(new Intent("MAP_LOADED_INTENT"));
        }
    };

    private void connectGoogleApiClient() {
        if (Build.VERSION.SDK_INT > 22) {
            int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
                return;
            } else {
                EMSPLocationRegister();
                googleApiClient = getGoogleApiClient();
                googleApiClient.connect();
            }
        } else {
            EMSPLocationRegister();
            googleApiClient = getGoogleApiClient();
            googleApiClient.connect();

        }
    }

    public GoogleApiClient getGoogleApiClient() {
        return new GoogleApiClient.Builder(SplashActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(SplashActivity.this)
                .addOnConnectionFailedListener(SplashActivity.this)
                .build();
    }


    public void EMSPLocationRegister() {
//EMS SDK
        Log.e("EMSP", "SDK setup");
        EMSP.enableLogs = true;
        EMSP.useLocation = true;
        EMSP.addRegistrationListener(new EMSPSDKRegistrationListener() {
            @Override
            public void onDeviceRegistrationSuccess() {
                Log.e("EMSP", "Registered successfully");
                //Toast.makeText(getApplicationContext(), "Device registered successfully", Toast.LENGTH_LONG).show();
                App.EMSP_INITIALIZED = true;
                EMSP.useLocation = true;

                Log.e("EMSP", "EMSPLocationRegister " + App.EMSP_INITIALIZED);
                if (App.EMSP_INITIALIZED) {
                    EMSPLocation emspLocation = EMSP.getLocation();
                    WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                    final WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    Log.e("EMSP", wifiInfo.getSSID());
                    //Toast.makeText(getApplicationContext(), " SSID : " + wifiInfo.getSSID(), Toast.LENGTH_LONG).show();
                    if (!TextUtils.isEmpty(wifiInfo.getSSID())) {
                        String ssid = wifiInfo.getSSID().replace("\"", "");
                        //Toast.makeText(getApplicationContext(), " SSID : " + ssid, Toast.LENGTH_LONG).show();
                        emspLocation.findWiFiLocations(ssid, new EMSPWiFiLocationsListener() {
                            @Override
                            public void onWiFiLocationSuccess(WiFiLocation[] wiFiLocations) {
                                Log.e("EMSP", "WiFiLocation Success " + wiFiLocations.length);
                                String locationId = null;
                                for (WiFiLocation location : wiFiLocations) {
                                    locationId = location.getLocationId();
                                    Log.e("EMSP", locationId);
                                    if (!TextUtils.isEmpty(locationId)) {
                                        //Toast.makeText(getApplicationContext(), "WiFiLocationSuccess " + locationId, Toast.LENGTH_LONG).show();
                                        break;
                                    }
                                }

                                if (!TextUtils.isEmpty(locationId)) {
                                    Log.e("EMSP", "WiFiLocation " + locationId);
                                    Constant.CISCO_LOCATION_TAG = locationId;
                                    App.EMSP_LOCATION_REGISTERED = true;
                                }
                            }

                            @Override
                            public void onWiFiLocationFailure(String s) {
                                App.EMSP_LOCATION_REGISTERED = false;
                                Log.e("EMSP", "WiFiLocation Failure " + s);
                                //Toast.makeText(getApplicationContext(), s + " SSID " + wifiInfo.getSSID(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                }

            }

            @Override
            public void onDeviceRegistrationFailure(StatusHandle.Status status) {
                Log.e("EMSP", "Registered Failure");
            }
        });

        EMSP.register(bearerToken, this);

		/*if ( EMSP.getLocation() != null &&
                ( Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1 ) )
		{*/
        //ClassCastException please confirm with Khaled.
        //try
        //{
        EMSP.getLocation().registerDeviceAtCurrentLocation(this);
            /*}
            catch ( Exception e )
			{
				e.printStackTrace();
			}*/
        //}
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    EMSPLocationRegister();
                    googleApiClient = getGoogleApiClient();
                    googleApiClient.connect();
                } else {
                    // Permission Denied
                    Toast.makeText(SplashActivity.this, "Location Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onStop() {
        //unregisterReceiver(progressReceiver);
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setNumUpdates(1);
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(progressReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        registerDeviceID(null);
        /*if (TextUtils.isEmpty(App.get().getDeviceId())) {
            GcmUtility.getGcmId(this, new GcmUtility.GcmListner() {
                @Override
                public void onGcmRegistered(String gcmKey, int code) {
                    registerDevice(0.0, 0.0, gcmKey);
                }
            }, Constant.GCM_SENDER_ID);
        } else {
            registerDevice(0.0, 0.0, App.get().getDeviceId());
        }*/
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            /*   outside the mall   */
        registerDeviceID(null);

       /* App.get().setInsideTheMall(false);
        if (TextUtils.isEmpty(App.get().getDeviceId())) {
            GcmUtility.getGcmId(this, new GcmUtility.GcmListner() {
                @Override
                public void onGcmRegistered(String gcmKey, int code) {
                    registerDevice(0.0, 0.0, gcmKey);
                }
            }, Constant.GCM_SENDER_ID);
        } else {
            registerDevice(0.0, 0.0, App.get().getDeviceId());
        }*/
    }

    @Override
    public void onLocationChanged(final Location location) {
        //mall location constants 25.118107,55.200608
        //office location constants 19.0243645,72.8442807
        double latDistance = Math.toRadians(location.getLatitude() - (Double.parseDouble(App.get().getSelectedMall().getLatitude())));
        double lngDistance = Math.toRadians(location.getLongitude() - (Double.parseDouble(App.get().getSelectedMall().getLongitude())));

        double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) +
                (Math.cos(Math.toRadians(location.getLatitude()))) *
                        (Math.cos(Math.toRadians(Double.parseDouble(App.get().getSelectedMall().getLatitude())))) *
                        (Math.sin(lngDistance / 2)) *
                        (Math.sin(lngDistance / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = 6371 * c;
        if (dist < 0.3) {
            App.get().setInsideTheMall(true); // inside the mall
        } else {
            App.get().setInsideTheMall(false); // outside the mall
        }

        //Register Device
        registerDeviceID(location);
        /*if (TextUtils.isEmpty(App.get().getDeviceId())) {
            Log.e("EnterRegisterDevice", "call GCM");
            GcmUtility.getGcmId(this, new GcmUtility.GcmListner() {
                @Override
                public void onGcmRegistered(String gcmKey, int code) {
                    registerDevice(location.getLatitude(), location.getLongitude(), gcmKey);
                }
            }, Constant.GCM_SENDER_ID);
        } else {
            Log.e("EnterRegisterDevice", "stored id");
            registerDevice(location.getLatitude(), location.getLongitude(), App.get().getDeviceId());
        }*/


        googleApiClient.disconnect();
    }

    private void registerDeviceID(final Location location) {

        if(!isGCMsent) {
            if (TextUtils.isEmpty(App.get().getDeviceId())) {
                OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                    @Override
                    public void idsAvailable(String userId, String registrationId) {
                        String text = "OneSignal UserID:\n" + userId + "\n\n";

                        Log.e(TAG, "OneSignal " + text);
                        if (registrationId != null) {
                            text += "Google Registration Id:\n" + registrationId;
                            Log.e(TAG, "OneSignal " + text);

                            App.get().setDeviceId(registrationId);

                            if (location != null) {
                                registerDevice(location.getLatitude(), location.getLongitude(), App.get().getDeviceId());
                            } else {
                                registerDevice(0.0, 0.0, App.get().getDeviceId());
                            }
                        } else {
                            text += "Google Registration Id:\nCould not subscribe for push";
                            Log.e(TAG, "OneSignal " + text);
                        }
                    }
                });
            } else {
                Log.e("EnterRegisterDevice", "stored id");
                if (location != null) {
                    registerDevice(location.getLatitude(), location.getLongitude(), App.get().getDeviceId());
                } else {
                    registerDevice(0.0, 0.0, App.get().getDeviceId());
                }

            }
        }
    }

    public void registerDevice(final double latitude, final double longitude, String gcmKey) {
        SOAPRequest pushRequest = new SOAPRequest();
        pushRequest.setBaseUrl(WebApi.BASE_URL);
        pushRequest.setNameSpace(WebApi.NAMESPACE);
        pushRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        pushRequest.setType(NetworkRequest.MethodType.POST);

        pushRequest.setActionName("SetDeviceToken");
        pushRequest.addSoapParameter(new SOAPRequest.Parameter("deviceToken", gcmKey + ""));
        pushRequest.addSoapParameter(new SOAPRequest.Parameter("mallID", App.get().getMallId()));
        pushRequest.addSoapParameter(new SOAPRequest.Parameter("AppType", "android"));
        pushRequest.addSoapParameter(new SOAPRequest.Parameter("latitude", latitude + ""));
        pushRequest.addSoapParameter(new SOAPRequest.Parameter("longitude", longitude + ""));

        new SOAPNetworkJob(pushRequest, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.w("PUSH REGISTRATION", result + "");
                isGCMsent = true;
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
            }

            @Override
            public boolean needAsyncResponse() {
                return true;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }).execute();
    }

    public final void navigateNext() {
        EntityUtils.updateMall(App.get().getSelectedMall());
        if (App.get().getSelectedMall().getFirstLaunch().equals("true")) {
            Malls mall = new Malls();
            mall.setMallName(App.get().getSelectedMall().getMallName());
            mall.setMallUrl(App.get().getSelectedMall().getMallUrl());
            mall.setImageUrl(App.get().getSelectedMall().getImageUrl());
            mall.setId(App.get().getSelectedMall().getIdentifier());
            mall.setMallCOde(App.get().getSelectedMall().getMallCOde());
            mall.setFirstLaunch("false");
            mall.setShoppingCoach("true");

            EntityUtils.updateMall(mall);
            startActivity(new Intent(SplashActivity.this, NotificationCoachActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        } else {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }

        // RegisterDevice
        if (!isGCMsent) {
            registerDeviceID(null);
            /*if (TextUtils.isEmpty(App.get().getDeviceId())) {
                GcmUtility.getGcmId(SplashActivity.this, new GcmUtility.GcmListner() {
                    @Override
                    public void onGcmRegistered(String gcmKey, int code) {
                        registerDevice(0.0, 0.0, gcmKey);
                    }
                }, Constant.GCM_SENDER_ID);
            } else {
                registerDevice(0.0, 0.0, App.get().getDeviceId());
            }*/
        }
        finish();
    }

    @Override
    public void onRegisterSuccess(WiFiLocation wiFiLocation) {

    }

    @Override
    public void onRegisterFailure(StatusHandle.Status status) {

    }

   /* @Override
    public void onRegisterSuccess(WiFiLocation wiFiLocation) {
        Log.e("App","WiFiLocation Success " + wiFiLocation.getLocationId());
        App.EMSP_LOCATION_REGISTERED=true;
        Constant.CISCO_LOCATION_TAG=wiFiLocation.getLocationId();
        navigateNext();
    }

    @Override
    public void onRegisterFailure(StatusHandle.Status status) {
        Log.e("App","WiFiLocation fail");
    }*/
}