package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.BaseService;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by webwerks on 27/6/15.
 */
public class ServiceAdapter extends ArrayAdapter<BaseService> {

    Context mContext;
    List<BaseService> serviceList=null;

    public ServiceAdapter(Context context, List<BaseService> objects) {
        super(context, 0, objects);
        mContext=context;
        serviceList=objects;
    }

    public class ViewHolder{
        ImageView imgServiceIcon;
        TextView lblServiceTitle;
        LinearLayout layout_service;
        View viewVer,viewHor;

        public ViewHolder(View convertView){
            lblServiceTitle= (TextView) convertView.findViewById(R.id.lblServiceTitle);
            imgServiceIcon= (ImageView) convertView.findViewById(R.id.imgServiceIcon);
            layout_service= (LinearLayout) convertView.findViewById(R.id.layout_service);
            viewVer=convertView.findViewById(R.id.view_vertical);
            viewHor=convertView.findViewById(R.id.view_horizontal);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.row_service,null);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }

        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        holder.layout_service.setLayoutParams(new LinearLayout.LayoutParams(display.getWidth() / 3, display.getWidth() / 3));



        holder.lblServiceTitle.setText(getItem(position).getServicePageTitle().toUpperCase());
        if(getItem(position).getServicePageIcon()!=0){
            Picasso.with(mContext).load(getItem(position).getServicePageIcon()).into(holder.imgServiceIcon);
        }

        if((serviceList.size())%3==0 ){
            if(position==serviceList.size()-1){
                holder.viewHor.setVisibility(View.GONE);
                holder.viewVer.setVisibility(View.GONE);
            }else if(position >= serviceList.size()-3){
                holder.viewHor.setVisibility(View.GONE);
                holder.viewVer.setVisibility(View.VISIBLE);
            }else{
                holder.viewHor.setVisibility(View.VISIBLE);
                holder.viewVer.setVisibility(View.VISIBLE);
            }
        }else if((serviceList.size())%3==1 ){
            if(position==serviceList.size()-1){
                holder.viewHor.setVisibility(View.GONE);
                holder.viewVer.setVisibility(View.VISIBLE);
            }else{
                holder.viewHor.setVisibility(View.VISIBLE);
                holder.viewVer.setVisibility(View.VISIBLE);
            }
        } else if((serviceList.size())%3==2 ){
            if(position >= serviceList.size()-2){
                holder.viewHor.setVisibility(View.GONE);
                holder.viewVer.setVisibility(View.VISIBLE);
            }else{
                holder.viewHor.setVisibility(View.VISIBLE);
                holder.viewVer.setVisibility(View.VISIBLE);
            }
        }

        if((position+1)%3==0 && position!=serviceList.size()-1){
            holder.viewHor.setVisibility(View.VISIBLE);
            holder.viewVer.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }
}
