package com.belongi.citycenter.ui.fragment.categories;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SectionIndexer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.android.utilities.Validation;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.ShopCategory;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.emsp.EmspHelper;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.logic.StoreListLogic;
import com.belongi.citycenter.logic.ValidationUtils;
import com.belongi.citycenter.ui.activity.ShopDetailActivity;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.adapters.CategoryAdapter;
import com.belongi.citycenter.ui.fragment.BaseFragment;
import com.belongi.citycenter.ui.view.swipelistview.Attributes;
import com.belongi.citycenter.ui.view.swipelistview.BaseSwipeAdapter;
import com.belongi.citycenter.ui.view.swipelistview.IndexedSwipeListView;
import com.belongi.citycenter.ui.view.swipelistview.SimpleSwipeListener;
import com.belongi.citycenter.ui.view.swipelistview.SwipeLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by webwerks on 7/4/16.
 */
public class StoreListingFragment<T extends Model> extends BaseFragment implements StoreListLogic.StoreListCallback,
        TextWatcher, AdapterView.OnItemSelectedListener, AbsListView.OnScrollListener,
        RadioGroup.OnCheckedChangeListener {

    protected IndexedSwipeListView lstSwipe;
    protected List<T> items;
    protected View root;
    protected CategoryType mType;
    private EditText txtSearch;
    private Spinner spnCategories;
    private Adapter adapter, searchAdapter;
    public static final String FAVOURITE_INTENT = "FAVOURITE_INTENT", FAVOURITED = "FAVOURITED";
    T selectedItem;
    List<T> sortedList;
    RadioButton rbtnShopping, rbtnDining, rbtnEntertainment, rbtnAll;

    private BroadcastReceiver receiverFavourite = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("selected", selectedItem.toString());
            if (selectedItem instanceof ShoppingShop) {
                if (adapter != null) {
                    for (int i = 0; i < adapter.getCount(); i++) {
                        Log.e("Identifier", ((ShoppingShop) adapter.getItem(i)).getIdentifier() + "");
                        if (adapter.getItem(i) instanceof ShoppingShop && ((ShoppingShop) adapter.getItem(i)).getIdentifier().equals(((ShoppingShop) selectedItem).getIdentifier())) {
                            ((ShoppingShop) selectedItem).setFavourited(intent.getBooleanExtra(FAVOURITED, false));
                            break;
                        }
                    }
                }
                if (searchAdapter != null) {
                    for (int i = 0; i < searchAdapter.getCount(); i++) {
                        if (adapter.getItem(i) instanceof ShoppingShop && ((ShoppingShop) searchAdapter.getItem(i)).getIdentifier().equals(((ShoppingShop) selectedItem).getIdentifier())) {
                            ((ShoppingShop) selectedItem).setFavourited(intent.getBooleanExtra(FAVOURITED, false));
                            break;
                        }
                    }
                }
            } else if (selectedItem instanceof DiningShop) {
                if (adapter != null) {
                    for (int i = 0; i < adapter.getCount(); i++) {
                        if (adapter.getItem(i) instanceof DiningShop && ((DiningShop) adapter.getItem(i)).getIdentifier().equals(((DiningShop) selectedItem).getIdentifier())) {
                            ((DiningShop) selectedItem).setFavourited(intent.getBooleanExtra(FAVOURITED, false));
                            break;
                        }
                    }
                }
                if (searchAdapter != null) {
                    for (int i = 0; i < searchAdapter.getCount(); i++) {
                        if (adapter.getItem(i) instanceof DiningShop && ((DiningShop) searchAdapter.getItem(i)).getIdentifier().equals(((DiningShop) selectedItem).getIdentifier())) {
                            ((DiningShop) selectedItem).setFavourited(intent.getBooleanExtra(FAVOURITED, false));
                            break;
                        }
                    }
                }
            } else if (selectedItem instanceof Entertainment) {
                if (adapter != null) {
                    for (int i = 0; i < adapter.getCount(); i++) {
                        if (adapter.getItem(i) instanceof Entertainment && ((Entertainment) adapter.getItem(i)).getIdentifier().equals(((Entertainment) selectedItem).getIdentifier())) {
                            ((Entertainment) selectedItem).setFavourited(intent.getBooleanExtra(FAVOURITED, false));
                            break;
                        }
                    }
                }
                if (searchAdapter != null) {
                    for (int i = 0; i < searchAdapter.getCount(); i++) {
                        if (adapter.getItem(i) instanceof Entertainment && ((Entertainment) searchAdapter.getItem(i)).getIdentifier().equals(((Entertainment) selectedItem).getIdentifier())) {
                            ((Entertainment) selectedItem).setFavourited(intent.getBooleanExtra(FAVOURITED, false));
                            break;
                        }
                    }
                }
            }

            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            if (searchAdapter != null) {
                searchAdapter.notifyDataSetChanged();
            }
        }
    };


    private IntentFilter favouruteFilter = new IntentFilter(FAVOURITE_INTENT);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(receiverFavourite, favouruteFilter);

        if (App.EMSP_INITIALIZED) {
            if (Validation.Network.isConnected(getActivity())) {
                // show General Ads
                EmspHelper.showOverlayAd(getActivity(), "store");

                //  EmspHelper.showFullScreenAd(getActivity(), "store");
                //show Location based Ads
                if (App.EMSP_LOCATION_REGISTERED) {

                    EmspHelper.showLocBasedOverlayAd(getActivity(), "store");
                    //  EmspHelper.showLocBasedFullscreenAd(getActivity(), "store");
                }
            }
        }
    }

    private void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public String getScreenName() {
        return "SHOP & DINE";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiverFavourite);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (radioGroup.getCheckedRadioButtonId() == R.id.rbtnShopping) {
            rbtnShopping.setTextColor(getResources().getColor(R.color.selected_tab_color));
            rbtnDining.setTextColor(getResources().getColor(R.color.white));
            rbtnEntertainment.setTextColor(getResources().getColor(R.color.white));
            rbtnAll.setTextColor(getResources().getColor(R.color.white));

            setType(CategoryType.SHOPPING);
            StoreListLogic.getStoreList(this, mType);
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.rbtnDining) {
            rbtnDining.setTextColor(getResources().getColor(R.color.selected_tab_color));
            rbtnShopping.setTextColor(getResources().getColor(R.color.white));
            rbtnEntertainment.setTextColor(getResources().getColor(R.color.white));
            rbtnAll.setTextColor(getResources().getColor(R.color.white));

            setType(CategoryType.DINING);
            StoreListLogic.getStoreList(this, mType);
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.rbtnEntertainment) {
            rbtnEntertainment.setTextColor(getResources().getColor(R.color.selected_tab_color));
            rbtnDining.setTextColor(getResources().getColor(R.color.white));
            rbtnShopping.setTextColor(getResources().getColor(R.color.white));
            rbtnAll.setTextColor(getResources().getColor(R.color.white));

            setType(CategoryType.ENTERTAINMENT);
            StoreListLogic.getStoreList(this, mType);
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.rbtnAll) {
            rbtnShopping.setTextColor(getResources().getColor(R.color.white));
            rbtnDining.setTextColor(getResources().getColor(R.color.white));
            rbtnEntertainment.setTextColor(getResources().getColor(R.color.white));
            rbtnAll.setTextColor(getResources().getColor(R.color.selected_tab_color));

            setType(CategoryType.ALL);
            StoreListLogic.getStoreList(this, mType);
        }
        txtSearch.setText("");
        checkAndHideKeyboard();
    }

    public void checkAndHideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isAcceptingText()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        List<T> objects = new ArrayList<T>();
        ShopCategory category = (ShopCategory) spnCategories.getItemAtPosition(position);
        switch (mType) {
            case DINING:
                for (T item : items) {
                    if (((DiningShop) item).getCategoryId().equals(category.getID())) {
                        objects.add(item);
                    }
                }
                break;
            case SHOPPING:
                for (T item : items) {
                    if (((ShoppingShop) item).getCategoryId().equals(category.getID())) {
                        objects.add(item);
                    }
                }
                break;
        }
        if (objects.size() > 0) {
            searchAdapter = new Adapter(objects);
            lstSwipe.setAdapter(searchAdapter);
        } else {
            lstSwipe.setAdapter(adapter);
            adapter.setMode(Attributes.Mode.Single);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onStoreListReceived(Object storeList) {
        items = (List<T>) ((List<Object>) storeList).get(0);
        Collections.sort(items, new Comparator<T>() {
            @Override
            public int compare(T lhs, T rhs) {
                return lhs.toString().trim().compareTo(rhs.toString().trim());
                    /*if (lhs instanceof DiningShop && rhs instanceof DiningShop) {
                        return ((DiningShop) lhs).getTitle().compareTo(((DiningShop) rhs).getTitle());
                    } else if (lhs instanceof ShoppingShop && rhs instanceof ShoppingShop) {
                        return ((ShoppingShop) lhs).getShopTitle().compareTo(((ShoppingShop) rhs).getShopTitle());
                    } else if (lhs instanceof Entertainment && rhs instanceof Entertainment) {
                        return ((Entertainment) lhs).getTitle().compareTo(((Entertainment) rhs).getTitle());
                    }
                    return 0;*/
            }
        });

        Set<T> categorySet = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 instanceof ShoppingShop && o2 instanceof ShoppingShop) {
                    if (((ShoppingShop) o1).getShopTitle().equalsIgnoreCase(((ShoppingShop) o2).getShopTitle())) {
                        return 0;
                    }
                } else if (o1 instanceof DiningShop && o2 instanceof DiningShop) {
                    if (((DiningShop) o1).getTitle().equalsIgnoreCase(((DiningShop) o2).getTitle())) {
                        return 0;
                    }
                } else if (o1 instanceof Entertainment && o2 instanceof Entertainment) {
                    if (((Entertainment) o1).getTitle().equalsIgnoreCase(((Entertainment) o2).getTitle())) {
                        return 0;
                    }
                }
                return 1;
            }
        });

        categorySet.addAll(items);
        sortedList = new ArrayList<>(categorySet);

        setListAdapter(sortedList);

        switch (mType) {
            //case DINING:
            case SHOPPING:
                //setActionbarTitle("SHOPPING");
                spnCategories.setVisibility(View.VISIBLE);
                List<ShopCategory> categoryList = new ArrayList<>();
                ShopCategory shop = new ShopCategory();
                shop.setCategoryName("Search by Category");
                categoryList.add(shop);
                categoryList.addAll((List<ShopCategory>) ((List<Object>) storeList).get(1));
                spnCategories.setAdapter(new CategoryAdapter(getActivity(), categoryList));
                break;

            case DINING:
                //setActionbarTitle("DINING");
                spnCategories.setVisibility(View.VISIBLE);
                List<ShopCategory> categoryList1 = new ArrayList<>();
                ShopCategory shop1 = new ShopCategory();
                shop1.setCategoryName("Search by Category");
                categoryList1.add(shop1);
                categoryList1.addAll((List<ShopCategory>) ((List<Object>) storeList).get(1));
                spnCategories.setAdapter(new CategoryAdapter(getActivity(), categoryList1));
                break;

            case ENTERTAINMENT:
                //setActionbarTitle("ENTERTAINMENT");
                spnCategories.setVisibility(View.GONE);
                break;

            case ALL:
                //setActionbarTitle("STORE");
                spnCategories.setVisibility(View.GONE);
                break;
        }

        //}
        spnCategories.setOnItemSelectedListener(this);
    }

    @Override
    public void onEmptyStoreList() {
        Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
    }

    public enum CategoryType {
        SHOPPING, DINING, ENTERTAINMENT, ALL
    }

    public static StoreListingFragment getInstance(CategoryType type) {
        StoreListingFragment frag = new StoreListingFragment();
        frag.setType(type);
        return frag;
    }

    private void setType(CategoryType type) {
        this.mType = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // load listings from database.
        //new LocalDataLoader(this, 0).execute();
        StoreListLogic.getStoreList(this, mType);
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_categories, null);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        root = view;
        root.findViewById(R.id.rg_category).setVisibility(View.VISIBLE);
        lstSwipe = (IndexedSwipeListView) root.findViewById(R.id.lstSwipe);
        // lstSwipe.setSwipeListViewListener(this);
        lstSwipe.setOnScrollListener(this);
        txtSearch = (EditText) root.findViewById(R.id.txtSearchStore);
        txtSearch.addTextChangedListener(this);
        spnCategories = (Spinner) root.findViewById(R.id.spnCategory);
        ((RadioGroup) root.findViewById(R.id.rg_category)).setOnCheckedChangeListener(this);
        rbtnShopping = (RadioButton) root.findViewById(R.id.rbtnShopping);
        rbtnDining = (RadioButton) root.findViewById(R.id.rbtnDining);
        rbtnEntertainment = (RadioButton) root.findViewById(R.id.rbtnEntertainment);
        rbtnAll = (RadioButton) root.findViewById(R.id.rbtnAll);

        switch (mType) {
            case ALL:
                rbtnAll.setChecked(true);
                break;
            case SHOPPING:
                rbtnShopping.setChecked(true);
                break;
            case DINING:
                rbtnDining.setChecked(true);
                break;
            case ENTERTAINMENT:
                rbtnEntertainment.setChecked(true);
        }
        setupUI(view);
    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {
        if (adapter != null) {
            adapter.closeAllItems();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (adapter != null) {
            adapter.closeAllItems();
        }
        // lstSwipe.closeOpenedItems();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (lstSwipe.getAdapter() != null) {
            ((Adapter) lstSwipe.getAdapter()).filter(s.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }


    protected void setListAdapter(List<T> list) {
        adapter = new Adapter(list);
        lstSwipe.setAdapter(adapter);
    }

    public class Adapter extends BaseSwipeAdapter implements SectionIndexer, View.OnClickListener {
        private String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        List<T> itemsList = null;
        List<T> cloneList = null;

        public Adapter() {
            itemsList = items;
            cloneList = new ArrayList<T>(itemsList);
        }

        public Adapter(List<T> itemsObj) {
            itemsList = itemsObj;
            cloneList = new ArrayList<T>(itemsObj);
        }

        @Override
        public int getSwipeLayoutResourceId(int i) {
            return R.id.swipe;
        }

        @Override
        public View generateView(int i, ViewGroup viewGroup) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.row_swipe_list, null);
            SwipeLayout swipeListView = (SwipeLayout) view.findViewById(getSwipeLayoutResourceId(i));
            swipeListView.setLeftSwipeEnabled(false);
            swipeListView.setShowMode(SwipeLayout.ShowMode.PullOut);
            swipeListView.addSwipeListener(new SimpleSwipeListener() {
                @Override
                public void onOpen(SwipeLayout layout) {
                    super.onOpen(layout);

                }
            });
            return view;
        }

        @Override
        public void fillValues(int position, View view) {
            TextView lblTitle = (TextView) view.findViewById(R.id.lblTitle);
            TextView lblLevel = (TextView) view.findViewById(R.id.lblLevel);
            LinearLayout layoutDetail = (LinearLayout) view.findViewById(R.id.layout_detail);

            View locator = view.findViewById(R.id.imgLocate);
            if (!App.get().checkMall())
                ((ImageView) locator).setAlpha(0.5f);
            ImageView favindicator = (ImageView) view.findViewById(R.id.imgFavIncon);
            ImageView swipeImg = (ImageView) view.findViewById(R.id.imgFav);
            View btnFav = view.findViewById(R.id.btnFav);
            View btnCall = view.findViewById(R.id.btnCall);

            btnFav.setOnClickListener(this);
            btnCall.setOnClickListener(this);
            locator.setOnClickListener(this);

            T item = itemsList.get(position);
            layoutDetail.setTag(item);
            btnFav.setTag(item);
            btnCall.setTag(item);

            layoutDetail.setOnClickListener(this);
            favindicator.setOnClickListener(this);
            favindicator.setTag(item);

            if (item instanceof ShoppingShop) {
                locator.setTag(item);
                String level = ((ShoppingShop) item).getFloor();
                if (ValidationUtils.isNotNullOrBlank(level)) {
                    lblLevel.setText(level);
                } else {
                    lblLevel.setText("");
                }
                if (((ShoppingShop) item).isFavourited()) {
                    favindicator.setImageResource(R.drawable.icon_fav_active);
                    swipeImg.setImageResource(R.drawable.icon_fav_white);
                    ((TextView) view.findViewById(R.id.lblFav)).setText("UNFAVOURITE");
                } else {
                    favindicator.setImageResource(R.drawable.icon_fav);
                    swipeImg.setImageResource(R.drawable.icon_fav);
                    ((TextView) view.findViewById(R.id.lblFav)).setText("FAVOURITE");
                }
                lblTitle.setText(((ShoppingShop) item).getShopTitle().toUpperCase());
            } else if (item instanceof DiningShop) {
                locator.setTag(item);
                String level = ((DiningShop) item).getFloor();
                if (ValidationUtils.isNotNullOrBlank(level)) {
                    lblLevel.setText(level);
                } else {
                    lblLevel.setText("");
                }
                lblTitle.setText(((DiningShop) item).getTitle().toUpperCase());
                if (((DiningShop) item).isFavourited()) {
                    swipeImg.setImageResource(R.drawable.icon_fav_white);
                    favindicator.setImageResource(R.drawable.icon_fav_active);
                    ((TextView) view.findViewById(R.id.lblFav)).setText("UNFAVOURITE");
                } else {
                    swipeImg.setImageResource(R.drawable.icon_fav);
                    favindicator.setImageResource(R.drawable.icon_fav);
                    ((TextView) view.findViewById(R.id.lblFav)).setText("FAVOURITE");
                }
            } else if (item instanceof Entertainment) {
                locator.setTag(item);
                String level = ((Entertainment) item).getFloor();
                if (ValidationUtils.isNotNullOrBlank(level)) {
                    lblLevel.setText(level);
                } else {
                    lblLevel.setText("");
                }
                lblTitle.setText(((Entertainment) item).getTitle().toUpperCase());
                if (((Entertainment) item).isFavourited()) {
                    swipeImg.setImageResource(R.drawable.icon_fav_white);
                    favindicator.setImageResource(R.drawable.icon_fav_active);
                    ((TextView) view.findViewById(R.id.lblFav)).setText("UNFAVOURITE");
                } else {
                    swipeImg.setImageResource(R.drawable.icon_fav);
                    favindicator.setImageResource(R.drawable.icon_fav);
                    ((TextView) view.findViewById(R.id.lblFav)).setText("FAVOURITE");
                }
            }
        }

        public void filter(final String s) {
            itemsList.clear();
            if (s.length() > 0) {
                for (T item : cloneList) {
                    if (item instanceof ShoppingShop) {
                        if (((ShoppingShop) item).getShopTitle().toLowerCase().contains(s.toLowerCase())) {
                            itemsList.add(item);
                        }
                    } else if (item instanceof DiningShop) {
                        if (((DiningShop) item).getTitle().toLowerCase().contains(s.toLowerCase())) {
                            itemsList.add(item);
                        }
                    } else if (item instanceof Entertainment) {
                        if (((Entertainment) item).getTitle().toLowerCase().contains(s.toLowerCase())) {
                            itemsList.add(item);
                        }
                    }
                }
            } else {
                itemsList.addAll(cloneList);
            }

            try {
                Collections.sort(itemsList, new Comparator<T>() {
                    @Override
                    public int compare(T s1, T s2) {
                        String key = s.toLowerCase();

                        if (s1.toString().toLowerCase().startsWith(key) && (!s2.toString().toLowerCase().startsWith(key))) {
                            return -1;
                        } else if (s2.toString().toLowerCase().startsWith(key) && (!s1.toString().toLowerCase().startsWith(key))) {
                            return 1;
                        } else {
                            return 0;
                        }

                        /*if(s1 instanceof ShoppingShop && s2 instanceof ShoppingShop){
                            if (((ShoppingShop) s1).getShopTitle().toLowerCase().startsWith(key) && (!((ShoppingShop) s2).getShopTitle().toLowerCase().startsWith(key))) {
                                return -1;
                            } else if (((ShoppingShop) s2).getShopTitle().toLowerCase().startsWith(key) && (!((ShoppingShop) s1).getShopTitle().toLowerCase().startsWith(key))) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }else if(s1 instanceof DiningShop && s2 instanceof DiningShop){
                            if (((DiningShop) s1).getTitle().toLowerCase().startsWith(key) && (!((DiningShop) s2).getTitle().toLowerCase().startsWith(key))) {
                                return -1;
                            } else if (((DiningShop) s2).getTitle().toLowerCase().startsWith(key) && (!((DiningShop) s1).getTitle().toLowerCase().startsWith(key))) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }else if(s1 instanceof Entertainment && s2 instanceof Entertainment){
                            if (((Entertainment) s1).getTitle().toLowerCase().startsWith(key) && (!((Entertainment) s2).getTitle().toLowerCase().startsWith(key))) {
                                return -1;
                            } else if (((Entertainment) s2).getTitle().toLowerCase().startsWith(key) && (!((Entertainment) s1).getTitle().toLowerCase().startsWith(key))) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }*//*else{
                        return 0;
                        *//**//*if (((Entertainment) s1).getTitle().toLowerCase().startsWith(key) && (!((Entertainment) s2).getTitle().toLowerCase().startsWith(key))) {
                            return -1;
                        } else if (((Entertainment) s2).getTitle().toLowerCase().startsWith(key) && (!((Entertainment) s1).getTitle().toLowerCase().startsWith(key))) {
                            return 1;
                        } else {
                            return 0;
                        }*//**//*
                    }*//*
                        return 0;*/
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception ", e.getMessage());
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (itemsList != null) {
                return itemsList.size();
            } else {
                return 0;
            }
        }

        @Override
        public Object getItem(int i) {
            return itemsList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Object[] getSections() {
            String[] sectionsArray = new String[sections.length()];
            for (int i = 0; i < sections.length(); i++)
                sectionsArray[i] = "" + sections.charAt(i);
            return sectionsArray;
        }

        @Override
        public int getPositionForSection(int sectionIndex) {
            for (int i = 0; i < itemsList.size(); i++) {
                String item = itemsList.get(i).toString();
                if (item.charAt(0) == sections.charAt(sectionIndex))
                    return i;
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imgLocate:
                    if (v.getTag() != null) {
                        if (App.get().checkMall()) {
                            Intent destinationIntent = new Intent(getActivity(), WebContentPreviewActivity.class);
                            String title = null, url = null, descId = null;
                            if (v.getTag() instanceof ShoppingShop) {
                                title = "SHOPPING";
                                descId = ((ShoppingShop) v.getTag()).getDestinationID();
                            } else if (v.getTag() instanceof DiningShop) {
                                title = "DINING";
                                descId = ((DiningShop) v.getTag()).getDestination_ID();
                            } else if (v.getTag() instanceof Entertainment) {
                                descId = ((Entertainment) v.getTag()).getDestination_ID();
                                title = "ENTERTAINMENT";
                            }

                            if (ValidationUtils.isNotNullOrBlank(descId)) {
                                url = Constant.STORE_LOCATOR + "?destId=" + descId;
                            } else {
                                url = Constant.STORE_LOCATOR;
                            }
                            destinationIntent.putExtra("title", "STORE LOCATOR");
                            destinationIntent.putExtra("url", url);
                            destinationIntent.putExtra("category", title + "- Locate");
                            getActivity().startActivity(destinationIntent);
                        }
                    }
                    break;

                case R.id.btnCall:
                    adapter.closeAllItems();
                    if (searchAdapter != null) {
                        searchAdapter.closeAllItems();
                    }
                    String number = null;
                    if (v.getTag() instanceof ShoppingShop) {
                        number = ((ShoppingShop) v.getTag()).getShopTelephone();
                    } else if (v.getTag() instanceof DiningShop) {
                        number = ((DiningShop) v.getTag()).getTelephone();
                    } else if (v.getTag() instanceof Entertainment) {
                        number = ((Entertainment) v.getTag()).getEntertainment_Telephone();
                    }
                    if (number != null) {
                        startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + number)));
                        getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    }
                    break;

                case R.id.btnFav:
                    adapter.closeAllItems();
                    if (searchAdapter != null) {
                        searchAdapter.closeAllItems();
                    }
                    final T item = (T) v.getTag();
                    new LocalDataLoader(new LocalDataLoader.QueryClient() {
                        @Override
                        public Object executeQuery() {
                            FavouriteItem favItem = new FavouriteItem();
                            ActiveAndroid.beginTransaction();
                            Long id = 0L;
                            String catId = "";
                            if (item instanceof DiningShop) {
                                if (((DiningShop) item).isFavourited()) {
                                    new Delete().from(FavouriteItem.class)
                                            .where("identifier=?", new String[]{((DiningShop) item).getIdentifier()})
                                            .where("mallid=?", App.get().getMallId()).execute();
                                } else {
                                    catId = ((DiningShop) item).getCategoryId();
                                    favItem.setIdentifier(((DiningShop) item).getIdentifier());
                                    favItem.setType("dining");
                                    favItem.setLabel(((DiningShop) item).getTitle());
                                    favItem.setMallId(App.get().getMallId());
                                    id = favItem.save();
                                }
                            } else if (item instanceof ShoppingShop) {
                                if (((ShoppingShop) item).isFavourited()) {
                                    new Delete().from(FavouriteItem.class)
                                            .where("identifier=?", new String[]{((ShoppingShop) item).getIdentifier()})
                                            .where("mallid=?", App.get().getMallId()).execute();
                                } else {
                                    catId = ((ShoppingShop) item).getCategoryId();
                                    favItem.setIdentifier(((ShoppingShop) item).getIdentifier());
                                    favItem.setType("shopping");
                                    favItem.setLabel(((ShoppingShop) item).getShopTitle());
                                    favItem.setMallId(App.get().getMallId());
                                    id = favItem.save();
                                }
                            } else {
                                if (((Entertainment) item).isFavourited()) {
                                    new Delete().from(FavouriteItem.class)
                                            .where("identifier=?", new String[]{((Entertainment) item).getIdentifier()})
                                            .where("mallid=?", App.get().getMallId()).execute();
                                } else {
                                    catId = ((Entertainment) item).getCategoryId();
                                    favItem.setIdentifier(((Entertainment) item).getIdentifier());
                                    favItem.setType("entertainment");
                                    favItem.setLabel(((Entertainment) item).getTitle());
                                    favItem.setMallId(App.get().getMallId());
                                    id = favItem.save();
                                }
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                            Log.e("FAV ID", id + "");
                            return id;
                        }

                        @Override
                        public void onBackgroundJobComplete(int requestCode, Object result) {
                            notifyDataSetChanged();
                            String catId = "";
                            if (item instanceof DiningShop) {
                                if (((DiningShop) item).isFavourited()) {
                                    ((DiningShop) item).setFavourited(false);
                                    Toast.makeText(getActivity(), "Deleted from favorite", Toast.LENGTH_SHORT).show();
                                } else {
                                    ((DiningShop) item).setFavourited(true);
                                    Toast.makeText(getActivity(), "Added to favorite", Toast.LENGTH_SHORT).show();
                                }
                            } else if (item instanceof ShoppingShop) {
                                if (((ShoppingShop) item).isFavourited()) {
                                    ((ShoppingShop) item).setFavourited(false);
                                    Toast.makeText(getActivity(), "Deleted from favorite", Toast.LENGTH_SHORT).show();
                                } else {
                                    ((ShoppingShop) item).setFavourited(true);
                                    Toast.makeText(getActivity(), "Added to favorite", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (((Entertainment) item).isFavourited()) {
                                    ((Entertainment) item).setFavourited(false);
                                    Toast.makeText(getActivity(), "Deleted from favorite", Toast.LENGTH_SHORT).show();
                                } else {
                                    ((Entertainment) item).setFavourited(true);
                                    Toast.makeText(getActivity(), "Added to favorite", Toast.LENGTH_SHORT).show();
                                }
                            }
                            if (item instanceof DiningShop) {
                                if (!((DiningShop) item).isFavourited()) {
                                    //  new Delete().from(FavouriteItem.class).where("identifier=?",new String[]{((DiningShop)item).getIdentifier()}).execute();
                                } else {
                                    catId = ((DiningShop) item).getCategoryId();
                                }
                            } else if (item instanceof ShoppingShop) {
                                if (!((ShoppingShop) item).isFavourited()) {
                                    //   new Delete().from(FavouriteItem.class).where("identifier=?",new String[]{((ShoppingShop)item).getIdentifier()}).execute();
                                } else {
                                    catId = ((ShoppingShop) item).getCategoryId();
                                }
                            } else {
                                if (!((Entertainment) item).isFavourited()) {
                                    //  new Delete().from(FavouriteItem.class).where("identifier=?",new String[]{((Entertainment)item).getIdentifier()}).execute();
                                } else {
                                    catId = ((Entertainment) item).getCategoryId();
                                }
                            }
                            if (ValidationUtils.isNotNullOrBlank(catId)) {
                                WebApi.updateUserHit(catId);
                            }
                        }

                        @Override
                        public void onBackgroundJobAbort(int requestCode, Object reason) {
                        }

                        @Override
                        public void onBackgroundJobError(int requestCode, Object error) {
                        }

                        @Override
                        public boolean needAsyncResponse() {
                            return true;
                        }

                        @Override
                        public boolean needResponse() {
                            return true;
                        }
                    }, 0).execute();
                    break;


                case R.id.layout_detail:
                    selectedItem = (T) v.getTag();
                    T obj = (T) v.getTag();
                    Intent detailIntent = new Intent(getActivity(), ShopDetailActivity.class);
                    detailIntent.putExtra(Constant.DETAIL_SHOP_TYPE, (Serializable) obj);
                    getActivity().startActivity(detailIntent);
                    getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    break;
            }
        }
    }
}
