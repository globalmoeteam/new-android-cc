package com.belongi.citycenter.ui.activity.spinwin;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 22/6/15.
 */
public class PlayContestActivity extends BaseActivity implements View.OnClickListener{

    ImageView imgSpinWheel;
    ObjectAnimator rotateImg;
    boolean stop=false;
    Campaigns.Compaign selectedContest;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_play_contest);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
        imgSpinWheel= (ImageView)findViewById(R.id.imgSpinWheel);
        imgSpinWheel.setOnClickListener(this);
        TextView lblContestName= (TextView)findViewById(R.id.lblContestName);
        //selectedContest= (Campaigns.Compaign) getIntent().getSerializableExtra(Constant.SELECTED_CONTEST);
        selectedContest= App.get().getSelectedContest();
        lblContestName.setText(selectedContest.getCampaignTitle());
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.imgSpinWheel:
                if(!stop){
                    rotateImg = android.animation.ObjectAnimator.ofFloat(imgSpinWheel, "rotation", 0.0f, 9360.0f);
                    rotateImg.setRepeatMode(Animation.INFINITE);
                    rotateImg.setDuration(40000);
                    rotateImg.setRepeatCount(Animation.INFINITE);
                    rotateImg.setInterpolator(new LinearInterpolator());
                    rotateImg.start();
                    stop=true;
                }else{
                    rotateImg.cancel();
                    startActivity(new Intent(this, QuestionActivity.class));
                    //.putExtra(Constant.SELECTED_CONTEST, selectedContest));
                    finish();
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                }
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN - Play";
    }
}
