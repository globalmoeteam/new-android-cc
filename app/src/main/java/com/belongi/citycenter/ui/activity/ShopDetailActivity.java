package com.belongi.citycenter.ui.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.android.utilities.DateTimeUtil;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.FeaturedProduct;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.logic.ShopDetailLogic;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.PDFRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.adapters.DetailImagesAdapter;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;
import com.belongi.citycenter.ui.fragment.categories.BaseCategoryListFragment;
import com.belongi.citycenter.ui.view.ScrollViewParax;
import com.belongi.citycenter.ui.view.viewpagerindicator.CirclePageIndicator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by yashesh on 6/27/2015.
 */
public class ShopDetailActivity extends BaseActivity implements ShopDetailLogic.ShopDetailsCallbacks, View.OnClickListener, CompoundButton.OnCheckedChangeListener, ScrollViewParax.ParaxScroll {

    ViewPager pager;
    ShopDetailLogic logic;
    View.OnClickListener offerClickListener;
    Model dataModel;
    LinearLayout lltoolbar, lltoolbarParx;
    Toolbar toolbar;
    int[] locationPerm = new int[2];
    TextView lblTitle;
    CirclePageIndicator indicator;

    List<String> images = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        logic = new ShopDetailLogic(getIntent().getSerializableExtra(Constant.DETAIL_SHOP_TYPE), this);
        //ActivityTransition.with(getIntent()).to(findViewById(R.id.pagerDetail)).start(arg0);
    }

    @Override
    public String getScreenName() {
        Model model = (Model) getIntent().getSerializableExtra(Constant.DETAIL_SHOP_TYPE);
        if (model instanceof ShoppingShop) {
            return "Shopping - " + ((ShoppingShop) model).getShopTitle();
        } else if (model instanceof DiningShop) {
            return "Dining - " + ((DiningShop) model).getTitle();
        } else if (model instanceof Entertainment) {
            return "Entertainment - " + ((Entertainment) model).getTitle();
        } else if (model instanceof Offer) {
            return "offer - " + ((Offer) model).getTitle();
        } else if (model instanceof Hotel) {
            return "Hotel - " + ((Hotel) model).getHotel_Name();
        } else if (model instanceof Event) {
            return "Event - " + ((Event) model).getTitle();
        } else {
            return Constant.DONT_SEND;
        }
    }

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_shop_detail);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();

        pager = (ViewPager) findViewById(R.id.pagerDetail);
        findViewById(R.id.btnCall).setOnClickListener(this);
        findViewById(R.id.btnLocate).setOnClickListener(this);
        if (!App.get().checkMall())
            ((ImageView) findViewById(R.id.btnLocate)).setAlpha(0.5f);
        findViewById(R.id.btnShare).setOnClickListener(this);
        findViewById(R.id.btnCallPrax).setOnClickListener(this);
        findViewById(R.id.btnLocatePrax).setOnClickListener(this);
        findViewById(R.id.btnSharePrax).setOnClickListener(this);

        lblTitle = (TextView) findViewById(R.id.lblShopTitle);
        lltoolbar = (LinearLayout) findViewById(R.id.layoutToolbar);
        lltoolbarParx = (LinearLayout) findViewById(R.id.layoutToolbarPrax);
        lltoolbarParx.getLocationOnScreen(locationPerm);

        toolbar = (Toolbar) findViewById(R.id.actionToolbar);
        toolbar.getMeasuredHeight();

        ScrollViewParax scrollView = (ScrollViewParax) findViewById(R.id.parax);
        scrollView.setListner(this);
        dataModel = (Model) getIntent().getSerializableExtra(Constant.DETAIL_SHOP_TYPE);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        if (dataModel instanceof ShoppingShop) {
            setActionbarTitle("SHOPPING");
            lblTitle.setText(Html.fromHtml(((ShoppingShop) dataModel).getShopTitle().toUpperCase()));
            Log.e("title", ((ShoppingShop) dataModel).getShopTitle());
            ((CheckBox) findViewById(R.id.btnFav)).setChecked(((ShoppingShop) dataModel).isFavourited());
            ((CheckBox) findViewById(R.id.btnFavPrax)).setChecked(((ShoppingShop) dataModel).isFavourited());
        } else if (dataModel instanceof DiningShop) {
            setActionbarTitle("DINING");
            lblTitle.setText(Html.fromHtml(((DiningShop) dataModel).getTitle().toUpperCase()));
            ((CheckBox) findViewById(R.id.btnFav)).setChecked(((DiningShop) dataModel).isFavourited());
            ((CheckBox) findViewById(R.id.btnFavPrax)).setChecked(((DiningShop) dataModel).isFavourited());
        } else if (dataModel instanceof Event) {
            setActionbarTitle("EVENTS");
            findViewById(R.id.btnSearch).setVisibility(View.GONE);
            findViewById(R.id.btnShareEvent).setVisibility(View.VISIBLE);
            findViewById(R.id.btnShareEvent).setOnClickListener(this);
            lblTitle.setText(Html.fromHtml(((Event) dataModel).getTitle().toUpperCase()));
            findViewById(R.id.lblDate).setVisibility(View.VISIBLE);
            if (((Event) dataModel).getEvent_Start_Date() != null && ((Event) dataModel).getEvent_End_Date() != null) {
                ((TextView) findViewById(R.id.lblDate)).setText(
                        DateTimeUtil.dateFormat(((Event) dataModel).getEvent_Start_Date(), "dd  MMM yyyy", "dd/MM/yyyy") + " - " +
                                DateTimeUtil.dateFormat(((Event) dataModel).getEvent_End_Date(), "dd  MMM yyyy", "dd/MM/yyyy"));
            } else {
                ((TextView) findViewById(R.id.lblDate)).setVisibility(View.GONE);
            }
            lltoolbar.setVisibility(View.GONE);
        } else if (dataModel instanceof Offer) {
            setActionbarTitle("OFFERS");
            lblTitle.setText(Html.fromHtml(((Offer) dataModel).getTitle().toUpperCase()));
            findViewById(R.id.lblDate).setVisibility(View.VISIBLE);
            if (((Offer) dataModel).getStart_Date() != null && ((Offer) dataModel).getEnd_Date() != null) {
                ((TextView) findViewById(R.id.lblDate)).setText(
                        DateTimeUtil.dateFormat(((Offer) dataModel).getStart_Date(), "dd  MMM yyyy", "dd/MM/yyyy") + " - " +
                                DateTimeUtil.dateFormat(((Offer) dataModel).getEnd_Date(), "dd  MMM yyyy", "dd/MM/yyyy"));

            } else {
                ((TextView) findViewById(R.id.lblDate)).setVisibility(View.GONE);
            }
            findViewById(R.id.btnCall).setVisibility(View.GONE);
            findViewById(R.id.viewCall).setVisibility(View.GONE);
            findViewById(R.id.viewFav).setVisibility(View.GONE);
            findViewById(R.id.llFav).setVisibility(View.GONE);
        } else if (dataModel instanceof Hotel) {
            setActionbarTitle("HOTELS");
            lblTitle.setText(Html.fromHtml(((Hotel) dataModel).getHotel_Name().toUpperCase()));
            List<FavouriteItem> selection = new Select().from(FavouriteItem.class).where("identifier = ?", new String[]{((Hotel) dataModel).getIdentifier()}).execute();
            if (selection.size() > 0) {
                ((Hotel) dataModel).setIsFavourited(true);
            } else {
                ((Hotel) dataModel).setIsFavourited(false);
            }
            ((CheckBox) findViewById(R.id.btnFav)).setChecked(((Hotel) dataModel).isFavourited());
            ((CheckBox) findViewById(R.id.btnFavPrax)).setChecked(((Hotel) dataModel).isFavourited());
        } else {
            setActionbarTitle("ENTERTAINMENT");
            lblTitle.setText(Html.fromHtml(((Entertainment) dataModel).getTitle().toUpperCase()));
            ((CheckBox) findViewById(R.id.btnFav)).setChecked(((Entertainment) dataModel).isFavourited());
            ((CheckBox) findViewById(R.id.btnFavPrax)).setChecked(((Entertainment) dataModel).isFavourited());
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((CheckBox) findViewById(R.id.btnFav)).setOnCheckedChangeListener(ShopDetailActivity.this);
                ((CheckBox) findViewById(R.id.btnFavPrax)).setOnCheckedChangeListener(ShopDetailActivity.this);
            }
        }, 100);
    }

    @Override
    public void setImages(List<String> images) {
        this.images.addAll(images);
        pager.setAdapter(new DetailImagesAdapter(this, this.images));
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);
    }

    WebView wvDescription;

    public static final String[] DOWNLOAD_PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    public void setDescription(String description) {

        Log.e("FormatDesc", description);

        String desc = description.replaceAll("&lt;", "<").
                replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                replaceAll("&amp;", "&").replaceAll("width=\"600\"", "width=\"100%\"");

        String demo = "<p><a href=" + "http://www.citycentremallbeirut.com/-/media/ccbeirut/events/maf_22735giftcardparticipatingshops.pdf?la=en" + ">Visit W3Schools.com!</a></p?";

        wvDescription = (WebView) findViewById(R.id.wvDesc);
        wvDescription.setVisibility(View.VISIBLE);
        wvDescription.getSettings().setJavaScriptEnabled(true);
        wvDescription.loadDataWithBaseURL("file:///android_asset/", getStyleString(desc), "text/html", "UTF-8", null);

        wvDescription.setBackgroundColor(0x00000000);
        wvDescription.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        wvDescription.setBackgroundColor(Color.argb(1, 0, 0, 0));
        wvDescription.setScrollContainer(false);
        wvDescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return (motionEvent.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        wvDescription.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                //download file using web browser
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        wvDescription.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, final String url) {

                if (url.contains("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                    startActivity(intent);
                } else if (url.contains("mailto:")) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                    emailIntent.setType("text/plain");
                    //emailIntent.putExtra(Intent.EXTRA_EMAIL,);
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } else {
               startActivity(new Intent(ShopDetailActivity.this, WebContentPreviewActivity.class)
                            .putExtra("url", url)
                            .putExtra("title", lblTitle.getText().toString())
                            .putExtra("category", Constant.DONT_SEND));
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);



                }
                return true;
            }
        });


    }

    @Override
    public void setOfferList(List<Offer> offers) {
        LinearLayout offersHolder = (LinearLayout) findViewById(R.id.layoutOffers);

        offerClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Offer offer = (Offer) v.getTag();
                Intent intent = new Intent(ShopDetailActivity.this, ShopDetailActivity.class);
                intent.putExtra(Constant.DETAIL_SHOP_TYPE, offer);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }
        };

       /* lblTitle.setText("OFFERS");
        lblTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        lblTitle.setTextSize(15);*/

        lblTitle.setVisibility(View.GONE);

        View view = LayoutInflater.from(this).inflate(R.layout.row_text, null);
        TextView lbl = (TextView) view.findViewById(R.id.lblText);
        lbl.setText("OFFERS");
        lbl.setGravity(Gravity.CENTER_HORIZONTAL);
        lbl.setTextColor(getResources().getColor(R.color.text_color));
        lbl.setTextSize(15);

        offersHolder.addView(view);

        for (Offer offer : offers) {
            View offerView = LayoutInflater.from(this).inflate(R.layout.row_offer_item, null);

            View divider = new View(this);
            divider.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5));

            offersHolder.addView(divider);

            final ImageView imgOfffer = (ImageView) offerView.findViewById(R.id.imgOffer);
            Picasso.with(this).load(WebApi.BASE_HOAST_URL + offer.getImage_URL())
                    .placeholder(R.drawable.place_holder_event1_6p).into(imgOfffer);
            (((TextView) offerView.findViewById(R.id.lblOfferTitle))).setText(Html.fromHtml(offer.getTitle().toUpperCase()));
            if (offer.getStart_Date() != null && offer.getEnd_Date() != null) {
                ((TextView) offerView.findViewById(R.id.lblOfferDate)).setText(DateTimeUtil.dateFormat(offer.getStart_Date(), "dd  MMM yyyy", "dd/MM/yyyy") + " - " +
                        DateTimeUtil.dateFormat(offer.getEnd_Date(), "dd  MMM yyyy", "dd/MM/yyyy"));
            } else {
                ((TextView) offerView.findViewById(R.id.lblOfferDate)).setVisibility(View.GONE);
            }

            // ((TextView) offerView.findViewById(R.id.lblOfferDesc)).setText(Html.fromHtml(offer.getDescription()));
            offerView.setTag(offer);
            offerView.setOnClickListener(offerClickListener);
            offersHolder.addView(offerView);
        }
    }

    PDFRequest pdfRequest = null;

    @Override
    public void setFeaturedProducts(List<FeaturedProduct> featuredProducts) {
        LinearLayout offersHolder = (LinearLayout) findViewById(R.id.layoutOffers);
        lblTitle.setVisibility(View.GONE);

        View view = LayoutInflater.from(this).inflate(R.layout.row_text, null);
        TextView lbl = (TextView) view.findViewById(R.id.lblText);
        lbl.setText("FEATURED PRODUCTS");
        lbl.setGravity(Gravity.CENTER_HORIZONTAL);
        lbl.setTextSize(15);
        lbl.setTextColor(getResources().getColor(R.color.text_color));
        offersHolder.addView(view);
        for (final FeaturedProduct offer : featuredProducts) {

            View offerView = LayoutInflater.from(this).inflate(R.layout.row_offer_item, null);

            View divider = new View(this);
            divider.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5));
            final ImageView img = (ImageView) offerView.findViewById(R.id.imgOffer);
            Picasso.with(this).load(WebApi.BASE_HOAST_URL + offer.getThumbnail())
                    .placeholder(R.drawable.place_holder_event1_6p).into(img);
            (((TextView) offerView.findViewById(R.id.lblOfferTitle))).setText(Html.fromHtml(offer.getTitle().replaceAll("&lt;", "<").
                    replaceAll("&quot;", "\"").replaceAll("&gt;", ">").
                    replaceAll("&amp;", "&").toUpperCase()));
            // ((TextView) offerView.findViewById(R.id.lblOfferDesc)).setText(Html.fromHtml(offer.getBrief_Description()));
            offersHolder.addView(offerView);
            offersHolder.addView(divider);

            offersHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = offer.getProduct_PDF();
                    if (url != null) {
                        if (url.startsWith("http")) {
                            // do nothing
                        } else {
                            url = WebApi.BASE_HOAST_URL + offer.getProduct_PDF();
                        }
                    }
                    showProgressLoading();

                    pdfRequest = new PDFRequest(new BackgroundJobClient() {
                        @Override
                        public void onBackgroundJobComplete(int requestCode, Object result) {
                            Log.w("RESULT", result + "");
                            stopLoading();
                            if (result != null) {
                                Intent viewIntent = new Intent(ShopDetailActivity.this, PDFViewActivity.class);
                                viewIntent.putExtra(Constant.PDF_FILE, result.toString());
                                startActivity(viewIntent);
                                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                            }
                        }

                        @Override
                        public void onBackgroundJobAbort(int requestCode, Object reason) {
                            Log.w("REASON", reason + "");
                        }

                        @Override
                        public void onBackgroundJobError(int requestCode, Object error) {
                            Log.w("ERROR", error + "");
                        }

                        @Override
                        public boolean needAsyncResponse() {
                            return true;
                        }

                        @Override
                        public boolean needResponse() {
                            return true;
                        }
                    }, new NetworkRequest.Builder(NetworkRequest.MethodType.GET, url, 0).build());
                    pdfRequest.execute();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (pdfRequest != null) {
            pdfRequest.closeConnection();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnCall:
            case R.id.btnCallPrax:
                logic.call();
                break;

            case R.id.btnLocate:
            case R.id.btnLocatePrax:

                Log.e("SHOP DETAIL",App.get().checkMall() + ":::::");

                if (App.get().checkMall()) {
                    logic.locate();
                }

                break;

            case R.id.btnShare:
            case R.id.btnSharePrax:
            case R.id.btnShareEvent:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
                    } else {
                        logic.share(ShopDetailActivity.this, (ImageView) ((LinearLayout) pager.getChildAt(0)).getChildAt(0));
                    }
                } else {
                    logic.share(ShopDetailActivity.this, (ImageView) ((LinearLayout) pager.getChildAt(0)).getChildAt(0));
                }

                //logic.share(ShopDetailActivity.this, (ImageView) ((LinearLayout) pager.getChildAt(0)).getChildAt(0));

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 10) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                logic.share(ShopDetailActivity.this, (ImageView) ((LinearLayout) pager.getChildAt(0)).getChildAt(0));
            } else {
                Toast.makeText(this, "Permission Denied !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.btnFav:
                if (!(((CheckBox) findViewById(R.id.btnFavPrax)).isChecked() == isChecked)) {
                    ((CheckBox) findViewById(R.id.btnFavPrax)).setOnCheckedChangeListener(null);
                    ((CheckBox) findViewById(R.id.btnFavPrax)).setChecked(isChecked);
                    ((CheckBox) findViewById(R.id.btnFavPrax)).setOnCheckedChangeListener(ShopDetailActivity.this);
                }

                break;
            case R.id.btnFavPrax:
                if (!(((CheckBox) findViewById(R.id.btnFav)).isChecked() == isChecked)) {
                    ((CheckBox) findViewById(R.id.btnFav)).setOnCheckedChangeListener(null);
                    ((CheckBox) findViewById(R.id.btnFav)).setChecked(isChecked);
                    ((CheckBox) findViewById(R.id.btnFav)).setOnCheckedChangeListener(ShopDetailActivity.this);
                }
                break;
        }
        Intent intent = new Intent(BaseCategoryListFragment.FAVOURITE_INTENT);
        intent.putExtra(BaseCategoryListFragment.FAVOURITED, isChecked);
        sendBroadcast(intent);
        logic.favourite(isChecked);
    }

    Animation fadeIn, fadeOut;

    @Override
    public void onParaxScroll() {
        indicator.setVisibility(View.GONE);
        int[] locationPager = new int[2];
        pager.getLocationOnScreen(locationPager);

        int[] location = new int[2];
        lltoolbar.getLocationOnScreen(location);

        // Initialize the fadeIn animation
        fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(1000);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                indicator.setVisibility(View.VISIBLE);
            }
        });

        // Initialize the fadeOut animation
        fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1000);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                indicator.setVisibility(View.GONE);
            }
        });

        if (pager.getHeight() + indicator.getHeight() <= location[1]) {
            indicator.startAnimation(fadeIn);
        } else {
            indicator.startAnimation(fadeOut);
        }

        if (dataModel instanceof Offer) {
            findViewById(R.id.btnCallPrax).setVisibility(View.GONE);
            findViewById(R.id.viewCallPrax).setVisibility(View.GONE);
            findViewById(R.id.viewFavPrax).setVisibility(View.GONE);
            findViewById(R.id.llFavPrax).setVisibility(View.GONE);
        }

        if (dataModel instanceof Event) {
        } else {
            if (location[1] <= toolbar.getHeight()) {
                lltoolbarParx.setVisibility(View.VISIBLE);
               /* FrameLayout.LayoutParams params= (FrameLayout.LayoutParams) wvDescription.getLayoutParams();
                params.setMargins(0,toolbar.getHeight(),0,0);
                wvDescription.setLayoutParams(params);*/
            } else {
                lltoolbarParx.setVisibility(View.GONE);
            }
        }
    }


    /*

    else if (url.endsWith(".pdf")) {


                    PermissionUtils.requestIfNotGranted(ShopDetailActivity.this, CAMERA_PERMISSIONS, 0, new PermissionUtils.PermissionCallback() {
                        @Override
                        public void onPermissionResult(int requestCode, boolean granted) {

                            if (granted) {
                                Uri source = Uri.parse(url);
                                // Make a new request pointing to the .apk url
                                DownloadManager.Request request = new DownloadManager.Request(source);
                                // appears the same in Notification bar while downloading
                                request.setDescription("Description for the DownloadManager Bar");
                                request.setTitle("YourApp.apk");
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    request.allowScanningByMediaScanner();
                                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                }
                                // save the file in the "Downloads" folder of SDCARD
                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "big_buck_bunny_720p_1mb.mp4");
                                // get download service and enqueue file
                                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                manager.enqueue(request);
                            }
                        }
                    });


                }
     */
}