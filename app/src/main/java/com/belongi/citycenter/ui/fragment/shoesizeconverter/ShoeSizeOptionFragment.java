package com.belongi.citycenter.ui.fragment.shoesizeconverter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.belongi.citycenter.ui.activity.utility.ShoeSizeConverterActivity;
import com.belongi.citycenter.ui.fragment.BaseFragment;

/**
 * Created by webwerks on 15/6/15.
 */
public class ShoeSizeOptionFragment extends BaseFragment implements View.OnClickListener {


    TextView btnMen,btnWomen,btnKids;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shoe_size_option,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("SHOE SIZE CONVERTER");
        btnMen= (TextView) view.findViewById(R.id.btnMen);
        btnWomen= (TextView) view.findViewById(R.id.btnWomen);
        btnKids= (TextView) view.findViewById(R.id.btnKids);

        btnMen.setOnClickListener(this);
        btnWomen.setOnClickListener(this);
        btnKids.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Animation animTopLeft= AnimationUtils.loadAnimation(getActivity(),R.anim.slide_top_left);
        Animation animTopRight=AnimationUtils.loadAnimation(getActivity(),R.anim.slide_top_right);
        Animation animSlideBottom=AnimationUtils.loadAnimation(getActivity(),R.anim.slide_bottom);
        btnMen.startAnimation(animTopLeft);
        btnWomen.startAnimation(animTopRight);
        btnKids.startAnimation(animSlideBottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMen :
                ((ShoeSizeConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        ShoeSizeConverterFragment.newInstance("Men"),R.id.container, true);

                break;
            case R.id.btnWomen :
                ((ShoeSizeConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        ShoeSizeConverterFragment.newInstance("Women"),R.id.container, true);
                break;
            case R.id.btnKids :
                ((ShoeSizeConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        ShoeSizeConverterFragment.newInstance("Children"),R.id.container, true);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Utilities - Shoe Size Converter";
    }
}
