package com.belongi.citycenter.ui.fragment.fashionconverter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.utilities.ui.base._Activity;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.activity.utility.FashionConverterActivity;
import com.belongi.citycenter.ui.fragment.BaseFragment;

/**
 * Created by yashesh on 15/6/15.
 */
public class FashionOptionFragment extends BaseFragment implements View.OnClickListener {

    TextView btnMen,btnWomen,btnKids;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fashion_option,null);
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionbarTitle("FASHION CONVERTER");
        btnMen= (TextView) view.findViewById(R.id.btnMen);
        btnWomen= (TextView) view.findViewById(R.id.btnWomen);
        btnKids= (TextView) view.findViewById(R.id.btnKids);

        btnMen.setOnClickListener(this);
        btnWomen.setOnClickListener(this);
        btnKids.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Animation animTopLeft= AnimationUtils.loadAnimation(getActivity(), R.anim.slide_top_left);
        Animation animTopRight=AnimationUtils.loadAnimation(getActivity(),R.anim.slide_top_right);
        Animation animSlideBottom=AnimationUtils.loadAnimation(getActivity(),R.anim.slide_bottom);
        btnMen.startAnimation(animTopLeft);
        btnWomen.startAnimation(animTopRight);
        btnKids.startAnimation(animSlideBottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMen :
                ((FashionConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        FashionTypeFragment.newInstance("Men"),R.id.container, true);

                break;
            case R.id.btnWomen :
                ((FashionConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        FashionTypeFragment.newInstance("Women"),R.id.container, true);
                break;
            case R.id.btnKids :
                ((FashionConverterActivity)getActivity()).fragmentTransaction(_Activity.REPLACE_FRAGMENT,
                        FashionTypeFragment.newInstance("Children"),R.id.container, true);
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Utilities - Fashion Converter";
    }
}
