package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Search;

import java.util.List;

/**
 * Created by yashesh on 6/18/2015.
 */
public class SearchResultsAdapter extends ArrayAdapter<Search>{

    Context mContext;
    public SearchResultsAdapter(Context context,List<Search> objects) {
        super(context, R.layout.row_search_result, objects);
        mContext=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.row_search_result,null);
        }
        Search search=getItem(position);
        TextView lbl= (TextView) convertView.findViewById(R.id.lblSearchResult);
        lbl.setText(Html.fromHtml(search.getLabel()));
        int icon=0;
        if(getItem(position).getCategory().equals("HOTEL")) {
            icon=R.drawable.icon_search_hotel;
            lbl.setCompoundDrawablePadding(20);
        }else if(getItem(position).getCategory().equals("DINING")){
            icon=R.drawable.icon_search_dine;
            lbl.setCompoundDrawablePadding(20);
        }else if(getItem(position).getCategory().equals("SHOPPING")){
            icon=R.drawable.icon_search_shopping;
            lbl.setCompoundDrawablePadding(30);
        }else if(getItem(position).getCategory().equals("EVENT")){
            icon=R.drawable.icon_search_event;
            lbl.setCompoundDrawablePadding(20);
        }else if(getItem(position).getCategory().equals("OFFER")){
            icon=R.drawable.icon_search_offers;
            lbl.setCompoundDrawablePadding(20);
        }else if(getItem(position).getCategory().equals("ENTERTAINMENT")) {
            icon = R.drawable.icon_search_entertainment;
            lbl.setCompoundDrawablePadding(20);
        }else if(getItem(position).getCategory().equals("SERVICE")){
            icon=R.drawable.icon_search_service;
            lbl.setCompoundDrawablePadding(20);
        }else if(getItem(position).getCategory().equals("ATM")){
            icon=R.drawable.icon_search_atm;
            lbl.setCompoundDrawablePadding(20);
        }
        lbl.setCompoundDrawablesWithIntrinsicBounds(icon,0,0,0);

        return convertView;
    }
}
