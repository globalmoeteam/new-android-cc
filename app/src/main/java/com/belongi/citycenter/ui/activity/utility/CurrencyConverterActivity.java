package com.belongi.citycenter.ui.activity.utility;


import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.utilities.datareader.DataReader;
import com.belongi.citycenter.R;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.network.NetworkResponse;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.adapters.CurrencyConverterAdapter;
import com.belongi.citycenter.logic.CurrencyConverterLogic;
import com.belongi.citycenter.model.CurrencyConverter;
import com.belongi.citycenter.ui.activity.BaseActivity;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;


/**
 * Created by webwerks on 10/4/15.
 */
public class CurrencyConverterActivity extends BaseActivity implements AdapterView.OnItemSelectedListener,
        BackgroundJobClient,View.OnClickListener,TextWatcher,TextView.OnEditorActionListener{

    Spinner spnrConvertFrom,spnrConvertTo;
    EditText txtConvertFrom;
    TextView txtConvertTo;
    List<String> lstCurrency = null;
    String selectedFromCurrencyCode,selectedToCurrencyCode;
    String selectedFromCurrency,selectedToCurrency;
    boolean fromSwap=false;
    int count=0;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_currency_converter);
    }

    CurrencyConverterAdapter currencyConverterAdapter=null;

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("CURRENCY CONVERTER");

        spnrConvertFrom=(Spinner)findViewById(R.id.spnrConvertFrom);
        spnrConvertTo=(Spinner)findViewById(R.id.spnrConvertTo);
        txtConvertFrom=(EditText)findViewById(R.id.txtConvertFron);
        txtConvertTo=(TextView)findViewById(R.id.txtConvertTo);

        spnrConvertFrom.setOnItemSelectedListener(this);
        spnrConvertTo.setOnItemSelectedListener(this);

        if(lstCurrency == null){
            lstCurrency = CurrencyConverterLogic.getAvailableCurrencies();
            currencyConverterAdapter=new CurrencyConverterAdapter(CurrencyConverterActivity.this, lstCurrency);
        }

        new Thread(){
            @Override
            public void run() {
                super.run();
                Message msg=new Message();
                msg.setData(CurrencyConverterLogic.getBundle(lstCurrency));
                handler.sendMessage(msg);
            }
        }.start();

        findViewById(R.id.btnSwap).setOnClickListener(this);
        txtConvertFrom.addTextChangedListener(this);
        txtConvertFrom.setOnEditorActionListener(this);
    }

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            spnrConvertFrom.setAdapter(currencyConverterAdapter);
            spnrConvertTo.setAdapter(currencyConverterAdapter);

            spnrConvertFrom.setSelection(msg.getData().getInt("From"));
            spnrConvertTo.setSelection(msg.getData().getInt("To"));
        }
    };


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.equals(spnrConvertFrom)) {
            selectedFromCurrency=(String) parent.getSelectedItem();
            selectedFromCurrencyCode = ((String) parent.getSelectedItem()).substring(((String) parent.getSelectedItem()).indexOf("(") + 1, ((String) parent.getSelectedItem()).indexOf(")"));
            if(fromSwap){
                count++;
            }
        } else {
            selectedToCurrency=(String) parent.getSelectedItem();
            selectedToCurrencyCode = ((String) parent.getSelectedItem()).substring(((String) parent.getSelectedItem()).indexOf("(") + 1, ((String) parent.getSelectedItem()).indexOf(")"));
            if(fromSwap){
                count++;
            }
        }

        if(txtConvertFrom.getText().length()>0) {
            if (selectedFromCurrencyCode != null && selectedToCurrencyCode != null) {
                if (fromSwap) {
                    if (count == 2) {
                        count = 0;
                        fromSwap = false;
                        CurrencyConverterLogic.requestCurrencyRate(this, selectedFromCurrencyCode, selectedToCurrencyCode);
                        showProgressLoading();
                    }
                } else {
                    CurrencyConverterLogic.requestCurrencyRate(this, selectedFromCurrencyCode, selectedToCurrencyCode);
                    showProgressLoading();
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                break;
        }
        return true;
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        stopLoading();
        Log.e("CurrencyConverter",((NetworkResponse) result).getResponseString());

        Object response=new Gson().fromJson(((NetworkResponse) result).getResponseString(),CurrencyConverter.class);
        if(result!=null){
            CurrencyConverter convertedData= (CurrencyConverter) response;
            if(txtConvertFrom.getText().length()>0) {

                DecimalFormat decimalFormat = new DecimalFormat("#,###.####");
                DecimalFormatSymbols dfs = new DecimalFormatSymbols();
                dfs.setDecimalSeparator('.');
                dfs.setGroupingSeparator(',');
                decimalFormat.setDecimalFormatSymbols(dfs);

                double rate=Double.parseDouble(convertedData.getQueryRes().getResults().getRow().getRate());
                double value=Double.parseDouble(txtConvertFrom.getText().toString());

                txtConvertTo.setText(decimalFormat.format(rate * value));
            }else{
                txtConvertTo.setText("");
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btnSwap:
                spnrConvertFrom.setSelection(lstCurrency.indexOf(selectedToCurrency));
                spnrConvertTo.setSelection(lstCurrency.indexOf(selectedFromCurrency));
                fromSwap=true;
                break;
        }
    }

    @Override
    public String getScreenName() {
        return "Utilities - Currency Converter";
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        //showLoading();
        CurrencyConverterLogic.requestCurrencyRate(this, selectedFromCurrencyCode, selectedToCurrencyCode);
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if(actionId== EditorInfo.IME_ACTION_DONE){
            CurrencyConverterLogic.requestCurrencyRate(this, selectedFromCurrencyCode, selectedToCurrencyCode);
            showProgressLoading();
        }
        return false;
    }
}
