package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;

import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class ContestAdapter extends ArrayAdapter<Campaigns.Compaign> {

    Context mContext;

    public ContestAdapter(Context context, List<Campaigns.Compaign> objects) {
        super(context, 0, objects);
        mContext=context;
    }

    class ViewHolder{
        //TextView lblT
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1,null);
        TextView lblTitle= (TextView) convertView.findViewById(android.R.id.text1);
        lblTitle.setText(getItem(position).getCampaignTitle());
        lblTitle.setTextColor(mContext.getResources().getColor(R.color.text_color));
        lblTitle.setTextSize(14);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1,null);
        TextView lblTitle= (TextView) convertView.findViewById(android.R.id.text1);
        lblTitle.setText(getItem(position).getCampaignTitle());
        lblTitle.setTextSize(14);
        return convertView;
    }
}
