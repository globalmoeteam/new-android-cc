package com.belongi.citycenter.ui.activity.spinwin;

import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.data.entities.DrawDatesAndPastWinner;
import com.belongi.citycenter.data.entities.PastWinners;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.ui.adapters.DrawDateAndPastWinnerAdapter;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 17/6/15.
 */
public class DateOfDrawAndWinnerActivity extends BaseActivity implements BackgroundJobClient{

    ListView lstContestResult;

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_dateofdraw_winner);
    }

    @Override
    protected void initializeUi() {
        super.initializeUi();
        setActionbarTitle("SPIN & WIN");
        lstContestResult= (ListView) findViewById(R.id.lstContestResult);
        TextView lblHeader= (TextView) findViewById(R.id.lblHeader);
        int typeClicked=getIntent().getIntExtra(Constant.TYPE_CLICKED,0);
        //String campaignId=((Campaigns.Compaign)getIntent().getSerializableExtra(Constant.SELECTED_CONTEST)).getCompaignId();
        String campaignId= App.get().getSelectedContest().getCompaignId();
        if(typeClicked==Constant.DATE_OF_DRAW){
            lblHeader.setText("Dates of Draw");
            SpinAndWinLogic.getDrawDates(this,campaignId);
        }else if(typeClicked==Constant.PAST_WINNERS){
            lblHeader.setText("Past Winners");
            SpinAndWinLogic.getPastWinners(this, campaignId);
        }
    }

    @Override
    public String getScreenName() {
        return "Spin & WIN";
    }

    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {

        Log.e("DateOfDrawAndWinner", result.toString());

        if (result != null) {
            switch (requestCode) {
                case SpinAndWinLogic.REQ_DRAW_DATES:
                   /* List<DrawDatesAndPastWinner> listRecords = new ArrayList<>();
                    listRecords = SpinAndWinLogic.getDrawDates(result.toString());*/
                    lstContestResult.setAdapter(new DrawDateAndPastWinnerAdapter(DateOfDrawAndWinnerActivity.this,
                            SpinAndWinLogic.getDrawDates(result.toString()), Constant.DATE_OF_DRAW));
                    break;

                case SpinAndWinLogic.REQ_PAST_WINNERS:
                    PastWinners pastWinners= (PastWinners) ContentDownloader.getObjectFromJson(result.toString(), PastWinners.class);
                    lstContestResult.setAdapter(new DrawDateAndPastWinnerAdapter(DateOfDrawAndWinnerActivity.this,
                            pastWinners.getWinnerLists(),Constant.PAST_WINNERS));
                    break;
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {

    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {

    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !isFinishing();
    }
}
