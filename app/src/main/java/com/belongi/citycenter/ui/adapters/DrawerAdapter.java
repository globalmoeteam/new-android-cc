package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DrawerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 2/7/15.
 */
public class DrawerAdapter extends ArrayAdapter<DrawerItem> {

    Context mContext;

    public DrawerAdapter(Context context , List<DrawerItem> objects) {
        super(context, 0, objects);
        mContext=context;
    }

    class ViewHolder{

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.row_drawer,null);
        }

        return convertView;
    }
}
