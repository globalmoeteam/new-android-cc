package com.belongi.citycenter.ui.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.ui.activity.BaseActivity;

/**
 * Created by webwerks on 23/6/15.
 */
public class PreviewWebContentFragment extends BaseFragment implements View.OnClickListener {

    WebView wvLinkPreview;

    public static PreviewWebContentFragment getInstance(String url, String title, String category) {
        PreviewWebContentFragment frag = new PreviewWebContentFragment();
        Bundle data = new Bundle();
        data.putString("url", url);
        data.putString("title", title);
        data.putString("category", category);
        frag.setArguments(data);

        Log.e("url_taxi", url);

        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_preview_web_content, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        wvLinkPreview = (WebView) view.findViewById(R.id.wvLinkPreview);
        setActionbarTitle(getArguments().getString("title"));
        WebSettings settings = wvLinkPreview.getSettings();
        String url = getArguments().getString("url");
        if (url != null) {
            url = url.trim();
        }
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        settings.setDisplayZoomControls(true);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setSaveFormData(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setJavaScriptEnabled(true);

        if (getArguments().getString("title").toLowerCase().equals("know more")) {
            /*view.findViewById(R.id.layout_nav_controls).setVisibility(View.GONE);
            wvLinkPreview.loadDataWithBaseURL("file:///android_asset/",getArguments().getString("url") , "text/html", "UTF-8", null);
*/
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            Log.e("Preview", url);
            view.findViewById(R.id.layout_nav_controls).setVisibility(View.VISIBLE);
            wvLinkPreview.loadUrl(url);
        } else {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            Log.e("Preview", url);
            view.findViewById(R.id.layout_nav_controls).setVisibility(View.VISIBLE);
            wvLinkPreview.loadUrl(url);
        }

        wvLinkPreview.setDownloadListener(new DownloadListener()
        {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength)
            {
                //download file using web browser
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                getActivity().finish();

            }
        });

        wvLinkPreview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                    startActivity(intent);
                } else if (url.contains("mailto:")) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse(url.replace("file:///android_asset/%22", "")));
                    emailIntent.setType("text/plain");
                    //emailIntent.putExtra(Intent.EXTRA_EMAIL,);
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } else {
                    view.loadUrl(url);
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (isAdded() && isVisible()) {
                    ((BaseActivity) getActivity()).stopLoading();
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (isAdded() && isVisible()) {
                    ((BaseActivity) getActivity()).stopLoading();
                    ((BaseActivity) getActivity()).showProgressLoading();
                }
            }
        });


        wvLinkPreview.setWebChromeClient(new WebChromeClient());
        view.findViewById(R.id.btnBack).setOnClickListener(this);
        view.findViewById(R.id.btnFwd).setOnClickListener(this);
        view.findViewById(R.id.btnRefresh).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                if (wvLinkPreview.canGoBack()) {
                    wvLinkPreview.goBack();
                }
                break;

            case R.id.btnFwd:
                if (wvLinkPreview.canGoForward()) {
                    wvLinkPreview.goForward();
                }
                break;

            case R.id.btnRefresh:
                wvLinkPreview.reload();
                break;
        }
    }

    @Override
    public String getScreenName() {
        if (isAdded() && isVisible()) {
            return getArguments().getString("category");
        } else {
            return Constant.DONT_SEND;
        }
    }
}
