package com.belongi.citycenter.ui.view.residemenu;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.global.PermissionUtils;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.activity.HomeActivity;
import com.belongi.citycenter.ui.activity.MallSelectionActivity;
import com.belongi.citycenter.ui.fragment.ContactUsFragment;
import com.belongi.citycenter.ui.fragment.EventListingFragment;
import com.belongi.citycenter.ui.fragment.GiftCardFragment;
import com.belongi.citycenter.ui.fragment.HotelListFragment;
import com.belongi.citycenter.ui.fragment.MyParkingFragment;
import com.belongi.citycenter.ui.fragment.OfferListingFragment;
import com.belongi.citycenter.ui.fragment.PreviewWebContentFragment;
import com.belongi.citycenter.ui.fragment.ServiceFragment;
import com.belongi.citycenter.ui.fragment.SettingsFragment;
import com.belongi.citycenter.ui.fragment.SpinToWinFragment;
import com.belongi.citycenter.ui.fragment.StoreLocatorNewFragment;
import com.belongi.citycenter.ui.fragment.TheMallFragment;
import com.belongi.citycenter.ui.fragment.UtilitiesFragment;
import com.belongi.citycenter.ui.fragment.categories.StoreListingFragment;
import com.belongi.citycenter.ui.fragment.home.InsideMallFragment;
import com.belongi.citycenter.ui.fragment.home.OutsidemallFragment;
import com.belongi.citycenter.ui.fragment.home.WhatsAppDeskFragment;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by webwerks on 17/6/15.
 */
public class ResideMenuManager implements View.OnClickListener, BackgroundJobClient {

    private static ResideMenuManager instance = null;
    private HomeActivity mActivity;
    PermissionUtils.PermissionProcessor permissionProcessor;
    public static int req_shopping = 60;

    TextView btnSetting, btnChangeMall;
    LinearLayout itemWhatsApp;

    ResideMenuItem itemSetting, itemUtilities, itemSpinToWin, itemThemall, itemSocialWall,
    /*itemShopping,itemDining,itemEntertainment,*/
            itemService, itemGiftCard, itemHotels, itemEvents, itemOffers, itemStoreLocator,
            itemHome, itemGiftVoucher, itemStore, itemContactUs,iteamMallPromotions;

    ResideMenu resideMenu;
    ResideMenuItem itemParking, itemBookTaxi;

    public static ResideMenuManager getInstance() {
        if (instance == null) {
            instance = new ResideMenuManager();
        }
        return instance;
    }

    public ResideMenu setResideMenu(HomeActivity activity, PermissionUtils.PermissionProcessor processor) {
        mActivity = activity;
        permissionProcessor = processor;
        resideMenu = new ResideMenu(activity);
        resideMenu.setBackground(R.drawable.sidebar_bg);
        resideMenu.attachToActivity(activity);
        //resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.5f);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

        LinearLayout layout = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.bottom_menus, null);//new LinearLayout(mActivity);
        btnSetting = (TextView) layout.findViewById(R.id.text_settings);
        btnChangeMall = (TextView) layout.findViewById(R.id.text_changemall);
        itemWhatsApp = (LinearLayout) layout.findViewById(R.id.layout_whatsapp);

        btnSetting.setOnClickListener(this);
        btnChangeMall.setOnClickListener(this);
        itemWhatsApp.setOnClickListener(this);

        itemHome = new ResideMenuItem(activity, 0, "HOME");
        itemStore = new ResideMenuItem(activity, 0, "SHOP & DINE");
        itemUtilities = new ResideMenuItem(activity, 0, "UTILITIES");
        itemSpinToWin = new ResideMenuItem(activity, 0, "SPIN & WIN");
        itemThemall = new ResideMenuItem(activity, 0, "THE MALL");
        /*itemShopping=new ResideMenuItem(activity, 0, "SHOPPING");
        itemDining=new ResideMenuItem(activity, 0, "DINING");
        itemEntertainment=new ResideMenuItem(activity, 0, "ENTERTAINMENT");*/
        itemParking = new ResideMenuItem(activity, 0, "MY PARKING");
        itemBookTaxi = new ResideMenuItem(activity, 0, "CALL A TAXI");
        itemService = new ResideMenuItem(activity, 0, "SERVICES");
        itemGiftCard = new ResideMenuItem(activity, 0, "GIFT CARDS");
        itemGiftVoucher = new ResideMenuItem(activity, 0, "GIFT VOUCHER");
        itemHotels = new ResideMenuItem(activity, 0, "HOTELS");
        itemEvents = new ResideMenuItem(activity, 0, "EVENTS");
        itemOffers = new ResideMenuItem(activity, 0, "OFFERS");
        itemSocialWall = new ResideMenuItem(activity, 0, "SOCIAL WALL");
        itemStoreLocator = new ResideMenuItem(activity, 0, "STORE LOCATOR");
        itemContactUs = new ResideMenuItem(activity, 0, "CONTACT US");
        //itemSetting = new ResideMenuItem(activity,/* R.drawable.icon_settings*/0, "SETTINGS");
        iteamMallPromotions = new ResideMenuItem(activity, R.drawable.ic_new, mActivity.getResources().getString(R.string.mall_promotions_section));

        itemHome.setOnClickListener(this);
        itemStore.setOnClickListener(this);
       /*itemShopping.setOnClickListener(this);
        itemDining.setOnClickListener(this);
        itemEntertainment.setOnClickListener(this);*/
        //itemSetting.setOnClickListener(this);
        itemUtilities.setOnClickListener(this);
        itemSpinToWin.setOnClickListener(this);
        itemSpinToWin.setVisibility(View.GONE);
        itemThemall.setOnClickListener(this);
        itemParking.setOnClickListener(this);
        itemBookTaxi.setOnClickListener(this);
        itemService.setOnClickListener(this);
        itemGiftCard.setOnClickListener(this);
        itemHotels.setOnClickListener(this);
        itemEvents.setOnClickListener(this);
        itemOffers.setOnClickListener(this);
        itemSocialWall.setOnClickListener(this);
        itemStoreLocator.setOnClickListener(this);
        //itemChangeMall.setOnClickListener(this);
        itemContactUs.setOnClickListener(this);
        iteamMallPromotions.setOnClickListener(this);

        switch (App.get().getSelectedMall().getIdentifier()) {
            case DEIRA:
                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
               /* resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemHotels, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStoreLocator, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemGiftCard, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                if (App.get().isHasCampaigns())
                    resideMenu.addMenuItem(itemSpinToWin, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                if (Constant.BOOKATAXI != null) {
                    if (!Constant.BOOKATAXI.equals("NOTACTIVE")) {
                        resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                    }
                } else {
                    resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                }
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);

                break;
            case BAHRAIN:

                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
               /* resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemHotels, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStoreLocator, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemGiftCard, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                if (App.get().isHasCampaigns())
                    resideMenu.addMenuItem(itemSpinToWin, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);

                break;

            case MIRDIF:
            case AJMAN:
            case FUJAIRAH:
            case SHARJAH:
                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
               /* resideMenu.addMenuItemhanks for Registration(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemStoreLocator, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemGiftCard, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                if (App.get().isHasCampaigns())
                    resideMenu.addMenuItem(itemSpinToWin, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                if (Constant.BOOKATAXI != null) {
                    if (!Constant.BOOKATAXI.equals("NOTACTIVE")) {
                        resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                    }
                } else {
                    resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                }
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);
                break;

            case MUSCAT:
            case QURUM:
                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
               /* resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemStoreLocator, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                if (App.get().isHasCampaigns())
                    resideMenu.addMenuItem(itemSpinToWin, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                if (Constant.BOOKATAXI != null) {
                    if (!Constant.BOOKATAXI.equals("NOTACTIVE")) {
                        resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                    }
                } else {
                    resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                }
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);
                break;
            case ALEXANDRIA:
            case MAADI:
                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
               /* resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemStoreLocator, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                if (App.get().isHasCampaigns())
                    resideMenu.addMenuItem(itemSpinToWin, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);
                break;
            case BEIRUT:

                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
               /* resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemStoreLocator, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemGiftCard, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                if (App.get().isHasCampaigns())
                    resideMenu.addMenuItem(itemSpinToWin, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);


                break;

            case MEAISEM:
                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
              /*  resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemGiftCard, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                if (Constant.BOOKATAXI != null) {
                    if (!Constant.BOOKATAXI.equals("NOTACTIVE")) {
                        resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                    }
                } else {
                    resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                }
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);
                break;

            case SHINDAGHA:
                resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemStore, ResideMenu.DIRECTION_LEFT);
                /*resideMenu.addMenuItem(itemShopping, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemDining, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEntertainment,ResideMenu.DIRECTION_LEFT);*/
                resideMenu.addMenuItem(itemOffers, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(iteamMallPromotions, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemThemall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemGiftCard, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemSocialWall, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemUtilities, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemParking, ResideMenu.DIRECTION_LEFT);
                if (Constant.BOOKATAXI != null) {
                    if (!Constant.BOOKATAXI.equals("NOTACTIVE")) {
                        resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                    }
                } else {
                    resideMenu.addMenuItem(itemBookTaxi, ResideMenu.DIRECTION_LEFT);
                }
                resideMenu.addMenuItem(itemService, ResideMenu.DIRECTION_LEFT);
                resideMenu.addMenuItem(itemContactUs, ResideMenu.DIRECTION_LEFT);
                break;

        }

        //resideMenu.addMenuItem(itemSetting, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(layout, ResideMenu.DIRECTION_LEFT);

        //Check if Compaigns are available..
        SpinAndWinLogic.getCampaigns(ResideMenuManager.this);

        return resideMenu;
    }

    @Override
    public void onClick(final View view) {
        if (view == itemHome) {
            mActivity.setActionbarTitle(App.get().getSelectedMall().getMallName());
            Fragment fragment = null;
            if (App.get().isInsideTheMall()) {
                fragment = new InsideMallFragment();
            } else {
                fragment = new OutsidemallFragment();
            }
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, fragment, R.id.container, false);
        } /*else if (view == itemSetting) {
            mActivity.setActionbarTitle("SETTINGS");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new SettingsFragment(), R.id.container, false);
        }*/ else if (view == itemUtilities) {
            mActivity.setActionbarTitle("UTILITIES");
            //mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, UtilitiesFragment.newInstance(permissionProcessor), R.id.container, false);
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new UtilitiesFragment(), R.id.container, false);

        } else if (view == itemSpinToWin) {
            //mActivity.showProgressLoading();
            //SpinAndWinLogic.getCampaigns(ResideMenuManager.this);

            mActivity.setActionbarTitle("SPIN & WIN");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new SpinToWinFragment(), R.id.container, false);

        } else if (view == itemThemall) {
            mActivity.setActionbarTitle("THE MALL");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new TheMallFragment(), R.id.container, false);
        }/*else if(view == itemDining){
            mActivity.setActionbarTitle("DINING");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.DINING),R.id.container,false);
        } else if(view == itemShopping){
            if(App.get().getSelectedMall().getShoppingCoach().equals("true")){
                Malls mall=new Malls();
                mall.setMallName(App.get().getSelectedMall().getMallName());
                mall.setMallUrl(App.get().getSelectedMall().getMallUrl());
                mall.setImageUrl(App.get().getSelectedMall().getImageUrl());
                mall.setId(App.get().getSelectedMall().getIdentifier());
                mall.setMallCOde(App.get().getSelectedMall().getMallCOde());
                mall.setFirstLaunch(App.get().getSelectedMall().getFirstLaunch());
                mall.setShoppingCoach("false");
                EntityUtils.updateMall(mall);
                mActivity.startActivityForResult(new Intent(mActivity, ShoppingCoachActivity.class),req_shopping);
                mActivity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }else{
                mActivity.setActionbarTitle("SHOPPING");
                mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.SHOPPING),R.id.container,false);
            }
        }else if(view == itemEntertainment){
            mActivity.setActionbarTitle("ENTERTAINMENT");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, BaseCategoryListFragment.CategoryListFragmentFactory.getInstance(BaseCategoryListFragment.CategoryType.ENTERTAINMENT),R.id.container,false);
        }*/ else if (view == itemParking) {
            mActivity.setActionbarTitle("MY PARKING");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new MyParkingFragment(), R.id.container, false);
        } else if (view == itemBookTaxi) {
            String url = null;
            if (TextUtils.isEmpty(Constant.BOOKATAXI)) {
                url = App.get().getSelectedMall().getImageUrl();
            } else {
                url = Constant.BOOKATAXI;
            }
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance(url, "CALL A TAXI", "Call A Taxi"),
                    R.id.container, false);
        } else if (view == itemService) {
            mActivity.setActionbarTitle("SERVICES");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new ServiceFragment(), R.id.container, false);
        } else if (view == itemGiftCard) {
            mActivity.setActionbarTitle("MALL GIFT CARD");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new GiftCardFragment(), R.id.container, false);
        } else if (view == itemHotels) {
            mActivity.setActionbarTitle("HOTELS");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new HotelListFragment(), R.id.container, false);
        } else if (view == itemEvents) {
            mActivity.setActionbarTitle("EVENTS");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new EventListingFragment(), R.id.container, false);
        } else if (view == itemOffers) {
            mActivity.setActionbarTitle("OFFERS");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new OfferListingFragment(), R.id.container, false);
        }  else if (view == iteamMallPromotions) {
            String title = mActivity.getResources().getString(R.string.title_mall_promotions);
            mActivity.setActionbarTitle(title);
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance(App.get().getSelectedMall().getMallPromotionUrl(), title, title),
                    R.id.container, false);
        }else if (view == itemSocialWall) {
            mActivity.setActionbarTitle("SOCIAL WALL");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance(Constant.SOCIAL_WALL, "SOCIAL WALL", "Social Wall"),
                    R.id.container, false);
        } else if (view == itemStoreLocator) {

            switch (App.get().getSelectedMall().getIdentifier()) {
                case SHINDAGHA:
                case MEAISEM:
                    mActivity.setActionbarTitle("STORE LOCATOR");
                    mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, PreviewWebContentFragment.getInstance(Constant.STORE_LOCATOR, "STORE LOCATOR", "Store Locator"),
                            R.id.container, false);
                    break;

                default:

//                    if (App.get().isMapEnabled()) {
                    mActivity.setActionbarTitle("STORE LOCATOR");
                    mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new StoreLocatorNewFragment(), R.id.container, false);
//                    } else {
//                        Toast.makeText(mActivity, "Map not available", Toast.LENGTH_SHORT).show();
//                    }
                    break;
            }
        } else if (view == itemGiftVoucher) {

        } else if (view.getTag() != null && view.getTag().toString().equals("change mall")) {
            try {
                mActivity.startActivity(new Intent(mActivity, MallSelectionActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                mActivity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            } catch (Exception e) {
                Log.e("MallChangeException", "MallChangeException " + e);
            }

        } else if (view == itemStore) {
            mActivity.setActionbarTitle("SHOP & DINE");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, StoreListingFragment.getInstance(StoreListingFragment.CategoryType.ALL), R.id.container, false);
        } else if (view == itemContactUs) {
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new ContactUsFragment(), R.id.container, false);
        } else if (view == itemWhatsApp) {
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new WhatsAppDeskFragment(), R.id.container, false);
        } else if (view.equals(btnSetting)) {
            mActivity.setActionbarTitle("SETTINGS");
            mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new SettingsFragment(), R.id.container, false);
        } else if (view.equals(btnChangeMall)) {
            try {
                mActivity.startActivity(new Intent(mActivity, MallSelectionActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                mActivity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            } catch (Exception e) {
                Log.e("MallChangeException", "MallChangeException " + e);
            }
        }

        if (view instanceof ResideMenuItem) {
            new Thread() {
                @Override
                public void run() {
                    App.get().getTracker().setScreenName("MENU_" + ((ResideMenuItem) view).getTitle());
                    // Send a screen view.
                    App.get().getTracker().send(new HitBuilders.ScreenViewBuilder().build());
                }
            }.start();
        }

        resideMenu.closeMenu();
    }


    @Override
    public void onBackgroundJobComplete(int requestCode, Object result) {
        //mActivity.stopLoading();
        if (result != null) {
            Campaigns compaigns = (Campaigns) ContentDownloader.getObjectFromJson(result.toString(), Campaigns.class);
            if (compaigns.getListCompagins() != null && compaigns.getListCompagins().size() > 0) {
                App.get().setCompaignList(compaigns.getListCompagins());
                //mActivity.fragmentTransaction(mActivity.REPLACE_FRAGMENT, new SpinToWinFragment(), R.id.container, false);
                itemSpinToWin.setVisibility(View.VISIBLE);
            } else {
                //Toast.makeText(mActivity, "No Campaigns available at this time.", Toast.LENGTH_SHORT).show();
                itemSpinToWin.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackgroundJobAbort(int requestCode, Object reason) {
        // Toast.makeText(mActivity,"Enter Abort",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackgroundJobError(int requestCode, Object error) {
        mActivity.stopLoading();
        //Toast.makeText(mActivity, "No campaigns available in the selected Mall.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean needAsyncResponse() {
        return true;
    }

    @Override
    public boolean needResponse() {
        return !mActivity.isFinishing();
    }
}
