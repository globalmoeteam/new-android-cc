package com.belongi.citycenter.ui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belongi.citycenter.R;

import java.util.List;

/**
 * Created by webwerks on 10/4/15.
 */
public class CurrencyConverterAdapter extends ArrayAdapter<String>{

    Context context;

    public CurrencyConverterAdapter(Context context, List<String> objects) {
        super(context, 0, objects);
        this.context=context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView= LayoutInflater.from(context).inflate(R.layout.row_text,null);
        TextView lblCountryCurrency=(TextView)convertView.findViewById(R.id.lblText);
        lblCountryCurrency.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        lblCountryCurrency.setGravity(Gravity.CENTER);
        lblCountryCurrency.setTypeface(null, Typeface.BOLD);
        lblCountryCurrency.setText(getItem(position).substring(getItem(position).indexOf(" (")+2,getItem(position).indexOf(")")));
        lblCountryCurrency.setTextColor(context.getResources().getColor(R.color.white));
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        convertView= LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,null);
        TextView lblCountryCurrency=(TextView)convertView.findViewById(android.R.id.text1);
        lblCountryCurrency.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        lblCountryCurrency.setText(getItem(position));

        final TextView lblCurrency=lblCountryCurrency;
        lblCountryCurrency.post(new Runnable() {
            @Override
            public void run() {
                lblCurrency.setSingleLine(false);
            }
        });
        return convertView;
    }
}
