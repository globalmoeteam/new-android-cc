package com.belongi.citycenter.network;

import android.util.Log;

import com.belongi.citycenter.threading.BackgroundJob;
import com.belongi.citycenter.threading.BackgroundJobClient;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by yashesh on 6/11/2015.
 */
public class SOAPNetworkJob extends BackgroundJob {
    SOAPRequest request;

    public SOAPNetworkJob(SOAPRequest request,BackgroundJobClient client) {
        super(client);
        this.request=request;
    }

    @Override
    public void run() {
        callSOAPWebService();
    }


    private boolean callSOAPWebService() {


        OutputStream out = null;
        int respCode = -1;
        boolean isSuccess = false;
        URL url = null;
        HttpURLConnection httpURLConnection = null;

        try {
            url = new URL(request.getBaseUrl());
            Log.e("URL CONNECT",url + "::::"+ url.getProtocol());

            httpURLConnection = (HttpURLConnection) url.openConnection();
            do {

                httpURLConnection.setRequestMethod(request.getType().toString());
                httpURLConnection.setRequestProperty("Connection", "keep-alive");
                httpURLConnection.setRequestProperty("Content-Type", request.getContentType().toString());
                httpURLConnection.setRequestProperty("SendChunked", "True");
                httpURLConnection.setRequestProperty("UseCookieContainer", "True");
                HttpURLConnection.setFollowRedirects(false);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setUseCaches(true);
                httpURLConnection.setRequestProperty("Content-length", getReqData(request).length + "");
                httpURLConnection.setReadTimeout(60 * 1000);
                httpURLConnection.setConnectTimeout(60 * 1000);
                httpURLConnection.connect();
                out = httpURLConnection.getOutputStream();
                if (out != null) {
                    out.write(getReqData(request));
                    out.flush();
                }
                if (httpURLConnection != null) {
                    respCode = httpURLConnection.getResponseCode();
                    Log.d("respCode", ":" + respCode);
                }
            } while (respCode == -1);

            InputStream responce=null;
            // If it works fine
            if (respCode == 200) {
                String str="";
                try {
                    responce = httpURLConnection.getInputStream();
                    str=convertStreamToString(responce);
                    XmlPullParser parser=    XmlPullParserFactory.newInstance().newPullParser();
                    parser.setInput(new StringReader(str));
                    int eventType = parser.getEventType();
                    boolean nextRequired=true;
                    while(nextRequired && (eventType!=XmlPullParser.END_DOCUMENT)){
                        switch (eventType) {
                            case XmlPullParser.START_TAG:
                                String tagname = parser.getName();
                                if (tagname.equalsIgnoreCase("soap:Body")) {
                                    // next tag is  response body.
                                    parser.nextTag();
                                    parser.nextTag();
                                    String response="";
                                    response= parser.nextText();
                                    //Log.e("NEXT TEXT",(response= parser.nextText()));
                                    notifyCompletion(request.getRequestCode(), response);
                                    nextRequired=false;
                                }
                                break;
                        }
                        eventType=parser.next();
                    }
                }
                catch (XmlPullParserException xEx){
                    xEx.printStackTrace();
                    notifyCompletion(request.getRequestCode(),str);
                }
                catch (Exception e1) {
                    e1.printStackTrace();
                    Log.d("ERROR",e1.toString());
                    notifyError(request.getRequestCode(),"");
                }
            } else {
                isSuccess = false;
                //Log.e("ERROR", convertStreamToString(httpURLConnection.getErrorStream()));
                notifyError(request.getRequestCode(),"");
            }
        } catch (IOException e) {
            e.printStackTrace();
            notifyError(request.getRequestCode(),"");
        }
        catch(Exception e) {
            e.printStackTrace();
            notifyError(request.getRequestCode(),"");
        }finally {
            if (out != null) {
                out = null;
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
                httpURLConnection = null;
            }
        }
        return isSuccess;
    }

    public  String createSoapHeader() {
        String soapHeader = null;
        soapHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<soap:Envelope "
                + "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" + ">";
        return soapHeader;
    }

    public  byte[] getReqData(SOAPRequest request) {
        StringBuilder requestData = new StringBuilder();
        requestData.append(createSoapHeader());
        String params=createSoapParameters(request);
        requestData.append("<soap:Body>" + "<" + request.getActionName() + " xmlns=\"" + request.getNameSpace() + "\">");
        if(params!=null) {
            requestData.append(params);
        }
        requestData.append("</"+request.getActionName()+"> </soap:Body> </soap:Envelope>");
        Log.e("SOAP PARAMS",requestData.toString()+"");
        return requestData.toString().trim().getBytes();
    }

    private String createSoapParameters(SOAPRequest request){
        if(request.getSoapParameters()!=null) {
            String parameString="";
            for (SOAPRequest.Parameter param : request.getSoapParameters()) {
                parameString+=   "<"+param.getName()+">"+param.getValue()+"</"+param.getName()+">\n";
            }
            return parameString;
        }
        return null;
    }

    private  String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        String result="";
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result=sb.toString();
            is.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sb=null;
        return result;
    }
}
