package com.belongi.citycenter.network;

import android.os.Environment;
import android.util.Log;


import com.belongi.citycenter.threading.BackgroundJob;
import com.belongi.citycenter.threading.BackgroundJobClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by yashesh on 6/7/2015.
 */
public class NetworkJob extends BackgroundJob {

    NetworkRequest request;

    public NetworkJob(BackgroundJobClient client, NetworkRequest request) {
        super(client);
        this.request=request;
    }

    @Override
    public void run() {
        HttpURLConnection httpConn=null;
        String urlString=request.getUrl();
        try {
            if(request.getType().equals(NetworkRequest.MethodType.GET)){
                String params=null;
                if((params=request.getParameterAsString())!=null) {
                    urlString += "?"+params;
                }

            }
            URL url=new URL(urlString);
            httpConn= (HttpURLConnection) url.openConnection();
            httpConn.setRequestMethod(request.getType().toString());
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setDoInput(true);

            if(request.getHeaders()!=null) {
                for (Object key : request.getHeaders().keySet().toArray()) {
                    httpConn.setRequestProperty(key.toString(), request.getHeaders().get(key));
                }
            }

            for(Object key : httpConn.getRequestProperties().keySet().toArray()){
                Log.e("PROP", key+" property");
            }
            Log.e("METHOD", httpConn.getRequestMethod() + " method");
            String params=null;
            switch(request.getType()){

                case GET :

                    break;
                case POST :
                    httpConn.setDoOutput(true);

                    if((params=request.getParameterAsString())!=null){

                        OutputStreamWriter outWriter=new OutputStreamWriter(httpConn.getOutputStream());
                        outWriter.write(params);
                        outWriter.close();
                    }
                    break;
            }

            httpConn.connect();
            int responseCode=  httpConn.getResponseCode();
            Log.e("CONNECTION", "STARTED "+responseCode);
            Map<String,List<String>> headers= httpConn.getHeaderFields();
            String resopnseString= readIS(httpConn.getInputStream(), 1024);
            NetworkResponse response=new NetworkResponse();
            response.setResponseCode(responseCode);
            response.setResponseHeaders(headers);
            response.setResponseString(resopnseString);
            notifyCompletion(request.getRequestCode(),response);
        } catch (MalformedURLException e) {
            notifyAbort(request.getRequestCode(),e);
            e.printStackTrace();
        } catch (IOException e) {
            notifyAbort(request.getRequestCode(),e);
            e.printStackTrace();
        }finally {
            if(httpConn!=null){
                httpConn.disconnect();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readIS(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        //GZIPInputStream gzis = new GZIPInputStream(stream);
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader in = new BufferedReader(reader);
        String readed=null;
        String result="";
        while ((readed = in.readLine()) != null) {
            result+=readed;
        }
        return result;
    }
}
