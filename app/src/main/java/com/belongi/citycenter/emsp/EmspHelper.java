package com.belongi.citycenter.emsp;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.july.emsp.EMSP;
import com.july.emsp.EMSPAppXOverlay;
import com.july.emsp.EMSPCustomSchemeListner;

import java.util.HashMap;

/**
 * Created by webwerks on 13/5/16.
 */
public class EmspHelper {

    /*public void showFullScreenAndOverlayAds(){
        try {
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", getActivity().getPackageName());

            if(!(this instanceof AbstractHomeFragment))
                EMSP.loadAppXFullPage(getActivity(), Constant.CISCO_MALL_TAG + "_home_fullpage");
            EMSPAppXOverlay mAppXOverlay = new EMSPAppXOverlay(getActivity(), Constant.CISCO_MALL_TAG + "_home_overlay", R.drawable.appx_overlay_closebutton, true, appaxParams);
            mAppXOverlay.setAnimationStyle(R.style.APPX_ANIMATION);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showFullScreenAndOverlayAdsLoc(){
        try {
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", getActivity().getPackageName());

            if(!(this instanceof AbstractHomeFragment))
                EMSP.loadAppXFullPage(getActivity(), Constant.CISCO_LOCATION_TAG+"_home_fullpage");

            EMSPAppXOverlay mAppXOverlay = new EMSPAppXOverlay(getActivity(), Constant.CISCO_LOCATION_TAG + "_home_overlay", R.drawable.appx_overlay_closebutton, true, appaxParams);
            mAppXOverlay.setAnimationStyle(R.style.APPX_ANIMATION);
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

   /* public static void showFullScreenAd(Context context,String screenName){
        try {
            Log.e("App","Full screen " +Constant.CISCO_MALL_TAG +"_"+ screenName+"_fullpage");
            EMSP.loadAppXFullPage(context, Constant.CISCO_MALL_TAG +"_"+ screenName+"_fullpage");
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    public static void showOverlayAd(Activity context, String screenName) {

        try {
            Log.e("App", "Overlay " + Constant.CISCO_MALL_TAG + "_" + screenName + "_overlay");
            HashMap<String, String> appaxParams = new HashMap<String, String>();
            appaxParams.put("packageName", context.getPackageName());
            EMSPAppXOverlay mAppXOverlay = new EMSPAppXOverlay(context, Constant.CISCO_MALL_TAG + "_" + screenName + "_overlay"
                    , R.drawable.appx_overlay_closebutton, true, appaxParams);
            mAppXOverlay.setAnimationStyle(R.style.APPX_ANIMATION);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void showLocBasedFullscreenAd(Context context,String screenName){
        if(!TextUtils.isEmpty(Constant.CISCO_LOCATION_TAG)) {
            try {
                Log.e("App", "Location Tag " + Constant.CISCO_LOCATION_TAG + "_" + screenName.toUpperCase() + "_FULLPAGE");
                EMSP.loadAppXFullPage(context, Constant.CISCO_LOCATION_TAG + "_" + screenName.toUpperCase() + "_FULLPAGE");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/

    public static void showLocBasedOverlayAd(Activity context, String screenName) {
        if (!TextUtils.isEmpty(Constant.CISCO_LOCATION_TAG)) {
            try {
                Log.e("App", "Location Tag " + Constant.CISCO_LOCATION_TAG + "_" + screenName.toUpperCase() + "_OVERLAYLOCATION");
                HashMap<String, String> appaxParams = new HashMap<String, String>();
                appaxParams.put("packageName", context.getPackageName());
                EMSPAppXOverlay mAppXOverlay = new EMSPAppXOverlay(context, Constant.CISCO_LOCATION_TAG + "_" + screenName.toUpperCase() + "_OVERLAYLOCATION"
                        , R.drawable.appx_overlay_closebutton, true, appaxParams);
                mAppXOverlay.setAnimationStyle(R.style.APPX_ANIMATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
