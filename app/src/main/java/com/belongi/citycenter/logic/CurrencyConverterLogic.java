package com.belongi.citycenter.logic;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.belongi.citycenter.Constant;
import com.belongi.citycenter.network.NetworkJob;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;
import java.util.Set;

/**
 * Created by webwerks on 10/4/15.
 */
public class CurrencyConverterLogic {

    public static void requestCurrencyRate(BackgroundJobClient client,String from,String to) {
        String query="select%20*%20from%20csv%20where%20url%3D%22http%3A%2F%2Ffinance.yahoo.com%2Fd%2Fquotes.csv%3Fe%3D.csv%26f%3Dc4l1%26s%3D"+from+""+to+"%3DX%22%3B";
        NetworkRequest request=new NetworkRequest.Builder(NetworkRequest.MethodType.GET,Constant.WebConstants.URL_YAHOO_API,Constant.WebConstants.RESCODE_CURRENCY_CONVERTER)
                .addParameter("q", query).setContentType(NetworkRequest.ContentType.FORM_ENCODED)
                .addParameter("format", "json").setParamEncodingNeeded(false).build();
        NetworkJob job=new NetworkJob(client,request);
        job.execute();

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static List<String> getAvailableCurrencies() {
        List<String> listCountryCurrency=new ArrayList<>();
        Set<Currency> currencySet = Currency.getAvailableCurrencies();
        //Map<String, String> currencies = new TreeMap<String, String>();

        for (Currency currency : currencySet) {
            try {

                if(!currency.getCurrencyCode().equalsIgnoreCase("ILS")){
                    listCountryCurrency.add(currency.getDisplayName()+
                            " ("+currency.getCurrencyCode()+")");
                }

            } catch (Exception e) {}
        }

        String[] currencyArr = new String[listCountryCurrency.size()];
        Arrays.sort(listCountryCurrency.toArray(currencyArr), new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });

        return listCountryCurrency;
    }

    public static Bundle getBundle(List<String> currency){
        Bundle data=new Bundle();
        for(int i=0;i<currency.size();i++){
            String country=currency.get(i);
            if(country.substring(country.indexOf(" (")+2,country.indexOf(")")).equals("USD")){
                data.putInt("From",i);
            }else if(country.substring(country.indexOf(" (")+2,country.indexOf(")")).equals("AED")){
                data.putInt("To",i);
            }
        }
        return data;
    }
}
