package com.belongi.citycenter.logic;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import a_vcard.android.syncml.pim.PropertyNode;
import a_vcard.android.syncml.pim.VDataBuilder;
import a_vcard.android.syncml.pim.VNode;
import a_vcard.android.syncml.pim.vcard.VCardException;
import a_vcard.android.syncml.pim.vcard.VCardParser;

/**
 * Created by webwerks on 16/6/15.
 */
public class QRLogic {

    public static void sendSms(Activity activity,String content){
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        String sendTo=content.substring(content.indexOf(":")+1,content.lastIndexOf(":"));
        sendIntent.setData(Uri.parse("smsto:" + sendTo));
        String msg=content.substring(content.lastIndexOf(":")+1,content.length());
        sendIntent.putExtra("sms_body",msg);
        activity.startActivity(sendIntent);
    }

    public static void sendEmail(Activity activity,String content){
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        String strTo=content.substring(nthOccurrence(content,':',1)+1,content.indexOf(";"));
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{strTo});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,content.substring(nthOccurrence(content,':',2)+1,
                nthOccurrence(content,';',1)));
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,content.substring(nthOccurrence(content,':',3)+1,
                content.lastIndexOf(";")-1));
        activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    public static void parseVcard(Activity activity,String content){
        VCardParser parser = new VCardParser();
        VDataBuilder builder = new VDataBuilder();
        boolean parsed = false;
        try {
            parsed = parser.parse(content, "UTF-8", builder);
        } catch (VCardException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(parsed){
            VNode contact=builder.vNodeList.get(0);
            ArrayList<PropertyNode> props = contact.propList;
            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            for (PropertyNode prop : props) {
                Log.e("prop", prop.propName + " : " + prop.propValue);
                switch (prop.propName){
                    case "EMAIL":
                        intent.putExtra(ContactsContract.Intents.Insert.EMAIL,prop.propValue);
                        break;

                    case "TEL":
                        intent.putExtra(ContactsContract.Intents.Insert.PHONE,prop.propValue);
                        break;

                    case "ADR":
                        intent.putExtra(ContactsContract.Intents.Insert.POSTAL,prop.propValue.replace(";"," "));
                        break;

                    case "N":
                        intent.putExtra(ContactsContract.Intents.Insert.NAME,prop.propValue);
                        break;

                    case "URL":
                        intent.putExtra(ContactsContract.Intents.Insert.DATA,prop.propValue);
                }
            }
            activity.startActivity(intent);
        }
    }

    public static int nthOccurrence(String str, char c, int n) {
        int pos = str.indexOf(c, 0);
        while (n-- > 0 && pos != -1)
            pos = str.indexOf(c, pos+1);
        return pos;
    }
}
