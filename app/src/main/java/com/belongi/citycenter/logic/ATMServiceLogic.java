package com.belongi.citycenter.logic;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.belongi.citycenter.data.entities.ATMService;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.BackgroundJobManager;
import com.belongi.citycenter.global.ContentDownloader;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.fragment.ServiceFragment;

/**
 * Created by webwerks on 29/6/15.
 */
public class ATMServiceLogic {

    public static final int REQ_GET_ATMS=45;

    public static void getAtmService(final BackgroundJobClient client) {

        Log.e("AtmService",EntityUtils.getAtmListCount()+"");
        if(EntityUtils.getAtmListCount() <= 0){
            SOAPRequest resquestAtms=new SOAPRequest();
            resquestAtms.setBaseUrl(WebApi.BASE_URL);
            resquestAtms.setActionName("GetATMLocations");
            resquestAtms.setNameSpace(WebApi.NAMESPACE);
            resquestAtms.setRequestCode(REQ_GET_ATMS);
            resquestAtms.setContentType(NetworkRequest.ContentType.TEXT_XML);
            resquestAtms.setType(NetworkRequest.MethodType.POST);
            resquestAtms.addSoapParameter(new SOAPRequest.Parameter("SiteID", App.get().getMallId(), SOAPRequest.Parameter.DataType.INT));
            //new SOAPNetworkJob(resquestAtms, client).execute();
            BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(resquestAtms, new BackgroundJobClient() {
                @Override
                public void onBackgroundJobComplete(int requestCode, Object result) {

                    ATMService[] atmList= (ATMService[]) ContentDownloader.getObjectFromJson(result.toString(), ATMService[].class);
                    Log.e("AtmService",atmList.length+"");
                    if(atmList!=null){
                        ActiveAndroid.beginTransaction();
                        for(ATMService service:atmList){
                            if(service!=null){
                                service.save();
                                EntityUtils.getSearchObject(service).save();
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                    client.onBackgroundJobComplete(REQ_GET_ATMS, "");
                }

                @Override
                public void onBackgroundJobAbort(int requestCode, Object reason) {
                }

                @Override
                public void onBackgroundJobError(int requestCode, Object error) {
                }

                @Override
                public boolean needAsyncResponse() {
                    return false;
                }

                @Override
                public boolean needResponse() {
                    return true;
                }
            }));
        }else{
            client.onBackgroundJobComplete(REQ_GET_ATMS, "");
        }
    }


}
