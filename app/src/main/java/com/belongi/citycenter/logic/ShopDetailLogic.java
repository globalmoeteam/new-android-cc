package com.belongi.citycenter.logic;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.android.utilities.DateTimeUtil;
import com.android.utilities.FileUtils;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.R;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.FeaturedProduct;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.ShopImage;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.ui.activity.WebContentPreviewActivity;
import com.belongi.citycenter.ui.fragment.categories.BaseCategoryListFragment;
import com.google.android.gms.analytics.HitBuilders;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 6/29/2015.
 */
public class ShopDetailLogic {
    private ShopDetailsCallbacks callbacks;
    private Model dataModel;
    private BaseCategoryListFragment.CategoryType type;

    public ShopDetailLogic(Serializable obj, ShopDetailsCallbacks callbacks) {
        type = getType(obj);
        this.callbacks = callbacks;

        extractDetailPage();
        extractImages();

        String categoryId = "";

        FavouriteItem item = new FavouriteItem();

        if (dataModel instanceof ShoppingShop) {
            //  id= ((ShoppingShop)dataModel).getIdentifier();
            categoryId = ((ShoppingShop) dataModel).getCategoryId();
            item.setType("shopping");
        } else if (dataModel instanceof DiningShop) {
            //  id=  ((DiningShop)dataModel).getIdentifier();
            categoryId = ((DiningShop) dataModel).getCategoryId();
            item.setType("dining");
        } else if (dataModel instanceof Hotel) {
            // id=((Hotel) dataModel).getIdentifier();
            categoryId = ((Hotel) dataModel).getIdentifier();
            item.setType("hotel");
        } else if (dataModel instanceof Offer) {
            categoryId = ((Offer) dataModel).getCategory_ID();
            item.setType("offer");
        } else if (dataModel instanceof Event) {
            categoryId = ((Event) dataModel).getIdentifier();
            item.setType("event");
        } else {
            categoryId = ((Entertainment) dataModel).getIdentifier();
            // id= ((Entertainment)dataModel).getIdentifier();
            item.setType("entertainment");
        }
        // item.setIdentifier(id);
        //  item.save();
        WebApi.updateUserHit(categoryId);


    }

    public void extractDetailPage() {

        LocalDataLoader localDataLoader = new LocalDataLoader(new LocalDataLoader.QueryClient() {
            @Override
            public Object executeQuery() {
                String id = "";

                if (dataModel instanceof ShoppingShop) {
                    id = ((ShoppingShop) dataModel).getIdentifier();
                } else if (dataModel instanceof DiningShop) {
                    id = ((DiningShop) dataModel).getIdentifier();
                } else if (dataModel instanceof Hotel) {
                    return null;
                } else if (dataModel instanceof Event) {
                    return null;
                } else if (dataModel instanceof Offer) {
                    return null;
                } else {
                    id = ((Entertainment) dataModel).getIdentifier();
                }

                /*Log.e("extractDetailPage",id + "");

                List<Offer> offerList=new Select().from(Offer.class).where("mallid=?",App.get().getMallId())
                        .where("forHome=?", 1).execute();
                Log.e("extractDetailPage",offerList.size()+"");
                for(Offer offer:offerList){
                    Log.e("extractDetailPage",offer.getStoreId() + " : " +offer.getTitle());
                }*/

                return new Select().from(Offer.class)
                        .where("Store_ID =?", new String[]{id})
                        .where("mallid=?", App.get().getMallId())
                        .where("forHome=?", 1).execute();
            }

            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    List<Offer> offers = (List<Offer>) result;

                    Log.e("OfferList", offers.size() + "");

                    if (offers.size() > 0) {
                        callbacks.setOfferList(offers);
                        if (dataModel instanceof ShoppingShop) {
                            List<FeaturedProduct> featuredProducts = new Select().from(FeaturedProduct.class).where("shopId = ?", new String[]{((ShoppingShop) dataModel).getIdentifier()}).execute();
                            if (featuredProducts != null && featuredProducts.size() > 0) {
                                callbacks.setFeaturedProducts(featuredProducts);
                            }
                        }
                    } else {
                        String description = "";
                        if (dataModel instanceof ShoppingShop) {
                            List<FeaturedProduct> featuredProducts = new Select().from(FeaturedProduct.class).where("shopId = ?", new String[]{((ShoppingShop) dataModel).getIdentifier()}).execute();
                            if (featuredProducts != null && featuredProducts.size() > 0) {
                                callbacks.setFeaturedProducts(featuredProducts);
                            } else {
                                description = ((ShoppingShop) dataModel).getDescription();
                            }
                        } else if (dataModel instanceof DiningShop) {
                            description = ((DiningShop) dataModel).getDescription();
                        } else {
                            description = ((Entertainment) dataModel).getDescription();
                        }

                        if (ValidationUtils.isNotNullOrBlank(description)) {
                            callbacks.setDescription(description);
                        }
                    }
                } else {
                    String description = "";
                    if (dataModel instanceof ShoppingShop) {
                        List<FeaturedProduct> featuredProducts = new Select().from(FeaturedProduct.class).where("shopId = ?", new String[]{((ShoppingShop) dataModel).getIdentifier()}).execute();//.where("shopId = ?",new String[]{((ShoppingShop)dataModel).getIdentifier()}).execute();
                        Log.e("FEATURED PROD", "" + featuredProducts.size());
                        if (featuredProducts != null && featuredProducts.size() > 0) {
                            callbacks.setFeaturedProducts(featuredProducts);
                        } else {
                            description = ((ShoppingShop) dataModel).getDescription();
                        }
                    } else if (dataModel instanceof DiningShop) {
                        description = ((DiningShop) dataModel).getDescription();
                    } else if (dataModel instanceof Hotel) {
                        description = ((Hotel) dataModel).getDescription();
                    } else if (dataModel instanceof Event) {
                        description = ((Event) dataModel).getEvent_Details();
                    } else if (dataModel instanceof Offer) {
                        description = ((Offer) dataModel).getDescription();
                    } else {
                        description = ((Entertainment) dataModel).getDescription();
                    }
                    Log.d("DESCRIPTION", description + " >>");
                    if (ValidationUtils.isNotNullOrBlank(description)) {
                        callbacks.setDescription(description);
                    }
                }
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {
            }

            @Override
            public boolean needAsyncResponse() {
                return true;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }, 0);
        localDataLoader.execute();
    }

    private void extractImages() {
        List<String> shopImages = new ArrayList<String>();

        if (dataModel instanceof ShoppingShop) {

            List<ShopImage> images = ((ShoppingShop) dataModel).getShopImages();
            if (images != null && images.size() > 0) {
                Log.e("images size", images.size() + "");
                for (ShopImage image : images) {
                    if (image.getImage() != null) {
                        if (image.getImage().startsWith("http")) {
                            shopImages.add(image.getImage());
                        } else {
                            shopImages.add(WebApi.BASE_HOAST_URL + image.getImage());
                        }
                    }
                }
                shopImages.add(WebApi.BASE_HOAST_URL + ((ShoppingShop) dataModel).getImage());
            } else {
                shopImages.add(WebApi.BASE_HOAST_URL + ((ShoppingShop) dataModel).getImage());
            }
        } else if (dataModel instanceof DiningShop) {
            shopImages.add(WebApi.BASE_HOAST_URL + ((DiningShop) dataModel).getThumbnail());
        } else if (dataModel instanceof Entertainment) {
            shopImages.add(WebApi.BASE_HOAST_URL + ((Entertainment) dataModel).getThumbnail());
        } else if (dataModel instanceof Hotel) {
            shopImages.add(WebApi.BASE_HOAST_URL + ((Hotel) dataModel).getImage());
        } else if (dataModel instanceof Event) {
            shopImages.add(WebApi.BASE_HOAST_URL + ((Event) dataModel).getEvent_Image());
        } else if (dataModel instanceof Offer) {
            shopImages.add(WebApi.BASE_HOAST_URL + ((Offer) dataModel).getImage_URL());
        }
        callbacks.setImages(shopImages);
    }

    private BaseCategoryListFragment.CategoryType getType(Serializable obj) {

        dataModel = (Model) obj;
        if (dataModel instanceof ShoppingShop) {
            return BaseCategoryListFragment.CategoryType.SHOPPING;
        } else if (dataModel instanceof DiningShop) {
            return BaseCategoryListFragment.CategoryType.DINING;
        } else {
            return BaseCategoryListFragment.CategoryType.ENTERTAINMENT;
        }
    }

    public static interface ShopDetailsCallbacks {

        public void setImages(List<String> images);

        public void setDescription(String description);

        public void setOfferList(List<Offer> offers);

        public void setFeaturedProducts(List<FeaturedProduct> featuredProducts);

    }


    public void call() {
        String number = "";
        if (dataModel instanceof ShoppingShop) {
            sendEvent("Call", "Shopping");
            number = ((ShoppingShop) dataModel).getShopTelephone();
        } else if (dataModel instanceof DiningShop) {
            sendEvent("Call", "Dining");
            number = ((DiningShop) dataModel).getTelephone();
        } else if (dataModel instanceof Hotel) {
            number = ((Hotel) dataModel).getTelephone();
            sendEvent("Call", "Hotel");
        } else {
            sendEvent("Call", "Entertainment");
            number = ((Entertainment) dataModel).getEntertainment_Telephone();
        }
        if (ValidationUtils.isNotNullOrBlank(number)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            ((Activity) callbacks).startActivity(intent);
            ((Activity) callbacks).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        } else {
            Toast.makeText((Activity) callbacks, "Phone number is not available", Toast.LENGTH_LONG).show();
        }
    }


    public void favourite(boolean favourite) {
        String id = "";
        String categoryId = "";
        if (favourite) {
            FavouriteItem item = new FavouriteItem();

            if (dataModel instanceof ShoppingShop) {
                sendEvent("Favorite", "Shopping");
                id = ((ShoppingShop) dataModel).getIdentifier();
                categoryId = ((ShoppingShop) dataModel).getCategoryId();
                item.setLabel(((ShoppingShop) dataModel).getShopTitle());
                item.setType("shopping");
            } else if (dataModel instanceof DiningShop) {
                sendEvent("Favorite", "Dining");
                id = ((DiningShop) dataModel).getIdentifier();
                categoryId = ((DiningShop) dataModel).getCategoryId();
                item.setLabel(((DiningShop) dataModel).getTitle());
                item.setType("dining");
            } else if (dataModel instanceof Hotel) {
                sendEvent("Favorite", "Hotel");
                id = ((Hotel) dataModel).getIdentifier();
                categoryId = ((Hotel) dataModel).getIdentifier();
                item.setLabel(((Hotel) dataModel).getHotel_Name());
                item.setType("hotel");
            } else {
                sendEvent("Favorite", "Entertainment");
                categoryId = ((Entertainment) dataModel).getIdentifier();
                id = ((Entertainment) dataModel).getIdentifier();
                item.setLabel(((Entertainment) dataModel).getTitle());
                item.setType("entertainment");
            }
            item.setIdentifier(id);
            item.setMallId(App.get().getMallId());
            item.save();
            WebApi.updateUserHit(categoryId);
            Toast.makeText((Activity) callbacks, "Added to favorite", Toast.LENGTH_LONG).show();
        } else {
            if (dataModel instanceof ShoppingShop) {
                id = ((ShoppingShop) dataModel).getIdentifier();
            } else if (dataModel instanceof DiningShop) {
                id = ((DiningShop) dataModel).getIdentifier();
            } else if (dataModel instanceof Hotel) {
                id = ((Hotel) dataModel).getIdentifier();
            } else {
                id = ((Entertainment) dataModel).getIdentifier();
            }
            new Delete().from(FavouriteItem.class)
                    .where("identifier=?", new String[]{id})
                    .where("mallid=?", App.get().getMallId()).execute();
            Toast.makeText((Activity) callbacks, "Deleted from favorite", Toast.LENGTH_LONG).show();
        }
    }


    public void share(Activity activity, ImageView img) {
        String title = null;
        if (dataModel instanceof ShoppingShop) {
            sendEvent("Sharing", "Shopping");
            title = ((ShoppingShop) dataModel).getShopTitle();
        } else if (dataModel instanceof DiningShop) {
            sendEvent("Sharing", "Dining");
            title = ((DiningShop) dataModel).getTitle();
        } else if (dataModel instanceof Entertainment) {
            sendEvent("Sharing", "Entertainment");
            title = ((Entertainment) dataModel).getTitle();
        } else if (dataModel instanceof Hotel) {
            sendEvent("Sharing", "Hotel");
            title = ((Hotel) dataModel).getHotel_Name();
        } else if (dataModel instanceof Event) {
            sendEvent("Sharing", "Event");
            title = ((Event) dataModel).getTitle();

            if (((Event) dataModel).getEvent_Start_Date() != null && ((Event) dataModel).getEvent_End_Date() != null) {
                title += "\n" +
                        DateTimeUtil.dateFormat(((Event) dataModel).getEvent_Start_Date(), "dd  MMM yyyy", "dd/MM/yyyy") + " - " +
                        DateTimeUtil.dateFormat(((Event) dataModel).getEvent_End_Date(), "dd  MMM yyyy", "dd/MM/yyyy");
            }
        } else if (dataModel instanceof Offer) {
            sendEvent("Sharing", "Offer");
            title = ((Offer) dataModel).getTitle();
        }

        Log.e("Enter", "SHARE");

        Uri imagePath = FileUtils.getLocalBitmapUri(img);
        if (imagePath != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, title);
            shareIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
            shareIntent.setType("image/*");
            activity.startActivity(Intent.createChooser(shareIntent, "Send with"));

        } else {
            Toast.makeText(activity, "Sending fail", Toast.LENGTH_SHORT).show();
        }
    }


    public void locate() {

      /* String destId="";
        if(dataModel instanceof  ShoppingShop){
            destId=  ( (ShoppingShop) dataModel).getDestinationID();
        }else if(dataModel instanceof  DiningShop){
            destId=  ( (DiningShop) dataModel).getDestination_ID();
        }else{
            destId=  ( (Entertainment) dataModel).getDestination_ID();
        }
        Intent destinationIntent=new Intent((Activity)callbacks, FragmentHostActivity.class);
        destinationIntent.putExtra(Constant.FRAGMENT_TYPE,FragmentHostActivity.DESTINATION_FRAGMENT);
        destinationIntent.putExtra(Constant.DEST_ID,destId);
        ((Activity)callbacks).startActivity(destinationIntent);*/

        Intent destinationIntent = new Intent((Activity) callbacks, WebContentPreviewActivity.class);
        String title = null, url = null, descId = null;
        if (dataModel instanceof ShoppingShop) {
            title = "SHOPPING";
            sendEvent("Locate", "Shopping");
            descId = ((ShoppingShop) dataModel).getDestinationID();
        } else if (dataModel instanceof DiningShop) {
            title = "DINING";
            sendEvent("Locate", "Dining");
            descId = ((DiningShop) dataModel).getDestination_ID();
        } else if (dataModel instanceof Entertainment) {
            descId = ((Entertainment) dataModel).getDestination_ID();
            title = "ENTERTAINMENT";
            sendEvent("Locate", "Entertainment");
        } else if (dataModel instanceof Hotel) {
            // descId=dataModel.getDe
            title = "HOTELS";
            sendEvent("Locate", "Hotel");
        } else if (dataModel instanceof Offer) {
            title = "OFFERS";
            sendEvent("Locate", "Offer");
        }


        if (ValidationUtils.isNotNullOrBlank(descId)) {
            url = Constant.STORE_LOCATOR + "?destId=" + descId;
        } else {
            url = Constant.STORE_LOCATOR;
           // Toast.makeText(((Activity) callbacks), "Destination not available", Toast.LENGTH_SHORT).show();
        }

        Log.e("URL", url);
        destinationIntent.putExtra("title", "STORE LOCATOR");
        destinationIntent.putExtra("url", url);
        destinationIntent.putExtra("category", Constant.DONT_SEND);
        ((Activity) callbacks).startActivity(destinationIntent);
    }


    public void sendEvent(String eventName, String category) {
        // Build and send an Event.
        App.get().getTracker().send(new HitBuilders.EventBuilder()
                .setCategory(App.get().getSelectedMall().getMallCOde() + ":" + category)
                .setAction(eventName)
                .setLabel("Click")
                .build());
    }

}
