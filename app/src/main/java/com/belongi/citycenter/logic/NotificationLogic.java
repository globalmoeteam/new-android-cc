package com.belongi.citycenter.logic;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.utilities.Preferences;
import com.belongi.citycenter.Constant;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.model.Notification;
import com.google.gson.Gson;

/**
 * Created by webwerks on 14/6/15.
 */
public class NotificationLogic {

    public static void saveNotification(Notification notification,Context context) {
        /*Preferences.setObject(Constant.PREF_NAME,context,
                Constant.PREF_KEY_NOTIFICATION,notification);*/
        Gson gson=new Gson();
        String strNotificationSetting=gson.toJson(notification);
        App.get().getSelectedMall().setNotificationSetting(strNotificationSetting);
        App.get().getSelectedMall().save();
    }

    public static Notification getNotification(Context context) {
        Gson gson=new Gson();
        /*Notification notification = (Notification) Preferences.getObject(Constant.PREF_NAME, context,
                Constant.PREF_KEY_NOTIFICATION, Notification.class);*/
        String strNotificationSetting=App.get().getSelectedMall().getNotificationSetting();
        if(!TextUtils.isEmpty(strNotificationSetting)) {
            Log.e("NNOTIFICATIONLOGIC", strNotificationSetting);
            Notification notification = gson.fromJson(strNotificationSetting, Notification.class);
            return notification;
        }
        return null;
    }
}
