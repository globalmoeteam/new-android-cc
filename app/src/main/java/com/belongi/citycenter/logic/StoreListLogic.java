package com.belongi.citycenter.logic;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.belongi.citycenter.data.entities.Category;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.FavouriteItem;
import com.belongi.citycenter.data.entities.ShopCategory;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.utils.LocalDataLoader;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.ui.fragment.categories.StoreListingFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 7/4/16.
 */
public class StoreListLogic {

    private static StoreListCallback mStoreListCallback;

    public static void getStoreList(final StoreListCallback storeListCallback,final StoreListingFragment.CategoryType type){
        mStoreListCallback=storeListCallback;
        new LocalDataLoader(new LocalDataLoader.QueryClient() {
            @Override
            public Object executeQuery() {
                Log.e("Category selected",type + " ");

                switch (type){
                    case SHOPPING:
                        List<Object> shopList=new ArrayList<>();
                        List<FavouriteItem> shopFav= new  Select().from(FavouriteItem.class)
                                .where("type =?", new String[]{"shopping"})
                                .where("mallid=?", App.get().getMallId()).execute();
                        List<ShoppingShop> itemShop=new Select().from(ShoppingShop.class)
                                .where("mallid=?", App.get().getMallId()).execute();

                        for(ShoppingShop dining : itemShop){
                            for(FavouriteItem fav:shopFav){
                                if(dining.getIdentifier().equals(fav.getIdentifier())){
                                    dining.setFavourited(true);
                                    break;
                                }else{
                                    dining.setFavourited(false);
                                }
                            }
                        }

                        List<ShopCategory> shopCategories=new Select().from(ShopCategory.class)
                                .where("type =?", Category.SHOPPING)
                                .where("mallid=?", App.get().getMallId()).execute();

                        shopList.add(itemShop);

                        if(shopCategories!=null){
                            shopList.add(shopCategories);
                        }
                        Log.e("Shop list respo",shopList.size() + " ");
                        return shopList;

                        //return shops;

                    case DINING:

                        List<Object> dineList=new ArrayList<>();

                        List<FavouriteItem> dineFav = new Select().from(FavouriteItem.class)
                                .where("type =?", new String[]{"dining"})
                                .where("mallid=?", App.get().getMallId()).execute();
                        List<DiningShop> itemDine = new Select().from(DiningShop.class)
                                .where("mallid=?", App.get().getMallId()).execute();
                        for (DiningShop dining : itemDine) {
                            for (FavouriteItem fav : dineFav) {
                                if (dining.getIdentifier().equals(fav.getIdentifier())) {
                                    dining.setFavourited(true);
                                    break;
                                } else {
                                    dining.setFavourited(false);
                                }
                            }
                        }

                        List<ShopCategory> dineCategories=new Select().from(ShopCategory.class)
                                .where("type =?", Category.DINING)
                                .where("mallid=?", App.get().getMallId()).execute();

                        dineList.add(itemDine);
                        dineList.add(dineCategories);

                        Log.e("Dine list respo", dineList.size() + " ");
                        return dineList;

                    case ENTERTAINMENT:
                        List<Object> enterList=new ArrayList<>();

                        List<FavouriteItem> enterFavs = new Select().from(FavouriteItem.class)
                                .where("type =?", new String[]{"entertainment"})
                                .where("mallid=?", App.get().getMallId()).execute();
                        List<Entertainment> itemEntertainment = new Select().from(Entertainment.class)
                                .where("forHome=?", 1)
                                .where("mallid=?", App.get().getMallId()).execute();
                        Log.w("Entertainment list", itemEntertainment.size() + "entertainment size");

                        for (Entertainment dining : itemEntertainment) {
                            for (FavouriteItem fav : enterFavs) {
                                if (dining.getIdentifier().equals(fav.getIdentifier())) {
                                    dining.setFavourited(true);
                                    break;
                                } else {
                                    dining.setFavourited(false);
                                }
                            }
                        }

                        enterList.add(itemEntertainment);
                        return enterList;

                    case ALL:
                    default:
                        List<Object> allList=new ArrayList<>();
                        List<FavouriteItem> defaultFav= new  Select().from(FavouriteItem.class)/*.where("type =?",new String[]{"shopping"})*/.execute();
                        List<Model> allStores=new ArrayList<Model>();

                        List<Model> resultList=new ArrayList<Model>();

                        List<ShoppingShop> defaultShops=new Select().from(ShoppingShop.class)
                                .where("mallid=?", App.get().getMallId()).execute();
                        if(defaultShops!=null && defaultShops.size()>0)
                            allStores.addAll(defaultShops);
                        List<DiningShop> defaultDines = new Select().from(DiningShop.class)
                                .where("mallid=?", App.get().getMallId()).execute();
                        if(defaultDines!=null && defaultDines.size()>0)
                            allStores.addAll(defaultDines);
                        List<Entertainment> defaultEntertainments =  new Select().from(Entertainment.class)
                                .where("forHome=?", 1)
                                .where("mallid=?", App.get().getMallId()).execute();
                        if(defaultEntertainments!=null && defaultEntertainments.size()>0)
                            allStores.addAll(defaultEntertainments);

                      //resultList.addAll(allStores);

                        for(Model shop : allStores){
                            for(FavouriteItem fav:defaultFav){
                                if(shop instanceof ShoppingShop){
                                    if(((ShoppingShop) shop).getIdentifier().equals(fav.getIdentifier())){
                                        ((ShoppingShop) shop).setFavourited(true);
                                        break;
                                    }else{
                                        ((ShoppingShop) shop).setFavourited(false);
                                    }
                                }else if(shop instanceof DiningShop){
                                    if(((DiningShop) shop).getIdentifier().equals(fav.getIdentifier())){
                                        ((DiningShop) shop).setFavourited(true);
                                        break;
                                    }else {
                                        ((DiningShop) shop).setFavourited(false);
                                    }
                                }else if(shop instanceof Entertainment){
                                    if(((Entertainment) shop).getIdentifier().equals(fav.getIdentifier())){
                                        ((Entertainment) shop).setFavourited(true);
                                        break;
                                    }else{
                                        ((Entertainment) shop).setFavourited(false);
                                    }
                                }
                            }

                                boolean isContain = false;
                                if( resultList.size()>0){
                                    for (int i=0; i<resultList.size(); i++ ) {
                                        if (shop.toString().equals(resultList.get(i).toString())) {
                                            isContain = true;
                                            break;
                                        }
                                    }
                                    if(!isContain){
                                        resultList.add(shop);
                                    }
                                }else {
                                    resultList.add(shop);
                                }
                        }

                        Log.e("All list respo",allStores.size() + "::::" + resultList.size() + " ");

                        //allList.add(allStores);
                        allList.add(resultList);
                        return allList;
                }
            }

            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if(result!=null) {
                    storeListCallback.onStoreListReceived(result);
                }else{
                    storeListCallback.onEmptyStoreList();
                }
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {

            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return true;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        },0).execute();

    }


    public static void getShop(final StoreListCallback storeListCallback,final String shopName){
        mStoreListCallback=storeListCallback;
        new LocalDataLoader(new LocalDataLoader.QueryClient() {
            @Override
            public Object executeQuery() {

                List<Model> allStores=new ArrayList<Model>();
                List<ShoppingShop> defaultShops=new Select().from(ShoppingShop.class)
                        .where("mallid=?", App.get().getMallId()).execute();
                if(defaultShops!=null && defaultShops.size()>0){
                    for(ShoppingShop shop:defaultShops){
                        if(shop.getShopTitle().toLowerCase().trim().equalsIgnoreCase(shopName.trim())){
                            allStores.add(shop);
                            return allStores;
                        }
                    }
                }

                List<DiningShop> defaultDines = new Select().from(DiningShop.class)
                        .where("mallid=?", App.get().getMallId()).execute();
                if(defaultDines!=null && defaultDines.size()>0){
                    for(DiningShop shop:defaultDines){
                        if(shop.getTitle().toLowerCase().trim().equalsIgnoreCase(shopName.trim())){
                            allStores.add(shop);
                            return allStores;
                        }
                    }
                }

                List<Entertainment> defaultEntertainments = new Select().from(Entertainment.class)
                        .where("mallid=?", App.get().getMallId()).execute();
                if(defaultEntertainments!=null && defaultEntertainments.size()>0){
                    for(Entertainment shop:defaultEntertainments){
                        if(shop.getTitle().toLowerCase().trim().equalsIgnoreCase(shopName.trim())){
                            allStores.add(shop);
                            return allStores;
                        }
                    }
                }

                return allStores;
            }

            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if(result!=null){
                    storeListCallback.onStoreListReceived(result);
                }else{
                    storeListCallback.onEmptyStoreList();
                }
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {

            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return true;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        },200).execute();
    }

    public interface StoreListCallback {

        public void onStoreListReceived(Object storeList);

        public void onEmptyStoreList();

    }
}
