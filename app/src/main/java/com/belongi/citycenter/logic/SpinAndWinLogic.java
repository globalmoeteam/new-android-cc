package com.belongi.citycenter.logic;

import android.util.Log;

import com.belongi.citycenter.data.entities.DrawDatesAndPastWinner;
import com.belongi.citycenter.data.entities.Registration;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Created by webwerks on 18/6/15.
 */
public class SpinAndWinLogic {

    public static final int REQ_GET_COMPAIGNS = 30;
    public static final int REQ_USER_LOGIN = 31;
    public static final int REQ_USER_REGISTRATION = 32;
    public static final int REQ_FORGOT_PASSWORD = 33;
    public static final int REQ_RESET_PASSWORD = 34;
    public static final int REQ_DRAW_DATES = 35;
    public static final int REQ_PAST_WINNERS = 36;
    public static final int REQ_GET_QUESTION = 37;
    public static final int REQ_REGISTER_ANSWER = 38;

    public static void getCampaigns(BackgroundJobClient client) {
        SOAPRequest campaignsRequest = new SOAPRequest();
        campaignsRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        campaignsRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        campaignsRequest.setType(NetworkRequest.MethodType.POST);
        campaignsRequest.setRequestCode(REQ_GET_COMPAIGNS);
        campaignsRequest.setActionName(WebApi.ACTION_GETCOMPAIGNS);
        campaignsRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        campaignsRequest.addSoapParameter(new SOAPRequest.Parameter("mallcode",App.get().getSelectedMall().getMallCOde(), SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(campaignsRequest, client).execute();
    }

    public static void userLogin(BackgroundJobClient client, String email, String pwd) {
        SOAPRequest loginRequest = new SOAPRequest();
        loginRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        loginRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        loginRequest.setType(NetworkRequest.MethodType.POST);
        loginRequest.setRequestCode(REQ_USER_LOGIN);
        loginRequest.setActionName(WebApi.ACTION_USER_LOGIN);
        loginRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        loginRequest.addSoapParameter(new SOAPRequest.Parameter("Email", email, SOAPRequest.Parameter.DataType.STRING));
        loginRequest.addSoapParameter(new SOAPRequest.Parameter("Password", pwd, SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(loginRequest, client).execute();
    }

    public static void userRegistration(BackgroundJobClient client, Registration model) {
        SOAPRequest registrationRequest = new SOAPRequest();
        registrationRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        registrationRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        registrationRequest.setType(NetworkRequest.MethodType.POST);
        registrationRequest.setRequestCode(REQ_USER_REGISTRATION);
        registrationRequest.setActionName(WebApi.ACTION_USER_REGISTRATION);
        registrationRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("title", model.getTitle(), SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("name", model.getName(), SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("Email", model.getEmail(), SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("Mobile", model.getMobile(), SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("Country_of_Residence", model.getCountryRes(), SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("Password", model.getPassword(), SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("OptionId", "1", SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("emirates_passportId", "123456789", SOAPRequest.Parameter.DataType.STRING));
        registrationRequest.addSoapParameter(new SOAPRequest.Parameter("nationality", model.getNationality(), SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(registrationRequest, client).execute();
    }

    public static void forgotPassword(BackgroundJobClient client, String email) {
        SOAPRequest forgotPasswordRequest = new SOAPRequest();
        forgotPasswordRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        forgotPasswordRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        forgotPasswordRequest.setType(NetworkRequest.MethodType.POST);
        forgotPasswordRequest.setRequestCode(REQ_FORGOT_PASSWORD);
        forgotPasswordRequest.setActionName(WebApi.ACTION_FORGOT_PASSWORD);
        forgotPasswordRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        forgotPasswordRequest.addSoapParameter(new SOAPRequest.Parameter("Email", email, SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(forgotPasswordRequest, client).execute();
    }

    public static void resetPassword(BackgroundJobClient client, String email, String securityCode, String password) {
        SOAPRequest resetPasswordsRequest = new SOAPRequest();
        resetPasswordsRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        resetPasswordsRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        resetPasswordsRequest.setType(NetworkRequest.MethodType.POST);
        resetPasswordsRequest.setRequestCode(REQ_RESET_PASSWORD);
        resetPasswordsRequest.setActionName(WebApi.ACTION_RESET_PASSWORD);
        resetPasswordsRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        resetPasswordsRequest.addSoapParameter(new SOAPRequest.Parameter("Email", email, SOAPRequest.Parameter.DataType.STRING));
        resetPasswordsRequest.addSoapParameter(new SOAPRequest.Parameter("SecurityCode", securityCode, SOAPRequest.Parameter.DataType.STRING));
        resetPasswordsRequest.addSoapParameter(new SOAPRequest.Parameter("newpassword", password, SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(resetPasswordsRequest, client).execute();
    }

    public static void getDrawDates(BackgroundJobClient client, String campaignId) {
        SOAPRequest drawDatesRequest = new SOAPRequest();
        drawDatesRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        drawDatesRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        drawDatesRequest.setType(NetworkRequest.MethodType.POST);
        drawDatesRequest.setRequestCode(REQ_DRAW_DATES);
        drawDatesRequest.setActionName(WebApi.ACTION_DRAW_DATES);
        drawDatesRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        drawDatesRequest.addSoapParameter(new SOAPRequest.Parameter("campaignId", campaignId, SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(drawDatesRequest, client).execute();
    }

    public static void getPastWinners(BackgroundJobClient client, String campaignId) {
        SOAPRequest pastWinnersRequest = new SOAPRequest();
        pastWinnersRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        pastWinnersRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        pastWinnersRequest.setType(NetworkRequest.MethodType.POST);
        pastWinnersRequest.setRequestCode(REQ_PAST_WINNERS);
        pastWinnersRequest.setActionName(WebApi.ACTION_PAST_WINNERS);
        pastWinnersRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        pastWinnersRequest.addSoapParameter(new SOAPRequest.Parameter("campaignId", campaignId, SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(pastWinnersRequest, client).execute();
    }

    public static void getQuestionAnswer(BackgroundJobClient client, String contestId) {
        SOAPRequest questionAnswerRequest = new SOAPRequest();
        questionAnswerRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        questionAnswerRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        questionAnswerRequest.setType(NetworkRequest.MethodType.POST);
        questionAnswerRequest.setRequestCode(REQ_GET_QUESTION);
        questionAnswerRequest.setActionName(WebApi.ACTION_GET_QUESTION);
        questionAnswerRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        questionAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("campaignId", contestId, SOAPRequest.Parameter.DataType.STRING));
        questionAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("userId", App.get().getLoginResponse().getUserId(), SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(questionAnswerRequest, client).execute();
    }

    public static void registerUserRightAnswer(BackgroundJobClient client, String campaignId, String questionId, String answerId, String attemptId) {
        SOAPRequest registerUserRightAnswerRequest = new SOAPRequest();
        registerUserRightAnswerRequest.setBaseUrl(WebApi.BASE_URL_SPIN_WIN);
        registerUserRightAnswerRequest.setNameSpace(WebApi.NAMESPACE_SPIN_WIN);
        registerUserRightAnswerRequest.setType(NetworkRequest.MethodType.POST);
        registerUserRightAnswerRequest.setRequestCode(REQ_REGISTER_ANSWER);
        registerUserRightAnswerRequest.setActionName(WebApi.ACTION_REGISTER_USER_RIGHT_ANSWER);
        registerUserRightAnswerRequest.setContentType(NetworkRequest.ContentType.TEXT_XML);
        registerUserRightAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("userId", App.get().getLoginResponse().getUserId(), SOAPRequest.Parameter.DataType.STRING));
        registerUserRightAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("campaignId", campaignId, SOAPRequest.Parameter.DataType.STRING));
        registerUserRightAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("questionId", questionId, SOAPRequest.Parameter.DataType.STRING));
        registerUserRightAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("AnswerId", answerId, SOAPRequest.Parameter.DataType.STRING));
        registerUserRightAnswerRequest.addSoapParameter(new SOAPRequest.Parameter("AttemptId", attemptId, SOAPRequest.Parameter.DataType.STRING));
        new SOAPNetworkJob(registerUserRightAnswerRequest, client).execute();
    }


    public static List<DrawDatesAndPastWinner> getDrawDates(String result) {
        List<DrawDatesAndPastWinner> listRecords = new ArrayList<>();
        try {
            JSONObject resultObj = new JSONObject(new JSONTokener(result.toString()));
            JSONArray items = resultObj.getJSONArray("Data");
            Iterator<String> iterator = items.getJSONObject(0).keys();
            DrawDatesAndPastWinner dataObj = null;
            // DrawDatesAndPastWinner obj=new DrawDatesAndPastWinner();
            while (iterator.hasNext()) {
                String key = iterator.next();
                if (key.contains("drawDate")) {
                    dataObj = new DrawDatesAndPastWinner();
                    dataObj.setSequence(Integer.parseInt(key.substring(8, key.length())));
                    dataObj.setDate(items.getJSONObject(0).getString((key)));
                    ///// inner loop for getting description on current date.
                    Iterator<String> descIterator = items.getJSONObject(0).keys();
                    while (iterator.hasNext()) {
                        String descKey = descIterator.next();
                        if (descKey.contains("drawDesc" + dataObj.getSequence())) {
                            dataObj.setDesc(items.getJSONObject(0).getString((descKey)));
                            listRecords.add(dataObj);
                            break;
                        }
                    }
//////////////////////////////////////////////////////////
                }
            }
            Log.e("list count", listRecords.size() + "");

           Collections.sort(listRecords, new Comparator<DrawDatesAndPastWinner>() {
                @Override
                public int compare(DrawDatesAndPastWinner drawDatesAndPastWinner, DrawDatesAndPastWinner t1) {
                    return drawDatesAndPastWinner.getSequence() < t1.getSequence() ? -1 : 1;
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listRecords;
    }
}
