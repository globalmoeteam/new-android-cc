package com.belongi.citycenter.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.belongi.citycenter.data.entities.ATMService;
import com.belongi.citycenter.data.entities.Campaigns;
import com.belongi.citycenter.data.entities.Dining;
import com.belongi.citycenter.data.entities.DiningShop;
import com.belongi.citycenter.data.entities.Entertainment;
import com.belongi.citycenter.data.entities.Event;
import com.belongi.citycenter.data.entities.FeaturedProduct;
import com.belongi.citycenter.data.entities.Hotel;
import com.belongi.citycenter.data.entities.Offer;
import com.belongi.citycenter.data.entities.Search;
import com.belongi.citycenter.data.entities.Service;
import com.belongi.citycenter.data.entities.ShopCategory;
import com.belongi.citycenter.data.entities.ShopImage;
import com.belongi.citycenter.data.entities.ShoppingShop;
import com.belongi.citycenter.data.entities.Sopping;
import com.belongi.citycenter.data.entities.utils.EntityUtils;
import com.belongi.citycenter.data.entities.web.entities.DiningStoreResponse;
import com.belongi.citycenter.data.entities.web.entities.EntertainmentResponse;
import com.belongi.citycenter.data.entities.web.entities.ShoppingStoreResponse;
import com.belongi.citycenter.global.App;
import com.belongi.citycenter.global.BackgroundJobManager;
import com.belongi.citycenter.global.WebApi;
import com.belongi.citycenter.logic.SpinAndWinLogic;
import com.belongi.citycenter.network.NetworkRequest;
import com.belongi.citycenter.network.SOAPNetworkJob;
import com.belongi.citycenter.network.SOAPRequest;
import com.belongi.citycenter.threading.BackgroundJobClient;
import com.belongi.citycenter.ui.fragment.categories.BaseCategoryListFragment;

import static com.belongi.citycenter.global.ContentDownloader.getObjectFromJson;


/**
 * Created by webwerks on 18/4/17.
 */

public class DataLoadService extends IntentService {

    private static final String TAG=DataLoadService.class.getSimpleName();



    public DataLoadService() {
        super(TAG);
        Log.e(TAG,"Enter in DataLoadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG,"Enter in onHandleIntent");
        requestInitialContent();
    }

    public static  void  requestInitialContent(){
        final String siteId= App.get().getMallId();
        // check Spin
        SpinAndWinLogic.getCampaigns(new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if(result!=null) {
                    Campaigns compaigns= (Campaigns) getObjectFromJson(result.toString(), Campaigns.class);
                    if(compaigns.getListCompagins() != null && compaigns.getListCompagins().size()>0){
                        App.get().setHasCampaigns(true);
                    }else{
                        App.get().setHasCampaigns(false);
                    }
                }
            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {

            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return false;
            }
        });
        // Dining Categories
        SOAPRequest requestDineCategories = new SOAPRequest();
        requestDineCategories.setBaseUrl(WebApi.BASE_URL);
        requestDineCategories.setActionName("GetDiningCategories");
        requestDineCategories.setNameSpace(WebApi.NAMESPACE);
        requestDineCategories.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestDineCategories.setType(NetworkRequest.MethodType.POST);
        requestDineCategories.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));
        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestDineCategories, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    ShopCategory[] dineCategory= (ShopCategory[]) getObjectFromJson(result.toString(),ShopCategory[].class);
                    new Delete().from(ShopCategory.class).where("mallid=?",siteId)
                            .where("type=?", BaseCategoryListFragment.CategoryType.DINING).execute();
                    ActiveAndroid.beginTransaction();
                    for(ShopCategory cat:dineCategory){
                        cat.setMallId(App.get().getMallId());
                        cat.setType(BaseCategoryListFragment.CategoryType.DINING);
                        cat.save();
                    }
                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();
                }
                Log.e("Counter","GetDiningCategories"+App.get().getMallId());

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));


//////////////////////////////////////////////////////////////////////////////////////////////

        SOAPRequest requestShoppingCategories = new SOAPRequest();
        requestShoppingCategories.setBaseUrl(WebApi.BASE_URL);
        requestShoppingCategories.setActionName("GetShoppingCategories");
        requestShoppingCategories.setNameSpace(WebApi.NAMESPACE);
        requestShoppingCategories.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestShoppingCategories.setType(NetworkRequest.MethodType.POST);
        requestShoppingCategories.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestShoppingCategories, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    ShopCategory[] shopCategory= (ShopCategory[]) getObjectFromJson(result.toString(),ShopCategory[].class);
                    new Delete().from(ShopCategory.class).where("mallid=?",siteId)
                            .where("type=?",BaseCategoryListFragment.CategoryType.SHOPPING).execute();
                    ActiveAndroid.beginTransaction();
                    for(ShopCategory cat:shopCategory){
                        cat.setMallId(App.get().getMallId());
                        cat.setType(BaseCategoryListFragment.CategoryType.SHOPPING);
                        cat.save();
                    }
                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();
                }
                Log.e("Counter","GetShoppingCategories"+App.get().getMallId());

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        //get dininig list

        SOAPRequest requestDineItem = new SOAPRequest();
        requestDineItem.setBaseUrl(WebApi.BASE_URL);
        requestDineItem.setActionName("GetDiningListing");
        requestDineItem.setNameSpace(WebApi.NAMESPACE);
        requestDineItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestDineItem.setType(NetworkRequest.MethodType.POST);
        requestDineItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));
        requestDineItem.addSoapParameter(new SOAPRequest.Parameter("CatID", "0", SOAPRequest.Parameter.DataType.INT));
        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestDineItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("DINING.", result + "");
                if (result != null) {
                    DiningStoreResponse[] stores = (DiningStoreResponse[]) getObjectFromJson(result.toString(), DiningStoreResponse[].class);
                    if (stores != null) {
                        new Delete().from(DiningShop.class).where("mallid=?",siteId).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","DINING").execute();
                        ActiveAndroid.beginTransaction();
                        for (DiningStoreResponse resp : stores) {
                            if (resp != null) {
                                for (Dining dining : resp.getShopsList()) {
                                    dining.getShop().setCategoryId(resp.getID());
                                    dining.getShop().setCategoryName(resp.getCategoryName());
                                    dining.getShop().setMallId(App.get().getMallId());
                                    long id = dining.getShop().save();
                                    long searchId = EntityUtils.getSearchObject(dining.getShop()).save();
                                }
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                        EntityUtils.getSize();
                    }
                }
                // *1
                Log.e("Counter","GetDiningListing"+App.get().getMallId());

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        // get shopping list

        SOAPRequest requestShopItem = new SOAPRequest();
        requestShopItem.setBaseUrl(WebApi.BASE_URL);
        requestShopItem.setActionName("GetShoppingListing");
        requestShopItem.setNameSpace(WebApi.NAMESPACE);
        requestShopItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestShopItem.setType(NetworkRequest.MethodType.POST);
        requestShopItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        requestShopItem.addSoapParameter(new SOAPRequest.Parameter("CatID", "0", SOAPRequest.Parameter.DataType.INT));
        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestShopItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("SHOPPING.", result + "");
                if (result != null) {
                    ShoppingStoreResponse[] stores = (ShoppingStoreResponse[]) getObjectFromJson(result.toString(), ShoppingStoreResponse[].class);
                    if (stores != null) {
                        new Delete().from(ShoppingShop.class).where("mallid=?",App.get().getMallId()).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","SHOPPING").execute();

                        ActiveAndroid.beginTransaction();
                        for (ShoppingStoreResponse resp : stores) {
                            if (resp != null) {
                                for (Sopping shop : resp.getShopsList()) {

                                    new Delete().from(FeaturedProduct.class)
                                            .where("shopId=?",shop.getShop().getIdentifier()).execute();
                                    new Delete().from(ShopImage.class)
                                            .where("shopId=?",shop.getShop().getIdentifier()).execute();

                                    shop.getShop().setCategoryId(resp.getID());
                                    shop.getShop().setCategoryName(resp.getCategoryName());
                                    shop.getShop().setMallId(App.get().getMallId());
                                    if (shop.getShop().getImagesList() != null) {
                                        // new Delete().from(ShopImage.class).execute();
                                        Log.d("SHOP images size", shop.getShop().getImagesList().size() + "");
                                        for (ShopImage image : shop.getShop().getImagesList()) {
                                            if (image != null) {
                                                image.setShopId(shop.getShop().getIdentifier());
                                                long idImg = image.save();
                                                Log.e("SHOPPING IMG", idImg + "  ... " + image.getShopId() + " IMAGE");
                                            }
                                        }
                                    }

                                    if (shop.getShop().getFeatured_Products() != null) {
                                        Log.e("Featured Product",shop.getShop().getShopTitle()+ " : "+shop.getShop().getFeatured_Products().size()+"");
                                        for (FeaturedProduct product : shop.getShop().getFeatured_Products()) {

                                            if (product != null) {
                                                Log.e("Featured Product",product.getTitle());
                                                product.setShopId(shop.getShop().getIdentifier());
                                                product.save();
                                            }
                                        }
                                    }

                                    long id = shop.getShop().save();
                                    long searchId = EntityUtils.getSearchObject(shop.getShop()).save();
                                    // Log.e("SHOPPING",id+" SEARCH");
                                }
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                // *2
                Log.e("Counter","GetShoppingListing"+App.get().getMallId());

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        // get entertainment list

        SOAPRequest requestEntertainmentItem = new SOAPRequest();
        requestEntertainmentItem.setBaseUrl(WebApi.BASE_URL);
        requestEntertainmentItem.setActionName("GetEntertainmentListing");
        requestEntertainmentItem.setNameSpace(WebApi.NAMESPACE);
        requestEntertainmentItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestEntertainmentItem.setType(NetworkRequest.MethodType.POST);
        requestEntertainmentItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestEntertainmentItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("ENTER.", result + "");
                if (result != null) {
                    EntertainmentResponse[] stores = (EntertainmentResponse[]) getObjectFromJson(result.toString(), EntertainmentResponse[].class);
                    if (stores != null) {
                        new Delete().from(Entertainment.class).where("mallid=?", App.get().getMallId())
                                .where("forHome=?", 1).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","ENTERTAINMENT").execute();
                        ActiveAndroid.beginTransaction();
                        for (EntertainmentResponse resp : stores) {
                            if (resp != null) {
                                for (Entertainment shop : resp.getShops_list()) {
                                    shop.setCategoryId(resp.getID());
                                    shop.setCategoryName(resp.getCategoryName());
                                    shop.setMallId(App.get().getMallId());
                                    shop.setForHome(1);
                                    long id = shop.save();
                                    long searchId = EntityUtils.getSearchObject(shop).save();
                                    //Log.e("ENTER",id+" SEARCH");
                                }
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                // *3
                Log.e("Counter","GetEntertainmentListing"+App.get().getMallId());

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        // get events list

        SOAPRequest requestEventItem = new SOAPRequest();
        requestEventItem.setBaseUrl(WebApi.BASE_URL);
        requestEventItem.setActionName("GetEventsListing");
        requestEventItem.setNameSpace(WebApi.NAMESPACE);
        requestEventItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestEventItem.setType(NetworkRequest.MethodType.POST);
        requestEventItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestEventItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("ENTER EVENT.", result + "");
                if (result != null) {
                    Event[] stores = (Event[]) getObjectFromJson(result.toString(), Event[].class);
                    if (stores != null) {
                        new Delete().from(Event.class).where("mallid=?", App.get().getMallId())
                                .where("forHome=?", 1).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","EVENT").execute();
                        ActiveAndroid.beginTransaction();
                        for (Event event : stores) {
                            if (event != null) {
                                event.setMallId(App.get().getMallId());
                                event.setForHome(1);
                                long id = event.save();
                                long searchId = EntityUtils.getSearchObject(event).save();
                                //Log.e("EVENT",id+" SEARCH " +searchId);
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                // *4
                Log.e("Counter","GetEventsListing"+App.get().getMallId());

            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        // get hotels list

        SOAPRequest requestHotelItem = new SOAPRequest();
        requestHotelItem.setBaseUrl(WebApi.BASE_URL);
        requestHotelItem.setActionName("GetHotelsListing");
        requestHotelItem.setNameSpace(WebApi.NAMESPACE);
        requestHotelItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestHotelItem.setType(NetworkRequest.MethodType.POST);
        requestHotelItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestHotelItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("HOTEL.", result + "");
                if (result != null) {
                    Hotel[] stores = (Hotel[]) getObjectFromJson(result.toString(), Hotel[].class);
                    if (stores != null) {
                        new Delete().from(Hotel.class).where("mallid=?", App.get().getMallId()).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","HOTEL").execute();
                        ActiveAndroid.beginTransaction();
                        for (Hotel hotel : stores) {
                            if (hotel != null) {
                                hotel.setMallId(App.get().getMallId());
                                long id = hotel.save();
                                long searchId = EntityUtils.getSearchObject(hotel).save();
                                //Log.e("HOTEL",id+" SEARCH " +searchId);
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                // *5
                Log.e("Counter","GetHotelsListing"+App.get().getMallId());


            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        // get offers list

        SOAPRequest requestOfferItem = new SOAPRequest();
        requestOfferItem.setBaseUrl(WebApi.BASE_URL);
        requestOfferItem.setActionName("GetOffersListing");
        requestOfferItem.setNameSpace(WebApi.NAMESPACE);
        requestOfferItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestOfferItem.setType(NetworkRequest.MethodType.POST);
        requestOfferItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestOfferItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("offerResponse", result + "");
                if (result != null) {
                    Offer[] stores = (Offer[]) getObjectFromJson(result.toString(), Offer[].class);
                    if (stores != null) {
                        new Delete().from(Offer.class).where("mallid=?",App.get().getMallId())
                                .where("forHome=?",1).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","OFFER").execute();
                        ActiveAndroid.beginTransaction();
                        for (Offer offer : stores) {
                            if (offer != null) {
                                offer.setMallId(App.get().getMallId());
                                offer.setForHome(1);
                                long id = offer.save();
                                long searchId = EntityUtils.getSearchObject(offer).save();
                                Log.e("offerResponse",id+" SEARCH " +searchId);
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                // *6
                Log.e("Counter","GetOffersListing"+App.get().getMallId());


            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        // get guest services list

        SOAPRequest requestServiceItem = new SOAPRequest();
        requestServiceItem.setBaseUrl(WebApi.BASE_URL);
        requestServiceItem.setActionName("GetGuestServicesPages");
        requestServiceItem.setNameSpace(WebApi.NAMESPACE);
        requestServiceItem.setRequestCode(WebApi.REQ_GET_SERVICE);
        requestServiceItem.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestServiceItem.setType(NetworkRequest.MethodType.POST);
        requestServiceItem.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        BackgroundJobManager.getInstance().submitJob(new SOAPNetworkJob(requestServiceItem, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                Log.e("serviceResponse.", result + "");
                if (result != null) {
                    Service[] stores = (Service[]) getObjectFromJson(result.toString(), Service[].class);
                    if (stores != null) {
                        new Delete().from(Service.class).where("mallid=?",App.get().getMallId()).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","SERVICE").execute();
                        ActiveAndroid.beginTransaction();
                        for (Service service : stores) {
                            if (service != null) {
                                service.setMallId(App.get().getMallId());
                                long id = service.save();
                                long searchId = EntityUtils.getSearchObject(service).save();
                                Log.e("serviceResponse", id + " SEARCH " + searchId);
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                // *7
                Log.e("Counter","GetGuestServicesPages"+App.get().getMallId());


            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }));

        //// get Atms

        SOAPRequest requestATM = new SOAPRequest();
        requestATM.setBaseUrl(WebApi.BASE_URL);
        requestATM.setNameSpace(WebApi.NAMESPACE);
        requestATM.setType(NetworkRequest.MethodType.POST);
        requestATM.setActionName("GetATMLocations");
        requestATM.setContentType(NetworkRequest.ContentType.TEXT_XML);
        requestATM.addSoapParameter(new SOAPRequest.Parameter("SiteID", siteId, SOAPRequest.Parameter.DataType.INT));

        new SOAPNetworkJob(requestATM, new BackgroundJobClient() {
            @Override
            public void onBackgroundJobComplete(int requestCode, Object result) {
                if (result != null) {
                    ATMService[] atmList = (ATMService[]) getObjectFromJson(result.toString(), ATMService[].class);
//                    Log.e("AtmService", atmList.length + "");
                    if (atmList != null) {
                        new Delete().from(ATMService.class).where("mallid=?",App.get().getMallId()).execute();
                        new Delete().from(Search.class).where("mallid=?",App.get().getMallId()).where("category=?","ATM").execute();
                        ActiveAndroid.beginTransaction();
                        for (ATMService service : atmList) {
                            if (service != null) {
                                service.setMallId(App.get().getMallId());
                                service.save();
                                EntityUtils.getSearchObject(service).save();
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();
                    }
                }
                Log.e("Counter","GetATMLocations"+App.get().getMallId());


            }

            @Override
            public void onBackgroundJobAbort(int requestCode, Object reason) {
            }

            @Override
            public void onBackgroundJobError(int requestCode, Object error) {

            }

            @Override
            public boolean needAsyncResponse() {
                return false;
            }

            @Override
            public boolean needResponse() {
                return true;
            }
        }).execute();


    }
}
