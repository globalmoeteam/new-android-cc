/**
 * @author P Ravikant
 * Mumbai, India
 */
package com.belongi.citycenter.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.utilities.Validation;
import com.belongi.citycenter.BuildConfig;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;


public class GcmUtility {

	private static final String GCM_PREFERENCE	= "gcm_preference";
	private static final String GCM_REGISTER	= "gcm_register_id";
	private static final String GCM_PACKAGE		= "package_number";
	private static final String TAG_LOG			= "GcmUtility";
	private static final String KEY_DEVICE_ID	= "deviceUid";

	public static final int CODE_IOEXCEPTION	= -1;
	public static final int CODE_ERROR			= 0;
	public static final int CODE_SUCCESS			= 1;
	public static final int CODE_ALREADY_REGISTERED	= 2;

	private static String gcmSenderId;
	private static final String NULL			= "null";
	private static int gcmRegisterTry 			= 0;
	private static boolean isAlreadyRegistered 	= false;

	private static GcmListner listener 			= null;
	private static Handler handle 								= null;

	/**
	 * Returns the GCM Registration Id from {@link SharedPreferences}
	 * if {@link GoogleCloudMessaging} is registered
	 * else returns <code>null</code>
	 * @return {@link String}
	 * */
	private static String getGcmRegisterIdFromSharedPreference(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(GCM_PREFERENCE,Context.MODE_PRIVATE);
		return sharedPreferences.getString(GCM_REGISTER, null);
	}

	private static int getPackageFromSharedPreference(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(GCM_PREFERENCE,Context.MODE_PRIVATE);
		return sharedPreferences.getInt(GCM_PACKAGE, 0);
	}

	private static void getGcmRegisterId (Context context) {
		String registerId = getGcmRegisterIdFromSharedPreference(context);
        Log.e("getGcmRegisterId",getPackageFromSharedPreference(context) + " : " +  BuildConfig.VERSION_CODE);

		if(getPackageFromSharedPreference(context) != BuildConfig.VERSION_CODE)
			registerId = null;
		Log.e("", "GCMID:"+registerId+"");
		try {

			if(registerId == null) {
				GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
				registerId = gcm.register(gcmSenderId);
				Log.w("REG GCM", "GCMID:"+registerId+"");
				Editor editor = context.getSharedPreferences(GCM_PREFERENCE,Context.MODE_PRIVATE).edit();
				editor.putString(GCM_REGISTER, registerId);
				editor.putInt(GCM_PACKAGE,BuildConfig.VERSION_CODE);
				editor.commit();
			} else {
				//isAlreadyRegistered = false;
				//listener.onGcmRegistered(registerId, CODE_ALREADY_REGISTERED);
			}
			Message msg = new Message();
			Bundle bundle = new Bundle();
			bundle.putString(KEY_DEVICE_ID, registerId);
			msg.setData(bundle);
			handle.sendMessage(msg);

		} catch (IOException e) {
			registerId = NULL;
			e.printStackTrace();
		}
	}

	/**
	 * Retries 4 times to register Gcm if not registered.
	 *
	 * @param context - {@link Context}
	 * @param gcmSenderId - {@link String}
	 * */
	public static void getGcmKey(final Context context, final GcmListner listner, String gcmSenderId) {
		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)
				== ConnectionResult.SUCCESS) {
			GcmUtility.gcmSenderId = gcmSenderId;
			listener = listner;
			gcmRegisterTry ++;

			handle = new Handler() {

				@Override
				public void handleMessage(Message msg) {
					String deviceUid = msg.getData().getString(KEY_DEVICE_ID);
					if (deviceUid == null) {
						Log.v(TAG_LOG, "Try: " + gcmRegisterTry + "Unable to register Gcm");

						deviceUid = NULL;

						if (gcmRegisterTry <= 5) {
							Log.v(TAG_LOG, "Retrying to register Gcm: Count: " + gcmRegisterTry);
							getGcmKey(context, listner, GcmUtility.gcmSenderId);
							return;
						} else {
							listner.onGcmRegistered(deviceUid, CODE_ERROR);
						}
					} else if (deviceUid.equals(NULL)) {
						Log.v(TAG_LOG, "Try: " + gcmRegisterTry + "Unable to register Gcm. Exception - IOException");

						deviceUid = NULL;

						if (gcmRegisterTry <= 5) {
							Log.v(TAG_LOG, "Retrying to register Gcm: Count: " + gcmRegisterTry);
							getGcmKey(context, listner, GcmUtility.gcmSenderId);
							return;
						} else {
							listner.onGcmRegistered(deviceUid, CODE_IOEXCEPTION);
						}
					}
					Log.v(TAG_LOG, "Registration successfull");
					listner.onGcmRegistered(deviceUid, CODE_SUCCESS);
				}
			};

			Thread th = new Thread() {
				public void run() {
					getGcmRegisterId(context);

				}
			};
			th.start();

		} else {
			Validation.UI.showToast(context, "Google Play Services not available.");
		}
	}

	public interface GcmListner {
		public void onGcmRegistered(String gcmKey, int code);
	}
}

