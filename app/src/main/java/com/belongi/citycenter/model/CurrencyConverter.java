package com.belongi.citycenter.model;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 10/4/15.
 */
public class CurrencyConverter implements WebResponse<CurrencyConverter> {



    /*@SerializedName("to")
        private String to;

        @SerializedName("rate")
        private String rate;

        @SerializedName("from")
        private String from;

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }


        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }*/

    @Expose
    @SerializedName("query")
    private QueryResult queryRes;

    public QueryResult getQueryRes() {
        return queryRes;
    }

    public void setQueryRes(QueryResult queryRes) {
        this.queryRes = queryRes;
    }

    public class QueryResult{

        @Expose
       @SerializedName("count")
       private String count;

        @Expose
       @SerializedName("created")
       private String created;

        @Expose
       @SerializedName("lang")
       private String lang;

        @Expose
       @SerializedName("results")
       private Result results;

       public String getCount() {
           return count;
       }

       public void setCount(String count) {
           this.count = count;
       }

       public String getCreated() {
           return created;
       }

       public void setCreated(String created) {
           this.created = created;
       }

       public String getLang() {
           return lang;
       }

       public void setLang(String lang) {
           this.lang = lang;
       }

       public Result getResults() {
           return results;
       }

       public void setResults(Result results) {
           this.results = results;
       }
    }

    public class Result{

        @Expose
        @SerializedName("row")
        private Row row;

        public Row getRow() {
            return row;
        }

        public void setRow(Row row) {
            this.row = row;
        }
    }

    public class Row{

        @Expose
        @SerializedName("col0")
        private String countryCode;

        @Expose
        @SerializedName("col1")
        private String rate;

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }
    }


    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(CurrencyConverter another) {
        return 0;
    }
}
