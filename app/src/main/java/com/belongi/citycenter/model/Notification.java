package com.belongi.citycenter.model;

import com.google.gson.annotations.Expose;

/**
 * Created by webwerks on 14/6/15.
 */
public class Notification {

    @Expose
    private boolean isFashionLadies;

    @Expose
    private boolean isFashionMens;

    @Expose
    private boolean isDining;

    @Expose
    private boolean isOffers;

    @Expose
    private boolean isEvents;

    @Expose
    private boolean isWeekly1;

    @Expose
    private boolean isWeekly2;

    @Expose
    private String name;

    @Expose
    private String emailAddress;

    @Expose
    private String mobNumber;

    @Expose
    private int gender=-1;

    @Expose
    private String gcmId;

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    @Expose
    private boolean isJoined;


    public boolean isFashionLadies() {
        return isFashionLadies;
    }

    public void setIsFashionLadies(boolean isFashionLadies) {
        this.isFashionLadies = isFashionLadies;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isFashionMens() {
        return isFashionMens;
    }

    public void setIsFashionMens(boolean isFashionMens) {
        this.isFashionMens = isFashionMens;
    }

    public boolean isDining() {
        return isDining;
    }

    public void setIsDining(boolean isDining) {
        this.isDining = isDining;
    }

    public boolean isOffers() {
        return isOffers;
    }

    public void setIsOffers(boolean isOffers) {
        this.isOffers = isOffers;
    }

    public boolean isEvents() {
        return isEvents;
    }

    public void setIsEvents(boolean isEvents) {
        this.isEvents = isEvents;
    }

    public boolean isWeekly1() {
        return isWeekly1;
    }

    public void setIsWeekly1(boolean isWeekly1) {
        this.isWeekly1 = isWeekly1;
    }

    public boolean isWeekly2() {
        return isWeekly2;
    }

    public void setIsWeekly2(boolean isWeekly2) {
        this.isWeekly2 = isWeekly2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobNumber() {
        return mobNumber;
    }

    public void setMobNumber(String mobNumber) {
        this.mobNumber = mobNumber;
    }

    public boolean isJoined() {
        return isJoined;
    }

    public void setIsJoined(boolean isJoined) {
        this.isJoined = isJoined;
    }
}
