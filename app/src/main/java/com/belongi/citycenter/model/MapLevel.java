package com.belongi.citycenter.model;

/**
 * Created by webwerks on 26/12/16.
 */

public class MapLevel {

    public int level;
    public boolean selected;

    public MapLevel(int level, boolean selected){
        this.level=level;
        this.selected=selected;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
