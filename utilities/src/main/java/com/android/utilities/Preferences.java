package com.android.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * Created by webwerks on 14/6/15.
 */
public class Preferences {

    private static SharedPreferences init(String prefName,Context context) {

        SharedPreferences preferences = context.getSharedPreferences(prefName,
                Context.MODE_MULTI_PROCESS);

        return preferences;

    }

    public static void setBoolean(String prefName,Context context,String key,boolean value) {
        SharedPreferences preferences = init(prefName, context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }

    public static void setString(String prefName,Context context,String key,String value) {
        SharedPreferences preferences = init(prefName,context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean getBoolean(String prefName,Context context,String key,boolean defaultValue) {
        SharedPreferences preferences = init(prefName,context);
        return preferences.getBoolean(key,defaultValue);
    }

    public static String getString(String prefName,Context context,String key,String defaultValue) {
        SharedPreferences preferences = init(prefName,context);
        return preferences.getString(key, defaultValue);
    }

    public static void setObject(String prefName,Context context,String key,Object value) {

        String object = new Gson().toJson(value);
        setString(prefName,context,key,object);

    }

    public static Object getObject(String prefName,Context context,String key,Class<?> toClass) {
        String jsonObject = getString(prefName,context,key,null);
        Object obj = null;
        if(jsonObject != null) {
            obj = new Gson().fromJson(jsonObject,toClass);
        }
        return obj;
    }

}
