package com.android.utilities.soap;

import com.android.utilities.http.WebResponse;

/**
 * Created by webwerks on 10/3/15.
 */
public interface ResponseHandler<T extends WebResponse> {

    public T parseResponse(int reqCode,Object response);


}
