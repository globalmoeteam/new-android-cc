package com.android.utilities.soap;

import android.os.AsyncTask;
import android.util.Log;

import com.android.utilities.datareader.DataReader;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.Proxy;

/**
 * Created by Master Raj Gogri on 10/3/15.
 */
public class SoapRequest extends DataReader{
    /**
     * Instantiates a new _ data reader.
     *
     * @param client the client
     */
    private AsyncTask requestTask;
    private SoapSerializationEnvelope requestEnvelope;
    private String mRequestUrl,mActoionUrl;
    private int requestTimeout,requestCode;
    private ResponseHandler responseHandler;


    public ResponseHandler getResponseHandler() {
        return responseHandler;
    }

    public void setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
    }

    public SoapSerializationEnvelope getRequestObject() {
        return requestEnvelope;
    }


    public int getRequestTimeout() {
        return requestTimeout;
    }

    public void setRequestTimeout(int requestTimeout) {
        this.requestTimeout = requestTimeout;
    }

    public void setRequestObject(SoapSerializationEnvelope requestObject) {
        this.requestEnvelope = requestObject;
    }

    protected SoapRequest(DataClient client) {
        super(client);


    }


    public String getmRequestUrl() {
        return mRequestUrl;
    }

    public void setmRequestUrl(String mRequestUrl) {
        this.mRequestUrl = mRequestUrl;
    }


    public String getmActoionUrl() {
        return mActoionUrl;
    }

    public void setmActoionUrl(String mActoionUrl) {
        this.mActoionUrl = mActoionUrl;
    }

    /**
     * Read.
     *
     * @param readRquestCode
     */
    @Override
    public void read(int readRquestCode) {

      requestCode=  readRquestCode;


        requestTask=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {

                HttpTransportSE ht = getHttpTransportSE();
                try {
                    Log.e("SoapRequest", "URL: " + mRequestUrl+mActoionUrl);

                    ht.call(mActoionUrl, requestEnvelope);

                    Log.e("SoapRequest", "request: " + ht.requestDump);
                    Log.e("SoapRequest", "response: " + ht.responseDump);


                    return  requestEnvelope.getResponse();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if(o!=null){
                    if(o instanceof SoapFault){
                        notifyClientError(requestCode,o);
                    }else{
                        notifyClientData(requestCode, responseHandler.parseResponse(requestCode,o));
                    }
                }else{
                    notifyClientEmpty(requestCode);
                }



            }
        }.execute();
    }


    public void abort(){
        if(requestTask!=null && !requestTask.isCancelled()) {
            requestTask.cancel(true);
            notifyClientEmpty(requestCode);
            requestTask=null;
        }
    }


    private final HttpTransportSE getHttpTransportSE() {
        HttpTransportSE ht = new HttpTransportSE(mRequestUrl,requestTimeout==0?60000:requestTimeout);
        ht.debug = true;

      //  ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");
        return ht;
    }

    /**
     * Inits the.
     */
    @Override
    public void init() {

    }


    public static class Builder{

        SoapRequest request;

        public Builder(DataClient client,ResponseHandler handler){

            request=new SoapRequest(client);
            request.setResponseHandler(handler);
        }



        public Builder setBaseUrl(String baseUrl){

            request.setmRequestUrl(baseUrl);
            return  this;

        }

        public Builder setRequestEnvelope(SoapSerializationEnvelope envelope){
                request.setRequestObject(envelope);
            return this;
        }


        public  Builder setTimeOut(int timeOutMillies){
            request.setRequestTimeout(timeOutMillies);
            return this;
        }

        public Builder setActionName(String action){
            request.setmActoionUrl(action);
         return this;
        }

        public SoapRequest build(){

            return request;
        }

    }

    public static class EnvelopeBuilder{

        SoapSerializationEnvelope envelope;




        public EnvelopeBuilder(int soapVersion,SoapObject requestObject){

            envelope=new SoapSerializationEnvelope(soapVersion);
            envelope.setOutputSoapObject(requestObject);



        }

        public EnvelopeBuilder  setIsDotNet(boolean isDotNet){
            envelope.dotNet=isDotNet;



            return this;
        }

       public EnvelopeBuilder skipNullProperTies(boolean skip){

           envelope.skipNullProperties=skip;
           return this;
       }

        public EnvelopeBuilder setImplicitTypes(boolean implicit){

            envelope.implicitTypes=implicit;

            return  this;
        }
          // if true : will not set a parameter for this request
        public EnvelopeBuilder setIsBodyOutEmpty(boolean empty){
            envelope.setBodyOutEmpty(empty);
            return this;
        }


        public SoapSerializationEnvelope build(){
            return envelope;
        }

    }


}
