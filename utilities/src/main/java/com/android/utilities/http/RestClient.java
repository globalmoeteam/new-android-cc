package com.android.utilities.http;

import android.net.http.AndroidHttpClient;
import android.util.Log;

import com.android.utilities.IoUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

// TODO: Auto-generated Javadoc

/**
 * The Class RestClient.
 */
public class RestClient {

    // public static HttpClient mClient = null;

    /**
     * Gets the.
     *
     * @param url    the url
     * @param params the params
     * @throws ClientProtocolException the client protocol exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static String get(String url, HttpParams params)
            throws ClientProtocolException, IOException {
        //	Log.e("REST CLIENT ", "GET :\n"+url);
        HttpGet get = new HttpGet(url);
        if (params != null) {
            get.setParams(params);
        }
        return execute(get);
    }

    /**
     * Gets the.
     *
     * @param url    the url
     * @param params the params
     * @throws ClientProtocolException the client protocol exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static String get(String url, HttpParams params, Map<String, String> headers)
            throws ClientProtocolException, IOException {
        //	Log.e("REST CLIENT ", "GET :\n"+url);
        url = url + "?";
        if (params != null)
            for (NameValuePair pair : (List<NameValuePair>) params.getParameter("")) {
                url += pair.getName() + "=" + pair.getValue() + "&";
            }


        HttpGet get = new HttpGet(url.substring(0, url.length() - 1));

        if (headers != null) {
            for (Entry<String, String> item : headers.entrySet()) {
                get.addHeader(item.getKey(), item.getValue());
            }
        }

        if (params != null) {
            get.setParams(params);
        }
        return execute(get);
    }

    /**
     * Post.
     *
     * @param url             the url
     * @param params          the params
     * @param responseHandler the response handler
     * @throws IOException
     * @throws ClientProtocolException
     */
    public static String post(String url, HttpParams params,
                              AbstractHttpEntity body) throws ClientProtocolException,
            IOException {
        HttpPost post = new HttpPost(url);
        if (params != null) {
            post.setParams(params);
        }
        if (body != null) {
            post.setEntity(body);
        }

        return execute(post);

    }


    /**
     * Post.
     *
     * @param url             the url
     * @param params          the params
     * @param responseHandler the response handler
     * @throws IOException
     * @throws ClientProtocolException
     */
    public static String post(String url, HttpParams params,
                              AbstractHttpEntity body, Map<String, String> headers) throws ClientProtocolException,
            IOException {
        HttpPost post = new HttpPost(url);
        if (params != null) {
            post.setParams(params);

        }

        if (headers != null) {
            for (Entry<String, String> item : headers.entrySet()) {
                post.setHeader(item.getKey(), item.getValue());
                Log.e("REST client", item.getKey() + " : " + item.getValue());
            }
        }


        if (body != null) {
            post.setEntity(body);
        }

        return execute(post);

    }


    private static String execute(HttpUriRequest request)
            throws ClientProtocolException, IOException {
        AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        HttpParams params = client.getParams();
        // append the timeout values if specified
        HttpConnectionParams.setSoTimeout(params, 0);
        HttpConnectionParams.setConnectionTimeout(params, 0);

        HttpResponse res = client.execute(request);
        InputStream iStream = res.getEntity().getContent();
        if (iStream != null) {
            String str = IoUtils.getStringFromInputStream(iStream);
            iStream.close();
            client.close();
            //	Log.e("REST CLIENT", "EXECUTE RES:\n"+str);
            return str;
        }
//        client.close();
        return null;
    }
}
