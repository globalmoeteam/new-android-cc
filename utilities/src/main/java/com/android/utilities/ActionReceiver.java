package com.android.utilities;

public interface ActionReceiver {

    public void onAction(int actionCode, Object data);
}
